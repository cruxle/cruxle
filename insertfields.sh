# custom command insertfields code can be found under
# topo/miroutes/management/commands/insertfields.py

#docker-compose run web python3 manage.py insertfields

# django-dbbackup is so buggy... until they resolve it we just use a custom backup with rsync and djangos dumpdata/loaddata...

#docker-compose run web ./manage.py dbrestore --noinput
#docker-compose run web ./manage.py mediarestore --noinput

#scp -P 2200 mitopo_backup@localhost:backup.0/mitopo_db.json .

rsync --partial --progress --exclude "*/gitlab-backups/*" -a -v -e "ssh -p 2200" mitopo_backup@129.187.164.4:backup.0 /storage/sites/

#docker-compose run web ./manage.py loaddata backup.0/mitopo_db.json
#docker exec -i dev_postgresql_1 psql -U postgres < 20200421_mitopo_db.pgdump
docker-compose run web psql -h postgresql -U postgres < /storage/sites/backup.0/mitopo_db.pgdump

rsync -avh --delete /storage/sites/backup.0/www /storage/sites/mitopo.de/
