from django.forms import forms, CharField, EmailField, Textarea, TextInput
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Submit, Layout, Field, Fieldset,
    Button, HTML, Row, MultiField, Div)
from crispy_forms.bootstrap import (AppendedText, PrependedText, FormActions, InlineField)


class HoneypotContactForm(forms.Form):
    name = CharField(required=True, max_length=1024, widget=TextInput(attrs={'placeholder': _('Name')}))
    email = EmailField(required=True, max_length=1024, widget=TextInput(attrs={'placeholder': _('Email')}))
    reason = CharField(required=False, max_length=1024, label=_('Reason'))
    src = CharField(required=False, max_length=1024, label=_('Source'))
    body = CharField(max_length=4096, required=True, widget=Textarea(attrs={'placeholder': _('Message')}))
    phone = CharField(label="", required=False, max_length=50, widget=TextInput(attrs={'class':'tar'}))
    def clean(self):
        form_data = self.cleaned_data
        if form_data['phone'] != '':
            raise forms.ValidationError('Not a human')
        return form_data

    def __init__(self, *args, **kwargs):
        super(HoneypotContactForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-xs-12'
        self.helper.field_class = 'col-xs-12'
        self.helper.form_show_labels = False

        self.helper.form_id = 'contact-form'
        self.helper.form_method = 'post'

        send_btn = HTML(
            """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-envelope"></span> {% trans "Send" %}
            </button>
            """)
        cancel_btn = HTML(
            """{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-default">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>""")
        hide_phone = HTML("""<script>$(document).ready(function () {$("#div_id_phone").hide();})</script>""")

        self.helper.layout = Layout('name', 'email', 'body', 'reason', 'src', FormActions(send_btn, cancel_btn), 'phone', hide_phone)

class AuthenticatedUserContactForm(forms.Form):
    reason = CharField(required=False, max_length=1024, label=_('Reason'))
    src = CharField(required=False, max_length=1024, label=_('Source'))
    body = CharField(required=True, max_length=4096, widget=Textarea(attrs={'placeholder': _('Message')}))

    def __init__(self, *args, **kwargs):
        super(AuthenticatedUserContactForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        # self.helper.form_class = 'form-inline'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-xs-12'
        self.helper.field_class = 'col-xs-12'
        self.helper.form_show_labels = False

        self.helper.form_id = 'contact-form'
        self.helper.form_method = 'post'

        send_btn = HTML(
            """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-envelope"></span> {% trans "Send" %}
            </button>
            """)
        cancel_btn = HTML(
            """{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-default">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>""")

        self.helper.layout = Layout('body', 'reason', 'src', FormActions(send_btn, cancel_btn))
