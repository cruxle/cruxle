from django.conf.urls import *

from micontactform import views

app_name='micontactform'

urlpatterns = [
    url(r'contact', views.send_contact_form, name='contact_form'),
]
