from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.forms.widgets import HiddenInput
from django.shortcuts import render
# Create your views here.
from django.template.loader import render_to_string
from django.utils.translation import gettext
from mitopo.util import tuples_to_mail_string, tuple_to_mail_string

from micontactform.forms import HoneypotContactForm, AuthenticatedUserContactForm


def create_mail(contact_email, context, subject,
                mail_from = settings.DEFAULT_FROM_EMAIL):

    context["html"] = False
    text_content = render_to_string('micontactform/mail.html', context)

    context["html"] = True
    html_content = render_to_string('micontactform/mail.html', context)

    user_email = EmailMultiAlternatives(
        subject=settings.MAIL_SUBJECT_PREFIX + " - " + subject,
        body=text_content,
        to=contact_email,
        from_email=mail_from,
        headers={'Reply-To': mail_from}
    )
    user_email.attach_alternative(html_content, "text/html")
    return user_email


def send_contact_form(request):
    valid_form = False
    next_page = request.GET.get('next')

    initial_meta = {
        'src': request.GET.get('src', None),
        'reason': request.GET.get('reason', None),
    }

    if request.POST:
        if request.user.is_authenticated:
            form = AuthenticatedUserContactForm(request.POST)
        else:
            form = HoneypotContactForm(request.POST)

        if form.is_valid():
            valid_form = True

            if request.user.is_authenticated:
                contact_name = request.user.username
                contact_email = request.user.email
                contact_id = request.user.id
            else:
                contact_name = request.POST.get('name', '')
                contact_email = request.POST.get('email', '')
                contact_id = None

            # Email the profile with the
            # contact information
            context = {
                'user_id': contact_id,
                'user_name': contact_name,
                'user_email': contact_email,
                'message_body': request.POST.get('body', 'No Message Provided'),
                'src': request.POST.get('src', None),
                'reason': request.POST.get('reason', None),
                'base_url': request.build_absolute_uri(location="/")[:-1],
            }

            email = create_mail(tuples_to_mail_string(settings.MANAGERS), context, gettext("contact form submission"), mail_from=contact_email)
            email.send()

            # we only send authenticated user a copy of their message
            # otherwise someone could use our site to spam people
            context['mail_for_user'] = True
            if request.user.is_authenticated:
                user_mail_addr = tuple_to_mail_string((contact_name, contact_email))
                email = create_mail([user_mail_addr], context, gettext("Your contact form submission"))
                email.send()



    else:
        if request.user.is_authenticated:
            form = AuthenticatedUserContactForm(initial=initial_meta)
        else:
            form = HoneypotContactForm(initial=initial_meta)

    # hide reason and source fields in form if they weren't explicitly
    # sent with the post request
    for key in initial_meta.keys():
        if initial_meta[key] is None:
            form.fields[key].widget = HiddenInput()

    return render(request, 'micontactform/contact_form.html', {
        'form': form,
        'valid_form': valid_form,
        'src': request.GET.get('src', None),
        'reason': request.GET.get('reason', None),
        'next_page': next_page
    })
