from django import template
from django.utils.translation import gettext

from miroutes.management.commands.maintenance import MAINTENANCE_FILE

register = template.Library()

@register.simple_tag
def maintenance_message():
    try:
        with open(MAINTENANCE_FILE, 'r') as f:
            minutes = f.read()
            return gettext("Maintenance starts in %(minute)s minutes. We apologize for any inconvenience. We'll be back as soon as possible.")  % {'minute': minutes}

    except:
        return ""
