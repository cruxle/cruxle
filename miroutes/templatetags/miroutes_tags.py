"""miroutes template tags and filters."""
import logging
import markdown

from django import template
from django.conf import settings
from django.urls import reverse
from django.utils.safestring import mark_safe

logger = logging.getLogger(__name__)

register = template.Library()

@register.filter
def static_url(imgurl):
    """Remove the wrongly added path parts of thumbnails generated from static files."""
    return imgurl.replace(settings.MEDIA_ROOT, "")


@register.filter
def qrcode_svg(url):
    """ Create a qrcode for a given url and return the url to a svg """
    from miroutes.models import QRCode
    qr, created = QRCode.objects.get_or_create(url=url)

    try:
        return qr.svg.url
    except Exception as e:
        logger.error("Error during QR code filter qrcode_svg", e)
        return ""


@register.filter
def qrcode_img(url):
    """ Create a qrcode for a given url and return the url to a svg """
    from miroutes.models import QRCode
    try:
        qr = QRCode.objects.get(url=url)
    except QRCode.DoesNotExist:
        qr = QRCode(url=url)
        qr.save()

    try:
        return qr.img.url
    except Exception as e:
        logger.error("Error during QR code filter qrcode_img", e)
        return ""

@register.simple_tag(takes_context=True)
def absolute_url(context, rel_url):
    """ Return the absolute url of a given rel_url. I.e. add http(s)://<site, etc>/ """
    request = context['request']
    abs_url = request.build_absolute_uri(rel_url)  # This one would be more generic but has its limitations with the internal snapshotting because qr codes for example would point to https:nginx/... 
    # we rather use the custom uri from settings.py
    abs_url = '{}{}{}'.format(settings.PROTOCOL, settings.SITE_DOMAIN, rel_url)

    return abs_url

@register.simple_tag(takes_context=True)
def reverse_url_absolute(context, pattern_name, kwargs={}):
    url_path = reverse(pattern_name, kwargs=kwargs)
    return '{}{}{}'.format(settings.PROTOCOL, settings.SITE_DOMAIN, url_path)

@register.filter
def grade_system_display(grading_system):
    from miroutes.model_choices import GRADE_SYSTEMS_DICT
    return GRADE_SYSTEMS_DICT[grading_system]


@register.simple_tag()
def str_concat(s1, s2):
    """ Return the concatenation of 2 strings """
    return str(s1)+str(s2)

@register.filter
def markdownify(text):
    return markdown.markdown(text)

@register.filter
def tzify(text):
    return mark_safe("<span class='iso8601-timestamp'>{}</span>".format(text.isoformat()))
