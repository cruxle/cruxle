"""Utils for miroutes app."""
import re
from django.urls import reverse
from django.contrib.auth.models import User


def replace_objs(msg, html=False, absolute_urls=True):
    from stitcher.models import Pano
    from miroutes.models import Route, Wall
    from mitopo.utils import absolute_url
    """Find route, wall and user identifiers and replace by internal link."""
    def repl_route(match):
        route_id = match.group('route_id')
        try:
            route = Route.objects.get(pk=route_id)
            url = reverse('miroutes:route_detail', kwargs={'route_id': route.pk})
            if absolute_urls:
                url = absolute_url(url)
            if html:
                return """<a href="{}"><span class="mitopo-route"></span> {}</a>""".format(url, route.name)
            else:
                return """{}, {}""".format(route.name, url)
        except Route.DoesNotExist:
            print("Route {} not found.".format(route_id))
            return match.group()

    def repl_wall(match):
        wall_id = match.group('wall_id')
        try:
            wall = Wall.objects.get(pk=wall_id)
            url = reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})
            if absolute_urls:
                url = absolute_url(url)
            if html:
                return """<a href="{}"><span class="mitopo-wall"></span> {}</a>""".format(url, wall.name)
            else:
                return """{}, {}""".format(wall.name, url)
        except Wall.DoesNotExist:
            print("Wall {} not found.".format(wall_id))
            return match.group()

    def repl_user(match):
        user_id = match.group('user_id')
        try:
            user = User.objects.get(pk=user_id)
            url = reverse('users:user_detail', kwargs={'user_id': user.pk})
            if absolute_urls:
                url = absolute_url(url)

            if html:
                return """<a href="{}"><span class="glyphicon glyphicon-user"></span> {}</a>""".format(url, user.username)
            else:
                return """{}, {}""".format(user.username, url)
        except User.DoesNotExist:
            print("User {} not found.".format(user_id))
            return match.group()

    def repl_pano(match):
        pano_id = match.group('pano_id')
        try:
            pano = Pano.objects.get(pk=pano_id)
            url = reverse('stitcher:view_pano', kwargs={'pano_id': pano.pk})
            if absolute_urls:
                url = absolute_url(url)

            if html:
                return """<a href="{}"><span class="fa fa-camera"></span> Pano {}</a>""".format(url, pano.pk)
            else:
                return """Pano {}, {}""".format(pano.pk, url)
        except Pano.DoesNotExist:
            print("Pano {} not found.".format(pano_id))
            return match.group()

    route_regexp = re.compile(r"#Route(?P<route_id>\d+)", re.IGNORECASE)
    msg = re.sub(route_regexp, repl_route, msg)
    wall_regexp = re.compile(r"#Wall(?P<wall_id>\d+)", re.IGNORECASE)
    msg = re.sub(wall_regexp, repl_wall, msg)
    user_regexp = re.compile(r"#User(?P<user_id>\d+)", re.IGNORECASE)
    msg = re.sub(user_regexp, repl_user, msg)
    pano_regexp = re.compile(r"#Pano(?P<pano_id>\d+)", re.IGNORECASE)
    msg = re.sub(pano_regexp, repl_pano, msg)

    return msg


def slugify_me(obj):
    """Generate a unique obj ID. Used for loosely coupled models (e.g., edit tags and comments).
    """
    return "{}{}".format(obj.__class__.__name__.lower(), obj.pk)


# Reverses a viewname url and then removes the i18n
# language from the url
def reverse_no_i18n(viewname, *args, **kwargs):
    result = reverse(viewname, *args, **kwargs)
    m = re.match(r'(/[^/]*)(/.*$)', result)
    return m.groups()[1]
