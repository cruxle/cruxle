"""Views for *consumers* of wall/route information."""
import datetime
import json
import os

import numpy as np
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D
from django.core import serializers
from django.db.models import Count, Q
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _
from reversion.models import Version

from edit_spot.forms import RouteForm, WallForm
from micomments.models import Comment
from micomments.utils import generate_thread_id
from micomments.views import jsonifyComment
from miroutes.forms import AscentForm
from miroutes.forms import UserpicForm
from miroutes.model_choices import GRADE_CHOICES, equalize_grade, GRADE_SYSTEMS, \
    POSSIBLE_REDUC_GRADES_N_LABELS_BY_GRADE_SYSTEM
from miroutes.models import Route, RouteGeometry, Ascent, Userpic, UserView, License
from miroutes.models import Wall, WallView
from places.models import Place
from users.models import UserProfile
from miroutes.utils import replace_objs
from edit_spot.utils import save_pdf_material


def errors_to_json(errors):
    """
    Convert a Form error list to JSON::
    """
    return dict(
        (k, list(map(str, v)))
        for (k,v) in list(errors.items())
    )

@login_required
def save_mapview(request):
    """Set the mapview via ajax."""

    user = request.user
    mapview = request.POST.get('mapview')

    if mapview:
        user.userprofile.last_map_view = mapview
        user.save()
        payload = {
            'message': None,
            'data': None,
            'success': True
        }
    else:
        payload = {
            'message': None,
            'data': None,
            'success': False
        }

    return JsonResponse(json.dumps(payload), safe=False)

@login_required
def save_baselayer(request):
    """Set the mapview via ajax."""

    user = request.user
    baselayer = request.POST.get('baselayer')

    if baselayer:
        user.userprofile.baselayer = baselayer
        user.userprofile.save()
        payload = {
            'message': None,
            'data': None,
            'success': True
        }
    else:
        payload = {
            'message': None,
            'data': None,
            'success': False
        }

    return JsonResponse(json.dumps(payload), safe=False)

def index(request):
    """
    Main view for landing page
    """
    lat = request.GET.get('lat')
    lng = request.GET.get('lng')
    use_filter = request.GET.get('filter')
    context = {
        'grade_systems': GRADE_SYSTEMS,
        'filter': use_filter
    }
    if lat and lng:
        map_location = {
            "center": (lng, lat),
            "zoom": 18,
        }
        context["goto_map_location"] = json.dumps(map_location)

    return render(request, 'miroutes/index.html', context)

def check_wallview_updates(request, since):
    """Check if any new walls have been published since the timestamp."""
    import datetime
    date_since = datetime.datetime.utcfromtimestamp(float(since))
    changes = Version.objects.get_for_model(WallView).filter(revision__date_created__gte=date_since)
    if changes:
        payload = {'success': True,
                   'message': str(_("Wall updated! Click to reload!"))}
    else:
        payload = {'success': False}

    return JsonResponse(json.dumps(payload), safe=False)

def get_filter_labels(request, grade_system):
    """Get labels for the grade filter via ajax."""
    grade_labels = [ label for key, label, _ in POSSIBLE_REDUC_GRADES_N_LABELS_BY_GRADE_SYSTEM[int(grade_system)]]
    grade_positions = list(np.linspace(0, 100, 10))
    if len(grade_labels) != 10:
        payload = {
            'success': False,
            'message': str(_("Not a valid grade system."))
        }
    else:
        payload = {
            'success': True,
            'data': {
                'labels': grade_labels,
                'positions': grade_positions
            }
        }
    return JsonResponse(json.dumps(payload), safe=False)


def imprint(request):
    """
    Imprint with legal information. (Static page)
    """

    licenses = License.objects.all()

    return render(request, 'miroutes/imprint.html', {"licenses": licenses})

def privacy(request):
    """
    Imprint with legal information. (Static page)
    """
    next_page = request.GET.get('next')

    return render(request, 'miroutes/privacy.html', {'next_page': next_page})


def help(request):
    """
    Help page. (Static page)
    """

    return render(request, 'miroutes/help.html', {})

def news(request):
    """
    News -- wall set comes from REST call
    """
    return render(request, 'miroutes/news.html', {})

def about(request):
    """
    About mitopo.de
    """

    return render(request, 'miroutes/about.html', {})

def toggle_show_inactive(request):
    """
    Save a value called show_inactive in session dict
    which is used to blend in blend out inactive spots and walls
    i.e. controls if you are in manage mode or just a regular visitor

    Once we changed the value of show_inactive,
    just return to the url that was given with
    the request tag ''from''
    or if none was given, go back too root
    """
    request.session['show_inactive'] = not request.session.get('show_inactive', False)
    return redirect(request.GET.get('from', '/'))


def wall_on_map(request, wall_id, **kwargs):
    """
    Show the index leaflet map zooming in on a specific wall.
    """

    wall = get_object_or_404(Wall, pk=wall_id)
    lon, lat = wall.pnt
    map_location = {
        "center": (lon, lat),
        "zoom": 18,
    }
    context = {
        "goto_map_location": json.dumps(map_location),
        'grade_systems': GRADE_SYSTEMS
    }
    return render(request, 'miroutes/index.html', context)


def userpic_display(request, wall_id=None, route_id=None, **kwargs):
    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
    elif route_id:
        obj = get_object_or_404(Route, pk=route_id)
    else:
        return HttpResponse('userpic_display can not be called without wall_id or route_id: {} :: {}'.format(wall_id, route_id))

    contenttype = request.GET.get('content', 'personal')
    pics = obj.userpic_set.filter(content=contenttype)
    if wall_id:
        pics = pics | Userpic.objects.filter(on_route__in=obj.pub_view.route_set.all()).filter(content=contenttype)

    pic_ids = list(pics.values_list('id', flat=True))

    for pic in pics:
        pic.description = replace_objs(escape(pic.description), html=True)

    start_id = request.GET.get('start_id')
    context = {
        'pics' : pics,
        'pic_ids' : pic_ids,
        'start_id': start_id,
        'content': contenttype
    }

    style = request.GET.get('style')
    if style == "single":
        return render(request, 'miroutes/single_userpics.html', context)
    elif style == "list":
        return render(request, 'miroutes/userpic_list.html', context)
    else:
        return render(request, 'miroutes/userpic_tiles.html', context)

def userpic(request, userpic_id, **kwargs):
    pic = get_object_or_404(Userpic, pk=userpic_id)
    pic.description = replace_objs(escape(pic.description), html=True)
    context = {
            'pic' : pic,
            'thread_id': generate_thread_id(pic),
            }
    return render(request, 'miroutes/userpic.html', context)


def del_userpic(request, userpic_id, **kwargs):
    """
    Delete userpicture.
    """
    from edit_spot.views import go_to
    from django.utils.translation import ugettext

    pic = get_object_or_404(Userpic, pk=userpic_id)

    next_page = request.GET.get('next')
    deleted = False
    if pic.created_by == request.user:
        messages.add_message(request, messages.SUCCESS,
                             _('Successfully deleted the file.'))
        pic.delete()
        deleted=True
    else:
        messages.add_message(request, messages.ERROR,
                            _('Can not delete the file. Are you the initial creator?'))

    if request.is_ajax():
        if deleted:
            payload = {
                'message': ugettext("Successfully deleted Picture."),
                'success': True
            }
        else:
            payload = {
                'message': ugettext("Unable to delete Picture."),
                'success': False
            }
        return JsonResponse(json.dumps(payload), safe=False)

    return go_to(next_page)


@login_required
def add_userpic(request, wall_id=None, route_id=None):
    """Add additional material."""
    from edit_spot.views import go_to
    from miroutes.wall_img_functions import get_exif_location
    from django.contrib.gis.geos import Point

    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
    else:
        obj = get_object_or_404(Route, pk=route_id)
    next_page = request.GET.get('next', None)

    if request.POST:
        form = UserpicForm(request.POST, request.FILES)
        if form.is_valid():
            request.user.userprofile.change_puzzle_pieces('add_userpic')
            images = form.cleaned_data['img']
            if(images):
                description = form.cleaned_data.pop('description')
                for m_file in images:
                    name, ext = os.path.splitext(m_file.name)

                    if ext in [".pdf", ".PDF"]:
                        pic = Userpic(
                            pdf_file=m_file,
                            content='personal',
                            pnt=obj.pnt,
                            description=description,
                            created_by=request.user)
                        save_pdf_material(pic)
                    else:
                        pic = Userpic(
                            img=m_file,
                            pnt=obj.pnt,
                            description=description,
                            created_by=request.user,
                            content='personal')
                        pic.save()
                        pic.licenses.set(form.cleaned_data['licenses'])
                        exif_location = get_exif_location(pic.img)
                        if(exif_location):
                            pic.pnt = Point(*exif_location)
                        pic.save()
                    obj.userpic_set.add(pic)

            form.delete_temporary_files()
            messages.add_message(request, messages.SUCCESS,
                                 _('Saved Userpic!'))
            return go_to(next_page)
        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to save Material!'))
    else:
        form = UserpicForm()

    context = {
        'userpic_form': form,
        'next_page': next_page,
        'obj': obj
    }
    return render(request, 'miroutes/userpic_form.html', context)


@login_required
def userpic_edit(request, userpic_id, **kwargs):
    from edit_spot.views import go_to
    pic = get_object_or_404(Userpic, pk=userpic_id)

    # we use http referer here. Not super elegant but it works.
    next_page = request.META.get('HTTP_REFERER', "/")

    if request.user != pic.created_by:
        messages.add_message(request, messages.ERROR,
                            _('Can not edit the file. Are you the initial creator?'))
        return go_to(next_page)

    if request.POST:
        form = UserpicForm(request.POST, request.FILES, instance=pic)
        if form.is_valid():
            edit = form.save(commit=False)
            edit.save()
            form.save_m2m()
            form.delete_temporary_files()
            messages.add_message(request, messages.SUCCESS,
                                 _('Saved Userpic!'))
            return go_to(next_page)
        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to save Material!'))
    else:
        form = UserpicForm(instance=pic)

    context = {
        'pic' : pic,
        'userpic_form': form,
        'next_page': next_page,
        'editing': True
    }
    return render(request, 'miroutes/userpic_form.html', context)


@login_required
def link_userpics_to_wall(request, wall_id, **kwargs):
    """
    Return a template to link material to a wall or route.
    """
    wall = get_object_or_404(Wall, pk=wall_id)
    next_page = request.GET.get('next', None)
    # store next_page in sessions.
    # This way the redirect is persistent also when coming from add material.
    if(next_page):
        request.session['next_page'] = next_page
    else:
        next_page = request.session.get('next_page', None)


    pics = request.user.userpic_set.all()
    not_linked = list(pics.exclude(on_wall=wall))
    linked = list(pics.filter(on_wall=wall))
    for pic in linked:
        pic.onthiswall = True

    piclist = linked + not_linked
    for mat in piclist:
        mat.thread_id = generate_thread_id(mat)


    context = {
        'obj': wall,
        'piclist': piclist,
        'content': request.GET.get('content', 'personal'), # Default content is personal
        'next_page': next_page
    }

    return render(request, 'miroutes/link_userpics.html', context)

def show_userpics(request, wall_id=None, route_id=None, **kwargs):
    """Show userpics for a wall or route."""
    wall = None
    route = None
    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
        wall = obj
    else:
        obj = get_object_or_404(Route, pk=route_id)
        route = obj
    contenttype = request.GET.get('content', 'personal')

    context = {
        'wall' : wall,
        'route' : route,
        'content': contenttype
    }
    return render(request, 'miroutes/userpic_modal.html', context)


def wall_detail(request, wall_id, wallview=None, just_published=False, **kwargs):
    """
    Details of a wall and the public view on the wall.
    """
    from datetime import datetime
    wall = get_object_or_404(Wall, pk=wall_id)

    if wallview is None:
        wallview = wall.pub_view

    routegeomlist = wallview.routegeometry_set.all()
    # sort from left to right
    routegeomlist = list(routegeomlist)
    routegeomlist = sorted(routegeomlist, key=lambda x: x.anchorpoint[1])

    # we annotate with the index
    for num, geom in enumerate(routegeomlist):
        geom.label = num + 1 # humans count from 1
        geom.routethread = generate_thread_id(geom.route)

    # need to be logged in to show ascents
    user = request.user

    time_loaded = int((datetime.utcnow() - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds())

    # get nearby walls (no deprecated/unpublished)
    nearby_walls = Wall.active_objects.filter(deprecated=False).filter(
        pnt__distance_lte=(wall.pnt, D(m=20000))).exclude(pk=wall.pk)

    wall_form = WallForm(instance=wall, editable=False, filled_only=True)

    materials = wall.userpic_set.filter(content="info") | Userpic.objects.filter(
        on_route__in=wall.pub_view.route_set.all()).filter(content="info")
    userpics = wall.userpic_set.filter(content="personal") | Userpic.objects.filter(
        on_route__in=wall.pub_view.route_set.all()).filter(content="personal")

    show_materials = request.GET.get('show_materials')
    show_userpics = request.GET.get('show_userpics')

    fans = wall.userprofile_set.all()
    is_fan = False
    if not user.is_anonymous and fans.filter(user=request.user):
        is_fan = True

    nearby_places = Place.objects.filter(pnt__dwithin=(wall.pnt, 5e-2)) # use dwithin(faster than distance) with distance given in units of deg(1deg approx 100km)
    nearby_places = nearby_places.annotate(distance=Distance("pnt", wall.pnt)).order_by('distance')[:15]
    nearby_places = nearby_places.values_list('name', flat=True)
    nearby_places = ','.join(nearby_places)

    context = {
        'wall': wall,
        'comment_thread': generate_thread_id(wall),
        'wallview': wallview,
        'wall_routegeomlist': routegeomlist,
        'time_loaded': time_loaded,
        'nearby_walls': nearby_walls,
        'wall_form': wall_form,
        'materials': materials,
        'userpics': userpics,
        'show_materials': show_materials,
        'show_userpics': show_userpics,
        'fans': fans,
        'is_fan': is_fan,
        'nearby_places': nearby_places,
        'just_published': just_published
    }
    return render(request, 'miroutes/wall_detail.html', context)


def wall_route_histogram(request, wall_id, **kwargs):
    """
    Plot a histogram of route difficulties
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    context = {
        'wall': wall,
        'showlabel': request.GET.get('showlabel', True),
        }

    return render(request, 'miroutes/route_histogram.html', context)

def license(request, license_key, **kwargs):
    """
    Display a license.
    """

    template = "miroutes/licenses/{}.html".format(license_key)

    return render(request, template)


@login_required
def wall_toggle_favourite(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)
    addfav = True
    if request.user.userprofile.favourite_walls.filter(pk=wall.pk):
        addfav = False
        request.user.userprofile.favourite_walls.remove(wall)
    else:
        request.user.userprofile.favourite_walls.add(wall)
    payload = {
        'success': True,
        'data': {
            "is_added": addfav,
            "fans": wall.userprofile_set.all().count()
        }
    }
    return JsonResponse(json.dumps(payload), safe=False)


@login_required
def wall_detail_dev(request, wall_id, **kwargs):
    """
    Details of a wall and the public view on the wall.
    """

    wall = get_object_or_404(Wall, pk=wall_id)
    wallview = wall.dev_view

    return wall_detail(request, wall_id, wallview=wallview)




def route_detail(request, route_id, **kwargs):
    """
    Details of a route object.
    """
    route = get_object_or_404(Route, pk=route_id)
    route_geometries = route.routegeometry_set.filter(on_wallview__is_dev=False)

    # Order by the wall publish dates, the most recent ones first
    route_geometries = route_geometries.order_by('-on_wallview__wall__last_published_date')
    route_geometries_no_deprecated = route_geometries.filter(on_wallview__wall__deprecated=False)
    # if we have more than 1 non-deprecated walls in which they are listed, only show the non-deprecated ones
    if route_geometries_no_deprecated.count() > 1:
        route_geometries = route_geometries_no_deprecated
    else:
        # otherwise make sure that the non-deprecated ones are always first
        route_geometries = route_geometries.order_by('on_wallview__wall__deprecated')

    on_walls = [wallview.wall for wallview in route.walls.filter(is_dev=False)]

    geom_id = request.GET.get('geom_id')
    if geom_id:
        geom = get_object_or_404(RouteGeometry, pk=geom_id)
    else:
        geom = route.routegeometry_set.filter(on_wallview__is_dev=False).first()

    next_page = request.GET.get('next')

    route_form = RouteForm(instance=route, editable=False, filled_only=True)

    route_form.fields = [ f for f in route_form.fields if route_form[f].value() ]

    ascents = None if request.user.is_anonymous else route.ascent_set.filter(climber=request.user)

    context = {
        'route': route,
        'geom': geom,
        'comment_thread': generate_thread_id(route),
        'route_geometries': route_geometries,
        'on_walls': on_walls,
        'versions': get_versions(route),
        'route_form': route_form,
        'next_page': next_page,
        'ascents': ascents,
        'materials': route.userpic_set.filter(content="info"),
        'userpics': route.userpic_set.filter(content="personal")
    }
    return render(request, 'miroutes/route_detail.html', context)

def route_short_info(request, route_id, **kwargs):
    """
    Return a short version of route details for display in the routetable on wall_detail.
    """
    route = get_object_or_404(Route, pk=route_id)

    context = {
        'route': route,
    }
    return render(request, 'miroutes/route_info.html', context)


def route_qrcode(request, route_id):
    route = get_object_or_404(Route, pk=route_id)

    show_printer_friendly_button = request.GET.get('show_printer_friendly_button', False)

    context = {
        'route': route,
        'qrcode_type': 'svg',
        'show_printer_friendly_button': show_printer_friendly_button,
    }
    return render(request, 'miroutes/route_qrcode.html', context)


def wall_routelist(request, wall_id, **kwargs):
    """
    Return a short version of route details for display in the routetable on wall_detail.
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    routes = list(wall.pub_view.route_set.all())
    data = []
    # we serialize this by hand (seems more complicated with serializers...)
    for route in routes:
        grade_tendency = {}
        for choice in Ascent.TENDENCY_CHOICES:
            grade_tendency[choice[0]] = route.ascent_set.filter(tendency=choice[0]).count()
        ascent_count = 0 if request.user.is_anonymous else route.ascent_set.filter(climber=request.user).count()
        comment_count = Comment.objects.filter(threadId=generate_thread_id(route)).count()
        data.append({
            'pk': route.pk,
            'name': route.name,
            'rating': route.rating,
            'ascent_count': ascent_count,
            'comment_count': comment_count,
            'grade_tendency': grade_tendency
        })

    return JsonResponse(json.dumps(data), safe=False)

def wall_viewed(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)
    view = UserView(wall=wall)
    if not request.user.is_anonymous:
        view.user = request.user
    view.save()
    payload = {
        'success': True
    }
    return JsonResponse(json.dumps(payload), safe=False)


def route_timeline_chunk(request, route_id):
    """Get a chunk of route news items via JSON."""
    from itertools import chain
    from operator import attrgetter
    route = get_object_or_404(Route, pk=route_id)
    pagenum = int(request.GET.get('page_num', 1))
    ascents = list(Ascent.objects.filter(route=route).order_by('-datetime'))
    comments = list(Comment.objects.filter(threadId=route.thread_id).order_by('-created'))
    for num, comment in enumerate(comments):
        comment.datetime = comment.created
    result_list = sorted(list(chain(ascents, comments)), key=attrgetter('datetime'))[(pagenum - 1) * 10 : pagenum * 10]

    for num, result in enumerate(result_list):
        if isinstance(result, Comment):
            result_list[num] = {'comment': jsonifyComment(result)}
        elif isinstance(result, Ascent):
            result_list[num] = {'ascent': result.news_json}
    #import ipdb;ipdb.set_trace()
    return JsonResponse(result_list, safe=False)

def wall_popup(request, wall_id, **kwargs):
    """
    Popup for a wall object (to be ajaxed).
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    context = {'wall': wall}
    return render(request, 'miroutes/wall_popup.html', context)

def wall_selection(request):
    """Get a filtered selection of walls for index."""
    from django.db.models import Q
    # start with all routes and go to walls from there
    routes = Route.objects.all()
    #import ipdb;ipdb.set_trace()

    stylelist = request.GET.getlist('styles[]')
    stylelist = list(map(int, stylelist))
    if stylelist:
        for style in range(3):
            if style not in stylelist:
                routes = routes.exclude(climbingstyle=style)
    else:
        return JsonResponse(json.dumps([]), safe=False)


    grade_range = request.GET.getlist('grades[]')
    print(("Requesting walls with routes in the range {}.".format(grade_range)))

    if grade_range:
        gmin, gmax = [int(grade_range[0]), int(grade_range[1])]
        allowed_grades = [ g for g,label in GRADE_CHOICES if (equalize_grade(g) <= gmax) and (equalize_grade(g) >= gmin) ]
        routes = routes.filter(grade__in=allowed_grades)

    # show also trad routes?
    if int(request.GET.get('show_trad', 0)) == 0:
        routes = routes.filter(tradclimb=False)

    # Wallviews that are not active pub views?
    if int(request.GET.get('show_unpublished', 0)):
        wallviews = WallView.objects.filter(is_dev=True)
    else:
        wallviews = WallView.active_objects.all()

    # We now get the wallviews (including empty ones if required)
    if int(request.GET.get('show_empty', 0)):
        wallviews = wallviews.filter(Q(route__in=routes) | Q(route__isnull=True))
    else:
        wallviews = wallviews.filter(route__in=routes)


    # and the walls (with or without subwalls)
    if int(request.GET.get('show_sub', 0)):
        walls = Wall.objects.filter(wallview__in=wallviews)
    else:
        # first query for walls that are not subwalls
        walls_1 = Wall.objects.filter(subwall_of__isnull=True).filter(wallview__in=wallviews)
        # for walls that have subwalls that contain the wallviews
        walls_2 = Wall.objects.filter(wall__wallview__in=wallviews)
        # merge the two
        walls = (walls_1 | walls_2).distinct()

    if int(request.GET.get('show_deprecated', 0)) == 0:
        walls = walls.filter(deprecated=False)

    marker_list = [wall.marker_json for wall in walls]

    return JsonResponse(json.dumps(marker_list), safe=False)



@login_required
def add_ascent(request, route_id):

    route = get_object_or_404(Route, pk=route_id)
    user = request.user
    if request.method == 'POST':

        form = AscentForm(request.POST)

        if form.is_valid():
            ascent = form.save(commit=False)
            ascent.route = route
            ascent.climber = request.user
            ascent.save()
            form.save_m2m()
            payload = {
                'message': _('Ticked {}.').format(ascent.route.name),
                'success': True
            }
        else:
            payload = {
                'message': _('Could not create ascent.'),
                'data': errors_to_json(form.errors),
                'success': False
            }
        return JsonResponse(json.dumps(payload), safe=False)

    else:
        form = AscentForm(initial={
            'route': route,
            'climber': user,
            'date': datetime.date.today(),
            })


        context = {
            'route': route,
            'form': form
        }

        return render(request, 'miroutes/ascent_form.html', context)

def tick_route(request, route_id=None):
    """Provide a template with modal content to tick routes."""
    route = get_object_or_404(Route, pk=route_id)
    form = AscentForm(show_buttons=False, initial={
        'route': route,
        'climber': request.user,
        'date': datetime.date.today(),
        })
    return render(request, 'miroutes/tick_popup.html', {
        'route': route,
        'form': form
    })

@login_required
def del_ascent(request, ascent_id):
    ascent = get_object_or_404(Ascent, pk=ascent_id, climber=request.user)
    next_page = request.GET.get('next')
    ascent.delete()
    if request.is_ajax():
        payload = {
            'message': _('Unticked {}').format(ascent.route.name),
            'data': reverse('miroutes:add_ascent', kwargs={
                'route_id': ascent.route.id,
            }),
            'success': True
        }

        return JsonResponse(json.dumps(payload), safe=False)
    else:
        messages.add_message(
            request, messages.SUCCESS,
            _('Deleted Ascent.'))
        return redirect(next_page)


@login_required
def ascent_details(request, ascent_id):
    """
    Popup for a ascent object (to be ajaxed).
    """

    ascent = get_object_or_404(Ascent, pk=ascent_id, climber=request.user)

    next_page = request.GET.get('next')
    if request.POST:
        form = AscentForm(data=request.POST, instance=ascent)
        if request.is_ajax():
            if form.is_valid():
                ascent = form.save()
                payload = {
                    'message': str(_('Updated ticklist.')),
                    'data': reverse('miroutes:ascent_details', kwargs={'ascent_id': ascent.id}),
                    'success': True
                }
            else:
                payload = {
                    'message': str(_('Ticklist update failed.')),
                    'data': errors_to_json(form.errors),
                    'success': False
                }
            return JsonResponse(json.dumps(payload), safe=False)
        else:
            if form.is_valid():
                ascent = form.save()
                messages.add_message(
                    request, messages.SUCCESS,
                    _('Edited Ascent.'))
                return redirect(next_page)

    form = AscentForm(instance=ascent)

    context = {
        'form' : form,
        'ascent': ascent,
        'route': ascent.route,
        'next_page': next_page
    }

    return render(request, 'miroutes/ascent_form.html', context)

def search(request, **kwargs):
    """
    process search request
    """
    from users.serializers import UserProfileSerializer
    search_results = []

    query = request.GET.get('q', "")
    query_obj = request.GET.get('o', None)

    if query_obj == 'users':
        query_results = UserProfile.objects.filter(user__username__icontains=query).order_by('puzzle_pieces')[:40]
        search_results = UserProfileSerializer(query_results, many=True)
        return JsonResponse(search_results.data, safe=False)


    if query_obj == 'mitopo' or None:
        query_results = Wall.active_objects.filter(subwall_of=None).filter(name__icontains=query).order_by('name')[:10]

        favourites = []
        if not request.user.is_anonymous:
            favourites = request.user.userprofile.favourite_walls.filter(name__icontains=query).order_by('name')
            query_results = query_results.union(favourites)

        search_results += [
            {
                "model": "wall",
                "name": escape(wall.name),
                "id": wall.id,
                "Nroutes": wall.pub_view.route_set.count(),
                "pnt": wall.pnt.coords,
                "is_favourite": wall in favourites,
            }
        for wall in query_results]


        query_results = Route.objects.annotate(
            geom_count=Count('routegeometry')).filter(
                geom_count__gte=1, name__icontains=query, ).order_by('name')[:10]
        search_results += [
            {
                "model": "route",
                "name": escape(route.name),
                "id": route.id,
                "on_wall": escape(route.walls.first().wall.name) if route.walls.first() else '',
                "pnt": route.pnt.coords,
            }
                for route in query_results]

        return JsonResponse(search_results, safe=False)

    # Notice: we only handle fields.feature_class == T(mountains) and P(cities) .. we should filter that, it does not make sense to provide anything else in the output
    if query_obj == 'geonames' or None:
        query_results = Place.objects.filter(Q(feature_class="T")|Q(feature_class="P"))
        query_results = query_results.filter(Q(name__icontains=query)|Q(alt_names__icontains=query))
        query_results = query_results.order_by('name').order_by('-population').order_by('-elevation')[:40]

        search_results = serializers.serialize("json", query_results)
        return HttpResponse(search_results, content_type='application/json')

    if query_obj == 'geonames_fuzzy' or None:
        search_results = serializers.serialize("json", query_results)
        return HttpResponse(search_results, content_type='application/json')


def get_all_versions(obj, version_id=None):
    """Collect all obj versions"""
    from reversion.models import Version
    qs = Version.objects.get_for_object(obj).all()
    if version_id:
        qs.filter(id=version_id)
    return qs

def get_versions(obj, version_id=None):
    """Collect obj versions but ignore initial revisions."""
    from reversion.models import Version
    qs = Version.objects.get_for_object(obj).all()
    # ignore initial commit
    if qs.count() > 0:
        qs = qs[:qs.count()-1]
    if version_id:
        qs.filter(id=version_id)
    return qs

def get_revisions(obj):
    """Objekt revisions shortcut."""
    return [ver.revision for ver in get_versions(obj)]
