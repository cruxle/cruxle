from django.forms import Widget

from miroutes.models import License


class LicenseChooser(Widget):
    template_name = 'miroutes/widgets/licensechooser.html'
    allow_multiple_selected = True

    def __init__(self, licenses_callback=None):
        super().__init__()

        self.licenses_callback = licenses_callback

    def value_from_datadict(self, data, files, name):
        try:
            getter = data.getlist
        except AttributeError:
            getter = data.get
        return getter(name)

    def value_omitted_from_data(self, data, files, name):
        # An unselected <select multiple> doesn't appear in POST data, so it's
        # never known if the value is actually omitted.
        return False

    def get_context(self, name, value, attrs):

        if self.licenses_callback is None:
            self.licenses = []
        else:
            self.licenses = self.licenses_callback()

        context = super().get_context(name, value, attrs)

        for index, license in enumerate(self.licenses):
            if value is not None and (str(license.id) in value or license.id in value):
                self.licenses[index].checked = True
            else:
                self.licenses[index].checked = False

        context['licenses'] = self.licenses

        return context