for mat in Material.objects.all():
    userpic = Userpic(
	img=mat.img, 
	description=mat.description, 
	pdf_file=mat.pdf_file, 
	created_by=mat.created_by, 
	created_at=mat.created_at,
	content='info')
    userpic.save()
    for w in mat.linked_to.all():
        w.userpic_set.add(userpic)

