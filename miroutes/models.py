"""Central mitopo objects gathering around wall."""
import os
import json
import logging
from contextlib import closing
from datetime import timedelta, datetime

import reversion
from colorfield.fields import ColorField
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.postgres.fields import JSONField
from django.templatetags.static import static
from django.core import exceptions
from django.db.models import Avg, Case, When
from django.urls import reverse
from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from reversion.models import Version

from generic_markers.models import MapLocation
from generic_markers.models import RelationalMarker
from generic_markers.models import SimpleMarker, TextMarker, PitchMarker
from micomments.utils import generate_thread_id
from miexport.models import PhantomSnap
from miroutes.model_choices import *
from miroutes.tasks import (tile_image, create_thumb_image,
        update_Wall_after_publish, update_after_Route_save)
from miroutes.utils import slugify_me, reverse_no_i18n
from django.utils.text import slugify
from miroutes.wall_img_functions import (
    THUMB_SIZES,
    get_extended_dimensions,
    get_tile_image_file_extension,
    get_exif_location, get_exif_tags, copy_exif_tags,
    get_tiles_path,
    get_routegeom_img_path,
    get_routegeom_snapshot_path,
    get_tiler_img_path,
    get_thumb_img_upload_path,
    get_materials_upload_path,
    get_userpics_upload_path,
    get_orig_img_upload_path,
    get_wall_upload_path)


logger = logging.getLogger(__name__)

def find_creator_of_object(obj):
    version = Version.objects.get_for_object(obj).last()
    if version is not None:
        return version.revision.user
    else:
        User.objects.get(username='root')


class ActiveWallViewManager(models.Manager):
    """
    This manager filters all currently active non-dev views.
    """

    def get_queryset(self):
        return super(ActiveWallViewManager, self).get_queryset().filter(wall__is_active=True).filter(is_dev=False)


class WallView(models.Model):
    """
    A wallview hosts meta information that can be changed/improved
    within the lifecycle of a wall.
    It is related to a route through a RouteGeometry object.
    Currently, we statically create two views for each wall,
    one view that is used for publication and one that is used
    for further editing (development).

    Note:
        The many-to-many relation to the routes on the wallview is defined
        on the side of the routes.

    Attributes:
        is_dev (boolean): Flags the development version of the wall.
        wall (Wall): The wall object the view is bound to.

        objects (models.Manager): The generic manager of the model.
        active_objects (ActiveWallViewManager):
            A manager that filters the wallviews which are associated with
            active walls. The development views are excluded.
    """

    wall = models.ForeignKey('Wall', on_delete=models.CASCADE)
    is_dev = models.BooleanField(default=False)

    objects = models.Manager()
    active_objects = ActiveWallViewManager()

    @property
    def last_edit_user(self):
        return Version.objects.get_for_object(self).first().revision.user

    @property
    def last_edit_date(self):
        return Version.objects.get_for_object(self).first().revision.date_created

    @property
    def preferred_climbstyle(self):
        """Find the climbstyle to which most of the routes adhere."""
        import operator
        routes = self.route_set.all()
        if routes:
            counts = {}
            for choice in CLIMBINGSTYLE_CHOICES:
                counts[choice[0]] = routes.filter(climbingstyle=choice[0]).count()
            return sorted(list(counts.items()), key=operator.itemgetter(1))[-1][0]
        else:
            return None

    def __str__(self):
        if self.is_dev:
            return 'Dev View at {0:}'.format(self.wall.name)
        else:
            return 'Pub View at {0:}'.format(self.wall.name)

class ActiveWallManager(models.Manager):
    """
    The active wall manager is used to access the active walls.
    """

    def get_queryset(self):
        return super(ActiveWallManager, self).get_queryset().filter(is_active=True).exclude(deprecated=True)

class SubWallManager(models.Manager):
    """
    The active wall manager is used to access the sub walls.
    """

    def get_queryset(self):
        return super(SubWallManager, self).get_queryset().filter(subwall_of__isnull=False)


DURATION_CHOICES = (
    (timedelta(minutes=15), _('15 minutes or less')),
    (timedelta(minutes=30), _('30 minutes')),
    (timedelta(hours=1), _('1 hour')),
    (timedelta(hours=2), _('2 hours')),
    (timedelta(hours=3), _('more than 3 hours')))


class EditManager(models.Manager):
    """
    Find the Edit object (tag) for a model instance,
    return None if not found.
    """
    def tag_for_obj(self, obj):
        tag = super(EditManager, self).get_queryset().filter(obj=slugify_me(obj))
        if tag.exists():
            return tag.first()
        else:
            return None


class Edit(models.Model):
    """Mixinclass to add edit timestamp and user fields."""
    by = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    since = models.DateTimeField(blank=True, null=True)
    obj = models.CharField(max_length=100, db_index=True)
    objects = EditManager()


class UserView(models.Model):
    """Register user views via a Wall to User through model."""
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    wall = models.ForeignKey('Wall', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)


class License(models.Model):
    """
    License defines basic license information which can be linked with
    other objects. The actual license text is supplied by a translation
    key.

    for a translation key "ccl" this would be:

    e.g.:
    - licenses.ccl.fulltext
    """

    short_name = models.CharField(max_length=200, unique=True)
    long_name = models.CharField(max_length=200, unique=True)
    translation_key = models.CharField(max_length=200, unique=True)
    required = models.BooleanField(default=False)
    show = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)

@reversion.register(exclude=('number_of_views',))
class Wall(MapLocation):
    """
    Hosts basic informations on a wall. The informations that are put
    in the wall model are considered static, i.e.,
    they are not updated by users.

    Attributes:
        is_active (Boolean): Flags active (visible) walls.
        is_subwall (Boolean): Flags small sub-walls that are not shown on the map.
        name (str): Human readable string describing the wall.
        orig_img (ImageField): Hosts the image associated with the wall.
        objects (Manager): The generic model manager.
        active_objects (ActiveWallManager): Filters for walls with is_active=True.
        morning_sun, midday_sun, afternoon_sun (BooleanField):
            Is the sun shining in the morning/midday/afternoon?
        children_friendly (IntegerField): A children-friendly rating (5 choices).
        wall_height (IntegerField): The approximate height of the wall in meters.
        DIRECTIONS (tuple): The choices for 8 possible orientations of the wall.

    Properties:
        dev_view (WallView): Accesses the WallView with is_dev=True.
        pub_view (WallView): Accesses the WallView with is_dev=False.
    """
    DRY_DURATION_CHOICES = (
        (0, _('never gets wet')),
        (1, _('dries quickly')),
        (2, _('dries within days')),
        (3, _('never dry'))
    )

    name = models.CharField(
        max_length=100, unique=True, verbose_name=_("Name"),
        help_text=_('Has to be a unique name... If this is a series of views of just one block or a sector, maybe append a number to the name.'))

    is_active = models.BooleanField(default=False)
    subwall_of = models.ForeignKey(
        'Wall', on_delete=models.SET_NULL, null=True, blank=True,
        help_text=_('Select a (main)wall in the vicinity. Hides the current wall from the default map. It is reachable from the mainwall through link markers.')
    )

    description = models.CharField(
        blank=True, null=True, max_length=8192, verbose_name=_("Description"),
        help_text=_('General Info on the history, rock and culture of the place.'))
    urgent_message = models.CharField(
        blank=True, null=True, max_length=1024, verbose_name=_("Urgent Message"),
        help_text=_('Warnings about safety issues or access problems.'))

    approach = models.CharField(
        blank=True, null=True, max_length=4096, verbose_name=_("Approach"),
        help_text=_('How to get to the Wall, e.g. by public transport or by car'))
    approach_time = models.DurationField(
        blank=True, null=True, verbose_name=_("Approach Time"),
        choices=DURATION_CHOICES,
        help_text=_('How long does it take on average to get there?'))

    morning_sun = models.NullBooleanField(
        blank=True, null=True, verbose_name=_("Morning Sun"),
        help_text=_('Does direct sunlight hit the face of the wall during the morning hours?'))
    midday_sun = models.NullBooleanField(
        blank=True, null=True, verbose_name=_("Midday Sun"),
        help_text=_('During the day?'))
    afternoon_sun = models.NullBooleanField(
        blank=True, null=True, verbose_name=_("Afternoon Sun"),
        help_text=_('Or in the afternoon?'))

    children_friendly = models.NullBooleanField(
        blank=True, null=True, verbose_name=_("Children Friendly"),
        help_text=_('Could you relax if your kids were running around, unsupervised?'))
    wall_height = models.PositiveSmallIntegerField(
        blank=True, null=True, verbose_name=_("Wall Height"),
        help_text=_('average height of climbs on this wall'))
    dry_duration = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=DRY_DURATION_CHOICES,
        verbose_name=_("Humidity"), help_text=_('Is it worth the hike after rainfall?'))

    objects = models.Manager()

    active_objects = ActiveWallManager()
    sub_walls = SubWallManager()
    tiler = models.OneToOneField('Tiler', blank=True, null=True, on_delete=models.CASCADE)

    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='walls_created_by_me')

    invite_contrib = models.BooleanField(default=False, verbose_name=_("Work in Progress"),
        help_text=_('Actively invite contributions from the community for this wall.'))

    deprecated = models.BooleanField(
        default=False, verbose_name=_("Deprecated"),
        help_text=_('Deprecated walls are not visible on the default map and it is not possible to add new routes. It should be set if a newer/better version of this crag is available.'))

    gym = models.BooleanField(
        default=False, verbose_name=_("Gym"),
        help_text=_('Tag this wall as part of a climbing-gym. We have a simplified interface to create routes which are changing alot.'))

    snapshot = models.OneToOneField(PhantomSnap, on_delete=models.SET_NULL, null=True, blank=True)
    pdf_topo = models.OneToOneField(PhantomSnap, on_delete=models.SET_NULL, null=True, blank=True, related_name='wall_pdf_topo')

    last_edited_date = models.DateTimeField(auto_now=True)
    last_published_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        permissions = (
            ("miroutes.publish_wall", "Can publish a wall from staging to official"),
        )

    def get_absolute_url(self):
        return reverse('miroutes:wall_detail', kwargs={'wall_id':self.id})

    @property
    def is_subwall(self):
        return self.subwall_of is not None

    @property
    def get_full_route_set(self):
        """ Return all routes associated with the wall, including subwalls that point to it. """
        own_routes = self.pub_view.route_set
        for subwall in Wall.sub_walls.filter(subwall_of=self):
            own_routes = (own_routes.all() | subwall.pub_view.route_set.all())
        return own_routes.distinct()

    # properties that replace attributes on subwalls
    @property
    def get_description(self):
        if self.description == "" and self.subwall_of is not None:
            return self.subwall_of.description
        else:
            return self.description

    @property
    def get_urgent_message(self):
        if self.urgent_message == "" and self.subwall_of is not None:
            return self.subwall_of.urgent_message
        else:
            return self.urgent_message

    @property
    def get_approach(self):
        if self.approach == "" and self.subwall_of is not None:
            return self.subwall_of.approach
        else:
            return self.approach

    @property
    def get_approach_time(self):
        if self.approach_time is None and self.subwall_of is not None:
            return self.subwall_of.get_approach_time_display()
        else:
            return self.get_approach_time_display()

    @property
    def get_morning_sun(self):
        if self.morning_sun is None and self.subwall_of is not None:
            return self.subwall_of.morning_sun
        else:
            return self.morning_sun

    @property
    def get_midday_sun(self):
        if self.midday_sun is None and self.subwall_of is not None:
            return self.subwall_of.midday_sun
        else:
            return self.midday_sun

    @property
    def get_afternoon_sun(self):
        if self.afternoon_sun is None and self.subwall_of is not None:
            return self.subwall_of.afternoon_sun
        else:
            return self.afternoon_sun

    @property
    def get_children_friendly(self):
        if self.children_friendly is None and self.subwall_of is not None:
            return self.subwall_of.children_friendly
        else:
            return self.children_friendly

    @property
    def get_wall_height(self):
        if self.wall_height is None and self.subwall_of is not None:
            return self.subwall_of.wall_height
        else:
            return self.wall_height

    @property
    def get_dry_duration(self):
        if self.dry_duration is None and self.subwall_of is not None:
            return self.subwall_of.get_dry_duration_display()
        else:
            return self.get_dry_duration_display()

    @property
    def last_revision(self):
        last = Version.objects.get_for_object(self).first()
        if last:
            last_revision = last.revision
        else:
            last_revision = None
        return last_revision

    @property
    def popup_url(self):
        """This property defines the url from which the popup content
        is ajaxed.
        """
        return reverse('miroutes:wall_popup', kwargs={'wall_id': self.pk})

    @property
    def click_url(self):
        """This property defines the url to direct to when the marker is clicked.
        """
        return reverse('miroutes:wall_detail', kwargs={'wall_id': self.pk})


    @property
    def marker_json_editwall(self):
        """Config dict for the marker. This is used for one-shot setup of
        a marker on the edit_wall template.
        """

        marker_dict = {
            'icon_config': self.icon_config,
            'popup_url': self.popup_url,
            'click_url': reverse('edit_spot:edit_wall', kwargs={'wall_id': self.pk}),
            'popup_on_hover': self.popup_on_hover,
        }
        if(self.pnt):
            marker_dict['point'] = self.pnt.coords
        return json.dumps(marker_dict)

    @property
    def icon_config_small(self):
        """Configuration of the icon used with the location."""
        return {
            'iconUrl': self.icon_url,
            'iconSize': [24, 28],
            'iconAnchor': [12, 28],
            'popupAnchor': [0, -28]
        }

    @property
    def marker_json_small(self):
        """Config dict for the marker. This is used for one-shot setup of
        a marker on the minimap.
        """

        marker_dict = {
            'icon_config': self.icon_config_small,
            'popup_content': self.name,
            'click_url': reverse('miroutes:wall_detail', kwargs={'wall_id': self.pk}),
            'popup_on_hover': True,
        }
        if(self.pnt):
            marker_dict['point'] = self.pnt.coords
        return json.dumps(marker_dict)

    @property
    def marker_dict(self):
        m_dict = super(Wall, self).marker_dict
        m_dict['label_text'] = self.name
        return m_dict

    @property
    def popup_on_hover(self):
        return True

    @property
    def icon_url(self):
        if not self.is_active or self.deprecated:
            return static('generic_markers/img/wall_inactive_map_icon.png')
        if self.is_subwall:
            return static('generic_markers/img/wall_subwall_map_icon.png')

        return static('generic_markers/img/wall_map_icon.png')

    @property
    def icon_url_edit(self):
        """URL to the icon for the marker when the marker position is editable."""
        return static('generic_markers/img/edit_wall_map_icon.png')

    @property
    def dev_view(self):
        return self.wallview_set.filter(is_dev=True)[0]

    @property
    def pub_view(self):
        return self.wallview_set.filter(is_dev=False)[0]


    @property
    def creator_by_revisions(self):
        return find_creator_of_object(self)

    @property
    def collaborators(self):
        """ Get all people who worked on this wall, ordered by sequence of the revisions, most recent first"""
        versions = Version.objects.get_for_object(self)
        rev_users = versions.values_list('revision__user', flat=True).order_by('-pk').distinct()
        ordering = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(rev_users)])
        return User.objects.filter(id__in=rev_users).order_by(ordering)

    @property
    def last_edit_user(self):
        return Version.objects.get_for_object(self).first().revision.user

    @property
    def last_edit_date(self):
        return Version.objects.get_for_object(self).first().revision.date_created

    def __str__(self):
        return '{}'.format(self.name)

    def publish(self, comment, user):
        """
        Deletes the current pub_view and copies the dev_view over the pub_view
        """
        import reversion

        with reversion.create_revision():
            reversion.set_user(user)
            reversion.set_comment(comment)
            self.last_published_date = datetime.datetime.now()
            # we used to set the wall.is_active here but this is now done in the async task
            # miroutes.tasks.update_Wall_after_publish
            # after all the routegeoms snapshots, wallmap snapshot and pdfs are done.
            # So that once it appears in the news entry etc, all is ready
            self.save()
            from miroutes.classviews import invalidate_RestWall_cache
            invalidate_RestWall_cache(self.id)

            # clear routes and markers from pubview
            # self.pub_view.routegeometry_set.all().delete()
            self.pub_view.simplemarker_set.all().delete()
            self.pub_view.relationalmarker_set.all().delete()
            self.pub_view.textmarker_set.all().delete()
            self.pub_view.pitchmarker_set.all().delete()

            # create routegeom pics
            for routegeom in self.dev_view.routegeometry_set.all():
                if routegeom.geojson:
                    # Check if something changed
                    pubgeom = self.pub_view.routegeometry_set.filter(route=routegeom.route).first()

                    if not pubgeom:
                        # create routegeom
                        routegeom.on_wallview = self.pub_view
                        routegeom.pk = None # setting the pk to none will result in the generation of a new db field
                        routegeom.snapshot = None # and unlink the dev view snapshotter
                        routegeom.save()
                    elif pubgeom.geojson != routegeom.geojson:
                        # copy geojson
                        pubgeom.geojson = routegeom.geojson
                        pubgeom.save()
                    else:
                        # save anyway to add to revision
                        reversion.add_to_revision(pubgeom)

            # delete
            for routegeom in self.pub_view.routegeometry_set.all():
                devgeom = routegeom.on_wallview.wall.dev_view.routegeometry_set.filter(route=routegeom.route).first()
                if not devgeom or not devgeom.geojson:
                    # deleted!
                    routegeom.delete()

            # and the decoration markers
            for marker in self.dev_view.simplemarker_set.all():
                marker.wallview = self.pub_view
                marker.pk = None
                marker.save()
            for marker in self.dev_view.relationalmarker_set.all():
                marker.wallview = self.pub_view
                marker.pk = None
                marker.save()
            for marker in self.dev_view.textmarker_set.all():
                marker.wallview = self.pub_view
                marker.pk = None
                marker.save()
            for marker in self.dev_view.pitchmarker_set.all():
                marker.wallview = self.pub_view
                marker.pk = None
                marker.save()

        # Recreate Route and Wall snapshots etc
        update_Wall_after_publish.run(self.id, activate=True)

    def revert_wall(self, revision, comment, user):
        """Revert wall *manually* from revision object."""
        # clear routes and markers from pubview
        with reversion.create_revision():
            reversion.set_user(user)
            reversion.set_comment("[Reverted view of {} back to state from {}] {}".format(
                self.name,
                revision.date_created.strftime("%d. %b %y %H:%M:%S"),
                comment))

            self.pub_view.routegeometry_set.all().delete()
            self.pub_view.simplemarker_set.all().delete()
            self.pub_view.relationalmarker_set.all().delete()
            self.pub_view.textmarker_set.all().delete()
            self.pub_view.pitchmarker_set.all().delete()
            # this creates the serialized objects
            # and reverts the wall meta
            revision.revert(delete=True)
        self.reset_dev_view()

        # Recreate Route and Wall snapshots etc
        if self.is_active:
            update_Wall_after_publish.run(self.id, activate=False)

    def reset_dev_view(self):
        """
        Deletes the current dev_view and replaces it with the pub_view.
        """
        # clear routes and markers from devview
        self.dev_view.routegeometry_set.all().delete()
        self.dev_view.simplemarker_set.all().delete()
        self.dev_view.relationalmarker_set.all().delete()
        self.dev_view.textmarker_set.all().delete()
        self.dev_view.pitchmarker_set.all().delete()

        # Now we copy the RouteGeometries on the dev view
        for routegeom in self.pub_view.routegeometry_set.all():
            if routegeom.geojson:
                routegeom.on_wallview = self.dev_view
                routegeom.pk = None
                # the snapshot must not be related to the dev routegeom
                routegeom.snapshot = None
                routegeom.save()

        # and the decoration markers
        for marker in self.pub_view.simplemarker_set.all():
            marker.wallview = self.dev_view
            marker.pk = None
            marker.save()
        for marker in self.pub_view.relationalmarker_set.all():
            marker.wallview = self.dev_view
            marker.pk = None
            marker.save()
        for marker in self.pub_view.textmarker_set.all():
            marker.wallview = self.dev_view
            marker.pk = None
            marker.save()
        for marker in self.pub_view.pitchmarker_set.all():
            marker.wallview = self.dev_view
            marker.pk = None
            marker.save()

    @property
    def is_edited(self):
        return Edit.objects.tag_for_obj(self)

    @property
    def is_deletable(self):
        """Check if the user created the wall and that no changes from other users occurred."""
        from edit_spot.views import get_versions

        creator = self.created_by
        # is it currently edited by someone else?
        if self.is_edited and self.is_edited.by != creator:
            return False

        # changes of other users to the wall object.
        versions = get_versions(self)
        for version in versions:
            if version.revision.user != creator:
                return False

        # changes of other users to the public wallviews.
        versions = get_versions(self.pub_view)
        for version in versions:
            if version.revision.user != creator:
                return False
        return True

    def pdf_topo_prefix(self, timestr):
        return 'CruxleTopo_{}_{}'.format(slugify(self.name), timestr)


@receiver(post_save, sender=Wall)
def post_save_Wall(sender, instance, created, update_fields, **kwargs):
    from miroutes.classviews import invalidate_RestWall_cache
    invalidate_RestWall_cache(instance.id)

    if not instance.wallview_set.all():
        dev_view = WallView(wall=instance, is_dev=True)
        dev_view.save()
        pub_view = WallView(wall=instance, is_dev=False)
        pub_view.save()

    if not instance.snapshot:
        export_url = reverse('miexport:wall_export', kwargs={'wall_id': instance.id})
        ps = PhantomSnap(url=export_url)
        ps.save(save_img=False) # only save the object here but dont run the snapshots, this will be done in the afterpublish hooks
        instance.snapshot = ps

        post_save.disconnect(post_save_Wall, sender=Wall)
        instance.save()
        post_save.connect(post_save_Wall, sender=Wall)

    if not instance.pdf_topo:
        export_url = reverse('miexport:wall_topo', kwargs={'wall_id': instance.id})
        ps = PhantomSnap(url=export_url)
        ps.save(save_img=False) # only save the object here but dont run the snapshots, this will be done in the afterpublish hooks
        instance.pdf_topo = ps

        post_save.disconnect(post_save_Wall, sender=Wall)
        instance.save()
        post_save.connect(post_save_Wall, sender=Wall)


class TilerStatus():
    PENDING       = 0
    QUEUED        = 1
    PREPROCESSING = 2
    TILING        = 3
    FINISHED      = 4
    ERROR         = 10
    CHOICES = (
        (PENDING      , 'not yet submitted'),
        (QUEUED       , 'queued'),
        (PREPROCESSING, 'preprocessing image'),
        (TILING       , 'tiling'),
        (FINISHED     , 'finished'),
        (ERROR        , 'error'))

@reversion.register(exclude=('status', 'percentage'))
class Tiler(models.Model):
    """ Hosts all the info about a tiled image """
    status = models.PositiveSmallIntegerField(default=0, choices=TilerStatus.CHOICES)
    percentage = models.PositiveSmallIntegerField(default=0)

    img = models.ImageField(blank=True, upload_to=get_tiler_img_path, max_length=256)
    licenses = models.ManyToManyField(License, verbose_name=_("Image Licenses"))
    orig_img = models.ImageField(blank=True, null=True, upload_to=get_orig_img_upload_path, max_length=256)

    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='wallimages_uploaded_by_me')

    def licenses_to_string(self):

        translated_licenses = []

        for license in self.licenses.filter(show=True):
            translated_licenses.append(ugettext(license.short_name))

        return ", ".join(translated_licenses)

    def __str__(self):
        if hasattr(self, 'wall'):
            return _("Image of {}").format(self.wall)
        else:
            return os.path.basename(self.orig_img.name)

    def create_tiles(self, force_foreground=False, force_tiles_generation=False):
        """Tiling procedure
        We take the background image (orig_img),
        resize the orig_img to fit the leaflet canvas better,
        create the tiles and save them in the local path to the media dir
        """

        if not self.pk:
            raise Exception("!!! create_tiles was called for a tiler object which has not yet been save, i.e. has no id yet?")
        if not self.orig_img:
            raise Exception("!!! No background image to create tiles for -- This is probably a bug or the upload went wrong?")
        orig_img_file_name = self.orig_img.name
        img_file_name = get_tiler_img_path(self)
        tiles_dir = get_tiles_path(self)

        print("Creating Tiles Tilerid={} : {} => {} => {}".format(self.pk, orig_img_file_name, img_file_name, tiles_dir))
        if force_foreground:
            tile_image(orig_img_file_name, img_file_name, tiles_dir, tiler_id=self.pk, force_tiles_generation=force_tiles_generation)
        else:
            tile_image.delay(orig_img_file_name, img_file_name, tiles_dir, tiler_id=self.pk, force_tiles_generation=force_tiles_generation)
        self.img = img_file_name
        self.save(update_fields=['img',])

    def get_tiles_url(self):
        """
        Return the web adress to the directory containing the tile dir structure
        """
        from django.core.files.storage import default_storage as storage

        tiles_file_path = get_tiles_path(self)

        print(("Return tiles web adress:",tiles_file_path,' :: ', storage.url(tiles_file_path)))
        if storage.exists(tiles_file_path):
            return storage.url(tiles_file_path)
        else:
            print(("No tiles found for {}".format(tiles_file_path)))
            return ""

    def get_img_size(self):
        """
        Return the size of the extended background image and the number of zoom levels in x and direction
        @TODO: is this what we really want -- shouldnt it be the image size of the zoom lvl 0 tile?
        """
        from django.core.files.storage import default_storage as storage
        import numpy as np
        if not self.img:
            return "Tiler: no background image available for {}".format(self.id)

        try:
            _, _, newwidth, newheight = get_extended_dimensions(storage.path(self.img.name))
            dim = (newwidth, newheight)
        except IOError as e:  # usually happens when no file exists yet
            return (-1)*4

        tile_width = 256
        zoom_levels = [ np.int(np.ceil(zl)) for zl in np.log(np.array(dim).astype(float) / tile_width) / np.log(2)]

        res = list(dim) + zoom_levels
        return res # return as list

    def get_tile_image_file_extension(self):
        if self.img is not None:
            return get_tile_image_file_extension(self.img.name)
        else:
            return None

    @property
    def thumbnail_url_s(self):
        return os.path.join(settings.MEDIA_URL, get_thumb_img_upload_path(self, self.orig_img.name, size_id='s'))

    @property
    def thumbnail_url_m(self):
        return os.path.join(settings.MEDIA_URL, get_thumb_img_upload_path(self, self.orig_img.name, size_id='m'))

    @property
    def thumbnail_url_l(self):
        return os.path.join(settings.MEDIA_URL, get_thumb_img_upload_path(self, self.orig_img.name, size_id='l'))

    @property
    def thumbnail_url_xl(self):
        return os.path.join(settings.MEDIA_URL, get_thumb_img_upload_path(self, self.orig_img.name, size_id='xl'))


    def create_thumbs(self, force_foreground=False):
        """
        Create a thumbnail from original picture
        """
        from django.core.files.storage import default_storage as storage
        if not self.orig_img:
            print ("No image to create thumbnail for")
            return ""
        for size_id, size in list(THUMB_SIZES.items()):
            thumb_path = get_thumb_img_upload_path(self, self.orig_img.name, size_id=size_id)
            if storage.exists(thumb_path):
                print("thumb_file already exists")
            try:
                if force_foreground:
                    create_thumb_image(self.orig_img.name, thumb_path, keep_aspect=True, size=(size,size))
                else:
                    create_thumb_image.delay(self.orig_img.name, thumb_path, keep_aspect=True, size=(size,size))
            except Exception as e:
                print(("Error creating thumb_file", e))

    def rotate_left(self):
        self.rotate(90)

    def rotate_right(self):
        self.rotate(-90)

    def rotate(self, angle, rotation_marker="_rotated"):
        from PIL import Image
        from django.core.files.storage import default_storage as storage
        import io
        import imghdr
        with closing(storage.open(self.orig_img.name, 'rb')) as fh:
            img_format = imghdr.what(fh)
            with closing(Image.open(fh)) as img:
                rotated_img = img.rotate(angle, expand=True)
                fname_before, ext = os.path.splitext(self.orig_img.name)
                if fname_before.endswith(rotation_marker):
                    # do not create _rotated_rotated_rotated chains
                    fname = fname_before
                else:
                    fname = fname_before + rotation_marker

                newfilename = fname + ext

                # Put new, rotated image data into buffer
                # and save it through the Django mechanism
                output = io.BytesIO()
                rotated_img.save(output, format=img_format)
                os.sync()

                # change wall.tiler.status to trigger the spinner on draw_routes
                self.status = 0
                self.orig_img.save(os.path.basename(newfilename), output, save=True)
                os.sync()

                # Finally copy the original exif tags over to the rotated img
                copy_exif_tags(storage.path(fname_before+ext), storage.path(self.orig_img.name))


    @property
    def exif_location(self):
        return get_exif_location(self.orig_img)

    @property
    def exif_tags(self):
        return get_exif_tags(self.orig_img.name)

@receiver(post_save, sender=Tiler)
def post_save_Tiler(sender, instance, created, update_fields, **kwargs):
    if update_fields is None:
        instance.create_tiles()
        instance.create_thumbs()

    try:
        instance.wall.save() # This will update the corresponding wall, including pdf generation etc.
    except Wall.DoesNotExist:
        pass



class Userpic(models.Model):
    """Intended to be used to store user pictures."""
    img = models.ImageField(upload_to=get_userpics_upload_path, max_length=256)
    licenses = models.ManyToManyField(License, verbose_name=_("Image Licenses"))
    description = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Description"))
    external_url = models.URLField(
        max_length=200, blank=True, null=True, verbose_name=_("External URL"),
        help_text=_('Is the Image from a publicly available webpage, maybe with more detailed info? Add the URL here!'))
    pnt = models.PointField(verbose_name="Location", blank=True, null=True)
    on_wall = models.ManyToManyField('Wall', blank=True)
    on_route = models.ManyToManyField('Route', blank=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    pdf_file = models.FileField(blank=True, null=True, upload_to=get_userpics_upload_path, max_length=256)

    CONTENT_CHOICES = (
        ('info', _('Information')),
        ('personal', _('Personal Content')),
    )
    content = models.CharField(max_length=20, choices=CONTENT_CHOICES)

    def __str__(self):
        return '{} on {}/{} by {}'.format(self.description, self.on_wall, self.on_route, self.created_by)

    def licenses_to_string(self):

        translated_licenses = []

        for license in self.licenses.filter(show=True):
            translated_licenses.append(ugettext(license.short_name))

        return ", ".join(translated_licenses)

    @property
    def comment_thread(self):
        return generate_thread_id(self)

    @property
    def thumbnail(self):
        if self.img:
            from easy_thumbnails.files import get_thumbnailer
            return get_thumbnailer(self.img)['userpic_thumb'].url


def get_grade_system(wall):
    """Check for the nearest grading system to pre-populate forms.
    1. Check on pub and dev view of wall.
    2. Check closest route (spatial lookup).
    """
    if wall.pub_view.route_set.count() > 0:
        for route in wall.pub_view.route_set.all():
            if route.grade > 0:
                return route.grading_system
    if wall.dev_view.route_set.count() > 0:
        for route in wall.dev_view.route_set.all():
            if route.grade > 0:
                return route.grading_system
    if Route.objects.all().count() > 0:
        next_routes = Route.objects.filter(
            pnt__dwithin=(wall.pnt, .5)).annotate(distance=Distance('pnt', wall.pnt)).order_by('distance')
        for route in next_routes:
            if route.grade > 0:
                return route.grading_system
    else:
        return 0

def get_common_route_attributes(attributes, pnt, radius=.5):
    """Check common values of attributes on nearby routes.

    Args:
        attributes: list of attributes
        pnt: position of search centre
        radius: radius in DjangoGIS units, default .5

    Returns:
        dict of the form {attr1: most_common_value1, attr2: ...}
    """
    import operator
    attribute_dict = dict.fromkeys(attributes)
    next_routes = Route.objects.filter(
        pnt__dwithin=(pnt, radius))
    if next_routes.count() == 0:
        # nothing found
        return attribute_dict
    for attr in attribute_dict:
        route_countdict = {}
        for route in next_routes:
            route_attribute = getattr(route, attr)
            if route_attribute is not None:
                value = route_countdict.get(route_attribute)
                if value is not None:
                    # increase counter, attr found
                    route_countdict[route_attribute] += 1
                else:
                    # new attr, set counter to 1
                    route_countdict[route_attribute] = 1
        attribute_dict[attr] = max(iter(list(route_countdict.items())), key=operator.itemgetter(1))[0]

    return attribute_dict

def get_grade_choices(grade_system):
    """
    We use the grade system of the wall to limit the possible grade choices for routes
    """
    grade_choices = [x for x in GRADE_CHOICES if grade_to_grade_system(x[0]) == grade_system]
    null_choice = GRADE_CHOICES[0]

    return [null_choice,] + grade_choices

def get_most_common_grade_system(routes):
    """Get the average grading system of a set of routes."""
    grade_systems = [ grade_to_grade_system(g) for g in routes.exclude(grade=0).values_list('grade', flat=True) ]

    if len(grade_systems)==0:
        return 0;

    import numpy as np
    return np.argmax(np.bincount(grade_systems))


class ActiveRouteManager(models.Manager):
    """This manager filters all routes that are not deprecated.
    """

    def get_queryset(self):
        return super(ActiveRouteManager, self).get_queryset().filter(deprecated=False)


class GymRouteManager(models.Manager):
    """This manager filters all routes that are on gyms.
    """

    def get_queryset(self):
        return super(GymRouteManager, self).get_queryset().filter(gym_route=True)




def get_qrcode_upload_path(qrcode, fname):
    path = os.path.join("qrcodes", fname)
    return path

class QRCode(models.Model):
    """
    Holds the qrcode and the associated url.
    Also supplies the routines to create an svg and jpg.
    """
    url = models.URLField(unique=True)
    img = models.ImageField(blank=True, null=True, upload_to=get_qrcode_upload_path, max_length=256)
    svg = models.FileField (blank=True, null=True, upload_to=get_qrcode_upload_path, max_length=256)

    def create_qrcodes(self):
        from django.core.files.storage import default_storage as storage
        from django.utils.text import slugify
        import qrcode
        import qrcode.image.svg

        svg_path = get_qrcode_upload_path(self, slugify(self.url))+'.svg'
        img_path = get_qrcode_upload_path(self, slugify(self.url))+'.jpg'

        def create_jpg_qr():
            print(("Creating JPG QRCode for",self.url))
            qr = qrcode.QRCode(version=None,  # determine with fit
                    error_correction=qrcode.constants.ERROR_CORRECT_H,)
            qr.add_data(self.url)
            qr.make(fit=True)
            img = qr.make_image()
            img.save(storage.path(img_path), quality=80, optimize=True)
            os.sync()
            self.img = img_path
            self.save()

        if not self.img:
            create_jpg_qr()

        if not storage.exists(img_path):
            create_jpg_qr()


        def create_svg_qr():
            print(("Creating SVG QRCode for",self.url))
            factory = qrcode.image.svg.SvgPathImage
            svg = qrcode.make(self.url,
                    error_correction=qrcode.constants.ERROR_CORRECT_H,
                    image_factory=factory)
            svg.save(storage.path(svg_path))
            os.sync()
            self.svg = svg_path
            self.save()

        if not self.svg:
            create_svg_qr()

        if not storage.exists(svg_path):  # recreate svg
            create_svg_qr()

    def save(self, *args, **kwargs):
        super(QRCode, self).save(*args, **kwargs)
        self.create_qrcodes()



@reversion.register(exclude=('walls', 'climbers'))
class Route(MapLocation):
    """
    Meta data for a route object. Similar to the wall object uniquely refers
    to an existing route, with multiple route geometries on different wall views.

    Attributes:
        walls: The walls the route can be found on.
            This list is generated from the RouteGeometry objects.
        name: The name of the route.
        grade: The grade of the route.
        rating: The (average) rating of the route.
            This should be chosen in agreement with the grading system of the area.
        length: The approximate length of the route in meters.
        number_of_bolts: The number of bolts and thus the number of
            quickdraws that are required to climb the route.
        bolt_quality (IntegerField): A choice field with a bolt quality rating (5 choices).
        security_rating: How secure is climbing the route? (1-5)
        description: Comments and caveats on the route.
    """
    objects = models.Manager()
    # active object manager yields only non-deleted routes
    active_objects = ActiveRouteManager()
    gym_objects = GymRouteManager()

    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True,
        related_name='routes_created_by_me', verbose_name=_("Created By"))

    bolt_quality = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=RATING_CHOICES, verbose_name=_("Bolt Quality"))

    # The relation to one or many walls is via the geometry of the route
    walls = models.ManyToManyField(WallView, through='RouteGeometry')
    climbers = models.ManyToManyField(User, through='Ascent')

    climbingstyle = models.PositiveSmallIntegerField(choices=CLIMBINGSTYLE_CHOICES, verbose_name=_("Style"))

    name = models.CharField(max_length=100, verbose_name=_("Name"))

    grade = models.PositiveSmallIntegerField(default=0, choices=GRADE_CHOICES, verbose_name=_("Grade"))
    aid_grade = models.PositiveSmallIntegerField(
        blank=True, null=True,
        choices=AID_GRADE_CHOICES, verbose_name=_("Aid Grade"))

    # rating = models.IntegerField(blank=True, null=True, choices=RATING_CHOICES)
    # security_rating = models.IntegerField(blank=True, null=True, choices=RATING_CHOICES)

    length = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=_("Length"))

    number_of_bolts = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=_("Number of Bolts"))
    description = models.CharField(blank=True, null=True, max_length=8192, verbose_name=_("Description"))
    additional_gear = models.CharField(blank=True, null=True, max_length=4096, verbose_name=_("Additional Gear"))

    developer = models.CharField(blank=True, max_length=100, verbose_name=_("Developer"))
    developed_year = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=YEAR_CHOICES, verbose_name=_("Year Developed"))

    editor_notes = models.CharField(blank=True, null=True, max_length=8192, verbose_name=_("Editor Notes"))

    deprecated = models.BooleanField(default=False, verbose_name=_("Deprecated"),
                                     help_text=_('Flag route information as deprecated'))
    tradclimb = models.BooleanField(default=False, verbose_name=_("Tradclimb"),
                                    help_text=_('Difficulty and required gear might vary on traditional climbs.'))

    color = ColorField(blank=True, null=True, verbose_name=_("Color"),
                       help_text=_('Leave blank to auto-choose the color according to difficulty'))

    gym_route = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('miroutes:route_detail', kwargs={'route_id':self.id})

    def __str__(self):
        return '{}'.format(self.name)

    @property
    def is_edited(self):
        return Edit.objects.tag_for_obj(self)

    @property
    def get_full_grade_display(self):
        grade_str = self.get_grade_display()
        if self.aid_grade in AID_GRADE_CHOICES_DICT:
            grade_str = '{}/{}'.format(grade_str, AID_GRADE_CHOICES_DICT[self.aid_grade])
        return grade_str

    @property
    def polylinecolor(self):
        if self.color:
            return self.color
        if self.grade == 0:
            return '#6666ff'
        diff = (self.grade % 1000)//100
        return LINE_COLORS[diff]

    @property
    def icon_url(self):
        return static('generic_markers/img/route_map_icon.png')

    @property
    def icon_url_edit(self):
        """URL to the icon for the marker when the marker position is editable."""
        return static('generic_markers/img/edit_route_map_icon.png')

    @property
    def grading_system(self):
        return grade_to_grade_system(self.grade)

    @property
    def rating(self):
        return Ascent.objects.filter(route=self).aggregate(Avg('rating'))['rating__avg'] or -1

    @property
    def creator_by_revision(self):
        return find_creator_of_object(self)

    @property
    def created_at(self):
        return Version.objects.get_for_object(self).last().revision.date_created

    @property
    def last_edit_user(self):
        return Version.objects.get_for_object(self).first().revision.user

    @property
    def last_edit_date(self):
        return Version.objects.get_for_object(self).first().revision.date_created

    @property
    def thread_id(self):
        return generate_thread_id(self)

    def can_be_deleted_by(self, user):
        """Check if this route can be deleted by a user."""
        from edit_spot.views import acquire_edit_lock
        if acquire_edit_lock(self, user) and self.created_by == user and not self.routegeometry_set.all():
            return True
        return False

    def get_ascent_count(self):
        return self.ascent_set.all().count()


@receiver(post_save, sender=Route)
def post_save_Route(sender, instance, created, update_fields, **kwargs):
    if update_fields is None:
        update_after_Route_save.run(route_id=instance.id)


class PolylineField(JSONField):
    """A polyline field accepts a JSON that represents a list with at least 2 elements
    being either

    - 2-element lists with floats or integers or
    - a dictionary of the form {'lat': value, 'lng': value}, where value is a float or an integer.
    """

    def to_python(self, value):
        """Check if we can generate a list and if the elements are given by a dict with lat and lngs
        we build a 2-element list instead.
        """

        value = super(PolylineField, self).to_python(value)
        try:
            line = list(value)
            for num, coordinate in enumerate(line):
                if isinstance(coordinate, dict):
                    value[num] = [coordinate['lat'], coordinate['lng']]
            return value
        except (TypeError, KeyError) as e:
            raise exceptions.ValidationError(
                _("Value must be a list of at least 2 2-element lists or {lat, lng} dicts."),
                code='invalid',
                params={'value': value},
            )


    def validate(self, value, model_instance):
        """Check if the list has correct size >= 2 and the elements are
        2-element lists of floats or ints."""
        super(PolylineField, self).validate(value, model_instance)
        try:
            if len(value) < 2:
                raise TypeError
            for num, coordinate in enumerate(value):
                if len(coordinate) != 2:
                    raise TypeError
                elif not isinstance(coordinate[0], (int, float)) or not isinstance(coordinate[1], (int, float)):
                    raise TypeError

        except (TypeError, KeyError) as e:
            raise exceptions.ValidationError(
                _("Value must be a list of at least 2 2-element lists or {lat, lng} dicts."),
                code='invalid',
                params={'value': value},
            )


class RouteGeometry(models.Model):
    """The geometry of a route that is specific to a wall."""

    on_wallview = models.ForeignKey(WallView, on_delete=models.CASCADE)
    route = models.ForeignKey(Route, on_delete=models.CASCADE)

    geojson = JSONField(blank=True, null=True)

    snapshot = models.OneToOneField(PhantomSnap, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        unique_together = ('on_wallview', 'route')
    def __str__(self):
        return '{0:} on {1:}'.format(self.route.name, self.on_wallview.wall.name)


    def _get_geojson_coordinates(self):
        coords = []
        for feature in self.geojson["features"]:
            co = feature["geometry"]["coordinates"]
            if feature["geometry"]["type"] == "Point":
                coords.append(co)
            elif feature["geometry"]["type"] == "Polygon":
                # it looks like polygons are packed like [[[x1, y1], [x2, y2]]]
                coords.extend(co[0])
            else:
                coords.extend(co)
        coords.sort(key=lambda coord: coord[1])
        return coords


    @property
    def anchorpoint(self):
        """Search the lowest point of the geojson geometry,
        if there is any.

        Format: (y, x) or (lat, lng)
        """
        if self.geojson:
            coords = self._get_geojson_coordinates()
            return coords[0][::-1]


    @property
    def popuppoint(self):
        """Search the top point of the linestring geometry,
        if there is any.

        Format: (y, x)
        """
        if self.geojson:
            coords = self._get_geojson_coordinates()
            return coords[-1][::-1]

    @property
    def geojson_dict(self):
        return {
            'geojson': self.geojson,
            'anchorpoint': self.anchorpoint,
            'popuppoint': self.popuppoint,
            'color': self.route.polylinecolor
        }

    @property
    def geojson_as_string(self):
        return json.dumps(self.geojson_dict)


@receiver(post_save, sender=RouteGeometry)
def post_save_RouteGeometry(sender, instance, created, update_fields, **kwargs):
    if not instance.on_wallview.is_dev:  # Only Create snaps for published route geoms
        if not instance.snapshot:
            export_url = reverse('miexport:display_routegeom', kwargs={'routegeom_id': instance.id})
            ps = PhantomSnap(url=export_url)
            ps.save()
            instance.snapshot = ps

            post_save.disconnect(post_save_RouteGeometry, sender=RouteGeometry)
            instance.save()
            post_save.connect(post_save_RouteGeometry, sender=RouteGeometry)

        if update_fields is None or 'snapshot' in update_fields:
            instance.snapshot.get_snap_signature().apply_async()



class Ascent(models.Model):
    """
    Relates a user (climber) to a route "through" the details of
    a climbing attempt.
    """
    TENDENCY_CHOICES = (
        (1, _('grade too high (climb felt easier)')),
        (2, _('grade just right')),
        (3, _('grade too low (climb felt harder)')),
    )
    climber = models.ForeignKey(User, on_delete=models.CASCADE)
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    # note that date field is expected by JS APIs.
    date = models.DateField(default=datetime.date.today)
    # this field is deprecated
    datetime = models.DateTimeField(default=datetime.date.today)

    style = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=STYLE_CHOICES, verbose_name=_("Style"))
    rating = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=RATING_CHOICES, verbose_name=_("Rating"))
    note = models.CharField(blank=True, null=True, max_length=4096, verbose_name=_("Note"))
    tendency = models.PositiveSmallIntegerField(
        blank=True, null=True, verbose_name=_("Tendency"),
        choices=TENDENCY_CHOICES,
        help_text=_('Do you second the difficulty rating?'))


    @property
    def news_json(self):
        return {
            "id": self.pk,
            "user_id": self.climber.pk,
            "user_name": self.climber.username,
            "avatar": self.climber.userprofile.thumb_url(),
            "style": self.get_style_display(),
            "rating": self.rating,
            "date": self.datetime
        }
