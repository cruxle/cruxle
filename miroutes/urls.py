from django.conf.urls import *
from rest_framework.routers import DefaultRouter

from miroutes import views
from miroutes import classviews
from django.http import HttpResponse
do_nothing = lambda *args, **kwargs: HttpResponse()

app_name = 'miroutes'

route_router = DefaultRouter()
route_router.register(r'rest/routes', classviews.RouteViewSet, basename='rest-route')

urlpatterns = [ url(r'^$', views.index, name='index'),
url(r'^search/', views.search, name='search'),
url(r'^about/', views.about, name='about'),
url(r'^imprint/', views.imprint, name='imprint'),
url(r'^license/(?P<license_key>[0-9a-z-]+)$', views.license, name='license'),
url(r'^privacy/', views.privacy, name='privacy'),
url(r'^news/', views.news, name='news'),
url(r'^help/', views.help, name='help'),
url(r'^wall_selection/', views.wall_selection, name='wall_selection'),
url(r'^check_wallview_updates/(?P<since>\d+)$', views.check_wallview_updates, name='check_wallview_updates'),

url(r'^pic(?P<userpic_id>\d+)/$', views.userpic, name='userpic'),
url(r'^pic(?P<userpic_id>\d+)/edit$', views.userpic_edit, name='userpic_edit'),
url(r'^pic$', do_nothing, name='userpic_stub'),
url(r'^picdel(?P<userpic_id>\d+)$', views.del_userpic, name='del_userpic'),
url(r'^picdel$', do_nothing, name='del_userpic_noid'),

url(r'^wall(?P<wall_id>\d+)/add_userpic', views.add_userpic, name='add_userpic'),
url(r'^route(?P<route_id>\d+)/add_userpic', views.add_userpic, name='add_userpic'),
url(r'^wall(?P<wall_id>\d+)/userpic_display$', views.userpic_display, name='userpic_display'),
url(r'^route(?P<route_id>\d+)/userpic_display$', views.userpic_display, name='userpic_display'),
url(r'^wall(?P<wall_id>\d+)/userpics', views.show_userpics, name='show_userpics'),
url(r'^route(?P<route_id>\d+)/userpics', views.show_userpics, name='show_userpics'),

url(r'^wall(?P<wall_id>\d+)/show_on_map', views.wall_on_map, name='wall_on_map'),
url(r'^wall(?P<wall_id>\d+)/dev$', views.wall_detail_dev, name='wall_detail_dev'),
url(r'^wall(?P<wall_id>\d+)/$', views.wall_detail, name='wall_detail'),
url(r'^wall(?P<wall_id>\d+)/just-published$', lambda *args, **kwargs:views.wall_detail(*args, **kwargs, just_published=True), name='wall_detail_published'),
url(r'^wall$', do_nothing, name='wall_detail_stub'),
url(r'^wall(?P<wall_id>\d+)/route_histogram$', views.wall_route_histogram, name='wall_route_histogram'),
url(r'^wall(?P<wall_id>\d+)/popup$', views.wall_popup, name='wall_popup'),
url(r'^wall(?P<wall_id>\d+)/routes$', views.wall_routelist, name='wall_routelist'),
url(r'^wall(?P<wall_id>\d+)/viewed$', views.wall_viewed, name='wall_viewed'),
url(r'^wall(?P<wall_id>\d+)/addfav$', views.wall_toggle_favourite, name='wall_toggle_favourite'),
url(r'^rest/wall/(?P<pk>\d+)$', classviews.RestWall.as_view(), name='rest_wall'),
url(r'^rest/news$', classviews.NewsViewSet.as_view(), name='rest_news'),
url(r'^rest/wall_activity/(?P<wall_id>\d+)$', classviews.RestWallActivity.as_view(), name='rest_wall_activity'),

url(r'^route(?P<route_id>\d+)$', views.route_detail, name='route_detail'),
url(r'^route$', do_nothing, name='route_detail_stub'),
url(r'^rest/route_activity/(?P<route_id>\d+)$', classviews.RestRouteActivity.as_view(), name='rest_route_activity'),
url(r'^route/short_info(?P<route_id>\d+)$', views.route_short_info, name='route_short_info'),
url(r'^route/short_info$', views.route_short_info, name='route_short_info'),
url(r'^route(?P<route_id>\d+)/timeline_chunk$', views.route_timeline_chunk, name='route_timeline_chunk'),
url(r'^route(?P<route_id>\d+)/qrcode$', views.route_qrcode, name='route_qrcode'),
url(r'^tick_route/(?P<route_id>\d+)$', views.tick_route, name='tick_route'),
url(r'^tick_route/$', views.tick_route, name='tick_route'),

url(r'^ticked/(?P<route_id>\d+)$', views.add_ascent, name='add_ascent'),
url(r'^ticked/$', views.add_ascent, name='add_ascent'),
url(r'^save_mapview$', views.save_mapview, name='save_mapview'),
url(r'^save_baselayer$', views.save_baselayer, name='save_baselayer'),
url(r'^ascent/details(?P<ascent_id>\d+)$', views.ascent_details, name='ascent_details'),
url(r'^ascent/details$', views.ascent_details, name='ascent_details'),
url(r'^ascent/delete(?P<ascent_id>\d+)$', views.del_ascent, name='del_ascent'),
url(r'^ascent/delete$', views.del_ascent, name='del_ascent'),

url(r'toggle_show_inactive', views.toggle_show_inactive, name='toggle_show_inactive'),
url(r'^filter_labels/$', do_nothing, name='filter_labels_ng'),
url(r'^filter_labels/(?P<grade_system>\d)$', views.get_filter_labels, name='filter_labels'),
]

urlpatterns += route_router.urls


from django.contrib.sitemaps.views import sitemap
from miroutes.sitemaps import sitemaps
from django.urls import path
from django.views.decorators.cache import cache_page

urlpatterns += [path('sitemap.xml', cache_page(60 * 60 * 24)(sitemap), {
    'sitemaps': sitemaps,
    'template_name': 'sitemap/MiroutesSitemap.html',},
    name='sitemap'), ]
