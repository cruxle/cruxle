from django.contrib import admin
from django.templatetags.static import static
from django.utils.safestring import mark_safe
from django.urls import reverse
from reversion.admin import VersionAdmin

from miroutes.models import Route, RouteGeometry, Ascent, Userpic, Tiler

from miroutes.models import Wall, WallView
from miroutes.models import QRCode
from generic_markers.models import SimpleMarker, RelationalMarker, TextMarker, PitchMarker

NUM_PAGE_ENTRIES = 500

# Register your models here.

@admin.register(QRCode)
class ModelAdmin(admin.ModelAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

@admin.register(Route)
class ModelAdmin(admin.ModelAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

@admin.register(RouteGeometry)
class ModelAdmin(VersionAdmin):
    list_display = ('__str__', 'len_geojson', 'wallview', 'thumb')
    list_per_page = NUM_PAGE_ENTRIES

    def len_geojson(self, obj):
        if obj.geojson:
            return '{}'.format(len(obj.geojson))
        else:
            return '-'

    def wallview(self, obj):
        return obj.on_wallview

    def thumb(self, obj):
        if obj.snapshot:
            return mark_safe("<a href='{url}' target='blank'><img src='{thumburl}' class='img-thumbnail' style='max-width:400px'></img></a>".format(url=obj.snapshot.url, thumburl=obj.snapshot.img.url))
        return 'No Snapshot obj'

@admin.register(Wall)
class ModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'gym', 'deprecated', 'is_active', 'created_by', 'thumb', 'insta_snapshot')
    list_per_page = NUM_PAGE_ENTRIES

    def thumb(self, obj):
        try:
            return mark_safe("<a href='{url}' target='blank'><img src='{thumburl}' class='img-thumbnail' style='max-width:400px'></img></a>".format(url=obj.snapshot.url, thumburl=obj.snapshot.img.url))
        except (AttributeError,ValueError):
            return static('topo/img/delayed_img.png')

    def insta_snapshot(self, obj):
        return mark_safe("<a href='{url}?width=1080&height=1350' target='blank'>Insta Snapshot</a>".format(url=reverse('miexport:wallimage_export', kwargs={'wall_id': obj.id})))

@admin.register(WallView)
class ModelAdmin(VersionAdmin):
    list_display = ['__str__',]+[f.name for f in WallView._meta.fields]
    list_per_page = NUM_PAGE_ENTRIES
    exclude = ['is_dev']
    ordering = ['wall',]
    pass

@admin.register(SimpleMarker)
class ModelAdmin(VersionAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

@admin.register(RelationalMarker)
class ModelAdmin(VersionAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

@admin.register(TextMarker)
class ModelAdmin(VersionAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

@admin.register(PitchMarker)
class ModelAdmin(VersionAdmin):
    list_per_page = NUM_PAGE_ENTRIES
    pass

admin.site.register(Ascent)

@admin.register(Tiler)
class ModelAdmin(admin.ModelAdmin):
    list_display = ('img', 'status', 'percentage', 'created_by', 'thumb')
    list_per_page = NUM_PAGE_ENTRIES

    def thumb(self, obj):
        if obj.img:
            return mark_safe("<a href='{url}' target='blank'><img src='{thumburl}' class='img-thumbnail' style='max-width:400px'></img></a>".format(url=obj.img.url, thumburl=obj.thumbnail_url_s))
        return 'No img obj'

@admin.register(Userpic)
class ModelAdmin(admin.ModelAdmin):
    list_display = ('img', 'description', 'external_url', 'created_by', 'thumb')
    list_per_page = NUM_PAGE_ENTRIES

    def thumb(self, obj):
        return mark_safe("<img src='{}' class='img-thumbnail' style='max-width:400px'></img>".format(obj.img.url))
