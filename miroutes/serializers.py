"""Serializers for public miroutes app."""
import html
from reversion.models import Revision, Version
from django.urls import reverse
from django.templatetags.static import static

from rest_framework import serializers
from miroutes.models import Tiler, Route, Ascent, RouteGeometry, Wall, Userpic
from micomments.utils import generate_thread_id
from micomments.models import Comment
from users.serializers import UserSerializer, UserSerializer_noprofile
from easy_thumbnails.files import get_thumbnailer

from miroutes.utils import replace_objs

def get_last_user_edit(serializer, obj):
    request = serializer.context.get('request', None)
    if request:
        most_recent_version = Version.objects.get_for_object(obj).filter(revision__user=request.user).first()
        return most_recent_version.revision.date_created

class TilerSerializer(serializers.ModelSerializer):
    """Serializer for the Tiler class."""
    class Meta:
        model = Tiler
        fields = [
            'id',
            'status',
            'percentage',
            'wall',
            'get_status_display',
            'get_img_size',
            'get_tiles_url',
            'get_tile_image_file_extension',
            'thumbnail_url_s',
            'thumbnail_url_m',
            'thumbnail_url_l',
            'thumbnail_url_xl',]

class TilerWorkSerializer(serializers.ModelSerializer):
    """Serializer for the Tiler class."""
    name = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    edit_url = serializers.SerializerMethodField(read_only=True)
    thumb = serializers.SerializerMethodField(read_only=True)
    last_edit = serializers.SerializerMethodField(read_only=True)
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = Tiler
        fields = [
            'id',
            'status',
            'created_by',
            'last_edit',
            'name',
            'url',
            'edit_url',
            'thumb']

    def get_name(self, tiler):
        return tiler.__str__()

    def get_thumb(self, tiler):
        return tiler.thumbnail_url_s

    def get_url(self, tiler):
        if hasattr(tiler, 'wall'):
            return reverse('miroutes:wall_detail', kwargs={'wall_id': tiler.wall.pk})

    def get_edit_url(self, tiler):
        return reverse('edit_spot:edit_wall_img', kwargs={'tiler_id': tiler.pk})

    def get_last_edit(self, tiler):
        return get_last_user_edit(self, tiler)

class RouteGeometrySerializer(serializers.ModelSerializer):
    class Meta:
        model = RouteGeometry
        fields = [
            'id',
            'on_wallview',
            'route',
            'geojson',
            'anchorpoint'
        ]


class RouteWorkSerializer(serializers.ModelSerializer):
    thumb = serializers.SerializerMethodField(read_only=True)
    last_edit = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    edit_url = serializers.SerializerMethodField(read_only=True)
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = Route
        fields = [
            'id',
            'name',
            'url',
            'edit_url',
            'grade',
            'aid_grade',
            'get_full_grade_display',
            'climbingstyle',
            'color',
            'polylinecolor',
            'gym_route',
            'pnt',
            'deprecated',
            'created_by',
            'description',
            'last_edit',
            'thumb'
        ]

    def get_last_edit(self, route):
        return get_last_user_edit(self, route)

    def get_thumb(self, route):
        from easy_thumbnails.exceptions import InvalidImageFormatError

        geom = route.routegeometry_set.filter(on_wallview__is_dev=False).first()
        if geom and geom.snapshot:
            try:
                return get_thumbnailer(geom.snapshot.img)['geometry_thumb'].url
            except InvalidImageFormatError:
                return static('topo/img/route_not_linked.png')
        else:
            return static('topo/img/route_not_linked.png')

    def get_edit_url(self, route):
        return reverse('edit_spot:edit_route', kwargs={'route_id': route.pk})

    def get_url(self, route):
        return reverse('miroutes:route_detail', kwargs={'route_id': route.pk})

class RouteSerializer(serializers.ModelSerializer):
    geometries = serializers.SerializerMethodField(read_only=True)
    grade_system = serializers.SerializerMethodField(read_only=True)
    grade_tendency = serializers.SerializerMethodField(read_only=True)
    ascent_count = serializers.SerializerMethodField(read_only=True)
    comment_count = serializers.SerializerMethodField(read_only=True)
    thread_id = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Route
        fields = [
            'id',
            'name',
            'grade',
            'aid_grade',
            'get_full_grade_display',
            'grade_system',
            'climbingstyle',
            'color',
            'polylinecolor',
            'gym_route',
            'pnt',
            'geometries',
            'deprecated',
            'grade_tendency',
            'ascent_count',
            'comment_count',
            'thread_id',
            'rating',
            'description',
            'url'
        ]

    def get_grade_tendency(self, route):
        """Check ascent counts for tendency ratings."""
        tendency = {}
        for choice in Ascent.TENDENCY_CHOICES:
            tendency[choice[0]] = route.ascent_set.filter(tendency=choice[0]).count()
        return tendency

    def get_ascent_count(self, route):
        """Ascent count for route. Can be filtered for user."""
        request = self.context.get('request', None)
        if request:
            user = request.GET.get('user')
            if user is not None:
                try:
                    return route.ascent_set.filter(climber=user).count()
                except ValueError:
                    return 0
        else:
            return route.ascent_set.all().count()

    def get_comment_count(self, route):
        """Comment count for route."""
        return Comment.objects.filter(threadId=generate_thread_id(route)).count()

    def get_thread_id(self, route):
        """Comment thread for route."""
        return generate_thread_id(route)

    def get_geometries(self, route):
        geometries = route.routegeometry_set.all()
        #import ipdb;ipdb.set_trace()
        request = self.context.get('request', None)
        if request:
            wallview = request.GET.get('wallview')
            if wallview:
                geometries = geometries.filter(on_wallview__pk=wallview)

        serializer = RouteGeometrySerializer(geometries, many=True)
        return serializer.data

    def get_grade_system(self, route):
        from miroutes.model_choices import GRADE_SYSTEMS_DICT, equalize_grade, LINE_COLORS, reduce_grade, GRADE_FILTER_LABELS
        system = route.grading_system
        reduced_grade = reduce_grade(route.grade)
        equalized_grade = equalize_grade(route.grade)
        reduced_grade_label = GRADE_FILTER_LABELS[reduced_grade]
        return {
            'uid' : system,
            'name' : GRADE_SYSTEMS_DICT[system],
            'reduced_grade': reduced_grade,
            'reduced_grade_label': reduced_grade_label,
            'equalized_grade': equalized_grade,
            'linecolor': LINE_COLORS[equalized_grade],
            }

    def get_url(self, route):
        return reverse('miroutes:route_detail', kwargs={'route_id': route.pk})

class AscentSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format=None)
    route = RouteSerializer(read_only=True)
    climbingstyle_name = serializers.SerializerMethodField(read_only=True)
    note = serializers.SerializerMethodField(read_only=True)
    climber = UserSerializer(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    del_url = serializers.SerializerMethodField(read_only=True)
    is_climber = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Ascent
        fields = [
            'id',
            'date',
            'climber',
            'route',
            'get_style_display',
            'get_rating_display',
            'rating',
            'note',
            'climbingstyle_name',
            'url',
            'del_url',
            'is_climber'
        ]

    def get_is_climber(self, ascent):
        request = self.context.get('request', None)
        if request:
            return ascent.climber == request.user
        return False

    def get_climbingstyle_name(self, ascent):
        from miroutes.model_choices import CLIMBINGSTYLE_CHOICES_DICT
        return CLIMBINGSTYLE_CHOICES_DICT[ascent.route.climbingstyle]

    def get_note(self, ascent):
        if ascent.note is None:
            return ascent.note
        else:
            note = html.escape(ascent.note, quote=False)
            return replace_objs(note, html=True, absolute_urls=False)

    def get_url(self, ascent):
        return reverse('miroutes:ascent_details', kwargs={'ascent_id': ascent.pk})

    def get_del_url(self, ascent):
        return reverse('miroutes:del_ascent', kwargs={'ascent_id': ascent.pk})


class WallWorkSerializer(serializers.ModelSerializer):
    thumb = serializers.SerializerMethodField(read_only=True)
    last_edit = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    edit_url = serializers.SerializerMethodField(read_only=True)
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = Wall
        fields = [
            'id',
            'name',
            'url',
            'edit_url',
            'thumb',
            'created_by',
            'last_edit',
            'url',
            'gym']

    def get_last_edit(self, wall):
        request = self.context.get('request', None)
        if request:
            wall_version = Version.objects.get_for_object(wall).filter(revision__user=request.user)
            wallview_version = Version.objects.get_for_object(wall.pub_view).filter(revision__user=request.user)
            # merge querysets and find youngest entry. This should always yield one hit at least.
            return (wall_version | wallview_version).order_by('-revision__date_created').first().revision.date_created

    def get_thumb(self, wall):
        return wall.tiler.thumbnail_url_s

    def get_edit_url(self, wall):
        return reverse('edit_spot:edit_wall', kwargs={'wall_id': wall.pk})

    def get_url(self, wall):
        return reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})

class WallFavSerializer(WallWorkSerializer):
    """We use the WallWork API here and replace the last edit field to be not user specific."""
    def get_last_edit(self, wall):
        request = self.context.get('request', None)
        if request:
            wall_version = Version.objects.get_for_object(wall).all()
            wallview_version = Version.objects.get_for_object(wall.pub_view).all()
            # merge querysets and find youngest entry. This should always yield one hit at least.
            return (wall_version | wallview_version).order_by('-revision__date_created').first().revision.date_created

class WallSerializer(serializers.ModelSerializer):
    """Serializer for the Wall class."""
    routes = serializers.SerializerMethodField(read_only=True)
    full_route_set = serializers.SerializerMethodField(read_only=True)
    most_common_grade_system = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Wall
        fields = [
            'id',
            'name',
            'gym',
            'most_common_grade_system',
            'routes',
            'full_route_set',
            ]

    def get_routes(self, wall):
        routes = wall.pub_view.route_set.all()
        serializer = RouteSerializer(routes, many=True)
        return serializer.data

    def get_full_route_set(self, wall):
        routes = wall.get_full_route_set.all()
        serializer = RouteSerializer(routes, many=True)
        return serializer.data

    def get_most_common_grade_system(self, wall):
        from miroutes.models import get_most_common_grade_system as common_grade_system
        from miroutes.model_choices import GRADE_SYSTEMS_DICT, POSSIBLE_REDUC_GRADES_N_LABELS_BY_GRADE_SYSTEM

        routes = wall.pub_view.route_set.all()

        system_id = common_grade_system(routes)

        return {
                'id': system_id,
                'name': GRADE_SYSTEMS_DICT[system_id],
                'reduc_grades_n_labels': POSSIBLE_REDUC_GRADES_N_LABELS_BY_GRADE_SYSTEM[system_id],
                }


class GenericMixedSerializer(serializers.Serializer):
    """ Mixed Serializer to perform serialization of multiple models at once,
        this is just a dummy layer in the serialization process """
    item_type = serializers.JSONField()
    data = serializers.JSONField()


class TimelineItemSerializer(serializers.ModelSerializer):
    @classmethod
    def get_serializer(cls, model):
        from reversion.models import Version, Revision
        from miroutes.models import Ascent
        from users.serializers import RevisionVersionSerializer, RevisionSerializer_with_User, UserpicSerializer

        if model == Ascent:
            return AscentSerializer
        elif model == Revision:
            return RevisionSerializer_with_User
        elif model == Userpic:
            return UserpicSerializer

    def to_representation(self, instance):
        serializer_cls = self.get_serializer(instance.__class__)
        serializer = serializer_cls(instance, context=self.context)
        serializer.fields['item_type'] =  serializers.ReadOnlyField(default=instance.__class__.__name__.lower())

        return serializer.data


class NewsWallSerializer(serializers.HyperlinkedModelSerializer):
    created_by = serializers.ReadOnlyField(source='last_revision.user.username')
    created_at = serializers.ReadOnlyField(source='last_revision.date_created')
    created_by_thumb = serializers.ReadOnlyField(source='last_revision.user.userprofile.thumb_url')
    created_by_id = serializers.ReadOnlyField(source='last_revision.user.pk')
    collaborators = serializers.SerializerMethodField()
    comment = serializers.ReadOnlyField(source='last_revision.comment')
    nr_routes = serializers.ReadOnlyField(source='pub_view.route_set.count')

    #image = serializers.ReadOnlyField(source='snapshot.img.url')
    image = serializers.SerializerMethodField()

    wall_url = serializers.SerializerMethodField()

    created_by_url = serializers.SerializerMethodField()

    def get_image(self, obj):
        try:
            return obj.snapshot.img.url
        except (AttributeError,ValueError):
            return static('topo/img/delayed_img.png')

    def get_wall_url(self, obj):
        return reverse('miroutes:wall_detail', kwargs={"wall_id": obj.id})

    def get_created_by_url(self, obj):
        return reverse('users:user_detail', kwargs={"user_id": obj.last_revision.user.pk})

    def get_collaborators(self, obj):
        users = obj.collaborators.all()
        serializer = UserSerializer(users, many=True)
        return serializer.data

    class Meta:
        model = Wall
        fields = (
            'id', 'name', 'description',
            'created_at', 'created_by', 'created_by_id', 'created_by_thumb', 'collaborators',
            'comment', 'image', 'wall_url', 'created_by_url', 'nr_routes')
