from django.contrib.sitemaps import GenericSitemap
from miroutes.models import Wall, Route
from django.urls import path
from django.contrib import sitemaps
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'monthly'
    protocol = 'https'
    i18n = True

    def items(self):
        return ['miroutes:about', ]

    def location(self, item):
        return reverse(item)

class NewsViewSitemap(sitemaps.Sitemap):
    priority = 0.6
    changefreq = 'daily'
    protocol = 'https'
    i18n = True

    def items(self):
        return ['miroutes:news', ]

    def location(self, item):
        return reverse(item)

class WallSitemap(sitemaps.Sitemap):
    priority = 0.7
    changefreq = 'weekly'
    protocol = 'https'
    i18n = True

    def items(self):
        walls = Wall.active_objects.filter(deprecated=False).select_related('snapshot')
        for w in walls:
            w.sitemap_images = [{
                'url': w.snapshot.img.url,
                'title': _("Climbing Topo of {name}").format(name=w.name),
                'caption': w.description,
                'geo_location': w.pnt.kml,
                },]

            w.news = {
                'title': _("A new Climbing Topo for {name}").format(name=w.name),
                'publication_date': w.last_edited_date,
                'tags': w.description,
                }
        return walls

class RouteSitemap(sitemaps.Sitemap):
    priority = 0.6
    changefreq = 'weekly'
    protocol = 'https'
    i18n = True

    def items(self):
        routes = Route.active_objects.all().prefetch_related('routegeometry_set')
        for r in routes:
            routegeoms = r.routegeometry_set.filter(on_wallview__is_dev=False).filter(snapshot__isnull=False)
            if routegeoms.first():
                r.sitemap_images = []
                for rg in routegeoms:
                    caption = _("Topo of Climbing Route `{name}` on Wall `{wall}`").format(
                            name=r.name, wall=rg.on_wallview.wall.name)
                    r.sitemap_images.append({
                        'url':rg.snapshot.img.url,
                        'title': _("Topo of Climbing Route `{name}` on Wall `{wall}`").format(
                            name=r.name, wall=rg.on_wallview.wall.name),
                        'caption': r.description,
                        'geo_location': r.pnt.kml,
                        })

            r.news = {
                'title': _("New Climbing Topo for Route `{name}`").format(name=r.name),
                'publication_date': r.last_edit_date,
                'tags': r.description,
                }
        return routes

sitemaps = {
        'static': StaticViewSitemap,
        'news': NewsViewSitemap,
        'walls': WallSitemap,
        'routes': RouteSitemap,
        }
