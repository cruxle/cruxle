# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-26 10:30


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0014_remove_routegeometry_img'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='routegeometry',
            unique_together=set([('on_wallview', 'route')]),
        ),
    ]
