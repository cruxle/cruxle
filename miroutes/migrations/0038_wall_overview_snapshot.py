# Generated by Django 2.0 on 2017-12-12 16:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('miexport', '0003_auto_20171024_0431'),
        ('miroutes', '0037_auto_20171211_2134'),
    ]

    operations = [
        migrations.AddField(
            model_name='wall',
            name='overview_snapshot',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='miexport.PhantomSnap'),
        ),
    ]
