# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-29 08:01


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0021_auto_20170911_2041'),
    ]

    operations = [
        migrations.AddField(
            model_name='ascent',
            name='note',
            field=models.TextField(blank=True, null=True),
        ),
    ]
