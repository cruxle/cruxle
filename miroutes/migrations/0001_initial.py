# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-25 08:49


import datetime
from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import miroutes.models
import miroutes.wall_img_functions


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ascent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today)),
                ('style', models.PositiveSmallIntegerField(blank=True, choices=[(1, b'on-sight'), (2, b'flash'), (3, b'red point'), (5, b'top rope')], null=True)),
                ('rating', models.PositiveSmallIntegerField(blank=True, choices=[(1, b'poor'), (2, b'soso'), (3, b'ok'), (4, b'good'), (5, b'excellent')], null=True)),
                ('climber', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Edit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('since', models.DateTimeField(blank=True, null=True)),
                ('by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pnt', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Location')),
                ('description', models.CharField(blank=True, max_length=200, null=True)),
                ('img', models.ImageField(max_length=256, upload_to=miroutes.wall_img_functions.get_materials_upload_path)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pnt', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Location')),
                ('climbingstyle', models.PositiveSmallIntegerField(choices=[(0, b'Sportclimb'), (1, b'Boulder'), (2, b'Alpineclimb')], default=0)),
                ('name', models.CharField(max_length=100)),
                ('grade', models.PositiveSmallIntegerField(choices=[(0, b'-'), (1, b'1a '), (3, b'1a+'), (5, b'1b '), (7, b'1b+'), (9, b'1c '), (10, b'1c+'), (11, b'2a '), (13, b'2a+'), (15, b'2b '), (17, b'2b+'), (19, b'2c '), (20, b'2c+'), (110, b'3a '), (120, b'3a+'), (130, b'3b '), (140, b'3b+'), (210, b'3c '), (220, b'3c+'), (230, b'4a '), (240, b'4a+'), (310, b'4b '), (320, b'4b+'), (330, b'4c '), (340, b'4c+'), (410, b'5a '), (420, b'5a+'), (430, b'5b '), (440, b'5b+'), (450, b'5c '), (460, b'5c+'), (480, b'6a '), (520, b'6a+'), (530, b'6b '), (540, b'6b+'), (610, b'6c '), (620, b'6c+'), (630, b'7a '), (640, b'7a+'), (710, b'7b '), (720, b'7b+'), (730, b'7c '), (740, b'7c+'), (810, b'8a '), (820, b'8a+'), (830, b'8b '), (840, b'8b+'), (910, b'8c '), (915, b'8c+'), (920, b'9a '), (925, b'9a+'), (950, b'9b '), (955, b'9b+'), (960, b'9c '), (965, b'9c+'), (1030, b'2-'), (1060, b'2 '), (1090, b'2+'), (1130, b'3-'), (1160, b'3 '), (1190, b'3+'), (1230, b'4-'), (1260, b'4 '), (1290, b'4+'), (1330, b'5-'), (1360, b'5 '), (1390, b'5+'), (1430, b'6-'), (1460, b'6 '), (1490, b'6+'), (1530, b'7-'), (1560, b'7 '), (1590, b'7+'), (1630, b'8-'), (1660, b'8 '), (1690, b'8+'), (1730, b'9-'), (1760, b'9 '), (1790, b'9+'), (1830, b'10-'), (1860, b'10 '), (1890, b'10+'), (1910, b'11-'), (1930, b'11 '), (1950, b'11+'), (1970, b'12-'), (1980, b'12 '), (1990, b'12+'), (2010, b'    1a'), (2020, b'    1b'), (2030, b'    1c'), (2040, b'    2a'), (2050, b'    2b'), (2060, b'    2c'), (2110, b'  M 3a'), (2115, b'  D 3a'), (2120, b'  M 3b'), (2125, b'  D 3b'), (2210, b'  D 3c'), (2215, b' VD 3c'), (2220, b'  S 4a'), (2225, b' HS 4a'), (2310, b'  S 4b'), (2315, b' HS 4b'), (2320, b' VS 4c'), (2325, b'HVS 4c'), (2410, b' VS 5a'), (2415, b'HVS 5a'), (2420, b' E1 5a'), (2510, b'HVS 5b'), (2515, b' E1 5b'), (2520, b' E2 5b'), (2530, b' E1 5c'), (2535, b' E2 5c'), (2540, b' E3 5c'), (2610, b' E2 6a'), (2615, b' E3 6a'), (2620, b' E4 6a'), (2625, b' E5 6a'), (2710, b' E4 6b'), (2715, b' E5 6b'), (2720, b' E6 6b'), (2725, b' E7 6b'), (2730, b' E8 6b'), (2740, b' E5 6c'), (2745, b' E6 6c'), (2750, b' E7 6c'), (2755, b' E8 6c'), (2760, b' E9 6c'), (2810, b' E6 7a'), (2815, b' E7 7a'), (2820, b' E8 7a'), (2825, b' E9 7a'), (2910, b' E7 7b'), (2915, b' E8 7b'), (2920, b' E9 7b'), (2950, b' E7 7c'), (2955, b' E8 7c'), (2960, b' E9 7c'), (3010, b'5.0'), (3020, b'5.1'), (3030, b'5.2'), (3110, b'5.3'), (3120, b'5.4'), (3210, b'5.5'), (3220, b'5.6'), (3230, b'5.7'), (3240, b'5.8'), (3410, b'5.9'), (3420, b'5.10a'), (3510, b'5.10b'), (3520, b'5.10c'), (3530, b'5.10d'), (3610, b'5.11a'), (3620, b'5.11b'), (3630, b'5.11c'), (3640, b'5.11d'), (3710, b'5.12a'), (3720, b'5.12b'), (3730, b'5.12c'), (3740, b'5.12d'), (3810, b'5.13a'), (3820, b'5.13b'), (3830, b'5.13c'), (3840, b'5.13d'), (3910, b'5.14a'), (3915, b'5.14b'), (3920, b'5.14c'), (3925, b'5.14d'), (3950, b'5.15a'), (3955, b'5.15b'), (3965, b'5.15c'), (3960, b'5.15d'), (4010, b'1A'), (4015, b'1A+'), (4020, b'1B'), (4025, b'1B+'), (4030, b'1C'), (4035, b'1C+'), (4110, b'2A'), (4115, b'2A+'), (4120, b'2B'), (4125, b'2B+'), (4130, b'2C'), (4135, b'2C+'), (4210, b'3A'), (4215, b'3A+'), (4220, b'3B'), (4225, b'3B+'), (4230, b'3C'), (4310, b'3C+'), (4320, b'4A'), (4325, b'4A+'), (4410, b'4B'), (4415, b'4B+'), (4510, b'4C'), (4515, b'4C+'), (4610, b'5A'), (4615, b'5A+'), (4620, b'5B'), (4625, b'5B+'), (4630, b'5C'), (4635, b'5C+'), (4710, b'6A'), (4715, b'6A+'), (4720, b'6B'), (4725, b'6B+'), (4730, b'6C'), (4810, b'6C+'), (4820, b'7A'), (4825, b'7A+'), (4830, b'7B'), (4835, b'7B+'), (4840, b'7C'), (4845, b'7C+'), (4850, b'8A'), (4910, b'8A+'), (4920, b'8B'), (4925, b'8B+'), (4930, b'8C'), (4935, b'8C+'), (5010, b'WI1-'), (5020, b'WI1'), (5030, b'WI1+'), (5110, b'WI2-'), (5120, b'WI2'), (5210, b'WI2+'), (5220, b'WI3-'), (5310, b'WI3'), (5320, b'WI3+'), (5410, b'WI4-'), (5420, b'WI4'), (5430, b'WI4+'), (5510, b'WI5-'), (5520, b'WI5'), (5610, b'WI5+'), (5620, b'WI6-'), (5710, b'WI6'), (5720, b'WI6+'), (5810, b'WI7-'), (5820, b'WI7'), (5830, b'WI7+'), (5910, b'WI8-'), (5920, b'WI8'), (5930, b'WI8+')], default=0)),
                ('length', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('number_of_bolts', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('developer', models.CharField(blank=True, max_length=100)),
                ('developed_year', models.PositiveSmallIntegerField(blank=True, choices=[(1922, 1922), (1923, 1923), (1924, 1924), (1925, 1925), (1926, 1926), (1927, 1927), (1928, 1928), (1929, 1929), (1930, 1930), (1931, 1931), (1932, 1932), (1933, 1933), (1934, 1934), (1935, 1935), (1936, 1936), (1937, 1937), (1938, 1938), (1939, 1939), (1940, 1940), (1941, 1941), (1942, 1942), (1943, 1943), (1944, 1944), (1945, 1945), (1946, 1946), (1947, 1947), (1948, 1948), (1949, 1949), (1950, 1950), (1951, 1951), (1952, 1952), (1953, 1953), (1954, 1954), (1955, 1955), (1956, 1956), (1957, 1957), (1958, 1958), (1959, 1959), (1960, 1960), (1961, 1961), (1962, 1962), (1963, 1963), (1964, 1964), (1965, 1965), (1966, 1966), (1967, 1967), (1968, 1968), (1969, 1969), (1970, 1970), (1971, 1971), (1972, 1972), (1973, 1973), (1974, 1974), (1975, 1975), (1976, 1976), (1977, 1977), (1978, 1978), (1979, 1979), (1980, 1980), (1981, 1981), (1982, 1982), (1983, 1983), (1984, 1984), (1985, 1985), (1986, 1986), (1987, 1987), (1988, 1988), (1989, 1989), (1990, 1990), (1991, 1991), (1992, 1992), (1993, 1993), (1994, 1994), (1995, 1995), (1996, 1996), (1997, 1997), (1998, 1998), (1999, 1999), (2000, 2000), (2001, 2001), (2002, 2002), (2003, 2003), (2004, 2004), (2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017)], null=True)),
                ('editor_notes', models.TextField(blank=True, null=True)),
                ('climbers', models.ManyToManyField(through='miroutes.Ascent', to=settings.AUTH_USER_MODEL)),
                ('is_edited', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='miroutes.Edit')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RouteGeometry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('linestring', miroutes.models.PolylineField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tiler',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, b'not yet submitted'), (1, b'queued'), (2, b'preprocessing image'), (3, b'tiling'), (4, b'finished'), (10, b'error')], default=0)),
                ('percentage', models.PositiveSmallIntegerField(default=0)),
                ('img', models.ImageField(blank=True, max_length=256, upload_to=miroutes.wall_img_functions.get_tiler_img_path)),
                ('orig_img', models.ImageField(blank=True, max_length=256, null=True, upload_to=miroutes.wall_img_functions.get_orig_img_upload_path)),
            ],
        ),
        migrations.CreateModel(
            name='Wall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pnt', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Location')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('is_active', models.BooleanField(default=False)),
                ('is_subwall', models.BooleanField(default=False)),
                ('description', models.TextField(blank=True, null=True)),
                ('approach', models.TextField(blank=True, null=True)),
                ('approach_time', models.DurationField(blank=True, choices=[(datetime.timedelta(0, 900), b'15 minutes or less'), (datetime.timedelta(0, 1800), b'30 minutes'), (datetime.timedelta(0, 3600), b'1 hour'), (datetime.timedelta(0, 7200), b'2 hours'), (datetime.timedelta(0, 10800), b'more than 3 hours')], default=datetime.timedelta(0, 900), null=True)),
                ('morning_sun', models.NullBooleanField()),
                ('midday_sun', models.NullBooleanField()),
                ('afternoon_sun', models.NullBooleanField()),
                ('children_friendly', models.NullBooleanField()),
                ('bolt_quality', models.PositiveSmallIntegerField(blank=True, choices=[(1, b'poor'), (2, b'soso'), (3, b'ok'), (4, b'good'), (5, b'excellent')], null=True)),
                ('wall_height', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('dry_duration', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=b'Dries after')),
                ('orig_img', models.ImageField(blank=True, max_length=256, null=True, upload_to=miroutes.wall_img_functions.get_orig_img_upload_path)),
                ('is_edited', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='miroutes.Edit')),
                ('tiler', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='miroutes.Tiler')),
            ],
            options={
                'permissions': (('miroutes.publish_wall', 'Can publish a wall from staging to official'),),
            },
        ),
        migrations.CreateModel(
            name='WallView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_dev', models.BooleanField(default=False)),
                ('wall', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='miroutes.Wall')),
            ],
        ),
        migrations.AddField(
            model_name='routegeometry',
            name='on_wallview',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='miroutes.WallView'),
        ),
        migrations.AddField(
            model_name='routegeometry',
            name='route',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='miroutes.Route'),
        ),
        migrations.AddField(
            model_name='route',
            name='walls',
            field=models.ManyToManyField(through='miroutes.RouteGeometry', to='miroutes.WallView'),
        ),
        migrations.AddField(
            model_name='material',
            name='linked_to',
            field=models.ManyToManyField(to='miroutes.Wall'),
        ),
        migrations.AddField(
            model_name='ascent',
            name='route',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='miroutes.Route'),
        ),
    ]
