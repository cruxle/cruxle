# Generated by Django 2.0.1 on 2018-05-06 08:51

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0052_auto_20180505_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='ascent',
            name='date',
            field=models.DateTimeField(default=datetime.date.today),
        ),
    ]
