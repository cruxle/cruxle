# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-11 20:41


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0020_auto_20170901_0731'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='route',
            managers=[
            ],
        ),
        migrations.AlterField(
            model_name='route',
            name='climbingstyle',
            field=models.PositiveSmallIntegerField(choices=[(0, b'Sportclimb'), (1, b'Boulder'), (2, b'Alpineclimb')], verbose_name=b'Style'),
        ),
    ]
