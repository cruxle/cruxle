import os

from crispy_forms.bootstrap import (FormActions)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout, HTML)
from django import forms
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django_file_form.forms import FileFormMixin, MultipleUploadedFileField

from miroutes.models import Ascent, Userpic, License
from miroutes.widgets import LicenseChooser

class AscentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        show_buttons = kwargs.pop('show_buttons', True)
        super(AscentForm, self).__init__(*args, **kwargs)
        # Using an empty label has the advantage that the widget is horizontally adjusted
        self.fields['rating'].label = " "

        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2 col-md-3 col-xs-12'
        self.helper.field_class = 'col-lg-7 col-md-8 col-xs-11'
        self.helper.layout = Layout(
            'style',
            'note',
            'tendency',
            'rating',
            'date'
        )
        self.helper.form_id = 'ascent_form'
        self.helper.form_method = 'post'

        rating_html = HTML("""<script>$(document).ready(function () {$("#id_rating").rating({min:0, max:5, step:1, size: 'sm'});})</script>""")
        self.helper.layout.append(rating_html)

        info_tooltips = HTML("""{% load i18n %}
                <script>
                $(document).ready(function () {
                    var stylesTable = $('<ul>')
                    .append($('<li>', { html:'<b>On-sight:</b> {% trans 'A clean ascent, <i class="text-info">without</i> prior practice or beta' %}', }))
                    .append($('<li>', { html:'<b>Flash:</b>    {% trans 'A clean ascent, with prior practice' %}', }))
                    .append($('<li>', { html:'<b>Redpoint:</b> {% trans 'Leading a sport route after inspecting it, and maybe after practising individual moves.' %}', }))
                    .append($('<li>', { html:'<b>Dogging:</b>  {% trans 'Trying and failing to cleanly ascent a route - falling or resting on the rope one or more times.' %}', }))
                    .append($('<li>', { html:'<b>TopRope:</b>  {% trans 'To climb with a rope above you.' %}', }));

                    var styleInfoBroebbler = $("<a>", {
                        href: '#',
                        tabindex: "0",
                        role: "button",
                        "data-toggle": "popover",
                        "title": "Climbing Styles",
                        "data-content": stylesTable.html(),
                        "data-html": "true",
                        "data-placement": 'auto right',
                        "data-trigger": 'focus',
                        "html": "<i class='fa fa-info-circle text-info fa-2x'></i>",
                    });

                    var noteInfoBroebbler = $("<a>", {
                        href: '#',
                        "tabindex": "0",
                        "role": "button",
                        "data-toggle": "popover",
                        "title": "Your Personal Note",
                        "data-content": 'In contrast to a regular comment, you rather have the chance to take a personal note of your day. E.g. who you did the climb with and how tired you were after that crux moves <i class="far fa-smile"></i>',
                        "data-html": "true",
                        "data-placement": 'auto right',
                        "data-trigger": 'focus',
                        "html": "<i class='fa fa-info-circle text-info fa-2x'></i>",
                    });

                    $("#div_id_style").append(styleInfoBroebbler);
                    $("#div_id_note").append(noteInfoBroebbler);

                    $('[data-toggle="popover"]').popover({html : true, container: 'body'});
                });
                </script>""")
        self.helper.layout.append(info_tooltips);

        if show_buttons:
            save_btn = HTML(
                """{% if request.is_ajax is False %}{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
            </button>
            {% endif %}
            """)
            cancel_btn = HTML(
                """{% if request.is_ajax is False %}{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>
            {% endif %}""")


            self.helper.layout.append(
                FormActions(save_btn, cancel_btn))

    class Meta:
        model = Ascent
        fields = ('style', 'rating', 'date', 'note', 'tendency')
        widgets = {
            'date': forms.widgets.TextInput(attrs={'type': 'date'}),
            'note': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '3'}),
        }

def get_licenses():
    return list(License.objects.all().order_by("-required", "short_name"))

class UserpicForm(FileFormMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserpicForm, self).__init__(*args, **kwargs)
        self.fields['img'] = MultipleUploadedFileField(label=_('Images'))
        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-1 col-md-2 col-xs-12'
        self.helper.field_class = 'col-lg-7 col-md-8 col-xs-12'
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields

        self.helper.form_id = 'userpic-form'
        self.helper.form_method = 'post'

        button_fix = HTML("""{% load i18n %}
                <script>
                $(document).ready(function () {
	            $("div.qq-upload-button").children().first().text("{% trans "Upload Pic" %}");
                });
                </script>""")
        self.helper.layout.append(button_fix)


        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
            </button>
            {% endif %}
            """)
        cancel_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>
            {% endif %}""")

        self.helper.layout.append(
            FormActions(save_btn, cancel_btn))

    def clean_img(self):
        from django.db.models.fields.files import ImageFieldFile
        pics = self.cleaned_data['img']
        if type(pics) == ImageFieldFile:  # if it is a single FileField already, I guess, this is an edit and doesnt need to be slugified
            return pics

        for pic in pics:
            fname, ext = os.path.splitext(pic.name)
            pic.name = slugify(fname) + ext
        return pics

    class Meta:
        model = Userpic
        fields = (
            'description',
            'img',
            'external_url',
            'licenses'
        )

        widgets = {
            'description': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '3'}),
            'licenses': LicenseChooser(licenses_callback=get_licenses)
        }
