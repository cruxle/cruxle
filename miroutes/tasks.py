import shutil

from celery import shared_task, group, chord, chain
import os
from datetime import datetime
import logging

from django.http import HttpRequest
from django.conf import settings
from django.contrib.gis.measure import D
from django.urls import reverse
from django.forms.models import model_to_dict
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404, render
from django.core.files.storage import default_storage as storage
from contextlib import closing
from PIL import Image, ExifTags
import numpy as np

from edit_spot.utils import wallview_revision_diff
from postbox.utils import message_from_cruxle

#from miroutes.models import Wall, Route
from reversion.models import Version

logger = logging.getLogger(__name__)

DEFAULT_TILE_EXTENSION='.png'
ALLOWED_TILE_EXTENSIONS=[]


@shared_task(time_limit=60*15, soft_time_limit=60*14)
def update_after_Route_save(route_id, **kwargs):

    from miroutes.models import Route
    from miroutes.classviews import invalidate_RestWall_cache

    route = Route.objects.get(id=route_id)

    # Recreate Route Snapshots and Topos where this route is part of.
    routegeom_jobs = [rg.snapshot.get_snap_signature()
                      for rg in route.routegeometry_set\
                      .filter(on_wallview__is_dev=False)\
                      .filter(geojson__isnull=False)]

    # Recreate Wall Overview Images where the route is visible
    walls = [ wallview.wall for wallview in route.walls.filter(is_dev=False) ]

    # Reset cached RestEntries
    wall_topo_jobs = []
    for wall in walls:
        invalidate_RestWall_cache(wall.id)
        routegeom_jobs.append(wall.snapshot.get_snap_signature())
        # we throw the outdated PDF away. Until the new one is ready, there is no pdf!
        if wall.pdf_topo.pdf:
            # note that we dont save the delete because deleting a file field calls save by itself
            # which would trigger a post_save and would immediately create a new pdf.
            # Instead we will save the object later with the new filename and start an async job to generate the pdf
            wall.pdf_topo.pdf.delete(save=False)
            wall.pdf_topo.img.delete(save=False)
        wall.pdf_topo.set_opt_file_name(wall.pdf_topo_prefix(datetime.now().strftime('%Y-%m-%d_%H_%M_%S')))
        wall.pdf_topo.save(save_img=False)
        wall_topo_jobs.append(wall.pdf_topo.get_snap_signature(signature_kwargs={'immutable':True}))

    routegeom_jobs = group(routegeom_jobs) # pack them jobs into groups which run in parallel
    wall_topo_jobs = group(wall_topo_jobs)

    return chain(routegeom_jobs, wall_topo_jobs).apply_async()


@shared_task(time_limit=30, soft_time_limit=10)
def set_Wall_active_after_publish(self, wall_id, **kwargs):
# FJ: to be honest, I have no idea why the celery worker add a self_reference here as an argument.
#     Even with bind=False, I could not get rid of this... anyway, for now, I just add the self argument here and wait until this bites me
    from miroutes.models import Wall
    try:
        wall = Wall.objects.get(id=wall_id)
        wall.is_active = True
        wall.save(update_fields=['is_active'])
    except Wall.DoesNotExist as e:
        return


@shared_task(time_limit=60*15, soft_time_limit=60*14)
def update_Wall_after_publish(wall_id, snapshots=True, pdf=True, activate=False, surrounding_topos=True, notify=True, **kwargs):
    from miroutes.models import Wall
    from miroutes.classviews import invalidate_RestWall_cache

    try:
        wall = Wall.objects.get(id=wall_id)
    except Wall.DoesNotExist:
        return

    # Reset cached RestEntries
    invalidate_RestWall_cache(wall.id)

    async_queue = []

    # generate_route_snapshots
    if snapshots:
        jobs = [ rg.snapshot.get_snap_signature() for rg in wall.pub_view.routegeometry_set.filter(geojson__isnull=False).distinct() ]
        jobs.append(wall.snapshot.get_snap_signature())
        async_queue.append(group(jobs))

    if pdf:
        if wall.pdf_topo:
            # note that we dont save the delete because deleting a file field calls save by itself
            # which would trigger a post_save and would immediately create a new pdf.
            # Instead we will save the object later with the new filename and start an async job to generate the pdf
            wall.pdf_topo.pdf.delete(save=False)
            wall.pdf_topo.img.delete(save=False)
        wall.pdf_topo.set_opt_file_name(wall.pdf_topo_prefix(datetime.now().strftime('%Y-%m-%d_%H_%M_%S')))
        wall.pdf_topo.save(save_img=False)
        topo_job = wall.pdf_topo.get_snap_signature(signature_kwargs={'immutable':True})
        async_queue.append(topo_job)


    if activate:
        activate_job = set_Wall_active_after_publish.signature(args=(wall_id,),)
        async_queue.append(activate_job)


    # Find out if there are walls in the vicinity that need updated topos also.
    # This is necessary because the leaflet overview maps may have changed as a result of this publish
    # Then create immutable celery signature which we will do after the routegeom jobs
    if surrounding_topos:
        surrouding_walls = Wall.active_objects.filter(pnt__distance_lte=(wall.pnt, D(m=2000))).exclude(pk=wall.pk)
        surrounding_wall_jobs = group([wall.pdf_topo.get_snap_signature(signature_kwargs={'immutable':True}) for wall in surrouding_walls])
        async_queue.append(surrounding_wall_jobs)

    # msg user about finished publishing
    if notify:
        notify_job = notify_users_about_publishing.signature(args=(wall_id,),)
        async_queue.append(notify_job)

    return chain(*async_queue).apply_async()


@shared_task(time_limit=60, soft_time_limit=30)
def notify_users_about_publishing(self, wall_id, **kwargs):
    from miroutes.models import Wall
    from edit_spot.views import compare_model_fields
    try:
        wall = Wall.objects.get(id=wall_id)
    except Wall.DoesNotExist:
        return

    all_users = wall.collaborators
    publisher = all_users.first()
    collaborators = all_users.exclude(pk=publisher.pk)

    request = HttpRequest()
    request.META['SERVER_NAME'] = settings.SITE_DOMAIN
    request.META['SERVER_PORT'] = 80
    # TODO: this would need to be the language a user prefers, however, we currently dont save that, we only have that per user session. If we want translated emails, we need to save that in the user profile
    lang = 'en'
    translation.activate(lang)
    request.LANGUAGE_CODE = lang
    c = render(request, "email/publish_notification_publisher.html", {
        'user': publisher,
        'wall': wall,
        'html': True,
        })
    translation.deactivate()

    message_from_cruxle(publisher, c.content.decode(), html=True)


    wall_versions = Version.objects.get_for_object(wall)
    current_version = wall_versions.first()
    target_version = wall_versions[1] if wall_versions.count() > 1 else current_version

    current_revision = current_version.revision
    target_revision  = target_version.revision

    this_wall_data = model_to_dict(wall)
    changed_fields = compare_model_fields(this_wall_data, target_version.field_dict)

    changeset = []
    for model, changes in list(wallview_revision_diff(target_revision, current_revision).items()):
        if changes:
            changeset += changes

    for user in collaborators:
        if user.userprofile.mail_on_collaborate:
            request = HttpRequest()
            request.META['SERVER_NAME'] = settings.SITE_DOMAIN
            request.META['SERVER_PORT'] = 80
            # TODO: this would need to be the language a user prefers, however, we currently dont save that, we only have that per user session. If we want translated emails, we need to save that in the user profile
            lang = 'en'
            translation.activate(lang)
            request.LANGUAGE_CODE = lang
            c = render(request, "email/publish_notification_collaborators.html", {
                'user': user,
                'wall': wall,
                'changed_fields': changed_fields,
                'changeset': changeset,
                'html': True,
                })
            translation.deactivate()

            message_from_cruxle(user, c.content.decode(), html=True)

    log = f"Sent publish message to {publisher} and notified {collaborators}"
    logger.info(log)


@shared_task(ignore_result=True, time_limit=60*15, soft_time_limit=60*14)
def create_thumb_image(src, dst, size=(128,128), keep_aspect=False):
    print(("Creating thumb for:",src,' => ', dst))
    if storage.exists(dst):
        print("Thumb seems to already exist... will assume I should not do anything and return")
        return

    try:
        with closing(storage.open(src, 'rb')) as fh:
            with closing(Image.open(fh, 'r')) as tmp:
                image = tmp.copy()

        if keep_aspect:
            aspect = min([ float(thumbsize)/float(imsize) for thumbsize,imsize in zip(size, image.size)])
            size = [ int(aspect*imsize) for imsize in image.size ]

        image.thumbnail(size, Image.ANTIALIAS)
        background = Image.new('RGBA', size)
        background.paste(image, ((size[0] - image.size[0]) // 2, (size[1] - image.size[1]) // 2))

        with closing(storage.open(dst, 'wb')) as fh:
            background.save(fh)
    except Exception as e:
        print(("Error occured creating thumb for:",src,' => ', dst, '::', e))
        pass


@shared_task(ignore_result=True)
def pngquant_image(src, dst=None, force=False, compression_lvl=3, verbose=False, skip_if_larger=True, strip=False, min_quality=None, max_quality=None):
    """
    Compress png image
    If dst is not supplied, new image will be saved as '<src>_quant.png'
      - compression_lvl: (1,slow)..(11,fast)
      - skip_if_larger: dont save image if result is bigger than original
      - strip: remove metadata
      - min_quality / max_quality:
            min and max are numbers in range 0 (worst) to 100 (perfect), similar to JPEG.  pngquant will use the least amount of colors required to meet or exceed the max
            quality. If conversion results in quality below the min quality the image won't be saved (or if outputting to stdin, 24-bit original will be output) and
            pngquant will exit with status code 99
    """
    from subprocess import call

    if settings.DEFAULT_FILE_STORAGE != 'django.core.files.storage.FileSystemStorage':
        return  # this only works for local files. if storage is somewhere else, we have to think of something different.... maybe copy it to and fro or so...

    def which(program):
        """
        Find a binary in the system path, just like a bash which...
        """
        def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

        if is_exe(program):
            return program

        fpath, fname = os.path.split(program)
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

        return None

    # fpath is the full storage to the file from / on
    fpath = storage.path(src)

    base,ext = os.path.splitext(fpath)
    if not ext.upper().endswith('PNG'):
        # do nothing if its not a png
        return

    base,ext = os.path.splitext(fpath)
    quant_tmp = '{}_quant_tmp{}'.format(base,ext)
    if dst is None:
        dst = '{}_quant.png'.format(base)

    if verbose:
        print(("quantizing image from {} => {} => {}".format(fpath, quant_tmp, dst)))

    size_before = os.stat(fpath).st_size
    pngquant_binary = which('pngquant')

    call_options = [pngquant_binary,]
    call_options.append('-s {}'.format(compression_lvl)) # compression lvl
    if skip_if_larger:
        call_options.append('--skip-if-larger')  # if filesize is larger than original, dont save it
    if strip:
        call_options.append('--strip')           # remove additional meta info
    call_options.append('--output={}'.format(dst))
    if force:
        call_options.append('--force')       # overwrite existing files
    if (min_quality is not None) and (max_quality is not None):
        call_options.append('-Q {}-{}'.format(min_quality,max_quality))
    if verbose:
        call_options.append('-v')

    call_options.append(fpath)
    if verbose:
        print(("Call options: {}".format(call_options)))

    rc = call(call_options)
    if rc == 0:
        size_after = os.stat(dst).st_size
    else:
        logger.warning("Couldn't quant image, returncode=%d, %s copying as is to: %s", rc, fpath, dst)
        size_after = size_before
        shutil.copy(fpath, dst)

    compression_ratio = float(size_after) / float(size_before) * 100

    if verbose:
        print(("Called pnquant for {} => {} :: returned {} :: compression {}".format(fpath, dst, rc, compression_ratio)))

    # FJ, I tried this package but the wrapper is crap, lets just call it with subprocess
    # import pngquant
    # pngquant.config(quant_file=pngquant_binary, tmp_file=quant_tmp, min_quality=min_quality, max_quality=max_quality)
    # pngquant.quant_image(src, dst)

    return rc, dst, compression_ratio


def get_tile_image_file_extension(src):
    filename, file_extension = os.path.splitext(src)
    if file_extension in ALLOWED_TILE_EXTENSIONS:
        return file_extension
    else:
        return DEFAULT_TILE_EXTENSION

def get_extended_dimensions(image_fname):
    MINSIZE = 512

    if not storage.exists(image_fname):
        raise IOError('Image file does not exist: {}'.format(storage.path(image_fname)))

    with closing(storage.open(image_fname, 'rb')) as fh:
        with closing(Image.open(fh)) as image:
            width, height = image.size

    if width >= height:
        # Compute new dimensions being a power of 2 in width while maintaining aspect ratio
        newwidth = np.int(2**np.ceil(np.log2(width)))
        newwidth = np.maximum(MINSIZE, newwidth)
        wpercent = (newwidth/float(width))
        newheight = np.int(height*wpercent)
    else:
        # Compute new dimensions being a power of 2 in height while maintaining aspect ratio
        newheight = np.int(2**np.ceil(np.log2(height)))
        newheight = np.maximum(MINSIZE, newheight)
        hpercent = (newheight/float(height))
        newwidth = np.int(width*hpercent)

    return width, height, newwidth, newheight

@shared_task(ignore_result=True, time_limit=3600*24, soft_time_limit=3600*2)
def tile_image(src, extended_fname, dst, tile_width=None, tiler_id=None, force_tiles_generation=False):
    """
    Tile an image for use in leafletjs
    Files will be save at dst/zoomlvl/x_direction/y_direction.png
    """
    if tile_width is None:
        tile_width = settings.TILE_WIDTH

    try:
        import shutil
        import gc
        from miroutes.models import TilerStatus

        width, height, newwidth, newheight = get_extended_dimensions(src)

        if not storage.exists(extended_fname):
            tiler_update_status(TilerStatus.PREPROCESSING, tiler_id)
            tiler_update_percentage(0, 100, tiler_id)

            if newwidth < settings.RESIZE_UPPER_LIMIT and newheight < settings.RESIZE_UPPER_LIMIT:  # Load the Image and save it resized so that it fits the 2**N tiles
                print("Resizing Image to completely fit the canvas {}".format((newwidth, newheight)))
                with closing(storage.open(src, 'rb')) as fh_orig:
                    with closing(Image.open(fh_orig, 'r')) as orig:
                        image = orig.resize((newwidth, newheight))

                        # Save enlarged image
                        with closing(storage.open(extended_fname, 'wb')) as fh_ext:
                            image.save(fh_ext)
                        os.sync()

            else: # If the image is very large, we just copy it and we dont resize it to fit exactly into 2**N pixels
                print("Image is very large, I will not resize the image to fit the canvas -- there will be an edge {} in {}".format((width, height), (newwidth, newheight)))
                shutil.copy(storage.path(src), storage.path(extended_fname))


        if storage.exists(dst):
            if force_tiles_generation:
                print("Tile directory seems to already exist... will delete the folder and recreate them")
                tiler_update_status(TilerStatus.PREPROCESSING, tiler_id)
                tiler_update_percentage(0, 100, tiler_id)
                shutil.rmtree(storage.path(dst))
            else:
                print("Tile directory seems to already exist... will assume I should not do anything and return")
                tiler_update_status(TilerStatus.FINISHED, tiler_id)
                tiler_update_percentage(100, 100, tiler_id)
                return

        print(("Tiling Image {} => {} => {}".format(src, extended_fname, dst)))

        image2 = place_img_file_into_canvas_center(extended_fname, newwidth, newheight)

        # Max number of zoom levels:
        zoom_levels = np.int( np.ceil( np.log2( np.max(image2.size) /tile_width) ))
        number_of_tiles = np.sum(np.ceil(float(newheight)/tile_width/2**np.arange(zoom_levels))*np.ceil(float(newwidth)/tile_width/2**np.arange(zoom_levels)))+1
        print(("Image Tiler zoom levels {} :: total number of tiles: {}".format(zoom_levels, number_of_tiles)))
        tiler_update_status(TilerStatus.TILING, tiler_id)

        # Iterate over zoom levels and for each line of tiles in the image, crop tiles out of the temporary image and save it to disk
        count = 0
        for z in range(zoom_levels+1):
            dim2 = image2.size
            x2 = 0
            print("zloop:",z,':: current image size:',dim2)
            for x in range(0, dim2[0]- 1 , tile_width):
                y2 = 0
                for y in range( 0, dim2[1]- 1 , tile_width):
                    # folder where current tile should go:
                    paths = ( str(dst), str(int(zoom_levels-z)), str(x2) )

                    # FJ: For local file storage we need to create the subfolders by hand.
                    # I could not find out how to achieve that through the storage API
                    if settings.DEFAULT_FILE_STORAGE == 'django.core.files.storage.FileSystemStorage':
                        for i, p in enumerate(paths):
                            path = os.path.join(*paths[:i+1])
                            if not storage.exists(path):
                                os.mkdir(storage.path(path))

                    tile = image2.crop((x,y, x + tile_width, y + tile_width))
                    fname = os.path.join( os.path.join(*paths), str(y2) + get_tile_image_file_extension(extended_fname))
                    with closing(storage.open(fname, 'wb')) as fh:
                        tile.save(fh)
                    del tile
                    count = count+1
                    if count % 1e3 == 0:
                        tiler_update_percentage(count, number_of_tiles, tiler_id)
                    if z > 0:
                        pngquant_image.delay(fname,min_quality=90, max_quality=95)

                    y2 = y2 + 1
                x2 = x2 + 1

            if z == 0: # First level we start from the original file and rescale image by a half
                del image2
                gc.collect()
                image2 = place_img_file_into_canvas_center(extended_fname, dim2[0]//2, dim2[1]//2, rescale_img_factor=.5)
            else: # otherwise it should be OK (not use too much memory) to just rescale the img
                image2.thumbnail((dim2[0]//2, dim2[1]//2), Image.ANTIALIAS)
        del image2
        tiler_update_percentage(100, 100, tiler_id)
        tiler_update_status(TilerStatus.FINISHED, tiler_id)
    except Exception as inst:
        print(("Error occured in tile_image",inst))
        tiler_update_status(TilerStatus.ERROR, tiler_id)


def place_img_file_into_canvas_center(img_fname, newwidth, newheight, rescale_img_factor=1):
    with closing(storage.open(img_fname, 'rb')) as fh_ext:
        with closing(Image.open(fh_ext, 'r')) as image:
            image.thumbnail(tuple(map(lambda x: x*rescale_img_factor, image.size)), Image.ANTIALIAS)
            return place_img_into_canvas_center(image, newwidth, newheight)


def place_img_into_canvas_center(image, newwidth, newheight):
    # Convert Image to RGBA to use alpha channel to get transparent edge areas (where no image is defined)
    new_img = Image.new('RGBA', (newwidth, newheight))
    # ... and center the original, eventually extended image into the bigger 2**N frame
    left = newwidth//2 - image.width//2
    upper = newheight//2 - image.height//2
    new_img.paste(image, (left, upper))
    return new_img


def tiler_update_percentage(count, number_of_tiles, tiler_id):
    """
    Update the database with the percentage value of computed tiles
    count should be in the range [1,number_of_tiles]
    """
    percentage = int(count / float(number_of_tiles) * 100)
    print(("Progress creating tiles: {}% ({}/{})".format(percentage, count, number_of_tiles)))

    if tiler_id is not None:
        from miroutes.models import Tiler
        try:
            tiler = Tiler.objects.get(pk=tiler_id)
            tiler.percentage = percentage
            tiler.save(update_fields=['percentage',])
        except Tiler.DoesNotExist:
            pass


def tiler_update_status(status, tiler_id):
    """
    Update the database with the status value
    """
    if tiler_id is not None:
        from miroutes.models import Tiler, TilerStatus
        try:
            tiler = Tiler.objects.get(pk=tiler_id)
            tiler.status = status
            tiler.save(update_fields=['status',])
        except Tiler.DoesNotExist:
            pass
