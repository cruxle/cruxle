from miroutes.tasks import get_tile_image_file_extension, get_extended_dimensions
import os
from time import gmtime, strftime

# Thumbnail sizes in pixel according to ids .. will be saved as thumb_xx.png
THUMB_SIZES = {'s':128, 'm':256, 'l':512, 'xl':1024}

def get_wall_upload_path(tiler, fname):
    """
    Define folder where to put wall images and tiles inside media dir
    Args:
      filename: Filename of image
    """
    time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
    basename = time + '_' + os.path.splitext(os.path.basename(fname))[0]
    path = os.path.join("wall_pictures", basename)
    # print('get_wall_upload_path', path)
    return path

def get_orig_img_upload_path(tiler, fname):
    """
    Define where to put the original image from user upload
    Args:
      filename: Filename of image
    """
    path = os.path.join(get_wall_upload_path(tiler, fname), os.path.basename(fname))
    print(('get_orig_img_upload_path', path))
    return path

def get_userpics_upload_path(image, fname):
    """Get upload path for an image file."""
    time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
    basename = time + '_' + os.path.basename(fname)
    path = os.path.join("userpics", basename)
    return path

def get_materials_upload_path(image, fname):
    """Get upload path for an image file."""
    time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
    basename = time + '_' + os.path.basename(fname)
    path = os.path.join("userpics", basename)
    return path

def get_tiler_img_path(tiler, fname=None):
    """
    Define where to put full size image

    !!! ATTENTION: orig_img has to be saved before !!!

    Args:
      filename: Filename of image
    """
    import os
    from django.core.files.storage import default_storage as storage

    orig_fname = tiler.orig_img.name
    orig_fpath = tiler.orig_img.path

    width, height, newwidth, newheight = get_extended_dimensions(orig_fpath)

    # always use png for extended version as e.g. jpg's do not allow dimensions bigger than 64k pixel
    base, ext = os.path.splitext(orig_fname)
    if max((newwidth, newheight)) > 64e3:
        extended_fname = '{}_extended{}'.format(base, '.png')
    else:
        extended_fname = '{}_extended{}'.format(base, ext)

    return extended_fname

def get_routegeom_snapshot_path(geom):
    pass

def get_routegeom_img_path():
    pass

def get_thumb_img_upload_path(tiler, fname, size_id='s'):
    """
    Define where to put thumbnail_img

    !!! ATTENTION: orig_img has to be saved before !!!

    Args:
      filename: Filename of image
    """
    path = os.path.dirname(tiler.orig_img.name)
    return os.path.join(path, 'thumb_{}.png'.format(size_id))

def get_tiles_path(tiler):
    """
    Define folder where to put tile images
    Args:
      filename: Filename of image
    """
    import os
    path = os.path.dirname(tiler.orig_img.name)
    return os.path.join(path, 'tiles/')


def get_exif_tags(img_fname):
    from subprocess import call, check_output, STDOUT, CalledProcessError
    from django.core.files.storage import default_storage as storage
    import json

    cmd = ['exiftool', '-j', '-coordFormat', '%.8f', storage.path(img_fname)]
    try:
        rc = check_output(cmd)
        exif_dict = json.loads(rc)[0]
    except CalledProcessError:
        print('Could not read exif data.. exiftool returned with error code:')
        return {}
    except TypeError as err:
        print('Could not read exif data.. exiftool returned:',rc)
        return {}
    except Exception as e:
        print(e, 'Could not read exif data..:', rc)
        return {}

    return exif_dict


def get_exif_location(img):
    """Extract the exif location of a pic if possible.
    From the github gist @maxbellec maxbellec/get_lat_lon_exif_pil.py

    ATTENTION: it turns out that the above mentioned method is not working for pngs...
    We have to instead use the exiftool commandline tool...
    """

    from subprocess import call, check_output, STDOUT
    import re
    from django.core.files.storage import default_storage as storage

    def get_lat_lon(info):
        try:
            lon, gps_longitude_ref = info['GPSLongitude'].split()
            lat, gps_latitude_ref  = info['GPSLatitude'].split()
            lon, lat = [float(_) for _ in (lon,lat)]
            if gps_latitude_ref != "N":
                lat *= -1

            if gps_longitude_ref != "E":
                lon *= -1
            return [lat, lon]
        except KeyError:
            print("Exif location tag not found.")
            return None

    exif_data = get_exif_tags(img.name)

    return get_lat_lon(exif_data)


def copy_exif_tags(infile, outfile):
    from subprocess import call, check_output, STDOUT
    cmd = ['exiftool', '-TagsFromFile', infile, outfile]
    rc = check_output(cmd, stderr=STDOUT)
    return rc
