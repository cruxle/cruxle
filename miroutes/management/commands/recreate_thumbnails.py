from django.contrib.auth.models import User
from django.core.management import BaseCommand


# Example:
# The class must be named Command, and subclass BaseCommand
from miroutes.models import Wall


class Command(BaseCommand):
    # Show this when the user types help
    help = "Recreates thumbnails"

    # A command must define handle()
    def handle(self, *args, **options):

        walls = Wall.objects.exclude(orig_img = '').all()

        for wall in walls:
            print(("Creating thumbs for wall: " + str(wall.pk) + " - " + wall.name))
            wall.create_thumbs(force_foreground=True)


