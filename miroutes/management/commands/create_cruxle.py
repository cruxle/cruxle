import os

from django.contrib.auth.models import User
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.management import BaseCommand
from django.utils import timezone

from users.models import UserProfile


class Command(BaseCommand):
    # Show this when the user types help
    help = "Create cruxle user to communicate with other users."

    # A command must define handle()
    def handle(self, *args, **options):

        try:
            User.objects.get(username="cruxle")
            self.stdout.write("== COMMAND create_cruxle: cruxle user exist, not creating new one ==")
        except User.DoesNotExist:
            cuser = User.objects.create_user('cruxle', 'team@cruxle.org', last_login=timezone.now())

            imgpath = 'topo/img/avatar/cruxle.png'
            imgfile = staticfiles_storage.open(imgpath)

            cuser.userprofile.avatar.save(os.path.basename(imgpath), imgfile, save=True)
            cuser.userprofile.thumbnail()
            self.stdout.write("== COMMAND create_cruxle: cruxle user created! ==")
