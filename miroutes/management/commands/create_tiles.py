from django.contrib.auth.models import User
from django.core.management import BaseCommand


# Example:
# The class must be named Command, and subclass BaseCommand
from miroutes.models import Wall


class Command(BaseCommand):
    # Show this when the user types help
    help = "Recreates Tiles"

    def add_arguments(self, parser):
        parser.add_argument('wall_id', type=int, help='provide wall id')
        parser.add_argument('--no_force', action='store_false', dest='force', help='should we not overwrite existing ones?')
        parser.add_argument('--foreground', action='store_true', dest='foreground', help='should we run the tiler in the foreground or push it to a celery queue?')
        parser.set_defaults(force=True, foreground=False)

    # A command must define handle()
    def handle(self, *args, **options):
        try:
            wall = Wall.objects.get(id=options['wall_id'])
        except Wall.DoesNotExist:
            print('Could not find a wall with id:', options['wall_id'])
            return

        if wall.tiler is None:
            print('Wall does not yet have a tiler object.. this is odd.. will exit now.. please have a look whats going on')
            return

        wall.tiler.create_tiles(force_foreground=options['foreground'], force_tiles_generation=options['force'])
