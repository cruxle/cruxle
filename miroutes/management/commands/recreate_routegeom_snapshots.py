from django.contrib.auth.models import User
from django.core.management import BaseCommand

from miroutes.tasks import update_Wall_after_publish

from miroutes.models import Wall, RouteGeometry

from PIL import Image

class Command(BaseCommand):
    # Show this when the user types help
    help = "Recreates thumbnails"

    def add_arguments(self, parser):
        parser.add_argument('--wall_id', type=int, help='provide wall id, -1 for Wall.objects.all() ')
        parser.add_argument('--repair', action='store_true', dest='repair', help='find all broken routegeom snapshots and try to repair them')
        parser.add_argument('-f --force', action='store_true', dest='force', help='should we not overwrite existing ones?')
        parser.set_defaults(repair=False)
        parser.set_defaults(force=False)

    # A command must define handle()
    def handle(self, *args, **options):

        if options['wall_id']:
            if options['wall_id'] == -1:
                walls = Wall.objects.all()
            else:
                walls = [Wall.objects.get(id=options['wall_id']), ]

            for wall in walls:
                update_Wall_after_publish(wall.id)

        if options.get('repair'):
            # all pub view routegeoms:
            rgs = RouteGeometry.objects.filter(on_wallview__is_dev=False)
            for rg in rgs:
                try:
                    with Image.open(rg.snapshot.img) as im:
                        im.verify()
                        print("RG {} seems OK".format(rg.id))
                except Exception as e:
                    rg.update_snapshot(delayed=False)

                try:
                    with Image.open(rg.snapshot.img) as im:
                        im.verify()
                except Exception as e:
                    print("Could not Repair Snapshot for RouteGeom: {}".format(rg.id))

