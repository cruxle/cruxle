# This removes all images including tiles and thumb which are not currently in use.
from django.core.management import BaseCommand

from glob import glob
import shutil
import os

from miroutes.models import Wall
from django.conf import settings
from django.core.files.storage import default_storage as storage

class Command(BaseCommand):
    help = "Remove all images including tiles and thumb which are not currently in use"

    def add_arguments(self, parser):
        parser.add_argument('--delete', action='store_true', dest='delete', default=False, help='Delete Image folders, otherwise we just do a dry run')

    def handle(self, *args, **options):

        if settings.DEFAULT_FILE_STORAGE != 'django.core.files.storage.FileSystemStorage':
            raise IOError("This only works for FileSystemStorage")

        active_wall_names, active_wall_dirs = list(zip(*[ (w.name, os.path.dirname(w.tiler.orig_img.path)) for w in Wall.objects.all() ]))

        wall_dirs = glob(os.path.join(settings.MEDIA_ROOT, 'wall_pictures', '*'))

        for d in wall_dirs:
            if not d in active_wall_dirs:
                print(('This Image is not attached to a wall anymore :: {}'.format(d)))
                if options['delete']:
                    print(('Deleting :: {}'.format(d)))
                    shutil.rmtree(d)
