from django.core.management import BaseCommand
from mitopo.celeryapp import app


# Example:
# The class must be named Command, and subclass BaseCommand
from miroutes.models import Wall


class Command(BaseCommand):
    # Show this when the user types help
    help = "Inspect the celery queue"

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument('--scheduled',
                            action='store_true',
                            dest='scheduled',
                            default=False,
                            help='show scheduled jobs')
        parser.add_argument('--registered',
                            action='store_true',
                            dest='registered',
                            default=False,
                            help='show registered jobs')
        parser.add_argument('--reserved',
                            action='store_true',
                            dest='reserved',
                            default=False,
                            help='show tasks that has been received, but are still waiting to be executed')
        parser.add_argument('--active',
                            action='store_true',
                            dest='active',
                            default=True,
                            help='show active jobs')
        parser.add_argument('--purge',
                            action='store_true',
                            dest='purge',
                            default=False,
                            help='Discard all waiting tasks')
        parser.add_argument('--revoke',
                            action='store_true',
                            dest='revoke',
                            default=False,
                            help='Cancel all running tasks')

    # A command must define handle()
    def handle(self, *args, **options):
        I = app.control.inspect()
        if options['active']:
            print("Active Jobs: ({})".format(len(I.active())))
            for worker, jobs in list(I.active().items()):
                print(('worker: {0:12s} ::'.format(worker)))
                for i, job in enumerate(jobs):
                    print((' {0:12d}  :: {1:}'.format(i+1, job)))

        if options['scheduled']:
            print("Scheduled Jobs: ({})".format(len(I.scheduled())))
            for worker, jobs in list(I.scheduled().items()):
                print(('worker: {0:12s} ::'.format(worker)))
                for i, job in enumerate(jobs):
                    print((' {0:12d}  :: {1:}'.format(i+1, job)))

        if options['registered']:
            print("Registered Jobs: ({})".format(len(I.registered())))
            for worker, jobs in list(I.registered().items()):
                print(('worker: {0:12s} ::'.format(worker)))
                for i, job in enumerate(jobs):
                    print((' {0:12d}  :: {1:}'.format(i+1, job)))

        if options['reserved']:
            print("Reserved Jobs: ({})".format(len(I.reserved())))
            for worker, jobs in list(I.reserved().items()):
                print(('worker: {0:12s} ::'.format(worker)))
                for i, job in enumerate(jobs):
                    print((' {0:12d}  :: {1:}'.format(i+1, job)))

        for i in range(10):
            if options['purge']:
                Njobs = app.control.purge()
                print(("purged {} jobs".format(Njobs)))

            if options['revoke']:
                for worker, jobs in list(I.active().items()):
                    for job in jobs:
                        print(("Revoking {} :: {}".format(job['id'], job)))
                        app.control.revoke(job['id'], terminate=True)
