from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db.models import Q
from django.utils import timezone


def get_superusers():
    return User.objects.filter(Q(is_superuser=True)).distinct()

# Example:
# The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):
    # Show this when the user types help
    help = "Creates initial admin user root with password root if no superuser exists"

    # A command must define handle()
    def handle(self, *args, **options):

        superusers = get_superusers()

        if len(superusers) > 0:
            self.stdout.write("== COMMAND create_admin: Superusers exist, not creating new one ==")
            for superuser in superusers:
                self.stdout.write(superuser.username)
        else:
            self.stdout.write("== COMMAND create_admin: Creating admin user")
            User.objects.create_superuser('root', 'myemail@example.com', 'root', last_login=timezone.now())


