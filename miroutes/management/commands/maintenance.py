import os

from django.conf import settings
from django.core.management import BaseCommand

MAINTENANCE_FILE = os.path.join(settings.BASE_DIR,"maintenance-active")

def write(path, content, mode='w'):
    with open(path, mode) as f:
        f.write(content)

def read(path, mode='r'):
    with open(path, mode) as f:
        return f.read()

class Command(BaseCommand):

    help = "allows to set maintenance mode (a permanent display which indicates maintenance)"

    def add_arguments(self, parser):
        parser.add_argument('command', type=str, metavar="CMD", help="start|stop the maintenance")
        parser.add_argument('minutes', type=str, nargs="?", help="minutes after when the platform is shut down", default="10")


    def handle(self, *args, **options):

        if options["command"] == "stop":
            try:
                os.unlink(MAINTENANCE_FILE)
            except Exception as e:
                print("Error during maintenance stop: " + str(e))

        elif options["command"] == "start":
            write(MAINTENANCE_FILE, options["minutes"])

