from django.core.management import BaseCommand
import os
import json

import math
from contextlib import closing
from PIL import Image

from django.conf import settings
from miroutes.models import WallView
from django.core.files.storage import default_storage as storage


class Command(BaseCommand):
    # Show this when the user types help
    help = "Extract images containing bolts, belays or anchors."

    def add_arguments(self, parser):
        parser.add_argument('wallview_id', nargs='?', type=int, default=-1)

    def handle(self, *args, **options):
        image_size = 512
        box_size = 61
        box_half = int((box_size - 1)/2)
        ignore_walls = [9,]
        only_wallview = options["wallview_id"]

        def transform_coordinates(img, x, y):
            if img.width >= img.height:
                if math.log2(img.width).is_integer():
                    conv = img.width/256
                else:
                    res_steps = 10**math.floor(math.log10(img.width))
                    width = math.ceil(img.width/res_steps)*res_steps
                    conv = width/256
                    print(f"Conversion factor for {img.path} is {conv}, assuming width {width}.")
            else:
                if math.log2(img.height).is_integer():
                    conv = img.height/256
                else:
                    res_steps = 10**math.floor(math.log10(img.height))
                    height = math.ceil(img.height/res_steps)*res_steps
                    conv = height/256
                    print(f"Conversion factor for {img.path} is {conv}, assuming height {height}.")
            return int(x * conv), -int(y * conv)

        def get_bounding_box(img, x, y):
            if img.width < box_size or img.height < box_size:
                return []

            x_0 = min(max(int(x - box_half), 0), img.width - box_size)
            y_0 = min(max(int(y - box_half), 0), img.height - box_size)

            return [x_0, y_0, x_0 + box_size - 1, y_0 + box_size - 1]

        desc = []

        ml_datadir = os.path.join(settings.MEDIA_ROOT, 'ml_data')
        os.makedirs(ml_datadir, exist_ok=True)

        wviews = WallView.objects.exclude(wall__pk__in=ignore_walls)
        if only_wallview >= 0:
            wviews = WallView.objects.filter(pk=only_wallview)

        for wv in wviews:
            if wv.simplemarker_set.count():
                img = wv.wall.tiler.img
                with closing(storage.open(img.path, 'rb')) as fh:
                    with closing(Image.open(fh, 'r')) as tmp:
                        image = tmp.copy()

                if img.width < image_size or img.height < image_size:
                    continue

                markers = wv.simplemarker_set.all()
                all_coordinates = {m.pk: transform_coordinates(img, m.pnt.y, m.pnt.x) for m in markers}
                coordinates = {k: v for k, v in all_coordinates.items() if (
                    (v[0] >= 0) and
                    (v[0] <= img.width - 1) and
                    (v[1] >= 0) and
                    (v[1] <= img.height - 1))}
                if len(coordinates) == 0:
                    continue

                xbounds = [max(0, min([x[0] for x in coordinates.values()])),
                           min(img.width-1, max([x[0] for x in coordinates.values()]))]
                ybounds = [max(0, min([x[1] for x in coordinates.values()])),
                           min(img.height-1, max([x[1] for x in coordinates.values()]))]

                # xbounds = [math.floor(xbounds[0]), math.ceil(xbounds[1])]
                # ybounds = [math.floor(ybounds[0]), math.ceil(ybounds[1])]

                nxparts = math.ceil((xbounds[1] - xbounds[0]) / image_size)
                nyparts = math.ceil((ybounds[1] - ybounds[0]) / image_size)

                print(f"Img wv{wv.pk} width {img.width} height {img.height} xbounds {xbounds} ybounds {ybounds}")

                assert nxparts > 0 and nyparts > 0

                for ny in range(nyparts):
                    y_0 = ybounds[0] + ny * image_size
                    y_0_bounds = y_0 - box_half
                    y_1 = y_0 + image_size - 1
                    y_1_bounds = y_1 + box_half
                    for nx in range(nxparts):
                        x_0 = xbounds[0] + nx * image_size
                        x_0_bounds = x_0 - box_half
                        x_1 = x_0 + image_size - 1
                        x_1_bounds = x_1 + box_half

                        subimgfname = os.path.join(ml_datadir, f"wv{wv.pk}_nx{nx}_ny{ny}.png")
                        anyfeatures = False
                        for key, co in coordinates.items():
                            if co[0] < x_0 or co[0] > x_1:
                                continue
                            if co[1] < y_0 or co[1] > y_1:
                                continue
                            anyfeatures = True
                            m = markers.get(pk=key)
                            bbox = get_bounding_box(img, co[0], co[1])
                            desc.append({
                                "image_path": img.path,
                                "subimage_path": subimgfname,
                                "imgbox": [x_0_bounds, y_0_bounds, x_1_bounds, y_1_bounds],
                                "bbox": bbox,
                                "rel_bbox": [bbox[0] - x_0_bounds, bbox[1] - y_0_bounds,
                                             bbox[2] - x_0_bounds, bbox[3] - y_0_bounds],
                                "marker_id": m.pk,
                                "wallview_id": wv.pk,
                                "category": m.markertype,
                            })

                        if anyfeatures:
                            image.crop([x_0_bounds, y_0_bounds, x_1_bounds, y_1_bounds]).save(subimgfname)

        with open(os.path.join(ml_datadir, "out.json"), 'w+') as f:
            f.write(json.dumps(desc))
