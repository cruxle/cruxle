import os
import random

import shutil
from django.core.management import BaseCommand

import miroutes
from miroutes.models import Wall, Route, RouteGeometry, Tiler
from django.contrib.auth.models import User

from django.core.files import File
from django.conf import settings

# The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):
    # Show this when the user types help
    help = "Creates initial database objects as sample content"

    # A command must define handle()
    def handle(self, *args, **options):
        self.stdout.write("== COMMAND insertfields: Creating initial database objects")
        # do as test user
        testuser = User(username__iexact="Testuser")
        testuser.save()

        gKochel = [11.344199180603026, 47.632660340454386]
        gPampa = [11.644199180603026, 47.632660340454386]

        gWiesenwand = "POINT(11.347973048686981 47.63621916824778)"
        gKeltenwand = "POINT(11.346226930618286 47.634681075868414)"

        fname_image = "/code/misc/kochel_wiesenwand.jpg"

        t = Tiler()
        bg_img_path = os.path.join(settings.MEDIA_ROOT, miroutes.models.get_orig_img_upload_path(t, fname_image))
        os.makedirs(os.path.dirname(bg_img_path))
        shutil.copy(fname_image, bg_img_path)
        t.orig_img.save('kochel_wiesenwand.jpg', File(open(bg_img_path)))
        t.save()
        w = Wall(name="Wiesenwand", is_active=True, pnt=gWiesenwand, tiler=t)
        w.save()
        
        for j in range(0, 25):
            r = Route(name="testRoute" + str(j), grade=1060+j/25.*1000, pnt=gWiesenwand)
            r.save()

            route_geom = {
                "type": "Feature",
                "properties": {
                    "type": "Feature"},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [[ -i, 90 + j * 3] for i in range(9, 25)]}
            }

            geom_obj = RouteGeometry(route=r, on_wallview=w.dev_view, geojson=route_geom)
            geom_obj.save()

        w.publish_dev_view("Publishing Testview.", testuser)

        fname_image = "/code/misc/lost_arrow.png"

        t = Tiler()
        bg_img_path = os.path.join(settings.MEDIA_ROOT, miroutes.models.get_orig_img_upload_path(t, fname_image))
        os.makedirs(os.path.dirname(bg_img_path))
        shutil.copy(fname_image, bg_img_path)
        t.orig_img.save('lost_arrow.png', File(open(bg_img_path)))
        t.save()
        w = Wall(name="Keltenwand", is_active=False, pnt=gKeltenwand, tiler=t)
        w.save()

        r = Route(name="testRoute1", grade=110, pnt=gKeltenwand)
        r.save()

        route_geom = {
            "type": "Feature",
            "properties": {
                "type": "Feature"},
            "geometry": {
                "type": "LineString",
                "coordinates": [[-i, 90] for i in range(9, 25)]}
            }

        geom_obj = RouteGeometry(route=r, on_wallview=w.wallview_set.filter(is_dev=False)[0], geojson=route_geom)
        geom_obj.save()
