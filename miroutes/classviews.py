import datetime

from rest_framework import viewsets
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.pagination import PageNumberPagination

from users.classviews import Paged_ListAPIView, chunkify
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.authentication import SessionAuthentication

from django.urls import reverse
from django.core.cache import caches

from django.db.models import F
from miroutes.models import Ascent, Wall, Route, RouteGeometry
from edit_spot.utils import del_unchanged_versions_from_wall_revision
from users.models import User
from miroutes.serializers import AscentSerializer, NewsWallSerializer, RouteSerializer, GenericMixedSerializer, \
    TimelineItemSerializer, WallSerializer
from users.serializers import RevisionVersionSerializer, RevisionSerializer_with_User
from django.shortcuts import get_object_or_404
from itertools import chain
from reversion.models import Version, Revision


def rest_wall_cache_key(wall_id):
    url = reverse('miroutes:rest_wall', kwargs={'pk': wall_id})
    key = 'rest_url_{}'.format(url)
    return key


class RestWall(RetrieveAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = WallSerializer
    queryset = Wall.objects.all()

    def dispatch(self, request, *args, **kwargs):
        key = rest_wall_cache_key(kwargs.get('pk'))
        cache = caches['memcached']
        response = cache.get(key)
        if response:
            return response

        response = super(RestWall, self).dispatch(request, *args, **kwargs).render()
        cache.set(key, response, 15*60)
        return response


def invalidate_RestWall_cache(wall_id):
    cache = caches['memcached']
    key = rest_wall_cache_key(wall_id)
    response = cache.get(key)
    cache.delete(key)
    print("Deleting cache entries for {} :: deleted? {}".format(key, response is not None))
    return key, response


class RouteViewSet(ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = RouteSerializer

    def get_queryset(self):
        """See if we are delivered a query parameter *wallview* and return
        a filtered set.
        """
        show_deprecated = self.request.query_params.get('deprecated', None)
        queryset = Route.objects.all()
        if show_deprecated is None:
            queryset = queryset.filter(deprecated=False)
        wallview = self.request.query_params.get('wallview', None)
        if wallview is not None:
            queryset = queryset.filter(walls__pk=wallview)
        return queryset


class RestWallActivity(Paged_ListAPIView):
    """
    Return the activity on a wall with paging:
    This turns out to be quite nasty..
    because if we want to have the most recent 10 entries
    of both, ascents and edits, we have to sort them together.
    This would need to fetch them all, combine them and then cut into fitting pages.
    I didnt find out if we can do that in the database and I think that is really ugly
    if we do it on the python/django side.
    """

    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = TimelineItemSerializer

    def get_queryset(self):
        wall_id = self.kwargs.get('wall_id')
        wall = get_object_or_404(Wall, pk=wall_id)
        ascents = Ascent.objects.filter(route__walls__id__exact=wall.pub_view.id).order_by('-date')

        # Find revisions of Wall
        wall_versions = Version.objects.get_for_object(wall).prefetch_related('revision')
        wall_revisions = Revision.objects.filter(version__in=wall_versions).distinct().annotate(date=F('date_created')).order_by('-date')

        # iterate revisions and compare them one by one
        # ids of the versions which are identical in two subsequent revisions
        # are filtered and added to the revision object
        for n in range(len(wall_revisions) - 1):
            del_unchanged_versions_from_wall_revision(
                wall_revisions[n], wall_revisions[n+1])

        # Find revisions of Routes
        routes = wall.pub_view.route_set
        route_versions = Version.objects.none();
        if routes.count() > 0:
            for route in routes.all():  # get a query result of all versions of all routes
                route_versions = route_versions | Version.objects.get_for_object(route).prefetch_related('revision')

        route_revisions = Revision.objects.filter(version__in=route_versions).distinct().annotate(date=F('date_created')).order_by('-date')

        # linked materials
        userpics = wall.userpic_set.annotate(date=F('created_at')).order_by('-date')

        def sort_func(x):
            if isinstance(x.date, datetime.date):
                return datetime.datetime(x.date.year, x.date.month, x.date.day)
            return x.date

        query_result = sorted(chain(ascents, userpics, wall_revisions, route_revisions),
                              key=sort_func, reverse=True)

        return chunkify(self, query_result)

class RestRouteActivity(Paged_ListAPIView):
    """
    Return the activity on a route with paging.
    """

    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = TimelineItemSerializer

    def get_queryset(self):
        route_id = self.kwargs.get('route_id')
        route = get_object_or_404(Route, pk=route_id)

        # ascents
        ascents = route.ascent_set.order_by('-date')

        # Find revisions of Route
        route_versions = Version.objects.get_for_object(route).prefetch_related('revision')
        route_revisions = Revision.objects.filter(version__in=route_versions).distinct().annotate(date=F('date_created')).order_by('-date')

        # revisions of Route Geometries
        geom_versions = Version.objects.get_for_model(RouteGeometry).filter(serialized_data__contains='"route": {}'.format(route.pk))
        geom_revisions = Revision.objects.filter(version__in=geom_versions).distinct().annotate(date=F('date_created')).order_by('-date')

        # linked materials
        userpics = route.userpic_set.annotate(date=F('created_at')).order_by('-date')

        def sort_func(x):
            if isinstance(x.date, datetime.date):
                return datetime.datetime(x.date.year, x.date.month, x.date.day)
            return x.date

        query_result = sorted(chain(ascents, route_revisions, geom_revisions, userpics), key=sort_func, reverse=True)

        return chunkify(self, query_result)


class SmallResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 20

class NewsViewSet(ListAPIView):
    """
    Get the list of last added walls and some meta data as news.
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Wall.active_objects.order_by('-last_published_date')
    serializer_class = NewsWallSerializer
    pagination_class = SmallResultsSetPagination
