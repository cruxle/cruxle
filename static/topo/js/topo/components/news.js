Vue.filter('formatDate', function(value) {
    if (value) {
        var d = new Date(String(value));
        return d.toLocaleDateString();
    }
});

Vue.filter('vuemarkdownify', function(mdstring) {
        var converter = new showdown.Converter();
        var mdescr = converter.makeHtml(escapeHtml(mdstring));
        return mdescr;
});


Vue.component('news-entry', {
    props: ['wall'],
    template: '<div class="news-entry row">' +
                    '<div class="news-col-left col-xs-12 col-sm-7">' +
                        '<a v-bind:href="wall.wall_url">' +
                            '<img v-bind:src="wall.image" class=\'img-responsive wall-thumb\'/>' +
                        '</a>' +
                    '</div>' +
                    '<div class="news-col-right col-xs-12 col-sm-5">' +
                        '<div class="table-responsive-not-working-on-iphone-...killing-scrolling">' +
                        '<div class="table table-hover table-sm table-condensed">' +
                            '<thead>' +
                                '<tr>' +
                                    '<th colspan="2">' +
                                    '<h1>' +
                                        '<a v-bind:href="wall.wall_url">' +
                                            '{{ wall.name }}' +
                                        '</a>' +
                                    '</h1>' +
                                    '</th>' +
                                '</tr>' +
                            '</thead>' +
                            '<tbody>' +
                                '<tr>' +
                                    '<th>{{trans_provided_by}}</th>' +
                                    '<th>' +
                                        '<div v-if="wall.collaborators.length > 4">' +
                                            '<span v-for="user in wall.collaborators">' +
                                                '<span data-toggle="tooltip" data-delay="0" data-animation="false" v-bind:title="user.userprofile.name">' +
                                                    '<a v-bind:href="user.url">' +
                                                        '<img class="news-user-thumb" v-bind:src="user.userprofile.thumb_url" />' +
                                                    '</a>' +
                                                '</span>' +
                                            '</span>' +
                                        '</div>' +
                                        '<div v-else>' +
                                            '<p v-for="user in wall.collaborators">' +
                                                '<a v-bind:href="user.url">' +
                                                    '<img class="news-user-thumb" v-bind:src="user.userprofile.thumb_url" />' +
                                                    '{{ user.userprofile.name }}' +
                                                '</a>' +
                                            '</p>' +
                                        '</div>' +
                                    '</th>' +
                                '</tr>' +
                                '<tr>' +
                                    '<th>{{trans_created_at}}</th>' +
                                    '<th>' +
                                        '<div class="news-date">' +
                                            '{{ wall.created_at | formatDate }}' +
                                        '</div>' +
                                    '</th>' +
                                '</tr>' +
                                '<tr v-if="wall.nr_routes > 0">' +
                                    '<th>{{trans_nr_routes}}</th>' +
                                    '<th>' +
                                        '<div class="news-nr-routes">' +
                                            '{{ wall.nr_routes }}' +
                                        '</div>' +
                                    '</th>' +
                                '</tr>' +
                                '<tr v-if="wall.description">' + // blocks have to disappear from table if value is None or empty
                                    '<th>{{trans_description}}</th>' +
                                    '<th>' +
                                        '<div class="news-wall-desc">' +
                                            '<span v-html="this.$options.filters.vuemarkdownify(wall.description)"></span>' +
                                        '</div>' +
                                    '</th>' +
                                '</tr>' +
                            '</tbody>' +
                        '</div>' + // end table
                        '</div>' + // end table-responsive
                    '</div>' + // end col-right
                '</div>', // end row
    data: function() {
        return {
            trans_provided_by: gettext('Provided by'),
            trans_created_at: gettext('Date'),
            trans_nr_routes: gettext('# Routes'),
            trans_description: gettext('Description'),
            isUpdating: false
        }
    },
});

Vue.component('news', {
    template: '<div id="news-panel" class="news-panel">' +
    '<news-entry' +
    '   v-for="wall in walls"' +
    '   v-bind:wall="wall"' +
    '   v-bind:key="wall.id">' +
    '</news-entry>' +
    '<div class="news-load-more-section" v-bind:class="{invisible: hide_next_section}" >' +
    '<div id="news-spinner"><span class="fa fa-spinner fa-spin fa-3x"></span></div>' +
    '<button v-on:click="fetchNews" id="load-more" class="btn btn-default">{{ button_text }}</button></div>' +
    '</div>' +
    '</div>',
    props: ['button_text'],
    data: function () {
        return {
            "walls": [],
            "next": "/miroutes/rest/news",
            "hide_next_section": false
        };
    },
    mounted:

        function () {
            this.fetchNews();
        }
    ,
    methods: {
        startSpinner: function () {
            document.getElementById("news-spinner").style.display = 'block';
            document.getElementById("load-more").style.display = 'none';
        },
        stopSpinner: function () {
            document.getElementById("news-spinner").style.display = 'none';
            document.getElementById("load-more").style.display = 'block';
        },
        fetchNews: function () {
            var vm = this;

            vm.startSpinner();

            Vue.http.get(vm.next)
                .then(function (response) {
                    var data = response.body;
                    for (i = 0; i < data.results.length; i++) {
                        vm.walls.push(data.results[i]);
                    }
                    Vue.set(vm, 'next', data.next);
                    vm.stopSpinner();
                    if(vm.next === null) {
                      vm.hide_next_section = true;
                    }
                });
        }
    }
});
