/* This component is currently unused and serves only for reference!
It was intended to replace the ascentlist on user_details.
*/
Vue.component('ascent', {
    props: ['ascent', 'path'],
    computed: {
        climbedAt: function(){
            var climbedAt = new Date(this.ascent.datetime);
            return climbedAt.toLocaleString();
        },
        popoverId: function(){
            return "ascent-popover" + this.ascent.id;
        },
        popoverContentId: function(){
            return "ascent-popover-content" + this.ascent.id;
        },
        editUrl: function(){
            return this.ascent.url + "?next=" + this.path;
        },
    },
    methods: {
        delAscent: function(){
            var vm = this;
            Vue.http.get(this.ascent.del_url)
                .then(function (response) {
                    vm.$emit('update');
                });

        }
    },
    mounted: function () {
        // Popover is inserted dynamically when shown. Reason:
        // The popover contains a vuejs method call (on:click) so we need to parse the template
        // by vuejs before it is attributed to the data-content property of the popover.
        var pID = "#" + this.popoverContentId;
        $("#" + this.popoverId).popover({
            content: $(pID)
        })
            .on('show.bs.popover', function() {
                $(pID).addClass('show');
            })
            .on('hide.bs.popover', function() {
                $(pID).addClass('hide');
            });

        // popover containing the comment
        $(".note-popover").popover();

    },
    data: function(){
        return {
            deletetext: gettext('Really Delete Ascent?')
        };
    },
    template: '<tr>' +
          '<td><span class="glyphicon glyphicon-check"></span></td>' +
          '<td>' +
          '  <a class="ascent_links" v-bind:href="ascent.route.url">' +
          '    {{ ascent.route.name }}' +
          '  </a>' +
          '</td>' +
          '<td>' +
          '  <a v-if="ascent.note" href="#" class="note-popover" data-toggle="popover"' +
          '     data-trigger="focus" data-html="true"' +
          '     v-bind:title="ascent.route.name"' +
          '     v-bind:data-content="ascent.note">' +
          '       <span class="fa fa-comment"></span>' +
          '  </a>' +
          '</td>' +
          '<td>{{ ascent.route.get_full_grade_display }}</td>' +
          '<td>{{ ascent.get_style_display }}</td>' +
          '<td v-bind:data-order="ascent.datetime">{{ climbedAt }}</td>' +
          '<td v-if="ascent.is_climber">' +
          '  <a class="ascent_links" v-bind:href="editUrl">' +
          '    <span class="fas fa-pencil-alt"></span>' +
          '  </a>' +
          '  <a v-bind:id="popoverId" href="javascript:void(0)" data-toggle="popover"' +
          '    data-placement="left" data-content="" data-trigger="focus" data-html="true"' +
          '    v-bind:title="deletetext">' +
          '    <span class="fas fa-trash-alt"></span>' +
          '  </a>' +
          '<div v-bind:id="popoverContentId" class="hide">' +
          '  <button class="btn btn-danger" v-on:click="delAscent">' +
          '    <span class="fas fa-trash-alt fa-2x"></span></button>' +
          '</div>' +
          '</td>' +
          '</tr>'
});

Vue.component('ascents', {
    data: function(){
        return {
            nametext: gettext('Name'),
            styletext: gettext('Style'),
            emptytext: gettext('No Ascents yet.'),
            ascents: [],
            isUpdating: false
        };
    },
    methods: {
        fetchAscents: function () {
            var vm = this;
            vm.isUpdating = true;
            return Vue.http.get(this.url)
                .then(function (response) {
                    if(response.body.length === 0){
                        $("#ascenttable").hide();
                        $("#no-ascents-message").show();
                    }else{
                        $("#ascenttable").show();
                        $("#no-ascents-message").hide();
                    }
                    vm.ascents = response.body;
                    vm.isUpdating = false;
                });

        },
    },
    computed: {
        isClimber: function(){
            if(this.ascents.length > 0){
                return this.ascents[0].is_climber;
            }
            return false;
        }
    },
    props: ['url', 'path'],
    mounted: function () {
        var vm = this;
        vm.fetchAscents();
    },
    template: '<div>' +
        '<div v-show="isUpdating"><span class="fa fa-spinner fa-spin fa-3x"></span></div>' +
        '<table id="ascenttable" class="table table-hover table-responsive table-condensed table-striped">' +
        '<thead>' +
        '  <tr>' +
        '    <th data-sortable="false"></th>' +
        '    <th data-field="name"  data-sortable="true">{{ nametext }}</th>' +
        '    <th data-field="note" data-sortable="false"><span class="fa fa-comment"></span></th>' +
        '    <th data-field="grade" data-sortable="true"><span class="fa fa-signal"></span></th>' +
        '    <th data-field="style" data-sortable="true">{{ styletext }}</th>' +
        '    <th data-field="date" data-sortable="true"><span class="far fa-clock"></span></th>' +
        '    <th v-if="isClimber" data-sortable="false"></th>' +
        '  </tr>' +
        '</thead>' +
        '<tbody>' +
        '<tr is="ascent"' +
        '   v-for="ascent in ascents"' +
        '   v-bind:ascent="ascent"' +
        '   v-on:update="fetchAscents"' +
        '   v-bind:key="ascent.id"' +
        '   v-bind:path="path">' +
        '  </tr>' +
        '</tbody>' +
        '</table>' +
        '<div id="no-ascents-message">{{ emptytext }}</div>' +
        '</div>'
});
