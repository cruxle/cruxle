Vue.component('message', {
    props: ['msg'],
    computed: {
        createdAt: function(){
            var dateCreated = new Date(this.msg.created_at);
            return dateCreated.toLocaleString();
        }
    },
    template: '<div class="row thread-msg">' +
        '<div class="col-xs-12 col-md-3 small">' +
        '   {{msg.sender.username}}' +
        '   <br>' +
        '   {{createdAt}}' +
        '</div>' +
        '<div class="col-xs-12 col-md-9 thread-msg-body">' +
        '   <span v-html="msg.body"></span>' +
        '</div>' +
        '</div>'
});

Vue.component('thread', {
    props: ['thread', 'maxlen_preview'],
    data: function(){
        return {
            replytext: gettext('Your reply'),
            submittext: gettext('Submit'),
            unreadMessagesText: this.thread.unread_msgs + " " + gettext("New Messages"),
            csrftoken: $.cookie('csrftoken'),
            isShown: false,
            isSending: false,
            isUnread: (this.thread.unread_msgs > 0),
            input: {
                "reply": ""
            }
        };
    },
    computed: {
        subject: function(){
            var result = this.thread.messages[0].sender.username + ": " + this.thread.messages[0].body;
            if (this.maxlen_preview === "")
                Vue.set(this, 'maxlen_preview', '80');

            if(result.length > this.maxlen_preview)
                result = result.substring(0, maxlen_preview) + "...";
            return result;

        },
        readClass: function(){
            return this.isUnread ? "unread-thread" : "already-read-thread";
        }
    },
    methods: {
        user_in_userlist: function(id){
            var idx = this.thread.users.findIndex(function(user) {
                  return user.id === id;
            });
            return idx !== -1;
        },
        sendReply: function(){
            var vm = this;
            if(vm.input.reply === "")
                return;
            vm.isSending = true;
            $.post({
                url: vm.thread.reply_to,
                beforeSend: function (request) {
                        request.setRequestHeader("X-CSRFToken", vm.csrftoken);
                },
                success: function(){
                    vm.input.reply = "";
                    vm.isSending = false;
                    vm.$emit('update');
                },
                data: JSON.stringify(vm.input),
                contentType: 'application/json',
                dataType: 'json',
            });

        },
        toggleShow: function(){
            this.isShown = !this.isShown;
            this.markRead();
        },
        markRead: function(){
            var vm = this;
            if(vm.isUnread)
                $.get(vm.thread.toggle_url, function(){
                    vm.isUnread = false;
                });
        }

    },
    template: '<div>' +
        '<div class="row thread-rule" v-bind:class="readClass" v-on:click="toggleShow">' +
        '<div class="col-xs-4 col-md-2">' +
        '   <a v-for="user in thread.users" v-bind:href="user.url" v-bind:title="user.userprofile.name">' +
        '     <div class="thread-party-thumb">' +
        '       <img v-bind:src="user.userprofile.thumb_url" />' +
        '       <p>{{user.userprofile.name}}</p>' +
        '     </div>' +
        '   </a>' +
        '</div>' +
        '<div class="col-xs-8 col-md-10">' +
        '   <div v-if="isUnread">{{unreadMessagesText}}</div>' +
        '   <h5><span v-html="subject"></span></h5>' +
        '</div>' +
        '</div>' +
        '<div v-show="isShown" class="row">' +
        '<div class="col-xs-12 thread-input">' +
        '  <div class="row thread-msg">' +
        '    <div class="col-xs-12">' +
        '      <div class="form-group">' +
        '        <textarea class="form-control" name="reply-text" rows="2" v-model="input.reply" v-bind:placeholder="replytext">' +
        '        </textarea>' +
        '      </div>' +
        '      <button class="btn btn-default" v-bind:disabled="isSending" v-on:click="sendReply()"><span v-if="isSending" class="fa fa-spinner fa-spin"></span>{{submittext}}</button>' +
        '    </div>' +
        '  </div>' +
        '</div>' +
        '<div class="col-xs-12 thread-msg-container">' +
        '  <message' +
        '   v-for="msg in thread.messages"' +
        '   v-bind:msg="msg"' +
        '   v-bind:key="msg.id"' +
        '   v-bind:class="{\'thread-highlighted-msg\': ! user_in_userlist(msg.sender.id)}"' +
        '  >' +
        '  </message>' +
        '</div>' +
        '</div></div>'
});

Vue.component('inbox', {
    template: '<div>' +
        '<h4>' +
        '  <span class="glyphicon glyphicon-inbox"></span> {{inboxTitle}}' +
        '  <a v-on:click="fetchThreads()" href="#"><span class="glyphicon glyphicon-refresh" v-bind:class="{\'fa-spin\': isUpdating}"></span></a>' +
        '</h4>' +
        '<div v-show="isLoading"><span class="fa fa-spinner fa-spin fa-3x"></span></div>' +
        '<thread' +
        '   v-for="thread in threads"' +
        '   v-bind:thread="thread"' +
        '   v-on:update="fetchThreads"' +
        '   v-bind:key="thread.id">' +
        '</thread>' +
        '</div>',
    props: ['url'],
    data: function () {
        return {
            "inboxTitle": gettext("Inbox"),
            "isUpdating": false,
            "isLoading": true,
            "threads": [],
            "initialTimeout": 2500
        };
    },
    mounted: function () {
        var vm = this;
        vm.repeatedFetch();
    },
    methods: {
        repeatedFetch: function (){
            var vm = this;
            vm.fetchThreads().then(function (){
                setTimeout(vm.repeatedFetch, vm.initialTimeout);
                vm.initialTimeout *= 1.3;
            });
        },
        fetchThreads: function () {
            var vm = this;
            vm.isUpdating = true;
            return Vue.http.get(this.url)
                .then(function (response) {
                    vm.threads = response.body;
                    vm.isUpdating = false;
                    vm.isLoading = false;
                });

        },
    }
});
