/**
 * ######### timeline_api.js #########
 */
"use strict";


function TimelineApi(div_id, url, options, trans) {
    /*
      Class to display timelines.

      Note that this api requires JSON with specific fields.
      Data has to be prepared in advance.
    */
    if(typeof trans === 'undefined') {
        trans = {};
    }
    if(typeof trans.ticked === 'undefined') { trans.ticked='ticked'; }
    if(typeof trans.edited === 'undefined') { trans.edited='edited'; }
    if(typeof trans.rated  === 'undefined') { trans.rated='rated'; }
    if(typeof trans.empty  === 'undefined') { trans.empty='No items found.'; }
    if(typeof trans.more  === 'undefined') { trans.empty='Show More'; }


    if(typeof options === 'undefined') {
        options = {};
    }
    // page chunk size defaults to 10 :: FJ: or lets just trust that the django side knows what to do....
    // if(typeof options.chunksize === 'undefined' || !Number.isInteger(options.chunksize))
    //     options.chunksize = 10;

    var self = this;

    self.timeline = [];
    self.nextPage = 1;

    self.$moreButton = null;
    self.$paginationInfo = null;

    self.renderMore = function () {

        var spinner = $('<span>', { class: 'fa fa-spinner fa-2x fa-spin'});
        $(div_id).append(spinner);

        if(self.$moreButton)
            self.$moreButton.remove();
        else
            self.$moreButton = $("<button>", {
                "class": "btn btn-default",
                "text": trans.more
            });

        $.getJSON(url, {chunksize: options.chunksize, page: self.nextPage}, function (json_ret) {

            if(json_ret.pagination) {
                var data = json_ret.data;
            } else {
                var data = json_ret;
            }
            // no data: show empty msg
            if(data.length === 0){
                if(self.nextPage == 1)
                    $(div_id).empty().append($("<span>", {"class": "timeline-empty-text"}).text(trans.empty));
                spinner.remove();
                return;
            }

            self.timeline = self.timeline.concat(self.prepareList(data));
            self.renderList();

            if(typeof options.dump_data !== 'undefined') {
                if(options.dump_data) {
                    var pre = $('<pre>');
                    pre.html(JSON.stringify(data, null,2));
                    $(div_id).append(pre);
                }
            }


            // if we get a full chunk we display a button
            var need_more_button = data.length == options.chunksize;
            if(json_ret.pagination) {
                need_more_button = self.timeline.length < json_ret.pagination.total_count;
                $(div_id).append($('<div>', {
                    class: 'top-buffer-margin',
                    text: gettext('showing ')+ self.timeline.length + gettext(' of ') + json_ret.pagination.total_count + gettext(' entries'),
                }));
            }
            if( need_more_button ){
                $(div_id).append(self.$moreButton);
                self.$moreButton.one('click', function(){
                    self.nextPage += 1;
                    self.renderMore();
                });
            }
        });
    };

    self._format_userpic = function (data) {
        var note = $('<div>');
        if(data.img.length > 0){
            var $link = $("<a>", {
                href: data.img
            }).append($("<img>", {"src": data.thumbnail}));
        }else if(data.pdf_file.length > 0){
            var $link = $("<a>", {
                href: data.pdf_file,
                text: data.pdf_file
            });
        }else{
            throw new TypeError("Provide either a image or a pdf file link.");
        }
        note.append($link);
        note.append("<br>");

        if(data.description && data.description.length > 0)
            note.append($('<span>', {
                text: data.description,
            }));

        var what = $('<span>', {
            text: data.subject,
        });

        return {
            'date'       : data.created_at,
            'user_id'    : data.created_by.id,
            'user_name'  : data.created_by.userprofile.name,
            'user_avatar': data.created_by.userprofile.thumb_url,
            'user_url'   : data.created_by.url,
            'icon'       : $("<span>", {"class": "glyphicon glyphicon-picture"}),
            'what'       : what,
            'text'       : note,
        }
    }

    self._format_ascent = function (data) {
        var note = $('<div>');
        if(data.note) {
            if(data.note.length > 0) {
                note.append($('<span>', {
                    html: data.note,
                }))
                .append($("<br>"));
            }
        }
        if(data.rating) {
            note.append($('<span>', {
                html: trans.rated+': '+rating(undefined, data.rating, false),
            }));
        }

        if(data.get_style_display) {
            var style_display = data.get_style_display;
        } else {
            var style_display = trans.ticked;
        }

        var route_str = $('<a />', {
          href: data.route.url,
          text: "" + data.route.name+' ('+data.route.get_full_grade_display+')',
        });

        var what = $('<span>', {
            text: style_display+' : ',
        });
        what.append(route_str);

        return {
            'date'       : data.date,
            'user_id'    : data.climber.id,
            'user_name'  : data.climber.userprofile.name,
            'user_avatar': data.climber.userprofile.thumb_url,
            'user_url'   : data.climber.url,
            'icon'       : $("<span>", {"class": "glyphicon glyphicon-check"}),
            'what'       : what,
            'text'       : note,
        }
    }

    self._format_revision = function (data) {
        var text = $('<ul>');
        for (var i in data.versions) {
            var version = data.versions[i];
            if(! version.annotation) { // sometimes, we could not annotate the version object...
                continue;  // skip this entry
            }

            var version_li = $('<li>', {
                class: 'timeline-version-li',
            });
            var version_entry = $('<a>', {
                href: version.annotation.url,
            });
            if(version.annotation.icon_url) {
                var icon = $('<img>', {
                    class: 'timeline-version-icon',
                    src: version.annotation.icon_url,
                });
                version_entry.append(icon);
            }
            if(version.annotation.icon) {
                version_entry.append($('<span>', {
                    html: version.annotation.icon,
                }));
            }
            version_entry.append($('<span>', {
                text: version.object_repr,
            }));
            version_li.append(version_entry);
            text.append(version_li);
        }
        if( ! data.user ) {
            data.user = {
                id: 0,
                userprofile: {
                    name: 'anonymous'
                },
                url: '#',
            };
        }
        return {
            'date'       : data.date_created,
            'user_id'    : data.user.id,
            'user_name'  : data.user.userprofile.name,
            'user_avatar': data.user.userprofile.thumb_url,
            'user_url'   : data.user.url,
            'icon'       : $("<span>", {"class": "glyphicon glyphicon-pencil"}),
            'what'       : trans.edited+' : '+data.comment,
            'text'       : text,
        };
    }

    self.prepareList = function(data) {
        /* Format and sort the raw ajax data to list entries. */
        var timeline = [];
        for (var i in data) {
            var entry = data[i];
            switch(entry.item_type){
                case "ascent":
                    timeline.push(self._format_ascent(entry));
                    break;
                case "revision":
                    timeline.push(self._format_revision(entry));
                    break;
                case "userpic":
                    timeline.push(self._format_userpic(entry));
                    break;
                default:
                    console.log('eha, dont know how to format the timeline entry:',entry.item_type, entry);
            }
        }

        timeline.sort(function(a, b) {
            a = new Date(a.date);
            b = new Date(b.date);
            return a>b ? -1 : a<b ? 1 : 0;
        });

        return timeline;

    };

    self.renderList = function () {
        var ul = $('<ul class="timeline-itemlist">');
        for (var i in self.timeline) {
            var entry = self.timeline[i];
            var $header = $("<div>", {"class": "timeline-item-head"})
                .append(entry.icon)
                .append($("<span>", {"class": "timeline-item-date"})
                    .text(self._formatDate(new Date(entry.date))));
            var $body = $("<div>", {"class": "timeline-item-body"});
            var $userContainer = $("<div>", {"class": "timeline-item-user"});
            var $userLink = $("<a>", {"href": entry.user_url})
                .append($("<img>", {
                    "class": "timeline-item-avatar",
                    "src": entry.user_avatar}));

            $body.append($userContainer.append($userLink));
            $body.append($("<a>", {
                "href": entry.user_url,
                "text": entry.user_name + " " }));
            $body.append($("<span>", {"class": "timeline-item-what"}).append(entry.what));

            if(entry.text) {
                if(entry.text.length > 0) {
                    $body.append($("<div>", {"class": "timeline-item-text"}).html(entry.text));
                }
            }

            var li = $("<li>", {"class": "timeline-item"}).append($header).append($body);
            ul.append(li);
        }
        $(div_id).html(ul);
    };

    self._formatDate = function(date) {
        return date.toLocaleString();
    };

}
