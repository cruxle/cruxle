/**
 * ######### timeline_api.js #########
 */
"use strict";


function loadAndRenderWorkTable(container_id, url, options) {
    /*
      Class to display routes, walls and tilers that were edited by user.

      Note that this api requires JSON with specific fields.
      Data has to be prepared in advance.
    */

    if(typeof options === 'undefined') {
        options = {};
    }

    var $container = $(container_id);

    var $spinner = $('<span>', { class: 'fa fa-spinner fa-2x fa-spin'});
    $container.append($spinner);

    function renderTable() {
        var $table = $('<table>', {class: "table table-condensed table-striped"});
        var $thead = $('<thead>')
            .append($('<tr>')
                    .append($('<th>', {"data-orderable": "false"}))
                    .append($('<th>', {"data-sortable": "true"}).text(gettext("Name")))
                    .append($('<th>', {"data-sortable": "true"}).text(gettext("Creator")))
                    .append($('<th>', {"data-sortable": "true"}).text(gettext("Last Edit")))
                    .append($('<th>', {"data-sortable": "false"}))
                   );
        var $tbody = $('<tbody>');

        $table.append($thead);
        $table.append($tbody);
        return $table;
    }

    function renderTableRows (data, $tbody) {
        for (var i in data) {
            var entry = data[i];
            var $tr = $('<tr>');
            // thumb
            var $thumb = $('<td>')
                .append($("<img>", {
                    "class": "workbench-thumb",
                    "src": entry.thumb
                }));
            $tr.append($thumb);

            // name
            var $name;
            var display = entry.name;
            // we add grade for routes
            if(typeof entry.get_full_grade_display !== 'undefined')
                display += ", " + entry.get_full_grade_display;
            if(entry.url != null){
                $name = $("<td>")
                    .append($("<a>", {
                        href: entry.url,
                        text: display
                    }));
            }else{
                $name = $("<td>").text(display);
            }
            $tr.append($name);

            // creator
            var $creator;
            if(entry.created_by == null)
                $creator = $("<td>").text(gettext("anonymous"));
            else{
                $creator = $("<td>")
                    .append($("<a>", {
                        href: entry.created_by.url,
                        text: entry.created_by.userprofile.name
                    }));
            }
            $tr.append($creator);

            // last edit
            var last_edit_date = new Date(entry.last_edit);
            var $edit = $("<td>", {
                "data-order": entry.last_edit
            }).text(last_edit_date.toLocaleString());
            $tr.append($edit);

            // edit link

            var $editLink = $("<td>")
                .append($("<a>", {
                    href: entry.edit_url,
                }).append($("<span>", {class: "glyphicon glyphicon-pencil"})));
            $tr.append($editLink);

            $tbody.append($tr);
        }
    }

    function getMore(url) {
        $.getJSON(url, function (json_ret) {
            if(json_ret.pagination) {
                var data = json_ret.data;
            } else {
                var data = json_ret;
            }

            $(".fa-spinner").remove();

            // no data: show empty msg
            if(data.length === 0){
                $container.append($("<span>").text(gettext("No Items to display.")));
                return;
            }

            if( $container.find('table').length === 0 ){
                var $table = renderTable();
                $container.append($table);
            }


            if(json_ret.pagination) {
                var $tbody = $container.find('tbody');

                renderTableRows(data, $tbody);

                var pagination_info = $container.find('.pagination_info');
                if( pagination_info.length === 0 ) {
                    pagination_info = $('<div>', { class: 'top-buffer-margin pagination_info', });
                    $container.append(pagination_info);
                }
                pagination_info.text(gettext('showing') + ' ' + $tbody.find('tr').length + ' / ' + json_ret.pagination.total_count);

                var need_more_button = $tbody.children('tr').length < json_ret.pagination.total_count;
                if( need_more_button ){
                    var $moreButton = $container.find('.more_btn');
                    if( $moreButton.length === 0 ){
                        var $moreButton = $("<button>", {
                            "class": "btn btn-default more_btn",
                            "text": gettext('Show More'),
                        });
                        $container.append($moreButton);
                    }

                    $moreButton.one('click', function(){
                        $('<span>', { class: 'fa fa-spinner fa-spin'}).prependTo($moreButton);
                        getMore(json_ret.pagination.next);
                    });
                }
            }
        });
    }
    getMore(url); // call once for the first time
}
