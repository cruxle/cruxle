function index_in_list_of_dict_with_name(list, key, name) { // linear search to check if array of dicts has given name in key
    for ( var i in list ) {
        if ( list[i][key] === name ) {
            return i;
        }
    }
    return -1;
}


function ascent_sunburst_chart(div_id, url) {
    //console.log('ascent_sunburst_chart, fetching data from:',url);
    var svg_id = div_id + ' svg';  // with this we try to find the svg inside the div

    if( $(svg_id).length != 1 ) {
        $(div_id).text(gettext('Uups, we tried to render an ascent sunburst chart into a div that does not contain an svg element. Could you please tell us where you found this confusion here? Many Thanks!'));
                return;
        }

        if( $(svg_id).length == 1 ) {
                var spinner = $('<span>', {
                        class: "fa fa-spinner fa-spin fa-2x"
                });
                $(div_id).parent().prepend(spinner);
        }

    $.getJSON(url, function(data) {

        /*
           Data for Sunburst has to be like:
        ascents_data = [{
            name: 'all_ascents',
            children: [
            {
                name: 'sport',
                children: [
                {
                    name: 'France',
                    children: [
                    {
                        name: '6a+',
                        children: [
                        { name: 'a routename 1', size: '10-style' },
                        { name: 'a routename 2', size: '10-style' },
                        { name: 'a routename 3', size: '10-style' }
                        ]
                    },
                    {
                        name: '6b+ France',
                        children: [
                        { name: 'a routename 4', size: '10-style' }
                        ]
                    }
                    ]
                }
                ]
            }
            ]
        }];
        */

        if( data.length == 0 ) {
            $(div_id).html('<b>'+gettext('No Ascents yet. Go Climbing!')+'</b>');
            $(div_id).css('height', '2em');
            $(spinner).remove();
            return;
        }

        var delim = "#_!_#";

        function find_current_entry(name_children_dict, key, new_entry) { // linear search to find the correct entry for given key, i.e. search entry for "Sportclimb in ascents_data[0]"
            var i = index_in_list_of_dict_with_name(name_children_dict.children, 'name', key);
            if(i>=0) {
                return name_children_dict.children[i];
            } else {
                var len = name_children_dict.children.push(new_entry);
                return name_children_dict.children[len-1];
            }
        }

        var ascents_data = [{
            name: 'Ascents',
            children: [],
        }];


        var unique_ascents = 0;
        for( var i in data ){
            //if (i>2) {break;}
            var ascent = data[i];

            var climb_type = ascent.climbingstyle_name;
            //var climb_type_short = ascent.climbingstyle_name.charAt(0);
            var climb_type_short = ascent.climbingstyle_name.slice(0,5);
            if (ascent.route.gym_route) {
                climb_type = 'Gym ' + climb_type;
                climb_type_short = 'G ' + climb_type_short;
            }

            var uname = climb_type;
            var e_climb_type = find_current_entry(ascents_data[0], uname, {  // always begin at root of ascents_data structure
                name: uname,
                label: climb_type_short,
                children: []
            });

            uname += delim + ascent.route.grade_system.name;
            var e_grade_system = find_current_entry(e_climb_type, uname, {
                name: uname,
                children: []
            });

            uname += delim + ascent.route.get_full_grade_display;
            uname = ascent.route.grade + delim + uname; // add grade in the beginning for sorting, because sort from chart is not called
            e_grade = find_current_entry(e_grade_system, uname, {
                name: uname,
                color: ascent.route.polylinecolor,
                label: ascent.route.get_full_grade_display,
                value: ascent.route.grade,
                children: []
            });

            uname += delim + 'Last_Entry' + delim + ascent.route.name;
            var label = ascent.route.name; // + ' ' + ascent.route.get_full_grade_display

            // check if there is already a corresponding entry: this happens e.g. for multiple ascents of the same route
            var uid = index_in_list_of_dict_with_name(e_grade.children, 'name', uname);

            if(uid>=0) { // already have this entry
                e_grade.children[uid].size += 1;
            } else {
                find_current_entry(e_grade, uname, { name: uname, label: label, size: 1 });
                unique_ascents++;
            }
        }

        //$("#data_dump").html(JSON.stringify(ascents_data, null, 2));

        nv.addGraph(function() {
            var chart = nv.models.sunburstChart();


            //chart.mode('count');
            chart.labelFormat(function(d){
                if (d['label']) {
                    //console.log('Found label:', d.label);
                    return d.label;
                };
                if (d.name === "Ascents") {
                    var label = data.length + ' ' + trans.Ascents;
                    if(data.length != unique_ascents) { // have some double ascents of routes
                        label += ' \n ' + unique_ascents + ' ' + trans.Routes;
                    }
                    return label;
                };
                return '';
            });
            chart.showLabels(true);

            //chart.color(d3.scale.category20c());

            chart.showTooltipPercent(true);

            chart.sort(function(d1, d2){
                return d1.value > d2.value;
            })

            chart.tooltip.keyFormatter(function (d) {
                var s = d.split(delim);
                return s[s.length-1];
            });

            chart.tooltip.valueFormatter(function (d) {
                //console.log('tooltip value',d);
                //if (d===1) { return ''; }
                return ': '+d+' '+trans.Routes;
            });

            d3.select(svg_id)
                .datum(ascents_data)
                .call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
        });
        $(spinner).remove();
    });
}




function wall_route_histogram_chart(div_id, url, options) {
    var svg_id = div_id + ' svg';  // with this we try to find the svg inside the div

    if( $(svg_id).length != 1 ) {
      $(div_id).text(gettext('Uups, we tried to render an route histogram of a wall into a div that does not contain an svg element. Could you please tell us where you found this confusion here? Many Thanks!'));
      return;
    }

    if( $(svg_id).length == 1 ) {
      var spinner = $('<span>', {
        class: "fa fa-spinner fa-spin fa-2x"
      });
      $(div_id).parent().prepend(spinner);
    }

    $.getJSON(url, function(wall) {

        /*
           Data has to be like:
        var bar_data = [{
            key: 'Difficulties',
            values: [
            {
                label: '4',
                value: 5,
                color: "#000000"
            },
            {
                label: '5',
                value: 2,
                color: "#ffff00"
            },
            {
                label: '7',
                value: 4,
                color: "#ff0000"
            },
            ]
        }];
        */

        if (options.datadump_id) {
            $(options.datadump_id).html(JSON.stringify(wall, null, 2));
        }

        var bar_data = [{
            key: gettext('Difficulties'),
            values: [],
        }
        ];


        if(options.populate_all) {
            var possible_reduc_grade_labels = wall.most_common_grade_system.reduc_grades_n_labels;
            for (var i in possible_reduc_grade_labels) {

                if(typeof(options.min_grade)!=='undefined') { // min_grade can be supplied as a lower bound of an equalized grade to limit empty bars
                    if(possible_reduc_grade_labels[i][2] < parseInt(options.min_grade)) continue;
                }

                var reduc_grade_label = possible_reduc_grade_labels[i][1];
                bar_data[0].values.push({
                    label: reduc_grade_label,
                    value: 0,
                    equalized: possible_reduc_grade_labels[i][2],
                });
            }
        }

        for( var i in wall.full_route_set ){
            var route = wall.full_route_set[i];
            if(route.grade===0) continue;

            var entry = {
                    label: route.grade_system.reduced_grade_label,
                    value: 0,
                    color: route.grade_system.linecolor,
                    equalized: route.grade_system.equalized_grade,
            };

            var ind = index_in_list_of_dict_with_name(bar_data[0].values, 'label', route.grade_system.reduced_grade_label); // find location of entry
            if (ind<0) { // could not find a value, lets create a new entry
                bar_data[0].values.push(entry);
            };
        }

        // sort according to equalized grades:
        bar_data[0].values.sort(function (a,b) {
            return parseInt(a.equalized) - parseInt(b.equalized)
        });

        for( var i in wall.full_route_set ){
            var route = wall.full_route_set[i];
            if(route.grade===0) continue;


            var ind = index_in_list_of_dict_with_name(bar_data[0].values, 'label', route.grade_system.reduced_grade_label); // find location of entry
                bar_data[0].values[ind].value += 1;
                bar_data[0].values[ind].color = route.grade_system.linecolor;
        }

        if (options.datadump_id) {
            $(options.datadump_id).html(JSON.stringify(bar_data, null, 2));
        }


        var yoffsetbottom = 25;

        nv.addGraph(function() {
            var chart = nv.models.discreteBarChart()
                .margin({left: 0, top: 5})
                .showYAxis(false)
                .x(function(d) { return d.label })    //Specify the data accessors.
                .y(function(d) { return d.value })
                .showValues(true)       //...instead, show the bar value right on top of each bar.
                .valueFormat(d3.format(',d'))
                .duration(100)
                .noData(gettext('No Routes'))
                .showLegend(false)
                ;

            if(options.staggered_labels!==undefined){
                chart.staggerLabels(options.staggered_labels);
            } else {
                chart.staggerLabels(true);
                yoffsetbottom += 25;
            }

            if(options.yaxis_label){
                chart.showYAxis(true);
                chart.margin({left: 50});
                chart.yAxis
                    .axisLabel('# '+gettext('Routes'))
                    .tickFormat(d3.format(',d'))
                    .axisLabelDistance(-25); //decrease distance between chart and label

            }
            if(options.xaxis_label){
                d3.select(".nv-y").selectAll("text.nv-axislabel")
                    .attr('dy', '2em');
                chart.xAxis
                    .axisLabel(gettext('Grades'));
                yoffsetbottom += 50;
            }

            if(options.tooltip) {
                chart.tooltip.enabled(true);
            } else {
                chart.tooltip.enabled(false);
            }

            chart.margin({bottom: yoffsetbottom});

            d3.select(svg_id)
                .datum(bar_data)
                .call(chart);

            nv.utils.windowResize(chart.update);

            if(options.hide_empty_chart) {
              if(bar_data[0].values.length === 0) {
                $(div_id).hide();
              }
            }

            return chart;
        });
        $(spinner).remove();

    });
}
