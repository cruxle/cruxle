function rating(span, rated_points, check_mobile) {
    rated_points = parseFloat(rated_points);
    check_mobile = (check_mobile === undefined) ? true : check_mobile; // if not provided, we always use the many star approach

    var star_grey = "<span class='fa fa-star fa-lg star-empty'></span>";
    var star_unrated = "<span class=''></span>";
    var star_full = "<span class='fa fa-star fa-lg star-full'></span>";
    var star_half_full = "<span class='star-stack'>" +
        "<span class='fa fa-star fa-lg star-empty'></span>" +
        "<span class='fas fa-star-half fa-lg star-full'></span>" +
        "</span>";

    var rating_content = "";

    if (isMobile() && check_mobile) {
        if (rated_points < 0) {
            rating_content = '<span class="fa-stack">\
                             <i class="far fa-stack-2x fa-star star-empty stacked"></i>\
                           </span>'
        } else {
            rating_content = '<span class="fa-stack rating">\
                             <i class="fa fa-stack-2x fa-star star-full stacked"></i>\
                             <i class="fa fa-stack-1x rating-number">' + Math.round(rated_points) + '</i>\
                           </span>'
        }

    } else {

        for (i = 1; i <= 5; i++) {
            if (rated_points < 0) {
                rating_content += star_unrated;
            } else {
                if (rated_points >= i) {
                    rating_content += star_full;
                } else if (rated_points >= i - 0.5) {
                    rating_content += star_half_full;
                } else {
                    rating_content += star_grey;
                }
            }
        }

    }

    if (span == undefined) {
        return rating_content;
    }
    else {
        var el = $(span).html(rating_content);
        if (el.length === 1) el[0].style.whiteSpace = 'nowrap';
    }

}
