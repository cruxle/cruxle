function prepare_comment_input(input_id) {
    var input = $("#"+input_id);
    input.focus();
    // Then a little hack to move the cursor to the end of the input line when opened again
    var tmp = input.val();
    input.val('');
    input.val(tmp);
}
