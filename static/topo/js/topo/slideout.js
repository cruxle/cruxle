/**
 * ######### slideout.js #########
 *
 * Slideout content.
 */
"use strict";

function Slideout(bar, slideoutContent, slideoutWrapper, bodyContent, layoutConent, mapsToRefresh) {

    this.slideoutContent = slideoutContent;
    this.slideoutWrapper = slideoutWrapper;
    this.bar = bar;
    this.bodyContent = bodyContent;
    this.layoutContent = layoutConent;
    this.mapsToRefresh = mapsToRefresh;
    this.isContentShrinked = false;
    this.layoutContentWidth = 0;

    var iconBaseUrl = window.location.protocol + "//" + window.location.host + "/";
    var closedIcon = iconBaseUrl + 'static/topo/img/slideout_closed.png';
    var openIcon = iconBaseUrl + 'static/topo/img/slideout_open.png';

    this.getSlideoutDefaultWidth = function () {
        var visible_width = 0;

        if (isMobile()) {
            visible_width = $(window).width() * 0.90;
        } else {
            visible_width = $(window).width() * 0.3;
        }

        return visible_width;
    };

    this.slideoutWidth = this.getSlideoutDefaultWidth();

    this.setMapsToRefresh = function (mapstoRefresh) {
        this.mapsToRefresh = mapstoRefresh;
        this.applyWidth(); // invalidates sizes of maps
    };

    this.setWidth = function (width) {
        if (!isMobile()) {
            this.slideoutWidth = width;
            this.applyWidth();
        }
    };

    this.updateLayoutContentWidth = function () {
        this.layoutContentWidth = $(window).width() - this.slideoutWrapper.width() + this.slideoutWidth;
        console.log("layoutContent: " + this.layoutContentWidth);
        this.layoutContent.css("margin-left", this.slideoutWrapper.width() + "px")
    };

    this.applyWidth = function () {
        // console.log("applyWidth: slideoutwidth = " + this.slideoutWidth);
        this.slideoutContent.css("width", this.slideoutWidth);
        this.correctContentHiddenPosition();
        // in case we have size dependent stuff in slideout, e.g. tables or route_histogram, send resize Event
        window.dispatchEvent(new Event('resize'));
    };

    this.setWidthPercentage = function (percentage) {
        if (!isMobile()) {
            this.slideoutWidth = $(window).width() * (percentage / 100.0);
            this.applyWidth();
        }
    };

    this.updateIcon = function () {
        if (this.isClosed()) {
            this.slideoutWrapper.css('background-image', 'url(' + closedIcon + ')');

        } else {
            this.slideoutWrapper.css('background-image', 'url(' + openIcon + ')');

        }
    };

    this.correctContentHiddenPosition = function () {
        // console.log("correctContentHiddenPosition: slideoutwidth = " + this.slideoutWidth);

        var newLeft = this.slideoutWidth * -1;
        this.bodyContent.css("margin-left", newLeft);
        this.updateLayoutContentWidth();

        if (!this.isClosed()) {
            this.bodyContent.css("transform", "translate(" + this.slideoutWidth + "px,0)");
        }
    };


    this.open = function (transitionOff) {
        var instance = this;

        if (instance.canToggle === false) {
            return;
        }

        var transitionOff = transitionOff || false;

        this.slideoutContent.removeClass("slideout-hidden");
        this.slideoutContent.css("width", this.slideoutWidth);

        if (!transitionOff) {
            this.bodyContent.css("transition", "transform 1s");
            this.bodyContent.css("-webkit-transition", "-webkit-transform 1s ease");
        } else {
            this.bodyContent.css("transition", "none");
            this.bodyContent.css("-webkit-transition", "none");
        }

        this.bodyContent.css("transform", "translate(" + this.slideoutWidth + "px,0)");

        this.updateIcon();

        if (transitionOff) {
            setTimeout(function () {
                instance.slideoutContent.trigger("transitionend");
            }, 100);
        }
    };

    this.close = function (transitionOff) {
        var instance = this;

        if (instance.canToggle === false) {
            return;
        }

        var transitionOff = transitionOff || false;

        this.slideoutContent.addClass("slideout-hidden");
        var newLeft = this.slideoutWidth * -1;

        if (!transitionOff) {
            this.bodyContent.css("transition", "transform 1s");
            this.bodyContent.css("-webkit-transition", "-webkit-transform 1s ease");
        } else {
            this.bodyContent.css("transition", "none");
            this.bodyContent.css("-webkit-transition", "none");
        }

        this.bodyContent.css("transform", "translate(0,0)");
        this.updateIcon();

        if (transitionOff) {
            setTimeout(function () {
                instance.slideoutContent.trigger("transitionend");
            }, 100);
        }
    };

    this.remove = function () {
        this.bar.hide();
        this.slideoutContent.hide();
    };

    this.isClosed = function () {
        return this.slideoutContent.hasClass("slideout-hidden");
    };

    this.toggle = function () {
        if (this.isClosed()) {
            this.open();
        } else {
            this.close();
        }
    };

    this.init = function () {
        var instance = this;

        this.slideoutWrapper.css("display", "block");

        this.slideoutWrapper.bind('touchstart click', function (e) {

            var pos_from_right_border = 999;

            // this a bit complex code is there so you do not have to hit
            // the "collapse slideout" button exactly on touch
            // (which is hard on mobile)
            if (e.type == "touchstart") {
                if (instance.isClosed()) {
                    pos_from_right_border = 0;
                } else {
                    if (instance.canToggle) {
                        pos_from_right_border = instance.slideoutWrapper.width() - e.originalEvent.targetTouches[0].clientX;
                    }
                }
            } else if (e.type == "click") {
                pos_from_right_border = instance.slideoutWrapper.width() - e.offsetX;
            }

            if (pos_from_right_border <= 30) {
                e.preventDefault();
                e.stopPropagation();
                instance.toggle();

            }
        });

        this.bodyContent.on('transitionend', function () {
            if (!isMobile()) {
                if (!instance.isClosed() && !instance.isContentShrinked) {
                    instance.layoutContent.width(instance.layoutContentWidth - instance.slideoutWidth);
                    instance.isContentShrinked = true;
                }
                if (instance.isClosed()) {
                    instance.layoutContent.width(instance.layoutContentWidth);
                    instance.isContentShrinked = false;
                }
            }
            $.each(instance.mapsToRefresh, function (index, value) {
                value.invalidateSize();
            });
            instance.applyWidth();
        });

        $(function () {
            instance.slideoutWrapper.show();
            instance.applyWidth();
            instance.updateIcon();
        })
    };

    this.init();

}
