/**
 * ######### utils.js #########
 *
 * This file should contain all globally accessible utility functions.
 */

"use strict";

/**
 * Checks if our mobile css is currently applied (which means we assume it's a mobile device) or not.
 *
 * @returns {boolean}
 */
function isMobile() {
    // The width of this off-screen element is modified when our mobile css kicks in.
    // var width = $('#mobile_detector').width();
    // console.log("mobile_detector: " + width);
    return $(window).width() < 768;

}

/**
 * Forces the browser to do a page reflow.
 * */
//function forceReflow(element) {
//    var reflow = element.offsetHeight;
//}

/**
 * Checks if a string ends with a certain suffix.
 *
 * @param str the string to check
 * @param suffix the suffix
 * @returns {boolean}
 */
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

/**
 Bootstrap needs a page reload to reflow the grid correctly when width of the browser
 changes. This can happen easily with mobile devices by turning them from portrait to
 landscape and vice versa.

 The most reliable way to detect this I found was to frequently poll the window width
 and if it changed do a page reload.

 Not the nicest way, but quite reliable.

 One important thing, it is not really possible to determine orientation by querying
 window-height + width and checking which one is greater. When the phones soft keyboard
 is hovering, even if the phone is in portrait mode the result would be landscape
 when comparing width and height as the window height is reduced by keyboard height.
 */

//function funcFactoryReflowOnOrientationChange(globalVars) {
//    return function () {
//        if ($(window).width() !== undefined && $(window).width() !== globalVars.lastCheckWidth) {
//            globalVars.lastCheckWidth = $(window).width();
//            forceReflow(document.body);
//            console.log("orientationchange detected");
//            forceMobileContentWidth();
//        }
//    }
//}

/*
Function to read out messages in JSON response and display the message to the user.
If no success callback is given in the ajax options we just display the message.

For display, we use append_message if it is present (see base.html) or otherwise the console.
*/
function verboseJsonAjax(options){
    options.dataType = "json";

    var originalCallback = options.success;
    var newCallback = function(data){
        var parsed_data = null;
        try{
            parsed_data = JSON.parse(data);
        }catch (err){
            var msg = 'Ajax: Returned object is not a valid JSON.'
            console.log(msg);
            return Promise.reject(msg);
        }
        if(parsed_data.message){
            try{
                if(parsed_data.success){
                    append_message(parsed_data.message, 'success');
                }else{
                    append_message(parsed_data.message, 'danger');
                }
            }catch(err){
                var msg = 'Ajax Call Returned: '+parsed_data.message;
                console.log(msg);
                return Promise.reject(msg);
            }
        }

        if(originalCallback){
            return originalCallback(data);
        }
    };
    options.success = newCallback;
    return $.ajax(options);
}

function messageCallback(data){
    /* Callback to be used directly with ajax functions that are not covered by $.ajax,
       i.e., ajaxSubmit. Just use on success: messageCallback.
    */
    var parsed_data = null;
    try{
        parsed_data = JSON.parse(data);
    }catch (err){
        console.log('Ajax: Returned object is not a valid JSON.');
        return;
    }
    if(parsed_data.message){
        try{
            if(parsed_data.success){
                append_message(parsed_data.message, 'success');
            }else{
                append_message(parsed_data.message, 'danger');
            }
        }catch(err){
            console.log('Ajax Call Returned: '+parsed_data.message);
        }
    }
}

// From: http://shebang.brandonmintern.com/foolproof-html-escaping-in-javascript/
// Use the browser's built-in functionality to quickly and safely escape
// the string
function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

// From: http://shebang.brandonmintern.com/foolproof-html-escaping-in-javascript/
// UNSAFE with unsafe strings; only use on previously-escaped ones!
function unescapeHtml(escapedStr) {
    var div = document.createElement('div');
    div.innerHTML = escapedStr;
    var child = div.childNodes[0];
    return child ? child.nodeValue : '';
}

// Get parameter after the # hash from a url
// From: https://stackoverflow.com/questions/15780717/get-parameter-values-from-href-in-jquery
function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}


// Update optional parameters in a url, you know... these parameters within question marks...
// Not supplying a value will remove the parameter, supplying one will add/update the parameter.
// If no URL is supplied, it will be grabbed from window.location
// From: https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter
function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}


// check if a html element is empty or has content
function element_is_empty(el) {
    return !$.trim(el.html());
}


// check if condition is the same for a period of time, e.g. return true if the condition was true for 5 seconds
// e.g. call as the trivial example:
// condition_was_same_repeatedly(function() {
//      return Math.random() > 0;}, 10 ).then(function(ret) {
//          console.log('Math.rand should always be higher than 0 :: i.e. ret should be true',ret);
//      })
function condition_was_same_repeatedly(condition_func, time, Nchecks) {
    if(typeof time === 'undefined') { var time = 1000; }
    if(typeof Nchecks === 'undefined') { var Nchecks = 10; }
    var time_between_checks = time/Nchecks;
    var initial_condition = condition_func();
    var p = new Promise( function(resolve, reject) {
        var iter = 0;
        var interval = setInterval( function() {
            var cond_now = condition_func();
            if (cond_now !== initial_condition) {
                clearInterval(interval);
                resolve(false);
                return;
            }
            iter++;
            if(iter>Nchecks) {
                clearInterval(interval);
                resolve(true);
                return;
            }
        }, time_between_checks);
    });
    return p;
}
