// Add function to round a datetime to just the day, suitable to use as key in a dict
Date.prototype.getDateKey = function() {
    function two(n) {
        return (n < 10 ? '0' : '') + n;
    }

    return two(this.getMonth() + 1) + '/' + two(this.getDate()) + '/' + this.getFullYear();
};

var K2degC = function (T) { return Math.round(T - 273.15); }
var K2degF = function (T) { return Math.round(T * 9. / 5. - 459.67); }

var reorder_weather_data = function(data) {
    var ret = [];
    for (var i in data.list) {
        var weather_entry = data.list[i];
        var date = new Date(weather_entry.dt*1000);
        var key = date.getDateKey();

        var weather_entry_exists= false;
        for (var idate in ret) {
            if(ret[idate].datekey === key) {
                weather_entry_exists = true;
                ret[idate]['3h_data'].push(weather_entry);
            }
        }
        if(! weather_entry_exists) {
            ret.push({
                'datekey': key,
                'timestamp': new Date(key),
                '3h_data': [weather_entry,],
            });
        }
    }

    // reductions for the day
    for (var i in ret) {
        var daydata = ret[i];
        var accum_precip = 0;
        var mintemp =  999999999;
        var maxtemp = -999999999;
        for (var i3 in daydata['3h_data']) {
            var data3h = daydata['3h_data'][i3];
            if(data3h['rain']!==undefined) { if(data3h.rain['3h']!==undefined) {accum_precip += data3h.rain['3h']; }}
            if(data3h['snow']!==undefined) { if(data3h.snow['3h']!==undefined) {accum_precip += data3h.snow['3h']; }}
            mintemp = (data3h.main.temp < mintemp) ? data3h.main.temp : mintemp;
            maxtemp = (data3h.main.temp > maxtemp) ? data3h.main.temp : maxtemp;
        }
        ret[i]['accum_precip'] = accum_precip;
        ret[i]['mintemp'] = mintemp;
        ret[i]['maxtemp'] = maxtemp;
    }
    return ret;
}

var render_daily_weather_table = function(weather_data, trans) {
    var weather_table = $('<table>', {
        id: "weather_table",
        class: 'table table-condensed table-hover'
    });

    { // thead
        var thead = $('<thead>');
        var tr = $('<tr>');

        var th = $('<th>');
        th.append($('<i>', { class: "far fa-clock fa-lg"}));
        th.append($('<span>', { class: "hidden-xs", text: ' '+trans.Date}));
        tr.append(th);

        th = $('<th>');
        th.append($('<i>', { class: "fas fa-thermometer-three-quarters fa-lg"}));
        th.append($('<i>', { text: ' °C'}));
        tr.append(th);

        th = $('<th>');
        th.append($('<i>', { class: "fas fa-thermometer-three-quarters fa-lg"}));
        th.append($('<i>', { text: ' °F'}));
        tr.append(th);

        th = $('<th>');
        th.append($('<i>', { class: "fa fa-tint fa-lg"}));
        th.append($('<i>', { text: ' mm'}));
        tr.append(th);


        weather_table.append( thead.append(tr) );
    }

    var tbody = $('<tbody>');

    for (var i in weather_data) {
        var weather_daily_tr_prefix = 'weather_daily_tr_';
        var weather_detail_tr_prefix = 'weather_detail_tr_';

        var day_entry = weather_data[i];
        var date = day_entry.timestamp;
        var date_options = { weekday: 'short', day: 'numeric', hour: undefined, hour12: false, minute: undefined, second: undefined, timeZoneName: undefined};
        var date_str = date.toLocaleDateString(undefined, date_options);

        var wtr = $('<tr>', {
            id: weather_daily_tr_prefix + day_entry.datekey,
            class: 'weather_table_daily_tr'
        });

        wtr.append($('<td>', {
            html: date_str,
        }));

        wtr.append($('<td>', {
            html: '<i class="mintemp">'+K2degC(day_entry.mintemp)+'</i>/<i class="maxtemp">'+K2degC(day_entry.maxtemp)+'</i>' ,
        }));

        wtr.append($('<td>', {
            html: '<i class="mintemp">'+K2degF(day_entry.mintemp)+'</i>/<i class="maxtemp">'+K2degF(day_entry.maxtemp)+'</i>' ,
        }));

        wtr.append($('<td>', {
            text: Math.round(day_entry.accum_precip*10)/10,
        }));

        tbody.append(wtr);


        // then add an additional entry for the detailed view
        var detail_tr = $('<tr>', { id: weather_detail_tr_prefix + day_entry.datekey});
        var detail_td = $('<td>', { colspan: 4 }).appendTo(detail_tr);
        var detail_div = $('<div>').appendTo(detail_td);
        var detail_table = $('<table>', {class: 'table table-condensed table-responsive'}).appendTo(detail_div);

        var detail_table_header = $('<thead>').appendTo(detail_table);
        detail_table_header.append(render_detailed_weather_table_header(trans));

        var detail_table_body = $('<tbody>').appendTo(detail_table);
        for (var i in day_entry['3h_data']) {
            var date_options = {hour: '2-digit', hour12: false, minute: '2-digit', second: undefined, timeZoneName: undefined };
            var detail_row = render_detailed_weather_table_row(day_entry['3h_data'][i], date_options);
            detail_row.addClass('active');
            detail_table_body.append(detail_row);
        }

        detail_tr.hide();
        tbody.append(detail_tr);


        // handle opening / closing of detail rows
        wtr.on('click', function (e) {
            var tr = $(e.target).closest('tr');
            var dk = tr.attr('id').split(weather_daily_tr_prefix)[1];
            var detail_tr = $("tr[id*='" + weather_detail_tr_prefix + dk + "']");

            var is_visible = detail_tr.is(":visible"); // if visible: this is probably a click on an already opened row, lets close all and directly return...

            $("tr[id*='" + weather_detail_tr_prefix + "']").each(function (i, el) { $(el).hide(); }); // close all detail rows

            if(is_visible) {return;};
            detail_tr.show();
        });

    } // end loop over datetimes

    weather_table.append(tbody);

    return weather_table;
}

var render_detailed_weather_table_header = function (trans) {
    var tr = $('<tr>');

    var th = $('<th>');
    th.append($('<i>', { class: "far fa-clock fa-lg"}));
    th.append($('<span>', { class: "hidden-xs", text: ' '+trans.Date}));
    tr.append(th);

    /*
    th = $('<th>');
    th.append($('<i>', { class: "fas fa-tachometer-alt fa-lg"}));
    //th.append($('<span>', { class: "hidden-xs", text: ' '+trans.Wind}));
    th.append($('<i>', { text: ' m/s'}));
    tr.append(th);
    */
    th = $('<th>');
    th.append($('<i>', { class: "fas fa-thermometer-three-quarters fa-lg"}));
    //th.append($('<span>', { class: "hidden-xs", text: ' '+trans.Temperature}));
    th.append($('<i>', { text: ' °C'}));
    tr.append(th);

    th = $('<th>');
    th.append($('<i>', { class: "fas fa-thermometer-three-quarters fa-lg"}));
    th.append($('<i>', { text: ' °F'}));
    tr.append(th);

    th = $('<th>');
    th.append($('<i>', { class: "fa fa-tint fa-lg"}));
    th.append($('<i>', { text: ' mm'}));
    tr.append(th);

    //tr.append($('<th>', { text: trans.cloudfraction}));

    var sun_cloud_icon = $('<span>', {
        class: 'fa-stack',
    });
    sun_cloud_icon.append($('<i>', {
        class: "far fa-sun fa-stack-1x fa-lg",
        style: "padding-left: .7em;"
    }));
    sun_cloud_icon.append($('<i>', {
        class: "fa fa-cloud fa-stack-1x fa-lg",
    }));

    th = $('<th>').append(sun_cloud_icon);

    tr.append(th);

    th = $('<th>');
    th.append($('<span>', { class: "hidden-xs", text: ' '+trans.Description}));
    tr.append(th);
    return tr;
}

var render_detailed_weather_table_row = function (weather_entry, date_options) {
    if(typeof date_options === 'undefined') {
        var date_options = { weekday: 'short', day: 'numeric', hour: '2-digit', hour12: undefined, minute: '2-digit', second: undefined, timeZoneName: undefined};
    }

    var date = new Date(weather_entry.dt*1000);
    var date_str = date.toLocaleDateString(undefined, date_options);

    // if weekday, year, month, day are undefined, use Time instead
    if(date_options.weekday===undefined && date_options.month===undefined && date_options.day===undefined && date_options.year===undefined) {
        date_str = date.toLocaleTimeString(undefined, date_options);
    }

    //var active_on_even = (date.getDate()%2 == 0) ? 'active' : ''; // make even numbered dates active in table(gray background)
    var wtr = $('<tr>', {
        'data-toggle': 'tooltip',
        title: weather_entry.weather[0].description,
        //class: active_on_even,
    });
    wtr.append($('<td>', {
        html: date_str,
    }));

    /*
    wtr.append($('<td>', {
        text: Math.round(weather_entry.wind.speed),
    }));
    */

    wtr.append($('<td>', {
        text: K2degC(weather_entry.main.temp),
    }));

    wtr.append($('<td>', {
        text: K2degF(weather_entry.main.temp),
    }));

    var precip = 0;
    if(weather_entry['rain']!==undefined) { if(weather_entry.rain['3h']!==undefined) {precip += weather_entry.rain['3h']; }}
    if(weather_entry['snow']!==undefined) { if(weather_entry.snow['3h']!==undefined) {precip += weather_entry.snow['3h']; }}

    wtr.append($('<td>', {
        text: Math.round(precip*10)/10,
    }));

    for (var i_weather_situation in weather_entry.weather) {
        var weather_situation = weather_entry.weather[i_weather_situation];
        var situation_td = $('<td>');
        var icon_url = 'https://openweathermap.org/img/w/'+weather_situation.icon+'.png';

        var icon = $('<img>', {
            class: 'weather-icon',
            src: icon_url,
        });
        situation_td.append(icon);

        wtr.append(situation_td);
    }

    wtr.append($('<td>', {
        text: weather_entry.weather[0].description,
    }));
    return wtr;
}

var render_full_weather_table = function(weather_data, trans) {
    var weather_table = $('<table>', {
        id: "weather_table",
        class: 'table table-condensed table-hover'
    });

    { // thead
        var thead = $('<thead>');

        var tr = render_detailed_weather_table_header(trans);
        weather_table.append( thead.append(tr) );
    }

    var tbody = $('<tbody>');

    for (var i in weather_data.list) {
        var weather_entry = weather_data.list[i];
        var wtr = render_detailed_weather_table_row(weather_entry);

        tbody.append(wtr);
    } // end loop over datetimes

    weather_table.append(tbody);
    return weather_table;
}

var create_weather_table = function (div_id, lat, lon, trans) {

    var weather_url = "https://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&appid=3b3e82c7f90c8eb78d7b86fef36343dc";
    //var weather_url = "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=3b3e82c7f90c8eb78d7b86fef36343dc";

    if (trans===undefined) {
        var trans = {};
    }
    if(! trans.Date)          { trans.Date          = "Date";}
    if(! trans.Temperature)   { trans.Temperature   = "Temperature";}
    if(! trans.Description)   { trans.Description   = "Description";}
    if(! trans.Wind)          { trans.Wind          = "Wind";}
    if(! trans.cloudfraction) { trans.cloudfraction = "CloudFraction";}

    $.getJSON(weather_url, function(weather_data) {

        var daily_data = reorder_weather_data(weather_data);

        var daily_table = render_daily_weather_table(daily_data, trans);
        var full_table = render_full_weather_table(weather_data, trans);

        var dailytable_div = $('<div>').append(daily_table);
        var fulltable_div = $('<div>').append(full_table);

        $(div_id).empty().append(dailytable_div); //.append(fulltable_div);
        //$('#weather_dump').html(JSON.stringify(weather_data, null, 2));

    });

}
