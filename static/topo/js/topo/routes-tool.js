/**
 * ######### routes-tool.js #########
 */
"use strict";

function RoutesTool() {
    /*
      Class to manage route data.
      Use tool.fetch(url) to update data.
      tool.draw(map) to draw on map.

      Containers:
      routes: route data jsons.
      polylines: the leaflet polylines for the routes.
      labels: the leaflet markers and icons for the routes.
     */
    var self = this;

    self.labelIcon = function (iconSize, iconText, iconClasses){
        if(typeof iconClasses === 'undefined')
            iconClasses = 'route-label text-center';
        return L.divIcon({
            iconSize: iconSize,
            html: iconText,
            className: iconClasses,   // Set class for CSS styling
            iconAnchor: [iconSize.x/2, 0],
        });
    };

    self.bigIconSize = function (grade_string) {
            grade_string = $.trim(grade_string);
            var icon_size = new L.Point(30, 30);
            if(grade_string.length === 4 || grade_string.length === 5)
                icon_size = new L.Point(40, 30);
            else if(grade_string.length > 5)
                icon_size = new L.Point(50, 30);
            return icon_size;
    };

    self.addRoutelabelWithGrade = function (anchorpoint, label, grade_string){

        var bigIcon = self.labelIcon(
            self.bigIconSize(grade_string),
            label + "<br><span class='route-label-grade'>" + grade_string + "</span>");

        var smallIconSize = new L.Point(20, 20);

        var smallIcon = L.divIcon({
            iconSize: smallIconSize,
            html: label
        });

        // let us put small icon by default
        var marker = L.marker(anchorpoint, {
            icon: smallIcon,
        });

        marker.label = label;

        return {marker: marker, smallIcon: smallIcon, bigIcon: bigIcon};

    };

    self.orderRoutes = function (a, b){
        // routes without anchorpoint are on top
        if(a.geometry.anchorpoint == null)
            return -1;
        else if (b.geometry.anchorpoint == null)
            return 1;
        else if (a.geometry.anchorpoint[1] < b.geometry.anchorpoint[1])
            return -1;
        else if (a.geometry.anchorpoint[1] > b.geometry.anchorpoint[1])
            return 1;
        else
            return 0;
    };

    self.routes = {};
    self.map = null;

    // feature group to store polyline and labels
    self.featureGroup = new L.featureGroup();

    self.fetch = function (url) {
        /* Fetches route and geometry data.
           Data is stored in global routesTool.routes variable.
           The routes are ordered from left to right, those with no linestring on top.

           Args:
           url: URL of the REST api to retrieve the route objects.
           Returns:
           A promise.
        */
        self.routes = {};
        // fetch routes
        return $.get({
            url: url,
            success: function (response){
                // for convenience: store first(only) geometry in route.geometry
                for(var n=0; n<response.length; n++){
                    response[n].geometry = response[n].geometries[0];
                }
                // sort from left to right
                response.sort(self.orderRoutes);

		response.forEach(function(route, pos){
		    self.routes[route.id] = route;
		    self.routes[route.id].pos = pos + 1;
                });
	    }
        });

    };

    self._highlight_tablerow = function($table, label){
        $table.children("#route"+label)
            .addClass('row-highlight');
    };

    self._un_highlight_tablerow = function($table, label){
        $table.children("#route"+label)
            .removeClass('row-highlight');
    };

    self.simple_routetable = function (options) {
        /* Generates a simple routetable: label, name and grade

           Args:
               options: object should contain
                   id: html id,
                   namecol: column label of name column,
                   gradecol: columns label of grade column,
                   emptynote: note given if no routes are present
           Returns:
               jQuery object with table or note (no routes found).
         */

        if(Object.keys(self.routes).length > 0){
            var $table = $("<table>", {
                "class": "table table-condensed table-hover",
                "id": options.id
            });
            var $head = $("<thead>")
                .append($("<tr>")
                         .append($("<th>").text("#"))
                         .append($("<th>").text(options.namecol))
                         .append($("<th>")
                                 .append($("<span>", {class: 'fa fa-signal route-grade-icon'}))
                                 .append($("<span>").text(" " + options.gradecol))
                                )
                        );
            var $body = $($("<tbody>"));
            $.each(self.routes, function (num){
                var $row = $("<tr>").attr("id", "route" + num)
                    .append($("<td>").text(this.pos))
                    .append($("<td>").text(this.name))
                    .append($("<td>").text(this.get_full_grade_display))
                    .on('mouseover', function(){
                        self.highlight(num);
                        self._highlight_tablerow($table, num)
                    })
                    .on('mouseout', function (){
                        self.un_highlight(num);
                        self._un_highlight_tablerow($table, num)
                    });
                $body.append($row);
            });
            $table.append($head);
            $table.append($body.find('tr').sort(function (a, b){
		var tda = $(a).find('td:eq(0)').text();
		var tdb = $(b).find('td:eq(0)').text();
		return tda > tdb ? 1
		    : tda < tdb ? -1
		    : 0;
	    }));

            // highlighting

            $.each(self.routes, function (num) {
                var geoj = self.routes[num].fg;
                var label = self.routes[num].label;
                if(geoj){
                    geoj.on('mouseover', function (e) {
                        self._highlight_tablerow($table, num);
                    });
                    geoj.on('mouseout', function (e) {
                        self._un_highlight_tablerow($table, num);
                    });
                    label.marker.on('mouseover', function (e) {
                        self._highlight_tablerow($body, num);
                    });
                    label.marker.on('mouseout', function (e) {
                        self._un_highlight_tablerow($body, num);
                    });
                }
            });


            return $table;
        }else{
            // return a note that there are no routes if we did not fetch any
            return $("<p>").text(options.emptynote);
        }

    };

    self.draw = function(map, options){
        /*
          Draw routes on a map, together with labels.

          The routelabels can be found in routeTool.labels
          The polylines in routeTool.polylines.

          Args:
              map: leaflet map.
              options: object.
                  - curved (bool): draw curved polylines.
          Returns a promise.

        */
        options = options || {};
        options.curved = options.curved || false;

        // Override the tooltip labels when drawing lines
        L.drawLocal.draw.handlers.polyline.tooltip.start = gettext("Click on the map to start drawing a new route");
        L.drawLocal.draw.handlers.polyline.tooltip.cont  = gettext("Continue to add more points along the course of the route");
        L.drawLocal.draw.handlers.polyline.tooltip.end   = gettext("Finish the route by either clicking again on the last point, hitting the green button or click on the entry in the route-list");

        self.map = map;
        // clean existing routes and labels
        self.featureGroup.clearLayers();

        // show a loader button
        // first remove any existing buttons
        if (self.routeLoaderButton) {
            self.routeLoaderButton.remove();
        }

        self.routeLoaderButton = L.easyButton({
            id: 'route_loader_btn',
            states: [
                {
                    stateName: 'loading',
                    icon: 'fa fa-spinner fa-spin'
                },
                {
                    stateName: 'finished',
                    icon: 'glyphicon glyphicon-ok btn-success'
                }]
        }).addTo(map);

        $.each(self.routes, function (num){
	    // store to route to not mess up this in eachLayer
	    var route = this;

            // is there a feature or a feature collection?
            if(route.geometry !== null &&
	       route.geometry.geojson !== null){
                var geojson = route.geometry.geojson;

		// apply curving to linestrings only
		geojson.features.forEach(function(feature){
		    if(feature.geometry.type == "LineString"){
			if(options.curved){
			    feature.geometry.coordinates.forEach(function(el, idx, arr){
			         arr[idx] = [el[1], el[0]];
			    });
			    feature.geometry.coordinates = self._curve(feature.geometry.coordinates);
			    feature.geometry.coordinates.forEach(function(el, idx, arr){
			         arr[idx] = [el[1], el[0]];
			    });
			}
		    }
		});

		var fgroup = L.geoJSON(geojson, {
		    pointToLayer: (feature, latlng) => {
			if (feature.properties.radius) {
			    return new L.Circle(latlng, feature.properties.radius);
			} else {
			    return new L.Marker(latlng);
			}
		    },
		    onEachFeature: (feature, layer) => {

			var color = route.deprecated ? "#d3d3d3" : route.polylinecolor;

			layer.setStyle({
			    color: color,
			    className: 'route-geom-polyline'
			});

			// leaflet bug fix, issue 804
			layer.options.editing = layer.options.editing || (layer.options.editing = {});

			layer.addTo(self.featureGroup);

			// due to $.each we do not need a closure
			// num is only defined within the loop scope
			layer.on('mouseover', function (e) {
			    self.highlight(num);
			});

			layer.on('mouseout', function (e) {
			    self.un_highlight(num);
			});

			// update label when editing
			layer.on('draw:editvertex', function (e) {
			    self.updateRoutelabel(num);
			});

		    }
		});
		route.fg = fgroup;


                // we add a label [marker, smallicon, bigicon]
                var routelabel = self.addRoutelabelWithGrade(
                    route.geometry.anchorpoint,
                    route.pos,
                    route.get_full_grade_display
                );

                route.label = routelabel;

                // the marker is the first element
                routelabel.marker.on('mouseover', function (e) {
                    return self.highlight(num);
                });

                routelabel.marker.on('mouseout', function (e) {
                    return self.un_highlight(num);
                });

                routelabel.marker.addTo(self.featureGroup);

            }else{
                // we fill with empty values
                route.fg = null
                route.label = null;
            }

            if(!map.hasLayer(self.featureGroup))
                self.featureGroup.addTo(map);

            // set polyline and marker size
            self.fixZoomLabels(map);
            self.fixZoomPolylines(map);

        });

        // After a Timeout, remove the loading spinner...
        self.routeLoaderButton.state('finished');
        self.routeLoaderButton.enable();

        $(self.routeLoaderButton.getContainer()).addClass('fade_wall_loader');

    };

    self._curve = function (linestring){
        return linesmoothing.curve_linestring(linestring, 20);
    };


    self.on = function (eventString, eventCallback) {
        self.eachGeoJ(function (line){
            line.on(eventString, eventCallback);
        });
    };

    self.eachGeoJ = function (doThis){
        for(var id in self.routes){
            var geoj = self.routes[id].fg;
            if(geoj)
                doThis(geoj);
        }
    };

    self.eachLabel = function (doThis){
        for(var id in self.routes){
            var label = self.routes[id].label;
            if(label)
                doThis(label);
        }
    };

    self.disableEdit = function () {
        self.eachGeoJ(function (geoj) {
	    geoj.eachLayer(function(layer){
		layer.editing.disable();
	    })
        });
    };

    self.fetchAndDraw = function(url, map, options){
        return self.fetch(url).then(function(){
            self.draw(map, options);
        });
    };

    self.fixZoom = function(){
        if(self.map){
            self.fixZoomPolylines(self.map);
            self.fixZoomLabels(self.map);
        }
    };

    self.highlight = function (num){
        if(self.map){
            var currentZoom = self.map.getZoom();
            var geoj = self.routes[num].fg;
            // should be null if not set
            if(geoj){
		geoj.eachLayer(function(layer){
                    $(layer._path).css("stroke-width", self._strokeWidth(currentZoom) * 2);

                    // move to top
                    layer.bringToFront();
		});
                // label highlighting
                var mean_zoom_lvl = self._zoom_level_threshold();

                var newIcon = null;
                if(self.map.getZoom() <= mean_zoom_lvl) {
		    var oldIcon = self.routes[num].label.smallIcon;
		    newIcon = self.labelIcon(
                        new L.Point(oldIcon.options.iconSize.x + 5, oldIcon.options.iconSize.y + 5),
                        oldIcon.options.html,
                        'route-label-highlight text-center'
		    );
                }else{
		    var oldIcon = self.routes[num].label.bigIcon;
		    newIcon = self.labelIcon(
                        new L.Point(oldIcon.options.iconSize.x*1.4, oldIcon.options.iconSize.y + 10),
                        oldIcon.options.html,
                        'route-label-highlight text-center'
		    );

                }
                // new icon
                // replace
                self.routes[num].label.marker.setIcon(newIcon);
                // read z-index of on-top class for labels
                var TOP_Z_INDEX = $(".on-top").css("z-index");
                // ... there is no bringToFront for markers :(
                $(self.routes[num].label.marker._icon).css("z-index", TOP_Z_INDEX);
            }
        }

    };

    self.un_highlight = function(num){
        if(self.map){
            var currentZoom = self.map.getZoom();
            var geoj = self.routes[num].fg;
            if(geoj){
		geoj.eachLayer(function(layer){
                    $(layer._path).css("stroke-width", self._strokeWidth(currentZoom));
		});
                // set auto css of markers (move to back)
                $(self.routes[num].label.marker._icon).css("z-index", "auto");

                self.routes[num].label.marker._icon.style['background-color'] = '';
                // labels
                self.routes[num].label.marker.setZIndexOffset(0);
                var mean_zoom_lvl = self._zoom_level_threshold();
                var icon = self.routes[num].label.bigIcon;
                if(self.map.getZoom() <= mean_zoom_lvl) {
                    icon = self.routes[num].label.smallIcon;
                }
                self.routes[num].label.marker.setIcon(icon);
            }
        }
    };

    self._zoom_level_threshold = function(){
        if(self.map)
            return (self.map.getMaxZoom() + self.map.getMinZoom()) / 2;
    };

    self.focus = function(num){
        if(! self.map){ // no map. We display a warning.
            console.log("[Routes Tool] Focus is called while no map instance is initialized.");
            return;
        }
        if(typeof num === 'undefined') { // if there is no specific route which is to be focused
            var bounds;
            if(self.featureGroup !== null) { // there are other routes, though
                bounds = self.featureGroup.getBounds();
            }
            if (!bounds.isValid()) {
                bounds = self.map.getBounds();
            }
            self.map.fitBounds(bounds);
            return;
        }
        var geoj = self.routes[num].fg;
        if(geoj)
            self.map.fitBounds(geoj.getBounds());
        else{
            console.log("[Routes Tool] Focus is called with no valid route geometry id: " + num);
            self.map.fitWorld();
        }
    };

    self.fixZoomLabels = function (){
        $.each(self.routes, function(){
	    var label = this.label;
            if(label && self.map){
                // be careful: this is [marker, icon, icon] now
                var mean_zoom_lvl = self._zoom_level_threshold();
                if(self.map.getZoom() <= mean_zoom_lvl) {
                    label.marker.setIcon(label.smallIcon);
                } else {
                    label.marker.setIcon(label.bigIcon);
                }
            }
        });

    };

    self.fixZoomPolylines = function (){
        if(self.map){
            var currentZoom = self.map.getZoom();
            $(".route-geom-polyline").css("stroke-width", self._strokeWidth(currentZoom));
        }
    };

    self.numFromGeom = function(geom){
        for(var id in self.routes){
            if(self.routes[id].geometry.id == geom)
                return id;
        }

        console.log("No route found with this geomid.");
        return -1;
    };

    self._strokeWidth = function(zoom) {
        //console.log('zoom',zoom,'stroke width',2 + Math.pow(1.25, zoom));
        return 2 + Math.pow(1.2, zoom);
    };

    self._getLatLngs = function(geoj){
	var latlngs = [];
	geoj.eachLayer(function(lyr){
	    if(typeof lyr.getLatLngs !== "undefined"){
		latlngs.push(...lyr.getLatLngs());
	    }else{
		latlngs.push(lyr.getLatLng());
	    }
	});
	latlngs.sort(function(a, b){
	    return a.lat - b.lat;
	});
	return latlngs;
    }

    self.getBasePoint = function(num){
        var geoj = self.routes[num].fg;
        if(geoj){
	    var ltln = self._getLatLngs(geoj);
            return ltln[0];
        }
    };

    self.getTopPoint = function(num){
        var geoj = self.routes[num].fg;
        if(geoj){
	    var ltln = self._getLatLngs(geoj);
            return ltln[ltln.length - 1];
        }
    };

    self.updateRoutelabel = function(num){
        var label = self.routes[num].label;
        var geoj = self.routes[num].fg;
        if(geoj && label){
            var basePoint = self.getBasePoint(num);
            label.marker.setLatLng(basePoint);
        }
    };

}
