/**
 * ######### initializers.js #########
 *
 * This file should contain all globally used initialization functions.
 */
"use strict";

var globalVars = {};

function initBootstrapComponents() {
    $('[data-toggle="tooltip"]').tooltip({html : true,});
    $('[data-toggle="popover"]').popover({html : true,});
}

//function initOrientationChangeWatcher() {
//    globalVars.lastCheckWidth = $(window).width();
//    setInterval(funcFactoryReflowOnOrientationChange(globalVars), 500);
//}

//function initSlideout() {
//    return
//    var slideoutBar = $('.slideout-bar');
//    var slideoutContent = $('#mapobjects_info');
//    var layoutContent = $('#content');
//    var bodyContent = $('#body-content');
//    var slideoutWrapper =$('.slideout-wrapper');
//    globalVars.slideout = new Slideout(slideoutBar, slideoutContent, slideoutWrapper, bodyContent, layoutContent);
//}


// initialize globally on document ready
$(document).ready(function () {
    initBootstrapComponents();
    //initOrientationChangeWatcher();

    // Replace ISO timestamps by localized version, see also the *tzify* templatetag in miroutes
    $(".iso8601-timestamp").each(function(){
        var dt = new Date($(this).text());
        $(this).text(dt.toLocaleString());
    });
});
