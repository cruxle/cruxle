/**
 * ######### baselayout.js #########
 *
 * All layout specific functions go here.
 */
"use strict";

var ALERT_DELAYS = {
    "alert-success" : 10000,
    "alert-info" : 10000,
    "alert-warning" : 30000,
    "alert-danger" : 60000,
}

function moveSearchbarOnMobile() {
    if (isMobile()) {
        //$(".searchbar-mover").detach().appendTo(".mobile-search");
    }
}

function moveFlipNavOnMobile() {
    if (isMobile()) {
        $(".flip-nav").detach().appendTo("#header");
    }
}

function autoHideAlerts() {
    var alerts = $(".alert");
    alerts.each(function () {
        var msg = $(this);
        if (! msg.hasClass('alert-danger')) { // dont remove danger warnings
            // Search for a corresponding delay time
            var delay_time = 10000;
            for(var i = 0; i < msg[0].classList.length; i++) {
                delay_time = ALERT_DELAYS[msg[0].classList[i]] || delay_time;
            };
            // and queue up the removal
            msg.delay(delay_time).slideUp(500, function () {
                msg.remove();
            });
        };
    });
}

function append_message(text, type, fade_time) {
    /* Use this function to show messages dynamically. */
    var last_msg = $("#messages").children().last()[0];

    if( last_msg && last_msg.classList.contains('alert-'+type) ) { // same class type already present... we overwrite that...
        last_msg.remove();
    }

    if(typeof fade_time === "undefined") {
        var delay_time = ALERT_DELAYS['alert-' + type] || 10000;
    } else {
        var delay_time = fade_time;
    }

    var msg = $('<div>')
        .attr('class', 'alert alert-' + type + ' alert-dismissable fade in')
        .html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + escapeHtml(text))
        .delay(delay_time)
        .slideUp(500, function () {
            $(this).remove();
        });

    $("#messages").append(msg);
}

function forceMobileContentWidth() {
    if(isMobile()) {
        //$(function () {
        //    setTimeout(function() {
        //        $('#content').width($(window).width());
        //        $('#content').css('margin-left', $('#body-content').width() - screen.width + 'px');
        //    }, 50);
        //});
    }
}

function handle_slideout_toggle_button() {
    var $slideout = $("#slideoutwrapper");
    var toggle_btns = $('.slideout-toggle-btn,#slideout-toggle-area');

    // initially hide the slideout
    close_slideout();

    if(element_is_empty($("#slideout"))) {
        // in case that we cant find a slideout or its empty, lets hide the button
        $slideout.hide();
        toggle_btns.each(function (index) {
            $(this).hide();
        });
        return;
    }

    toggle_btns.each(function (index) {
        var $button = $(this);
        $button.on('click mousedown touchstart', function (e) {
            e.preventDefault();
            toggle_slideout();
        });
    });
}

function toggle_slideout() {
    var is_closed = $("#slideoutwrapper").hasClass('slideout-hidden');
    if(is_closed) {
        open_slideout();
    }else{
        close_slideout();
    }

}

function open_slideout() {
    var $slideout = $("#slideoutwrapper");
    $slideout.removeClass();
    $slideout.addClass("full-height col-xs-11 col-sm-7 col-md-6 col-lg-4 slideout-open");
    $slideout.css("position", 'relative');

    var toggle_btns = $('.slideout-toggle-btn');
    toggle_btns.each(function (index) {
        $(this).children("span")
            .removeClass("glyphicon-triangle-right")
            .addClass("glyphicon-triangle-left")
            .addClass("btn-mitopo"); // have to add btn-mitopo here because css styling from bootstrap glyphclass somehow has precedence and btn-mitopo is not evaluated right away
        }
    );
    $(document).trigger("mitopo_layout_change");
}


function close_slideout() {
    var $slideout = $("#slideoutwrapper");
    $slideout.removeClass();
    $slideout.addClass("full-height slideout-hidden on-top");
    $slideout.css("position", 'absolute'); // this prevents the slideout to take up space and hence prevents the maps to wrap around

    var toggle_btns = $('.slideout-toggle-btn');
    toggle_btns.each(function (index) {
        $(this).children("span")
            .removeClass("glyphicon-triangle-left")
            .addClass("glyphicon-triangle-right")
            .addClass("btn-mitopo"); // have to add btn-mitopo here because css styling from bootstrap glyphclass somehow has precedence and btn-mitopo is not evaluated right away
        }
    );
    $(document).trigger("mitopo_layout_change");
}

$(document).ready(function () {
    handle_slideout_toggle_button();
    //moveSearchbarOnMobile();
    //moveFlipNavOnMobile();
    //autoHideAlerts();
    //forceMobileContentWidth();
});
