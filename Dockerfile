FROM ubuntu:focal
ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND "noninteractive"
RUN apt-get clean && apt-get update && apt-get install -y libgdal-dev ruby-sass netcat postgresql-client graphviz gettext ghostscript chromium-browser chromium-chromedriver git imagemagick wget curl
RUN apt-get install -y python3 python3-pip python3-gdal python3-numpy

# Install latest hugin build
#RUN apt-get install -fy software-properties-common && add-apt-repository -y ppa:hugin/hugin-builds && add-apt-repository -y ppa:hugin/nightly && apt-get update && apt-get -fy install hugin enblend
RUN apt-get install -fy software-properties-common && apt-get update && apt-get -fy install hugin enblend

# Force to use python 3 from here on
# RUN rm -f /usr/bin/python && ln -s /usr/bin/python3 /usr/bin/python

RUN mkdir /code
WORKDIR /code

# Install nodejs and npm:
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -fy nodejs

RUN npm install -g bower

RUN npm -g install yuglify && sed -i 's+#!/usr/bin/env node.*+#!/usr/bin/npm --stack_size=100000+' /usr/bin/yuglify

# Install PNG-Quantify (lossy png compression)
RUN cd; git clone --depth 1 --branch 2.17.0 https://github.com/kornelski/pngquant.git; cd pngquant/; ./configure; make -j install; ln -s /usr/local/bin/pngquant /usr/bin/pngquant; pngquant --version


# Allow Imagemagick to use more than 1GB disk space to convert and enhance images... this is a stupid low value from debian
RUN perl -i -ple 's/<policy domain="resource" name="disk" value="(.*)"\/>/<policy domain="resource" name="disk" value="30GiB"\/>/' /etc/ImageMagick-6/policy.xml
RUN perl -i -ple 's/<policy domain="resource" name="width" value="(.*)"\/>/<!-- <policy domain="resource" name="width" value="$1"\/> -->/' /etc/ImageMagick-6/policy.xml
RUN perl -i -ple 's/<policy domain="resource" name="height" value="(.*)"\/>/<!-- <policy domain="resource" name="height" value="$1"\/> -->/' /etc/ImageMagick-6/policy.xml
RUN perl -i -ple 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<policy domain="coder" rights="read" pattern="PDF" \/>/' /etc/ImageMagick-6/policy.xml

# Add montserrat font so pdf exports use correct font
RUN mkdir -p /root/.fonts
ADD static/topo/fonts/Montserrat* /root/.fonts/
RUN fc-cache -f -v

# Install headless chromium

ADD requirements.txt /code/
RUN python3 -m pip install --upgrade pip setuptools
RUN pip3 install -r requirements.txt
ADD docker-compose.yml /code/
ADD Dockerfile /code/
