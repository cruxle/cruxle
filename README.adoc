= cruxle.org - Collaborative Climbing Platform

[link=https://cruxle.org]
image::./static/topo/img/cruxle_logo2.png[]

https://cruxle.org[cruxle.org] is a database for climbing metadata
and climbing topos presented as overlays on real zoomable wall photographs.

It is the wikipedia idea applied to topos and climbing information.
Everything can be edited by everyone - no paywalls.

You can search for climbing topos via an intuitive map view or via text
search and you can contribute by uploading new topos or adding routes and
descriptions to existing ones (all changes to objects are in version control).

== Contribute

Contributions are welcome! Please file an issue before you start working on
a pull request, so we can jump in and support you.

If you know about Django or Javascript and have some spare time but don't know
where to start, don't hesitate to open a request-for-work in the issue tracker
and we can discuss what would be cool and how you can be of help.

== Developer Documentation

The developer documentation contains information on how to setup and run the
software.

link:./docs/developer/developer.adoc[Developer Documentation]

== License

link:LICENSE[GNU AFFERO GENERAL PUBLIC LICENSE Version 3]