import uuid
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Thread(models.Model):
    """Threads group messages."""
    party = models.ManyToManyField(User, through='Status')
    #subject = models.CharField(max_length=100, blank=True, null=True)

    @property
    def subject(self):
        last_msg = self.message_set.last()
        if last_msg:
            return "{}: {}".format(last_msg.sender, last_msg.body)
        else:
            return _("No Messages.")

class Message(models.Model):
    """User to User message object.
    """
    body = models.CharField(max_length=4096, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    sender = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)

    thread = models.ForeignKey(Thread, blank=True, null=True, on_delete=models.CASCADE)

class Status(models.Model):
    """Flag thread status per user."""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    new_msg_count = models.SmallIntegerField(default=0)
