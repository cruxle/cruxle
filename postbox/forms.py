from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Submit, Layout, Field, Fieldset, Hidden,
    Button, HTML, Row, MultiField, Div)
from crispy_forms.bootstrap import (AppendedText, PrependedText, FormActions)

from .models import Message

class MessageForm(forms.Form):
    message = forms.CharField(
        label='Message:',
        widget=forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2'})
    )
    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-1 col-md-2 col-xs-12'
        self.helper.field_class = 'col-lg-7 col-md-8 col-xs-12'

        self.helper.layout = Layout()

        self.helper.layout.append('message')

        self.helper.form_id = 'message-form'
        self.helper.form_method = 'post'

        send_btn = HTML(
            """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-envelope"></span> {% trans "Send" %}
            </button>
            """)
        cancel_btn = HTML(
            """{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-default">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>""")

        self.helper.layout.append(
            FormActions(send_btn, cancel_btn))
