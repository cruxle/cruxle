import json
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.models import User
from django.utils.translation import ugettext, ugettext_lazy as _
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse, translate_url
from django.http import JsonResponse

from django.db.models import F

from users.models import UserProfile

from .models import Message, Thread
from edit_spot.views import go_to

from .forms import MessageForm

from .utils import send_private_message, add_message_to_thread

@login_required
def send_message(request, user_id):
    """Send message to user."""
    dest = get_object_or_404(User, pk=user_id)

    next_page = request.GET.get('next')

    if request.POST:
        form = MessageForm(request.POST)
        if form.is_valid():
            msg = form.cleaned_data['message']
            send_private_message(request.user, dest, msg)

            return go_to(next_page)

    form = MessageForm()
    context = {
        "form": form,
        "receiver": dest,
        "next_page": next_page
    }
    return render(request, 'postbox/send_message.html', context)

@login_required
def reply(request, thread_id):
    """Send message to user."""
    thread = get_object_or_404(Thread, pk=thread_id)

    if request.user not in thread.party.all():
        from django.http import HttpResponseForbidden
        return HttpResponseForbidden()

    if request.method == 'POST':
        message = json.loads(request.body.decode('utf-8'))['reply']
        add_message_to_thread(message, request.user, thread)
        payload = {
            'message': 'Saved comment',
            'success': True
        }
        return JsonResponse(payload)

    payload = {
        'message': 'Use JSON POST for reply.',
        'success': False
    }
    return JsonResponse(payload)

@login_required
def inbox(request):
    """Show Inbox to user."""
    open_thread = request.GET.get('thread')

    return render(request, 'postbox/inbox.html', {"open_thread": open_thread})

@login_required
def thread_read(request, thread_id):
    """Toggle read status on thread."""
    thread = get_object_or_404(Thread, pk=thread_id)

    if request.user not in thread.party.all():
        from django.http import HttpResponseForbidden
        return HttpResponseForbidden()

    status = thread.status_set.filter(user=request.user).update(new_msg_count=0)
    payload = {
        'message': 'Flagged thread as read.',
        'data': None,
        'success': True
    }
    return JsonResponse(payload)
