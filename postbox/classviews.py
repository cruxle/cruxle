from rest_framework.generics import ListAPIView
from django.db.models import Max

from .serializers import ThreadSerializer

class ThreadList(ListAPIView):
    serializer_class = ThreadSerializer

    def get_queryset(self):
        user = self.request.user
        return user.thread_set.annotate(last_id=Max('message__id')).order_by('-last_id')
