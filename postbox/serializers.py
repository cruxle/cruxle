import html
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from django.db import models
from django.urls import reverse

from users.serializers import UserSerializer, UserSerializer_noprofile
from .models import Thread, Message
from miroutes.utils import replace_objs

class MessageSerializer(serializers.ModelSerializer):
    """Serializer Messages."""
    sender = UserSerializer_noprofile()
    body = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Message
        fields = [
            'sender',
            'created_at',
            'body'
        ]
    def get_body(self, message):
        msg = html.escape(message.body, quote=False)
        msg = replace_objs(msg, html=True, absolute_urls=False)
        return msg

class ThreadSerializer(serializers.ModelSerializer):
    """Serialize inbox threads."""
    unread_msgs = serializers.SerializerMethodField(read_only=True)
    reply_to = serializers.SerializerMethodField(read_only=True)
    toggle_url = serializers.SerializerMethodField(read_only=True)
    messages = serializers.SerializerMethodField(read_only=True)
    users = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Thread
        fields = [
            'id',
            'unread_msgs',
            'messages',
            'users',
            'reply_to',
            'toggle_url'
        ]

    def get_messages(self, thread):
        qset = thread.message_set.order_by('-id')
        return MessageSerializer(qset, many=True).data

    def get_unread_msgs(self, thread):
        request = self.context.get('request', None)
        if request:
            status = thread.status_set.filter(user=request.user).first()
            return status.new_msg_count
        return thread.status_set.aggregate(models.Sum('new_msg_count'))['new_msg_count__sum']


    def get_users(self, thread):
        request = self.context.get('request', None)
        party = thread.party.all()
        if request:
            party = party.exclude(pk=request.user.pk)
        serializer = UserSerializer(party, many=True)
        return serializer.data

    def get_reply_to(self, thread):
        return reverse('postbox:reply', kwargs={'thread_id': thread.pk });

    def get_toggle_url(self, thread):
        return reverse('postbox:toggle_read', kwargs={'thread_id': thread.pk });
