"""Utils for postbox app. Methods to send messages to a party and a private chat."""
import html
import re

from django.contrib.auth.models import User
from django.db.models import Count, F
from django.urls import reverse
from django.conf import settings
from django.templatetags.static import static
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _

from postbox.models import Thread, Message, Status
from mitopo.utils import absolute_url
from miroutes.utils import replace_objs


def cleanhtmltags(msg):
    """
    clean a html output so that it is more appropriate as a postbox msg or email
      * remove everything that is enclosed in <> brackets
      * make multiple new lines to just one
      * remove new lines at the beginning and the end of the line
    """
    msg = re.sub(re.compile('<.*?>'), '', msg)
    msg = re.sub(re.compile('(\s*\n\s*)+'),'\n',msg) # and make multiple newlines to one
    msg = re.sub(re.compile('^[\s\n]+'),'',msg) # remove newlines at the beginning
    msg = re.sub(re.compile('[\s\n]+$'),'',msg) # and at the end
    return msg


def mail_to(receiver, subject, template, options={}, reply="noreply@cruxle.org", use_new_mail=False):
    """Send mail using a template."""
    options['user'] = receiver

    # non-html message
    options['html'] = False
    # replace objs in message if present
    msg = options.get('message')
    if msg:
        msg_escaped = html.escape(cleanhtmltags(msg), quote=False)
        options['message'] = replace_objs(msg_escaped, options['html'])
    email_body_plain = render_to_string(template, options)

    options['html'] = True
    if msg:
        options['message'] = replace_objs(msg, options['html'])
    # image url has to be absolute!
    options['imgurl'] = absolute_url(static("topo/img/cruxle_logo2.png"))

    email_body_html = render_to_string(template, options)

    adress = receiver.userprofile.new_mail if use_new_mail else receiver.email

    try:
        send_mail(
            subject,
            email_body_plain,
            reply,
            [adress, ],
            html_message=email_body_html)
    except Exception as e:
        # One should find the exact error that is thrown here if the
        # backend mail tool refuses to collaborate.
        print("Error sending mail to {}".format(receiver.username))
        raise e  # reraise the error, if we cant send out mails, this is bad!
        pass

def notify_party(text, sender, thread):
    for dest in thread.party.exclude(pk=sender.pk):
        if dest.userprofile.message_mail:
            subject = _("[cruxle] Message from {}").format(sender.username)
            # we have to generate absolute URLs here
            link = absolute_url(reverse('postbox:inbox') + "?thread={}".format(thread.pk))
            options = {
                "sender": sender,
                "message": text,
                "link": link,
            }
            mail_to(dest, subject, 'email/message.html', options)


def add_message_to_thread(text, sender, thread, html=False):
    """Add the message to the thread and notify the party."""
    # create message
    msg = Message(body=cleanhtmltags(text), sender=sender, thread=thread)
    msg.save()
    # increase unread msg count of thread on receiver side
    thread.status_set.exclude(user=sender).update(new_msg_count=F('new_msg_count') + 1)
    # notify party
    notify_party(text, sender, thread)


def send_private_message(sender, receiver, body, html=False):
    """Initiate or resume a 2 person chat."""
    # is there a private thread?
    # annotate with number of users in thread, filter by sender and receiver and limit to max 2 users.
    thread = Thread.objects.annotate(count=Count('party')).filter(party=sender).filter(party=receiver, count=2).first()
    # if not, create
    if not thread:
        thread = Thread()
        thread.save()
        # status is a property of the thread-user relation
        send_status = Status(user=sender, thread=thread)
        send_status.save()
        recv_status = Status(user=receiver, thread=thread)
        recv_status.save()

    add_message_to_thread(body, sender, thread, html=html)

    return thread

def message_from_cruxle(user, body, html=False):
    """Send a message to user from cruxle via postbox interface."""
    mtpo = User.objects.get(username="cruxle")
    # see if mitopo thread exists (check also if this is )
    send_private_message(mtpo, user, str(body), html=html)
