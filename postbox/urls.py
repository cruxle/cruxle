from django.conf.urls import *

from postbox import views
from postbox import classviews

app_name = 'postbox'

urlpatterns = [
    url(r'^inbox/$', views.inbox, name='inbox'),
    url(r'^send(?P<user_id>\d+)/$', views.send_message, name='send_message'),
    url(r'^replyTo(?P<thread_id>\d+)/$', views.reply, name='reply'),
    url(r'^toggle(?P<thread_id>\d+)/$', views.thread_read, name='toggle_read'),
    url(r'^rest/threads$', classviews.ThreadList.as_view(), name='rest_threads'),
]
