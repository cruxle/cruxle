from django.conf import settings

import os

import requests
import shutil

from PIL import Image

import users.models


def get_avatar(backend, strategy, details, response,
        user=None, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = "http://graph.facebook.com/%s/picture?type=large"%response['id']
        # if you need a square picture from fb, this line help you
        url = "http://graph.facebook.com/%s/picture?width=150&height=150"%response['id']
    if backend.name == 'google-oauth2':
        url = response.get('picture',None)
    if url and not user.userprofile.avatar.name:
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            file_path = os.path.join(settings.MEDIA_ROOT, users.models.get_avatar_upload_path(user.userprofile, 'foo.jpg'))
            users.models.ensure_dir(file_path)
            with open(file_path, 'wb') as f:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)
            user.userprofile.avatar = file_path
            user.save()
            user.userprofile.thumbnail()