from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from users.models import User, UserProfile
from reversion.models import Revision, Version
from django.contrib.contenttypes.models import ContentType
from django.templatetags.static import static
from django.urls import reverse

from generic_markers.models import SimpleMarker, PitchMarker, TextMarker, RelationalMarker
from miroutes.models import Ascent, Wall, WallView, Route, RouteGeometry, Userpic, Tiler

from edit_spot.utils import deserialize_version

class UserProfileSerializer(serializers.ModelSerializer):
    """Combine fields from User and UserProfile class for serialization."""
    name = serializers.SerializerMethodField(read_only=True)
    model = serializers.SerializerMethodField(read_only=True)
    id = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = UserProfile
        fields = [
            'id',
            'name',
            'thumb_url',
            'puzzle_pieces',
            'model',
        ]
    def get_id(self, userprofile):
        """Id of user one-to-one object."""
        return userprofile.user.pk

    def get_name(self, userprofile):
        """Name is stored in user one-to-one object."""
        return userprofile.user.username

    def get_model(self, userprofile):
        """We deliver the model name for search where this field is required."""
        return UserProfile.__name__.lower()


class UserSerializer(serializers.ModelSerializer):
    userprofile = UserProfileSerializer(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model=User
        fields = [
            'id',
            'userprofile',
            'url',
        ]

    def get_url(self, user):
        return reverse('users:user_detail', kwargs={'user_id': user.id, });


class UserSerializer_noprofile(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model=User
        fields = [
            'id',
            'url',
            'username',
        ]

    def get_url(self, user):
        return reverse('users:user_detail', kwargs={'user_id': user.id, });


def annotate_version(version):
    """
    Create an activity feed entry for one version of a model.
    I.e. one revision may have a multiple of these here.
    """
    ctype = lambda model: ContentType.objects.get_for_model(model)
    #rev_obj = lambda version: version._object_version.object # this really reverts an object and we presume the usage is to be avoided...

    if version.content_type == ctype(RouteGeometry):
        # it is very likely that the actual route geometry object does not exist anymore
        # (it is recreated on publish)
        # thus we have to look in the serialized data
        data = deserialize_version(version)
        route = Route.objects.filter(pk=data["fields"]["route"]).first()
        if route:
            return {
                    'icon_url' : static("generic_markers/img/route_icon.png"),
                    'url' : reverse('miroutes:route_detail', kwargs={'route_id': route.pk}),
                    }

    elif version.content_type == ctype(Route):
        if version.object:
            route = version.object
            return {
                    'icon_url': static("generic_markers/img/route_icon.png"),
                    'url'     : reverse('miroutes:route_detail', kwargs={'route_id': route.pk}),
            }

    elif version.content_type in [ctype(RelationalMarker), ctype(TextMarker), ctype(PitchMarker)]:
        # it is very likely that the actual marker object does not exist anymore
        # (it is recreated on publish)
        # thus we have to look in the serialized data
        data = deserialize_version(version)
        if "related_wall" not in data["fields"]:
            # print("NO WALL FOUND: {}".format(data))
            return
        wall = Wall.objects.filter(pk=data["fields"]["related_wall"]).first()
        if wall:
            return {
                    'icon_url' : static(
                        RelationalMarker._icon_paths[data["fields"]["markertype"]]),
                    'url' : reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk}),
                    }

    elif version.content_type == ctype(SimpleMarker):
        return # disable generation of subitems in activity feed here... this is too much info...
        if version.object:
            marker = version.object
            return {
                    'icon_url' : marker.icon_url,
                    }

    elif version.content_type == ctype(WallView):
        if version.object:
            wall = version.object.wall
            return {
                    'icon_url' : wall.tiler.thumbnail_url_s,
                    'url' : reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk}),
                    }

    elif version.content_type == ctype(Wall):
        if version.object:
            wall = version.object
            return {
                    'icon_url' : wall.tiler.thumbnail_url_s,
                    'url' : reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk}),
                    }
    elif version.content_type == ctype(Tiler):
        if version.object:
            tiler = version.object
            result =  {'icon_url' : tiler.thumbnail_url_s}
            if hasattr(tiler, 'wall'):
                result['url'] = reverse('miroutes:wall_detail', kwargs={'wall_id': tiler.wall.pk}),
            return result
    return


class RevisionContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = '__all__'


class RevisionVersionSerializer(serializers.ModelSerializer):
    content_type = RevisionContentTypeSerializer(read_only=True)
    annotation = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Version
        fields = [
            'id',
            'content_type',
            'object_repr',
            'serialized_data',
            'annotation',
        ]

    def get_annotation(self, version):
        return annotate_version(version)

class RevisionSerializer(serializers.ModelSerializer):
    """Serialize Revisions"""
    versions = RevisionVersionSerializer(source='version_set', many=True)

    class Meta:
        model = Revision
        fields = [
            'id',
            'comment',
            'date_created',
            'user_id',
            'versions',
        ]


class RevisionSerializer_with_User(serializers.ModelSerializer):
    """Serialize Revisions including a resolution of the given User"""
    user = UserSerializer(read_only=True)
    versions = serializers.SerializerMethodField()

    class Meta:
        model = Revision
        fields = [
            'id',
            'comment',
            'date_created',
            'user_id',
            'user',
            'versions',
        ]

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('user', 'version_set')
        return queryset

    def get_versions(self, obj):
        if hasattr(obj, "versions_to_del"):
            versions = obj.version_set.exclude(pk__in=obj.versions_to_del)
        else:
            versions = obj.version_set.all()
        serializer = RevisionVersionSerializer(versions, many=True)
        return serializer.data


class UserpicSerializer(serializers.ModelSerializer):
    on_wall = serializers.StringRelatedField(many=True)
    on_route = serializers.StringRelatedField(many=True)

    created_by = UserSerializer(read_only=True)
    subject = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Userpic
        fields = [
            'id',
            'on_wall',
            'on_route',
            'description',
            'external_url',
            'img',
            'thumbnail',
            'pdf_file',
            'created_at',
            'created_by',
            'subject'
        ]

    def get_subject(self, userpic):
        if(userpic.content == 'info'):
            return _("added Info Material")
        else:
            return _("added Image")
