from django.conf.urls import *
from django.contrib.auth.views import LoginView, LogoutView

from users import views
from users import classviews

from django.http import HttpResponse
do_nothing = lambda *args, **kwargs: HttpResponse()

app_name='users'

urlpatterns = [
    url(r'^profile', views.profile, name='profile'),
    url(r'^activityfeed/(?P<user_id>\d+)', views.activityfeed, name='activityfeed'),
    url(r'^activityfeed', views.activityfeed, name='activityfeed'),

    url(r'^user_detail/(?P<user_id>\d+)$', views.user_detail, name='user_detail'),
    url(r'^user_detail/$', views.user_detail, name='user_detail_ajaxstub'),

    url(r'^change_password$', views.change_password, name='change_password'),
    url(r'^change_email$', views.change_email, name='change_email'),
    url(r'^reset_password$', views.reset_password, name='reset_password'),

    url(r'^user_ascents/(?P<user_id>\d+)$', views.user_ascents, name='user_ascents'),
    url(r'^upload_avatar$', views.upload_avatar, name='upload_avatar'),
    url(r'^delete_avatar$', views.delete_avatar, name='delete_avatar'),
    url(r'^register$', views.register, name='users.views.register'),
    url(r'^confirm/(?P<activation_key>.*)$', views.confirm, name='confirm'),
    url(r'^confirm_mail/(?P<activation_key>.*)$', views.confirm_email_change, name='confirm_email_change'),
    url(r'^login$',  LoginView.as_view(), name='django.contrib.auth.views.login'),
    url(r'^logout$', LogoutView.as_view(), name='django.contrib.auth.views.logout'),
    url(r'^route_histogram/(?P<user_id>\d+)$', views.user_route_histogram, name='user_route_histogram'),
    url(r'^test_confirmation$', views.test_confirmation, name='test_confirmation'),
    url(r'^test_message$', views.test_message, name='test_message'),
    url(r'^test_deletion$', views.test_deletion, name='test_deletion'),
    url(r'^test_success$', views.test_success, name='test_success'),

    url(r'^delete_user$', views.delete_user, name='delete_user'),

    url(r'^rest/revisions/(?P<user_id>\d+)$', classviews.RestRevisions.as_view(), name='rest_revisions'),
    url(r'^rest/revisions$', classviews.RestRevisions.as_view(), name='rest_revisions'),
    url(r'^rest/activities/(?P<user_id>\d+)$', classviews.RestUserActivities.as_view(), name='rest_user_activities'),
    url(r'^rest/ascents/(?P<user_id>\d+)$', classviews.RestUserAscents.as_view(), name='rest_user_ascents'),

    url(r'^rest/routework$', classviews.RouteWork.as_view(), name='rest_routework'),
    url(r'^rest/wallwork$', classviews.WallWork.as_view(), name='rest_wallwork'),
    url(r'^rest/tilerwork$', classviews.TilerWork.as_view(), name='rest_tilerwork'),
    url(r'^rest/panostitcherwork$', classviews.PanoStitcherWork.as_view(), name='rest_panostitcherwork'),

#    url(r'^rest/favroutes$', classviews.FavRoutes.as_view(), name='rest_favroutes'),
    url(r'^rest/favwalls$', classviews.FavWalls.as_view(), name='rest_favwalls'),

    url(r'^user_work/$', views.user_work, name='user_work'),
    url(r'^favourites/$', views.favourites, name='favourites'),
]
