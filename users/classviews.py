"""User related classviews, mostly REST."""
import datetime
from itertools import chain

from django.db.models import F, Q
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from reversion.models import Revision, Version

from miroutes.models import Wall, Route, Tiler, Ascent
from miroutes.serializers import TimelineItemSerializer, TilerWorkSerializer, \
    WallWorkSerializer, WallFavSerializer, RouteWorkSerializer, AscentSerializer

from users.models import User
from users.serializers import RevisionSerializer, RevisionSerializer_with_User
from edit_spot.utils import del_unchanged_versions_from_wall_revision

from stitcher.models import Pano
from stitcher.serializers import PanoWorkSerializer


class Paged_ListAPIView(ListAPIView):
    """ Expand the ListAPIView where we override the list function to annotate the result with pagination info """

    def list(self, request, *args, **kwargs):
        response = super(Paged_ListAPIView, self).list(request, args, kwargs)
        response.data = {
                'pagination': self.pagination,
                'data': response.data,
                }
        return response


def chunkify(obj, queryset, default_chunksize=50, default_page=1):
    chunksize = obj.request.query_params.get('chunksize', default_chunksize)
    page = obj.request.query_params.get('page', default_page)
    try:
        chunksize = int(chunksize)
        page = int(page)
    except:
        chunksize = default_chunksize
        page = default_page

    # Determine total number of entries in QuerySet / list
    from django.db.models.query import QuerySet
    if isinstance(queryset, QuerySet):
        N = queryset.count()
    else:
        N = len(queryset)

    obj.pagination = {
        'previous': obj.request.path+'?page={}&chunksize={}'.format(max(1,page-1), chunksize),
        'next'    : obj.request.path+'?page={}&chunksize={}'.format(min((N-1)/chunksize+1, page+1), chunksize),
        'total_count': N}

    return queryset[(page-1)*chunksize:page*chunksize]


class RestRevisions(Paged_ListAPIView):
    """List of Revisions filterable by user id."""
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = RevisionSerializer_with_User  # beware, if the url includes a user_id, we use the Revision Serializer without User Info

    def get_queryset(self):
        """See if we are delivered a query parameter *route* and return
        a filtered set.
        """
        revisions = Revision.objects.all()

        user_id = self.kwargs.get('user_id', None)
        if user_id is not None:
            self.serializer_class = RevisionSerializer
            user = get_object_or_404(User, pk=user_id)
            revisions = revisions.filter(user=user)

        revisions = revisions.order_by('-date_created')

        return chunkify(self, revisions)


class RestUserActivities(Paged_ListAPIView):
    """Collect user activities and deliver via REST Api.
    """

    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = TimelineItemSerializer

    def get_queryset(self):
        user_id = self.kwargs.get('user_id')
        user = get_object_or_404(User, pk=user_id)

        # ascents
        ascents = user.ascent_set.order_by('-date')

        # edits
        route_versions = Version.objects.filter(revision__user=user).filter(content_type__model='route')
        route_edits = Revision.objects.filter(version__in=route_versions)\
                                      .distinct().annotate(date=F('date_created')).order_by('-date')
        wall_versions = Version.objects.filter(revision__user=user).filter(content_type__model='wall')
        wall_edits = []
        for wv in wall_versions:
            rev = wv.revision
            # previous version
            prev = Version.objects.filter(content_type__model='wall').filter(object_id=wv.object_id)\
                           .filter(revision__date_created__lt=rev.date_created)\
                           .order_by('-revision__date_created').first()
            # if no prev, user is creator, we can ship the whole revision
            if prev:
                del_unchanged_versions_from_wall_revision(
                    rev, prev.revision)
            # pseudo annotation
            rev.date = rev.date_created
            wall_edits.append(rev)

        # userpics
        userpics = user.userpic_set.annotate(date=F('created_at')).order_by('-date')

        def sort_func(x):
            if isinstance(x.date, datetime.date):
                return datetime.datetime(x.date.year, x.date.month, x.date.day)
            return x.date

        query_result = sorted(chain(ascents, route_edits, wall_edits, userpics),
                              key=sort_func, reverse=True)

        return chunkify(self, query_result)

class RestUserAscents(ListAPIView):
    """Collect user ascents and deliver via REST Api.
    """

    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = AscentSerializer

    def get_queryset(self):
        user_id = self.kwargs.get('user_id')
        if user_id is not None:
            user = get_object_or_404(User, pk=user_id)
            queryset = user.ascent_set.all()
        else:
            queryset = Ascent.objects.all()
        route = self.request.query_params.get('route', None)
        if route is not None:
            queryset = queryset.filter(route__pk=route)
        return queryset.order_by('-date')

class RouteWork(Paged_ListAPIView):
    """Collect route edits and deliver via REST."""
    serializer_class = RouteWorkSerializer

    def get_queryset(self):
        user = self.request.user

        route_versions = Version.objects.filter(revision__user=user).filter(content_type__model='route')
        route_ids = list(route_versions.values_list('object_id', flat=True))

        routes = Route.objects.filter(pk__in=route_ids)
        routes = routes.prefetch_related('created_by')
        routes = routes.prefetch_related('routegeometry_set__snapshot')

        routes = routes.order_by('-pk')

        return chunkify(self, routes)


class TilerWork(Paged_ListAPIView):
    """Collect tiler edits and deliver via REST."""
    serializer_class = TilerWorkSerializer

    def get_queryset(self):
        user = self.request.user

        tiler_versions = Version.objects.filter(revision__user=user).filter(content_type__model='tiler')
        tiler_ids = list(tiler_versions.values_list('object_id', flat=True))

        tilers = Tiler.objects.filter(pk__in=tiler_ids).distinct().order_by('-pk')

        return chunkify(self, tilers)


class PanoStitcherWork(Paged_ListAPIView):
    """Collect panostitcher edits and deliver via REST."""
    serializer_class = PanoWorkSerializer

    def get_queryset(self):
        user = self.request.user

        panos = Pano.objects.filter(created_by=user).order_by('-pk').order_by('-updated_at')

        return chunkify(self, panos)


class WallWork(Paged_ListAPIView):
    """Collect wall edits."""
    serializer_class = WallWorkSerializer

    def get_queryset(self):
        user = self.request.user

        wall_versions = Version.objects.filter(revision__user=user).filter(content_type__model='wall')
        wall_ids = list(wall_versions.values_list('object_id', flat=True))

        wallview_versions = Version.objects.filter(revision__user=user).filter(content_type__model='wallview')
        wallview_ids = list(wallview_versions.values_list('object_id', flat=True))

        # get walls either part of wallview, tiler or wall edit
        walls = Wall.objects.filter(Q(pk__in=wall_ids) | Q(wallview__in=wallview_ids)).distinct().order_by('-pk')
        return chunkify(self, walls)


class FavWalls(Paged_ListAPIView):
    """Collect wall edits."""
    serializer_class = WallFavSerializer

    def get_queryset(self):
        user = self.request.user

        # get walls either part of wallview, tiler or wall edit
        walls = user.userprofile.favourite_walls.order_by('name')
        return chunkify(self, walls)

# To Be implemented: ManyToMany favourite Route field.
# class FavRoutes(Paged_ListAPIView):
#     """Collect wall edits."""
#     serializer_class = RouteWorkSerializer

#     def get_queryset(self):
#         user = self.request.user

#         # get walls either part of wallview, tiler or wall edit
#         walls = user.favourite_walls.order_by('name')
#         return chunkify(self, walls)
