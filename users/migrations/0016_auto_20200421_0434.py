# Generated by Django 3.0.5 on 2020-04-21 04:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0066_auto_20200421_0434'),
        ('users', '0015_userprofile_new_mail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='favourite_walls',
            field=models.ManyToManyField(blank=True, null=True, to='miroutes.Wall'),
        ),
    ]
