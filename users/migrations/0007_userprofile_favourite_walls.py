# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-27 15:03


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miroutes', '0018_auto_20170827_1459'),
        ('users', '0006_remove_userprofile_favourite_walls'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='favourite_walls',
            field=models.ManyToManyField(to='miroutes.Wall'),
        ),
    ]
