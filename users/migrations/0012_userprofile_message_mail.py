# Generated by Django 2.0 on 2018-01-22 09:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_remove_userprofile_unread_messages'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='message_mail',
            field=models.BooleanField(default=True),
        ),
    ]
