from django import forms
from django.contrib.auth.password_validation import validate_password
from django.core import validators
from django.contrib.auth.models import User

from users.models import UserProfile

from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Submit, Layout, Field, Fieldset,
    Button, HTML, Row, MultiField, Div)
from crispy_forms.bootstrap import (AppendedText, PrependedText, FormActions)

def isValidUsername(field_data):
  try:
    User.objects.get(username__iexact=field_data)
  except User.DoesNotExist:
    return
  raise validators.ValidationError('The username "%s" is already taken.' % field_data)

class AvatarForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('avatar',)

class MailSetupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MailSetupForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields

        self.helper.form_id = 'mail-form'
        self.helper.form_method = 'post'

        save_btn = HTML(
            """{% if request.is_ajax is False %}
            {% load i18n %}
            <button type="submit" name="mail_setup_form" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save Mail Settings" %}
            </button>
            {% endif %}
            """)

        self.helper.layout.append(
            FormActions(save_btn,))

    class Meta:
        model = UserProfile
        fields = ('message_mail','mail_on_collaborate')

class UserForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields

        self.helper.form_id = 'user-form'
        self.helper.form_method = 'post'

        self.fields['email'].disabled = True

        password_btn = HTML(
          """{% if request.is_ajax is False %}{% load i18n %}<a href="{% url "users:change_password" %}?next={{ request.path }}" class="btn btn-default top-buffer-margin">
            <span class="glyphicon glyphicon-pencil"></span> {% trans "Change Password" %}
            </a>
            {% endif %}
            """)
        email_btn = HTML(
          """{% if request.is_ajax is False %}{% load i18n %}<a href="{% url "users:change_email" %}?next={{ request.path }}" class="btn btn-default top-buffer-margin">
            <span class="glyphicon glyphicon-pencil"></span> {% trans "Change Email" %}
            </a>
            {% endif %}
            """)

        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button name="user_form" type="submit" class="btn btn-success top-buffer-margin">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
            </button>
            {% endif %}
            """)

        self.helper.layout.append(
            FormActions(password_btn, email_btn, save_btn))

    def save(self):
        if self.cleaned_data['username']:
            self.instance.username = self.cleaned_data['username']
        if self.cleaned_data['email']:
            self.instance.email = self.cleaned_data['email']

        self.instance.save()
        return self.instance

    def clean_username(self):
        if self.cleaned_data['username']:
            username = self.cleaned_data['username']
            if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
                raise forms.ValidationError('Username "%s" is already in use.' % username)
        return self.cleaned_data['username']

    class Meta:
        model = User
        fields = (
          'username',
          'email',
        )


class PasswordResetForm(forms.Form):
    username = forms.CharField(label=_('Username'), max_length=30)
    email    = forms.EmailField()
    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'
        self.helper.layout = Layout(
                Field('username'),
                'email')

        self.helper.form_id = 'user-form'
        self.helper.form_method = 'post'

        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button name="user_form" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Reset Password" %}
            </button>
            {% endif %}
            """)

        self.helper.layout.append(
            FormActions(save_btn))


class ChangeEmailForm(forms.Form):
    new_email    = forms.EmailField()
    def __init__(self, *args, **kwargs):
        super(ChangeEmailForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'
        self.helper.layout = Layout('new_email')

        self.helper.form_id = 'email-form'
        self.helper.form_method = 'post'

        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button name="email_form" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Submit" %}
            </button>
            {% endif %}
            """)
        cancel_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>
            {% endif %}""")

        self.helper.layout.append(
            FormActions(save_btn, cancel_btn))


    def clean_new_email(self):
        if self.cleaned_data['new_email']:
            new_email = self.cleaned_data['new_email']
            if User.objects.filter(email=new_email).exists():
                raise forms.ValidationError('Email "%s" is already in use.' % new_email)

        return self.cleaned_data['new_email']


class PasswordForm(forms.Form):
    password_current = forms.CharField(label='Password (Current)', max_length=255, widget=forms.PasswordInput, required=False)
    password = forms.CharField(label='Password (New)', max_length=255, widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label='Password (Confirmation)', max_length=255, widget=forms.PasswordInput, required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(PasswordForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4 col-xs-12'
        self.helper.field_class = 'col-md-6 col-xs-12'

        self.helper.layout = Layout('password', 'password2') # use all fields
        if self.user.has_usable_password():
            self.helper.layout.append('password_current')

        self.helper.form_id = 'password-form'
        self.helper.form_method = 'post'


        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
            </button>
            {% endif %}
            """)
        cancel_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>
            {% endif %}""")


        self.helper.layout.append(
              FormActions(save_btn, cancel_btn))

    def save(self):

        if self.cleaned_data['password']:
            self.user.set_password(self.cleaned_data['password'])

        self.user.save()
        return self.user


    def clean(self):
        if self.cleaned_data.get('password'):
            password1 = self.cleaned_data.get('password')
            password2 = self.cleaned_data.get('password2')

            if password1 and password1 != password2:
                raise forms.ValidationError("Passwords don't match")

            if self.user.has_usable_password():
                password_current = self.cleaned_data.get('password_current')
                if password1 and not password_current:
                    raise forms.ValidationError("Current password is missing")

                if password1 and not self.user.check_password(password_current):
                    raise forms.ValidationError("Current password is wrong")

            validate_password(password1)

        return self.cleaned_data


class RegistrationForm(forms.Form):
    username = forms.CharField(label=_('Username'), max_length=30, validators=[isValidUsername,])
    email    = forms.EmailField()
    password = forms.CharField(label=_('Password'), max_length=255, widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Password (Confirmation)'), max_length=255, widget=forms.PasswordInput)
    optin_privacy = forms.BooleanField(label=_('I agree to the privacy notice and the terms and conditions of use.'), required=True)

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'

        self.helper.form_id = 'registration-form'
        self.helper.form_method = 'post'

        privacy = FormActions(
          HTML("""{% load i18n %}
          <a target="_blank"
          href="{% url 'miroutes:privacy' %}?next={{ request.path }}">{% trans 'Click here to view the privacy notice (German).' %}</a>
          """))
        terms = FormActions(
          HTML("""{% load i18n %}
          <a target="_blank"
          href="{% url 'miroutes:imprint' %}?next={{ request.path }}">{% trans 'Click here to view the terms and conditions (German).' %}</a>
          """))

        save_btn = HTML(
          """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> Save
            </button>
            """)
        cancel_btn = HTML(
          """{% load i18n %}{% if next_page %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> """ + str(_("Cancel")) + """</a>
            {% endif %}
            """)

        footer_buttons = FormActions(save_btn, cancel_btn)

        self.helper.layout = Layout(
          'username',
          'email',
          'password',
          'password2',
          'optin_privacy',
          privacy,
          terms,
          footer_buttons,
        )


    def clean_email(self):
        if self.cleaned_data.get('email'):
            email = self.cleaned_data.get('email')
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('Email "%s" is already in use.' % email)
        return self.cleaned_data

    def clean(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        validate_password(password1)

        return self.cleaned_data

    def save(self, new_data):
        u = User.objects.create_user(new_data['username'], new_data['email'], new_data['password'])
        u.is_active = False
        u.save()
        return u
