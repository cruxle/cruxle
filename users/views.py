import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.core.signing import Signer
from django.http import HttpResponse, JsonResponse
from django.utils.html import escape
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, translate_url
from django.utils import timezone
from django.utils.translation import check_for_language, LANGUAGE_SESSION_KEY
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from social_django.models import UserSocialAuth

from pytz import utc
from reversion.models import Revision, RevertError
from django.core.exceptions import ObjectDoesNotExist
from generic_markers.models import SimpleMarker, PitchMarker, TextMarker, RelationalMarker
from miroutes.models import Ascent, Wall, WallView, Route, RouteGeometry
from users.forms import RegistrationForm, AvatarForm, UserForm, MailSetupForm, PasswordResetForm, ChangeEmailForm
from django.templatetags.static import static
from django.template.loader import render_to_string

from postbox.utils import message_from_cruxle
from edit_spot.utils import does_obj_change
from postbox.utils import mail_to
from mitopo.utils import absolute_url, notify_us

@login_required
def delete_avatar(request, **kwargs):
    """
    Show user profile if logged in.
    """
    if request.method == 'POST':
        if request.POST.get('delete', None):
            request.user.userprofile.avatar = None
            request.user.userprofile.save()
            return HttpResponse("")


@login_required
def upload_avatar(request, **kwargs):
    """
    Show user profile if logged in.
    """
    if request.method == 'POST':
        profile_form = AvatarForm(request.POST, request.FILES, instance=request.user.userprofile, )
        if profile_form.is_valid():
            profile = profile_form.save()
            profile.thumbnail()
            return redirect(reverse('users:profile'))

    profile_form = AvatarForm(instance=request.user.userprofile)

    return render(request, 'users/upload_avatar.html', {
        'profile_form': profile_form
    })


@login_required
def change_email(request):
    user = request.user
    context = {"next_page": request.GET.get('next', reverse('users:profile'))}

    if request.POST:
        email_form = ChangeEmailForm(request.POST)
        if email_form.is_valid():
            new_mail = email_form.data['new_email']

            # Build the activation key for the new mail adress
            signed = Signer().sign(new_mail)
            activation_key = ''.join(signed.split(':')[1:])
            key_expires = timezone.now() + datetime.timedelta(2)

            user.userprofile.activation_key = activation_key
            user.userprofile.key_expires = key_expires
            user.userprofile.new_mail = new_mail
            user.userprofile.save()

            # Send an email with the confirmation link
            email_subject = _("cruxle.org - confirm new mailadress")
            options = {
                'link': absolute_url(reverse('users:confirm_email_change', kwargs={'activation_key': activation_key})),
                "request": request
            }
            mail_to(
                user, email_subject, 'email/change_email.html', options, use_new_mail=True)

            context['success'] = True

    else:
        email_form = ChangeEmailForm()

    context['form'] = email_form
    return render(request, 'users/change_email.html', context)


@login_required
def confirm_email_change(request, activation_key):
    """Confirm users new mail adress using stored secret."""
    user = request.user
    if user.userprofile.key_expires < timezone.now():
        return render(request, 'users/confirm_email_change.html', {'expired': True})

    if activation_key != user.userprofile.activation_key:
        return render(request, 'users/confirm_email_change.html', {'tampered': True})

    # do not allow further changes with this key
    user.userprofile.key_expires = timezone.now()
    user.userprofile.save()

    user.email = user.userprofile.new_mail
    user.save()

    return render(request, 'users/confirm_email_change.html', {
        'success': True, 'next_page': request.GET.get('next', reverse('users:profile'))})



@login_required
def change_password(request):
    from users.forms import PasswordForm
    next_page = request.GET.get('next', reverse('users:profile'))

    if request.method == 'POST':
        form = PasswordForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            messages.add_message(
                request, messages.SUCCESS,
                _('Successfully changed password!'))
            return redirect(next_page)
        else:
            messages.add_message(
                request, messages.ERROR,
                _('Password could not be changed!'))

    else:
        form = PasswordForm(user=request.user)
    return render(request, 'users/change_password.html', {
        'form': form,
        'next_page': next_page
    })

def reset_password(request):
    next_page = request.GET.get('next', reverse('users:profile'))

    if request.method == 'POST':
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(username__iexact=form.data['username'], email=form.data['email'])
            except User.DoesNotExist:
                user = None
            if user is not None:
                messages.add_message(
                    request, messages.SUCCESS,
                    _('Successfully resetted password! - We sent you an Email with the new one.'))
                email_subject = _('Your Password has been resetted!')

                new_password = randompassword()

                mail_to(user, email_subject, 'email/reset_password.html', {
                    "request": request,
                    "new_password": new_password,
                    'link': absolute_url(reverse('users:change_password')), })
                user.set_password(new_password)
                user.save()
                #return test_reset_password_mail(request, user, randompassword())
                return redirect(next_page)
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Username and Email dont match!'))

        else:
            messages.add_message(
                request, messages.ERROR,
                _('Input is not valid!'))

    else:
        form = PasswordResetForm()
    return render(request, 'users/reset_password.html', {
        'form': form,
        'next_page': next_page
    })

def test_reset_password_mail(request, user, new_password):
    """Populate reset_password email and render a ordinary view."""
    return render(request, 'email/reset_password.html', {
        'user': user,
        'imgurl': absolute_url(static("topo/img/cruxle_logo2.png")),
        'link': reverse('users:profile'),
        'new_password': new_password,
        'html': True
    })


def randompassword():
    import string
    import random
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    size = random.randint(8, 12)
    return ''.join(random.choice(chars) for x in range(size))

@login_required
def favourites(request, **kwargs):
    """Show flagged favourite objects."""
    return render(request, 'users/favourites.html')

@login_required
def profile(request, **kwargs):
    """
    Show user profile if logged in.
    """

    try:
        google_login = request.user.social_auth.get(provider='google-oauth2')
    except UserSocialAuth.DoesNotExist:
        google_login = None

    social_auth = False
    if request.user.is_authenticated:
        if request.user.social_auth.filter(provider='google-oauth2'):
            social_auth = True

    if request.method == 'POST':
        if 'user_form' in request.POST:
            user_form = UserForm(request.POST, instance=request.user)
            mailsetup_form = MailSetupForm(instance=request.user.userprofile)

            if 'email' in user_form.changed_data:
                messages.add_message(
                    request, messages.ERROR,
                    _('Error saving profile! Currently you cannot change your mail address. Please get in touch with us if you wish to do so.'))
            else:
                if user_form.has_changed() and user_form.is_valid():
                    user_form.save()
                    messages.add_message(
                        request, messages.SUCCESS,
                        _('Successfully changed profile!'))
                else:
                    messages.add_message(
                        request, messages.ERROR,
                        _('Error saving profile!'))
        else:
            mailsetup_form = MailSetupForm(request.POST, instance=request.user.userprofile)
            user_form = UserForm(instance=request.user)
            if does_obj_change(request.user.userprofile, mailsetup_form) and mailsetup_form.is_valid():
                mailsetup_form.save()
                messages.add_message(
                    request, messages.SUCCESS,
                    _('Successfully changed mail settings!'))
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('No changes to mail options!'))

    else:
        user_form = UserForm(instance=request.user)
        mailsetup_form = MailSetupForm(instance=request.user.userprofile)

    return render(request, 'users/profile.html', {
        'user_form': user_form,
        'mail_form': mailsetup_form,
        'social_auth': social_auth,
        'google_login': google_login
    })

def test_confirmation(request):
    """Populate confirmation email and render a ordinary view."""
    return render(request, 'email/confirmation.html', {
        'user': {'username': 'username'},
        'imgurl': absolute_url(static("topo/img/cruxle_logo2.png")),
        'link': 'ThisIsTheActivationLinkBaseURL/2304923048thekeyxxx',
        'html': True
    })

def test_message(request):
    """Populate message notification email and render a ordinary view."""
    return render(request, 'email/message.html', {
        'user': {'username': 'username'},
        'message': 'This is some blabla from another user. It may be important, but most probably is not. Anyway, here is the message.',
        'imgurl': absolute_url(static("topo/img/cruxle_logo2.png")),
        'link': 'ThisIsTheReplyLinkURL/2304923048thekeyxxx',
        'html': True
    })

def user_work(request):
    return render(request, 'users/work_bench.html')

def test_deletion(request):
    """Populate deletion email and render a ordinary view."""
    return render(request, 'email/deletion.html', {
        'user': {'username': 'username'},
        'imgurl': absolute_url(static("topo/img/cruxle_logo2.png")),
        'html': True
    })

def test_success(request):
    """Populate confirmation email and render a ordinary view."""
    return render(request, 'users/register.html', {
        'register_successfully': True,
        'form': {
            'data': {
                'username': 'username',
                'email': 'email@email.de'
            },
        }
    })

def register(request):
    next_page = request.GET.get('next')

    if request.POST:
        register_form = RegistrationForm(request.POST)
        if register_form.is_valid():
            new_user = User.objects.create_user(register_form.data['username'], register_form.data['email'],
                                                register_form.data['password'])
            new_user.is_active = False
            new_user.save()

            # Build the activation key for their account
            signer = Signer()
            signed = signer.sign(new_user.email)
            activation_key = ''.join(signed.split(':')[1:])
            key_expires = timezone.now() + datetime.timedelta(2)

            new_user.userprofile.activation_key = activation_key
            new_user.userprofile.key_expires = key_expires
            new_user.userprofile.save()

            # Send an email with the confirmation link
            email_subject = _("Welcome to cruxle.org")
            options = {
                'link': absolute_url(reverse('users:confirm', kwargs={'activation_key': new_user.userprofile.activation_key})),
                "request": request
            }
            mail_to(
                new_user, email_subject, 'email/confirmation.html', options, 'registration@cruxle.org')

            return render(request, 'users/register.html', {'register_successfully': True, 'form': register_form})
    else:
        register_form = RegistrationForm()

    return render(request, 'users/register.html', {'form': register_form, 'next_page': next_page})


class ActivityFeedEntry:
    def __init__(self, date, activity, user, object_title, image_url, text, link, is_ascent=False):
        self.date = date
        self.activity = activity
        self.user = user
        self.object_title = object_title
        self.image_url = image_url
        self.text = text
        self.link = link
        self.is_ascent = is_ascent


def ascent2feed(ascent):
    if ascent.route.walls.first():
        link = reverse('miroutes:wall_detail', kwargs={'wall_id': ascent.route.walls.first().wall.pk})
    else:
        link = reverse('miroutes:route_detail', kwargs={'route_id': ascent.route.pk})

    return ActivityFeedEntry(
        date=utc.localize(datetime.datetime(ascent.date.year, ascent.date.month, ascent.date.day)),
        activity=ascent.get_style_display(),
        user=escape(ascent.climber),
        object_title=escape(ascent.route.name) + " ( " + ascent.route.get_full_grade_display + " )",
        image_url=static("generic_markers/img/edit_route_icon.png"),
        text=_("and rated it a <b>" + str(ascent.rating) + "</b>"),
        link=link,
        is_ascent=True
    )


def version2feed(version):
    """
    Create an activity feed entry for one version of a model.
    I.e. one revision may have a multiple of these here.
    """
    ctype = lambda model: ContentType.objects.get_for_model(model)
    #rev_obj = lambda version: version._object_version.object # this really reverts an object and we presume the usage is to be avoided...

    if version.content_type == ctype(RouteGeometry):
        if version.object: # it may happen that the route where this geometry was linked to, does not exist anymore...
            route = version.object.route
            image_url = static("generic_markers/img/route_icon.png")
            link = reverse('miroutes:route_detail', kwargs={'route_id': route.pk})
            text = escape(route.name)
            return (image_url, link, text)

    elif version.content_type == ctype(Route):
        if version.object:
            route = version.object
            image_url = static("generic_markers/img/route_icon.png")
            link = reverse('miroutes:route_detail', kwargs={'route_id': route.pk})
            text = escape(route.name)
            return (image_url, link, text)

    elif version.content_type in [ctype(RelationalMarker), ctype(TextMarker), ctype(PitchMarker)]:
        if version.object:
            marker = version.object
            wall = marker.wallview.wall
            image_url = marker.icon_url
            link = reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})
            text = marker.markertype
            return (image_url, link, text)

    elif version.content_type == ctype(SimpleMarker):
        return # disable generation of subitems in activity feed here... this is too much info...
        if version.object:
            marker = version.object
            image_url = marker.icon_url
            link = None
            text = marker.markertype
            return (image_url, link, text)

    elif version.content_type == ctype(WallView):
        if version.object:
            wall = version.object.wall
            image_url = wall.tiler.thumbnail_url_s
            link = reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})
            text = wall.name
            return (image_url, link, text)

    elif version.content_type == ctype(Wall):
        if version.object:
            wall = version.object
            image_url = wall.tiler.thumbnail_url_s
            link = reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})
            text = wall.name
            return (image_url, link, text)

    return



def revision2feed(revision):
    """
    Collect info about a revision and create a summary about it
    """
    ctype = lambda model: ContentType.objects.get_for_model(model)

    all_versions = revision.version_set.all()

    count_changed_obj = {}

    for v in all_versions:
        if v.content_type not in list(count_changed_obj.keys()):
            count_changed_obj[v.content_type] = 0
        count_changed_obj[v.content_type] = count_changed_obj[v.content_type] + 1

    version_strings = [ version2feed(v) for v in all_versions ]

    def version_string_2_html(vstr):
        a1, a2 = ("<a href='{}'>".format(vstr[1]), "</a>") if vstr[1] else ('','')
        return "<span> <small>" + a1 + "<img class='sub_img' src='{}' alt=''> {}".format(vstr[0], vstr[2]) + a2 + "</small></span>"

    image_url = None
    link = '#'

    activity = "edited"
    title = '{} objects '.format( sum(count_changed_obj.values()) )

    obj_list_str = ','.join([ "{} {}".format(v,k) for k,v in count_changed_obj.items()])
    obj_list_str = '<p><small>({})</small></p>'.format(obj_list_str) if obj_list_str else ''

    version_sub_strings = []
    for vstr in version_strings:
        if vstr:
            version_sub_strings.append(version_string_2_html(vstr))

    text = 'with the commit message: ' + escape(revision.comment) + obj_list_str + '<br>'.join(version_sub_strings)

    return ActivityFeedEntry(
            date=revision.date_created,
            activity=activity,
            user=revision.user,
            object_title=title,
            image_url=image_url,
            text=text,
            link=link
            )


def activityfeed(request, user_id=None):
    if user_id is None:
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        user = request.user
    else:
        user = get_object_or_404(User, pk=user_id)

    feed_entries = []

    #ascents = Ascent.objects.filter(climber=user).order_by('-date')[:50]
    #feed_entries = map(ascent2feed, ascents)

    revisions = Revision.objects.filter(user=user).order_by('-date_created')[:30]

    feed_entries += list(map(revision2feed, revisions))

    feed_entries = [_f for _f in feed_entries if _f]
    feed_entries.sort(key=lambda x: x.date, reverse=True)

    return render(request, 'users/activityfeed.html',
                  {'user': user, 'feed_entries': feed_entries})


def user_ascents(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    ascents = Ascent.objects.filter(climber=user).order_by('-date')

    context = {
            'user': user,
            'ascents': ascents,
            }

    return render(request, 'users/user_ascents.html', context)


def user_detail(request, user_id=None):
    user = get_object_or_404(User, pk=user_id)

    context = {
        'user': user,
        'navtab': request.GET.get('navtab'),
    }

    return render(request, 'users/user_detail.html', context)


def confirm(request, activation_key):
    from users.models import UserProfile
    if request.user.is_authenticated:
        return render(request, 'users/confirm.html', {'has_account': True})

    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)
    if user_profile.key_expires < timezone.now():
        return render(request, 'users/confirm.html', {'expired': True})

    user_account = user_profile.user

    signer = Signer()
    if user_account.is_active:
        pass

    elif signer.unsign('{}:{}'.format(user_account.email, activation_key)) == user_account.email:
        user_account.is_active = True
        user_account.save()

        # add message to messaging service
        message_from_cruxle(user_account,
                _("Welcome to cruxle! Check out contributions by other users on the news page or start creating your own walls!"))

        notify_us(
                "´{}´ registered".format(user_account.username),
                absolute_url(reverse('users:user_detail', kwargs={'user_id': user_account.pk})))

    else:
        return render(request, 'users/confirm.html', {'tampered': True})

    return render(request, 'users/confirm.html', {'success': True, 'user': user_account})



def user_route_histogram(request, user_id, **kwargs):
    """
    Plot a histogram of route difficulties
    """
    from collections import Counter
    from miroutes.models import LINE_COLORS, get_grade_choices
    from miroutes.model_choices import grade_to_grade_system, equalize_grade, GRADE_SYSTEMS

    user = get_object_or_404(User, pk=user_id)

    routes = user.route_set.exclude(grade=0)

    if routes.count == 0:
        return None

    # Populate counter with all possible entries:
    c = Counter([r.grade for r in routes])

    # Determine which grading systems we have to populate
    grade_systems = set([ grade_to_grade_system(k) for k in list(c.keys()) ])

    def create_hist(grade_system):
        grade_ids, grade_names = list(zip(*get_grade_choices(grade_system)))

        diff_hist = []
        for uid,name in zip(grade_ids, grade_names):
            color = LINE_COLORS[equalize_grade(uid)]
            count = c[uid]
            diff_hist.append([name, count, color, uid%100])
        return {'grade_system':GRADE_SYSTEMS[grade_system] , 'hist': diff_hist}

    hist_list = [ create_hist(grade_system) for grade_system in grade_systems ]

    context = {
        'user': user,
        'showlabel': request.GET.get('showlabel', True),
        'hist_list': hist_list,
        }


    return render(request, 'users/route_histogram.html', context)

def delete_user(request):
    """Anonymize user account."""
    from django.contrib.auth.hashers import make_password

    user = request.user

    email_subject = _('Your Account has been Deleted')
    mail_to(
        user, email_subject, 'email/deletion.html', {"request": request}, 'registration@cruxle.org')

    # delete social auth account if existing
    UserSocialAuth.objects.filter(user=user).delete()

    # image
    user.userprofile.avatar.delete()
    user.userprofile.save()

    # username and password
    user.username = "user{}".format(user.pk)
    user.password = make_password(User.objects.make_random_password())

    # first name last name email
    user.first_name = ""
    user.last_name = ""
    user.email = "user{}@cruxle.org".format(user.pk)

    user.is_active = False
    user.save()

    return redirect(reverse('miroutes:index'))
