import os

import time
from PIL import Image
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.templatetags.static import static
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from reversion.models import Version, Revision
from django.utils.translation import ugettext_lazy as _

from mitopo.storage import OverwriteStorage
from mitopo.util import md5sum_file

USERS_BASE_FOLDER = "users"
AVATAR_FOLDER = "avatar"
AVATAR_FULL = "fullsize"
AVATAR_MEDIUM = "medium"
AVATAR_THUMB = "thumb"
AVATAR_FILE_ENDING = ".png"
THUMB_SIZE = 64
MEDIUM_SIZE = 300


def get_avatar_upload_path(userprofile, fname):

    # This could be covered by python 3 type hints, if the UserProfile class would be defined before this
    # function, but the UserProfile has a dependency to this function so we can't declare
    # it before the function. The workaround is to check at runtime.
    if type(userprofile) is not UserProfile:
        raise Exception("ERROR: get_avatar_upload_path was called with wrong argument. UserProfile expected as first argument")

    path = os.path.join(USERS_BASE_FOLDER, str(userprofile.pk), AVATAR_FOLDER, AVATAR_FULL + os.path.splitext(fname)[1])
    return path


def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)


puzzle_map = {
    'add_route': 5,
    'edit_route': 3,
    'revert_route': 0,
    'add_wall': 30,
    'add_wall_img': 50,
    'edit_wall': 5,
    'revert_wall': 0,
    'revert_wall_img': 0,
    'link_route': 2,
    'unlink_route': 0,
    'edit_routegeometry': 3,
    'add_marker': 3,
    'move_marker': 2,
    'delete_marker': 0,
    'publish_wallview': 5,
    'revert_wallview': 0,
    'add_material': 10,
    'add_userpic': 5
}


def get_puzzles(key):
    """Obtain puzzle amount for a given key and 0 if none is defined."""
    return puzzle_map.get(key, 0)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    activation_key = models.CharField(max_length=40, blank=True, null=True)
    key_expires = models.DateTimeField(blank=True, null=True)
    avatar = models.ImageField(blank=True, upload_to=get_avatar_upload_path, storage=OverwriteStorage())
    puzzle_pieces = models.BigIntegerField(default=0)

    last_map_view = JSONField(blank=True, null=True)
    baselayer = models.CharField(max_length=30, blank=True, null=True)
    favourite_walls = models.ManyToManyField('miroutes.Wall', blank=True)

    # new adress if changed
    new_mail = models.EmailField(blank=True, null=True)

    # mail options
    message_mail = models.BooleanField(
        default=True,
        verbose_name=_("Message Notifications"),
        help_text=_('Send me an email when new messages arrive.'))
    mail_on_collaborate = models.BooleanField(
        default=True,
        verbose_name=_("Notifications on Collaborations"),
        help_text=_('Inform me when routes or walls change where I am involved.'))

    @property
    def unread_messages(self):
        return self.user.status_set.aggregate(models.Sum('new_msg_count'))['new_msg_count__sum']

    def medium_url(self):
        if self.avatar:
            if os.path.exists(self.medium_path()):
                cb = md5sum_file(self.medium_path())
            else:
                cb = str(time.time())
            return os.path.join(settings.MEDIA_URL, USERS_BASE_FOLDER, str(self.user.pk), AVATAR_FOLDER,
                                AVATAR_MEDIUM + AVATAR_FILE_ENDING + "?cb=" + cb)
        else:
            return static("topo/img/avatar/medium_default.png")

    def thumb_url(self):
        if self.avatar:
            if os.path.exists(self.medium_path()):
                cb = md5sum_file(self.thumb_path())
            else:
                cb = str(time.time())
            return os.path.join(settings.MEDIA_URL, USERS_BASE_FOLDER, str(self.user.pk), AVATAR_FOLDER,
                                AVATAR_THUMB + AVATAR_FILE_ENDING + "?cb=" + cb)
        else:
            return static("topo/img/avatar/thumb_default.png")

    def medium_path(self):
        return os.path.join(settings.MEDIA_ROOT, USERS_BASE_FOLDER, str(self.user.pk), AVATAR_FOLDER,
                            AVATAR_MEDIUM + AVATAR_FILE_ENDING)

    def thumb_path(self):
        return os.path.join(settings.MEDIA_ROOT, USERS_BASE_FOLDER, str(self.user.pk), AVATAR_FOLDER,
                            AVATAR_THUMB + AVATAR_FILE_ENDING)

    def thumbnail(self):
        if self.avatar:
            image = Image.open(os.path.join(settings.MEDIA_ROOT, self.avatar.path))

            image_medium = image.resize((MEDIUM_SIZE, MEDIUM_SIZE), Image.ANTIALIAS)
            medium_path = self.medium_path()
            ensure_dir(medium_path)
            image_medium.save(medium_path)

            image_thumb = image.resize((THUMB_SIZE, THUMB_SIZE), Image.ANTIALIAS)
            thumb_path = self.thumb_path()
            ensure_dir(thumb_path)
            image_thumb.save(thumb_path)

    def change_puzzle_pieces(self, action):
        """Give KPs to user and check if he has enough KP left for the desired action."""
        self.puzzle_pieces = self.puzzle_pieces + get_puzzles(action)
        if self.puzzle_pieces < 0:
            return False
        else:
            self.save()
            return True

    def get_contributed_versions(self, app, model):
        # a queryset of version which the user was involved in
        objtype = ContentType.objects.get(app_label=app, model=model)
        return Version.objects.filter(revision__user=self.user).filter(content_type=objtype)

    def objects_created(self, app, model):
        objtype = ContentType.objects.get(app_label=app, model=model)

        # find all versions of this particular type which contain myself
        contributed_versions = self.get_contributed_versions(app, model)
        contributed_obj_ids = set(contributed_versions.values_list('object_id',
                                                                   flat=True))  # this is a unique set of objects id in which the user was involved

        count = 0
        for object_id in contributed_obj_ids:
            first_version = Version.objects.filter(content_type=objtype).filter(object_id=object_id).order_by(
                '-revision__date_created').first()  # now check if the first version for a particular object is from the user
            if self.user.id == first_version.revision.user.id:
                count += 1
        return count

    @property
    def ascent_count(self):
        return self.user.ascent_set.all().count

    @property
    def edit_count(self):
        return Revision.objects.filter(user=self.user).count()

    @property
    def materials_uploaded(self):
        return self.user.userpic_set.filter(content="info").count()

    @property
    def userpics_uploaded(self):
        return self.user.userpic_set.filter(content="personal").count()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()


@receiver(post_save, sender=User)
def add_default_group_to_user_profile(sender, instance, **kwargs):
    group, group_created = Group.objects.get_or_create(name='Bambino')
    user = instance
    group.user_set.add(user)
