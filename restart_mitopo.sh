#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

docker-compose run --rm  web python3 manage.py collectstatic --noinput

docker-compose stop
docker-compose up -d
