#!/bin/bash
#
# Script to setup inital project
# includes
#   docker image bulding
#   creation of superuser

set -eu -o pipefail

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_ROOT=$SCRIPTDIR/..
cd $PROJECT_ROOT

# then use docker-compose up to restart all containers and run them in background
docker-compose build
docker-compose up -d
sleep 10

# make migrations and migrate to setup db
docker-compose run --rm web python3 manage.py migrate
sleep 5

# Create root admin user
docker-compose run --rm web python3 manage.py shell -c "
from django.contrib.auth.models import User;
if User.objects.filter(username='root'):
    root = User.objects.get(username='root');
    root.set_password('root');
    root.save();
else:
    User.objects.create_superuser('root', 'root@dev.cruxle.org', 'root')
"

# In order to overwrite the django-star_rating images, we need to call collectstatic at least once
docker-compose run --rm web bower install --allow-root
docker-compose run --rm web python3 manage.py collectstatic --noinput

sleep 5

docker-compose restart nginx || echo ""

exit 0
