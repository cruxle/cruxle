#!/bin/bash
# * Moves the Database Folder (to completely get rid of any database schemes
#   because if the are version conflicts, the container wont even start
# * generates a new database container
# * restores the backup database
#
# This sequence was used when migrating from postgresql 9.4 to 11

# Example arguments:
#DB_DIR=/storage/sites/staging.cruxle.org/db/pgdata
#DB_BACKUP=db_dump.gz

set -eu -o pipefail

DB_DIR=${1%/}
DB_BACKUP=$2

[ ! -d $DB_DIR ]    && (echo "arg1: DB_DIR does not exist $DB_DIR "; exit 1)
[ ! -f $DB_BACKUP ] && (echo "arg2: DB_BACKUP_FILE does not exist $DB_BACKUP_FILE "; exit 1)

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_ROOT="$SCRIPTDIR/../"
cd $PROJECT_ROOT

docker-compose down -v
DB_BAK=${DB_DIR}_$(date +"%Y_%m_%d_%H.%M")
echo Moving Database dir  $DB_DIR to $DB_BAK
mv -v $DB_DIR ${DB_BAK}

docker-compose up -d postgresql

# env holds the docker-compose prefix
. .env

CONTAINER=$(docker ps | grep " ${COMPOSE_PROJECT_NAME}[_]*postgresql_1" | awk '{print $NF}')
[ -s "$CONTAINER" ] && (echo Could not find a running container for docker ps; exit 2)
echo "Executing DB Restore on container: $CONTAINER"

READYMSG="PostgreSQL init process complete; ready for start up."
until [[ "$(docker-compose logs postgresql)" == *"$READYMSG"* ]] ; do echo "Waiting for DB Startup"; done


if [[ "${DB_BACKUP}" == *".gz" ]]; then
  zcat $DB_BACKUP | docker exec -i $CONTAINER psql -U postgres
else
  docker exec -i $CONTAINER psql -U postgres < $DB_BACKUP
fi
