#!/bin/bash

echo "== Find uncommited migrations =="

OUTPUT=$(docker-compose run --rm web python3 manage.py migrate 2>&1) 

echo $OUTPUT | grep -q "Your models have changes that are not yet reflected"

if [ $? -eq 0 ]; then
  echo "ERROR uncommited migrations found"
  echo $OUTPUT
  docker-compose run --rm web python3 manage.py makemigrations --dry-run
  exit 1
else
   echo "OK: no uncommitted migrations found"
   echo $OUTPUT
   exit 0
fi
