#!/usr/bin/env bash
#
# Script to delete the project container and the database
docker-compose down
docker-compose rm -f
docker system prune -f
