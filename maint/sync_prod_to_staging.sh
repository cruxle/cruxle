#!/bin/bash
# * calling user needs to be able to log into TARGET
# * TARGET user needs to be able to log into TARGET
# * TARGET user needs to be able to log into PROD
set -x

PROD=cruxle_prod@cruxle.org
TARGET=cruxle_staging@cruxle.org
TARGET_DB_DIR=/storage/sites/staging.cruxle.org/db/pgdata

# Copy data files
REMOTE_DATA=/storage/sites/cruxle.org/www
LOCAL_DATA=/storage/sites/staging.cruxle.org/
ssh ${TARGET} "rsync -rlptDP --delete ${PROD}:${REMOTE_DATA} ${LOCAL_DATA}"

# Generate a database dump
REMOTE_DB_DUMP=/tmp/db_dump$(date +"%Y_%m_%d_%H.%M").gz
ssh ${TARGET} "ssh ${PROD} \"topo/maint/dump_db.sh $REMOTE_DB_DUMP\" "

# and copy it to TARGET
LOCAL_DB_DUMP=$(dirname $REMOTE_DB_DUMP)/tmp_$(basename ${REMOTE_DB_DUMP})
ssh ${TARGET} "rsync -vP ${PROD}:${REMOTE_DB_DUMP} ${LOCAL_DB_DUMP}"
ssh ${TARGET} "ssh ${PROD}  \"rm $REMOTE_DB_DUMP\" "

# reset containers on target instance
ssh ${TARGET} "\
  cd topo                                                       && \
  docker-compose down                                           && \
  docker-compose build web                                      && \
  maint/restore_db_from_backup.sh $TARGET_DB_DIR $LOCAL_DB_DUMP && \
  rm $LOCAL_DB_DUMP                                             && \
  docker-compose run --rm web python3 manage.py migrate         && \
  docker-compose up -d --build                                  && \
  echo finished sync_prod_to_staging.sh"
