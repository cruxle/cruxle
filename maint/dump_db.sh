#!/bin/bash
# Dumps the database into gzipped dump

set -eu -o pipefail

DST=$1

if [[ ! "$DST" == *".gz" ]]; then
  DST=${DST}.gz
fi

echo exporting DB to $DST

[ -e $DST ] && (echo "destination file already exists! aborting..."; exit 1)

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_ROOT="$SCRIPTDIR/../"

cd $PROJECT_ROOT

# env holds the docker-compose prefix
. .env

CONTAINER=$(docker ps | grep " ${COMPOSE_PROJECT_NAME}[_]*postgresql_1" | awk '{print $NF}')
[ -s "$CONTAINER" ] && (echo Could not find a running container for docker ps; exit 2)
echo "Executing DB pg_dump on container: $CONTAINER"

docker exec -i $CONTAINER pg_dump -U postgres --clean | gzip > $DST

# or the compose version as an alternative but needs to start web:
#docker-compose run --rm web pg_dump -h postgresql -U postgres --clean | gzip > $DST
