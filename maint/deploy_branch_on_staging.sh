#!/bin/bash
# deploys the arg1 branch name on the staging server
# * calling user(e.g. gitlab-runner) needs to be able to log into TARGET
set -x -eu -o pipefail

BRANCH=$1
TARGET=cruxle_staging@cruxle.org

# reset containers on target instance
ssh ${TARGET} "\
  cd topo                                                       && \
  git fetch                                                     && \
  git checkout $BRANCH                                          && \
  git pull                                                      && \
  maint/sync_prod_to_staging.sh                                 && \
  docker-compose up -d                                          && \
  echo finished deploy_branch_on_staging.sh"
