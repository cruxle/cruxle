#!/bin/bash
# deploys the arg1 branch name on the dev server
# * calling user(e.g. gitlab-runner) needs to be able to log into TARGET
set -x -eu -o pipefail

BRANCH=$1
TARGET=fab@cruxle.org

# reset containers on target instance
ssh ${TARGET} "\
  cd topo                                                       && \
  git fetch                                                     && \
  git checkout $BRANCH                                          && \
  git pull                                                      && \
  maint/sync_prod_to_dev.sh                                     && \
  docker-compose up -d                                          && \
  echo finished deploy_branch_on_dev.sh"
