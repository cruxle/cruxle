#!/bin/bash

docker-compose stop
docker-compose rm web
docker-compose rm worker
docker-compose rm worker_low_priority
docker-compose rm worker_stitcher
docker-compose build --no-cache web
docker-compose build worker
docker-compose up -d
