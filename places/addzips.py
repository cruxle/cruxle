import sys
from places.models import Place, Zip
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance

def add():
    for num, city in enumerate(Place.objects.filter(feature_class='P')):
        sys.stdout.write("\rImported %i lines." % num)
        sys.stdout.flush()
        zipobj = Zip.objects.filter(
            pnt__dwithin=(city.pnt, .5)).annotate(
                distance=Distance('pnt', city.pnt)).order_by('distance').first()
        if zipobj:
            city.zipcode = zipobj
            city.save()
