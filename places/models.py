from django.contrib.gis.db import models

class Zip(models.Model):
    """Store geonames zip infos."""
    country_code = models.CharField(max_length=2)
    pnt = models.PointField(verbose_name="Location")
    code = models.CharField(max_length=20)

class Place(models.Model):
    """A generic place."""
    pnt = models.PointField(verbose_name="Location")
    name = models.CharField(max_length=200)
    alt_names = models.CharField(max_length=10000, blank=True, null=True)
    feature_class = models.CharField(max_length=1)
    feature_code = models.CharField(max_length=10)
    country_code = models.CharField(max_length=2)
    elevation = models.IntegerField(blank=True, null=True)
    population = models.BigIntegerField(blank=True, null=True)

    zipcode = models.ForeignKey(Zip, blank=True, null=True, on_delete=models.CASCADE)

    def __unicode__(self):
        return '{}'.format(self.name)


