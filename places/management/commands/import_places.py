import glob
import sys
import os
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from places.models import Place, Zip
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

class TYPES:
    place = 'place'
    zip = 'zip'

def get_flist(dirname='geonames/'):
    """Get *.txt files from geonames dir."""
    wcard = os.path.join(settings.MEDIA_ROOT, dirname, '*.txt')

    return glob.glob(wcard)

def insert_zipdata(line):
    """Insert a line of data from zipcode file."""
    country_code = line[0]
    zipcode = line[1]
    lat = float(line[9])
    lng = float(line[10])
    pnt = Point(lat, lng)
    Zip(
        country_code=country_code,
        code=zipcode,
        pnt=pnt
    ).save()

def insert_data(line, minpop=1000):
    """Insert a line of data into the database.
    
    Args:
        line (string): one line in the geonames file.
        minpop (int): minimum population of added cities.
    """
    name = line[1]
    alt_names = line[3]
    lat = float(line[4])
    lng = float(line[5])
    feature_class = line[6]
    feature_code = line[7]
    country_code = line[8]
    population = int(line[14]) if line[14] else None
    elevation = int(line[15]) if line[15] else None

    pnt = Point(lat, lng)

    if feature_class == 'T':
        # T are mountains, rocks, cliffs...
        if feature_code in ['BLDR', 'CLF', 'CNYN', 'MT', 'MTS', 'PK', 'PKS', 'RK', 'RKS', 'RDGE']:
            place = Place(
                name=name,
                pnt=pnt,
                alt_names=alt_names,
                feature_class=feature_class,
                feature_code=feature_code,
                country_code=country_code,
                population=population,
                elevation=elevation)
            place.save()
    elif feature_class == 'P' and population >= minpop:
        # P is for cities etc.
        place = Place(
            name=name,
            pnt=pnt,
            alt_names=alt_names,
            feature_class=feature_class,
            feature_code=feature_code,
            country_code=country_code,
            population=population,
            elevation=elevation)
        zipobj = Zip.objects.filter(country_code=country_code).filter(
            pnt__dwithin=(pnt, .5)).annotate(
                distance=Distance('pnt', pnt)).order_by('distance').first()
        if zipobj:
            place.zipcode = zipobj
        place.save()


def import_file(fname, ftype=TYPES.place):
    """Import the content of a file to the database."""
    content = []
    if not os.path.isfile(fname):
        print("File {} does not exist.".format(fname))
        return
    print("Importing {}".format(fname))
    with open(fname, encoding='utf-8') as infile:
        for num, line in enumerate(infile):
            sys.stdout.write("\rImported %i lines." % num)
            sys.stdout.flush()
            line.strip()
            columns = line.split('\t')
            if ftype == TYPES.place:
                insert_data(columns)
            elif ftype == TYPES.zip:
                insert_zipdata(columns)
    print("\nDone.")

def flush_all():
    """Remove all places from db."""
    print("Flushing Places...")
    Place.objects.all().delete()
    print("Flushing Zipcodes...")
    Zip.objects.all().delete()

def flush_country(countrycode):
    """Flush places and zips with countrycode."""
    print("Flushing places for {}.".format(countrycode))
    Place.objects.filter(country_code=countrycode).delete()
    print("Flushing zipcodes for {}.".format(countrycode))
    Zip.objects.filter(country_code=countrycode).delete()

def import_all():
    """Import all files in the geonames dir."""
    placedir = 'geonames/'
    zipdir = 'geonames/zip/'
    filelist = get_flist(placedir)
    for fl in filelist:
        import_file(fl)

    zipfilelist = get_flist(zipdir)
    for fl in zipfilelist:
        import_file(fl, TYPES.zip)

class Command(BaseCommand):
    help = 'Import geoname files to the database using the *Place* model.'

    def add_arguments(self, parser):
       parser.add_argument(
           '--all',
           action='store_true',
           dest='import_all',
           default=False,
           help='Import all files in media/geonames.',
       )
       parser.add_argument(
           '--zip',
           nargs='+',
           dest='import_zip',
           help='Import zipcode files in media/geonames/zip.',
       )
       parser.add_argument(
           '--country',
           nargs='+',
           dest='import_file',
           help='Import a file in media/geonames providing the countrycode.',
       )

    def handle(self, *args, **options):
        if options['import_all']:
            flush_all()
            import_all()
        if options['import_file']:
            for countrycode in options['import_file']:
                flush_country(countrycode)
                fname = os.path.join(settings.MEDIA_ROOT, 'geonames/zip/{}.txt'.format(countrycode))
                import_file(fname, ftype=TYPES.zip)
                fname = os.path.join(settings.MEDIA_ROOT, 'geonames/{}.txt'.format(countrycode))
                import_file(fname)
        if options['import_zip']:
            for countrycode in options['import_zip']:
                fname = os.path.join(settings.MEDIA_ROOT, 'geonames/zip/{}.txt'.format(countrycode))
                import_file(fname, ftype=TYPES.zip)
