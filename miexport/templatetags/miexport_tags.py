"""miroutes template tags and filters."""
from django import template
from django.conf import settings

register = template.Library()

@register.simple_tag
def gen_hash(name=None, **kwargs):
    import hashlib, time
    m = hashlib.md5()
    if name:
        m.update(str(name).encode('utf-8'))
    else:
        m.update(str(time.time()).encode('utf-8'))
    return m.hexdigest()

@register.simple_tag
def phantom_snap_img(url, **kwargs):
    """
        Create a snapshot for a given url and return the url to a img
        You can also provide dimensions of the browser with dimx dimy
    """
    from miexport.models import PhantomSnap

    args = {'url': url, }
    if 'dim_x' in kwargs:
        args['dim_x'] = kwargs['dim_x']
    if 'dim_y' in kwargs:
        args['dim_y'] = kwargs['dim_y']

    snap, snap_created = PhantomSnap.objects.get_or_create(**args)
    if kwargs.get('use_cached', False)==False:
        snap.get_snap_signature().apply()
    img = snap.get_img_blocking()

    try:
        from django.core.files.storage import default_storage as storage
        if not storage.exists(img.path):
            raise(IOError)
        return img.url
    except Exception as e:
        print('Whatever went wrong here, failed to create Snapshot...',e)
        return img.url
