import time
import logging
import traceback
from html import escape
from PyPDF2 import PdfFileReader

from django.contrib.gis.measure import D
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse, JsonResponse, HttpResponseServerError
from django.conf import settings

from miexport.models import PhantomSnap
from miexport.util import absolute_snapshot_url
from miroutes.models import Wall, RouteGeometry, Userpic

logger = logging.getLogger(__name__)

def wallmap_export(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    nearby_radius=int(request.GET.get('nearby_radius', 0))

    subwalls = Wall.objects.filter(pnt__distance_lte=(wall.pnt, D(m=nearby_radius))).all()

    context = {
            'zoom': int(request.GET.get('zoom', 18)),
            'wall': wall,
            'subwalls': subwalls
            }

    return render(request, 'miexport/wallmap_export.html', context)


def wall_export(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    wallview = wall.pub_view

    # wall decorations
    markers = []

    if bool(int(request.GET.get("simple_markers", True))):
        markers.append(wallview.simplemarker_set.all())

    if bool(int(request.GET.get("relational_markers", True))):
        markers.append(wallview.relationalmarker_set.all())

    if bool(int(request.GET.get("pitch_markers", True))):
        markers.append(wallview.pitchmarker_set.all())

    if bool(int(request.GET.get("text_markers", True))):
        markers.append(wallview.textmarker_set.all())

    context = {
        'wall': wall,
        'wallview': wallview,
        'draw_routes': bool(int(request.GET.get("routes", True))),
        'markers': markers
    }

    return render(request, 'miexport/wall_fullscreen.html', context)

def wallimage_export(request, wall_id, max_px_limit=4096):
    """Export a wallimage of specified width or height for the browser window.
    If width is given, height is inferred from ratio of wall image.
    Same for height. If none is given, original size is used.
    The resulting image may still be smaller, depending on the content of the snapshotted page.
    """
    wall = get_object_or_404(Wall, pk=wall_id)
    ratio = wall.tiler.orig_img.height/wall.tiler.orig_img.width

    x_res = request.GET.get("xres"  , None)
    x_res = request.GET.get("width" , x_res)
    y_res = request.GET.get("yres"  , None)
    y_res = request.GET.get("height", y_res)

    def scale_factor_for_ratio(imsize, targetsize):
        fscale_width  = float(targetsize[0]) / float(imsize[0])
        fscale_height = float(targetsize[1]) / float(imsize[1])
        return min(fscale_width,fscale_height)

    orig_size   = wall.tiler.orig_img.width, wall.tiler.orig_img.height

    size_limits = [max_px_limit, max_px_limit]
    if x_res is not None:
        size_limits[0] = min(int(x_res), size_limits[0])
    if y_res is not None:
        size_limits[1] = min(int(y_res), size_limits[1])

    # limit the max dimensions for browser to max_px_limit px, otherwise chrome crashes
    target_size = list(map( lambda _: min(_[0], _[1]), zip(size_limits, orig_size)))
    #print("target_size:", target_size)
    #print("limits:", size_limits)
    #print("scale_fac:", scale_factor_for_ratio(orig_size, target_size))

    scale_factor = scale_factor_for_ratio(orig_size, target_size)
    x_res, y_res = [ int(Npx * scale_factor) for Npx in orig_size ]

    export_url = reverse('miexport:wall_export', kwargs={'wall_id': wall.id})
    #print("Exporting wallimage with x: {} and y: {} <= {}".format(x_res, y_res, export_url))

    # We create 3 snapshots with different things on it
    # first with everything (routes and all markers)
    ps1, created = PhantomSnap.objects.get_or_create(url=export_url, dim_x=x_res, dim_y=y_res)
    if not created:
        ps1.save(force_recreate=True)

    # then one without markers
    export_url += '?simple_markers=0&pitch_markers=0'
    ps2, created = PhantomSnap.objects.get_or_create(url=export_url, dim_x=x_res, dim_y=y_res)
    if not created:
        ps2.save(force_recreate=True)

    # and one without routes, just the image
    export_url += '&simple_markers=0&relational_markers=0&pitch_markers=0&text_markers=0&routes=0'
    ps3, created = PhantomSnap.objects.get_or_create(url=export_url, dim_x=x_res, dim_y=y_res)
    if not created:
        ps3.save(force_recreate=True)

    return render(request, 'miexport/wallimage_export.html', {
        'snap1': ps1,
        'snap2': ps2,
        'snap3': ps3,
        })


def wallimage_check(request, phantom_id):
    """Check if the snapshot is ready (via ajax)."""
    snap = get_object_or_404(PhantomSnap, pk=phantom_id)

    try:
        height = snap.img.height
    except FileNotFoundError:
        payload = {'success': False}
        return JsonResponse(payload, safe=False)

    payload = {'success': True,
               'url': snap.img.url}
    return JsonResponse(payload, safe=False)



def display_routegeom(request, routegeom_id):
    """Provide a full-screen display of a specific route geometry."""
    selected_geom = get_object_or_404(RouteGeometry, pk=routegeom_id)
    wallview = selected_geom.on_wallview

    # routes
    routegeomlist = wallview.routegeometry_set.exclude(geojson__isnull=True).all()
    # sort from left to right
    routegeomlist = list(routegeomlist)
    routegeomlist = sorted(routegeomlist, key=lambda x: x.anchorpoint[1])

    # annotate with the index
    for num, geom in enumerate(routegeomlist):
        geom.label = num + 1 # humans count from 1

    # wall decorations
    markers = [
        wallview.simplemarker_set.all(),
        wallview.relationalmarker_set.all(),
        wallview.pitchmarker_set.all(),
        wallview.textmarker_set.all()
    ]

    context = {
        'draw_routes': bool(int(request.GET.get("routes", True))),
        'selected_geom': selected_geom,
        'wall': wallview.wall,
        'wallview': wallview,
        'wall_routegeomlist': routegeomlist,
        'markers': markers
    }
    return render(request, 'miexport/wall_fullscreen.html', context)

def wall_qrcode_labels(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    qrcode_url = reverse('miroutes:wall_detail', kwargs={'wall_id': wall_id})
    external_qrcode_url = '{}{}{}'.format(settings.PROTOCOL, settings.SITE_DOMAIN, qrcode_url)

    context = {
            'wall': wall,
            'external_qrcode_url': external_qrcode_url,
            }

    return render(request, 'miexport/wall_qrcode_labels.html', context)


def wall_qrcode_labels_pdf(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    url = request.build_absolute_uri(reverse('miexport:wall_qrcode_labels', kwargs={'wall_id': wall_id}))

    try:
        pdf = chrome_url2pdf(url)

        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="QR-Labels_{}.pdf"'.format(wall.name)
    except Exception as e:
        logger.error("PDF EXPORT ERROR while snapping " + url + " : " + str(e) + " STACKTRACE: " + traceback.format_exc())
        response = HttpResponseServerError("Error creating pdf")

    return response


def wall_topo(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    wallview = wall.pub_view
    routegeomlist = wallview.routegeometry_set.exclude(geojson__isnull=True).all()

    # sort from left to right
    routegeomlist = list(routegeomlist)
    routegeomlist = sorted(routegeomlist, key=lambda x: x.anchorpoint[1])

    # annotate with the index
    for num, geom in enumerate(routegeomlist):
        geom.label = num + 1 # humans count from 1

    # wall decorations
    markers = [
        wallview.simplemarker_set.all(),
        wallview.relationalmarker_set.all(),
        wallview.pitchmarker_set.all(),
        wallview.textmarker_set.all()
    ]

    qrcode_url = reverse('miroutes:wall_detail', kwargs={'wall_id': wall_id})
    external_qrcode_url = '{}{}{}'.format(settings.PROTOCOL, settings.SITE_DOMAIN, qrcode_url)

    wall_info_materials = wall.userpic_set.filter(content='info')
    wall_info_materials = wall.userpic_set.filter(content="info") | Userpic.objects.filter(
        on_route__in=wall.pub_view.route_set.all()).filter(content="info")

    # Annotate pdf page count and size
    for material in wall_info_materials:
        if bool(material.pdf_file):
            with open(material.pdf_file.path, 'rb') as pdf_fh:
                pdf = PdfFileReader(pdf_fh)
                page_count =  pdf.getNumPages()
                page_size = pdf.getPage(0).mediaBox
            material.pdf_file.page_counter = range(1,page_count+1)
            material.pdf_file.width, material.pdf_file.height = page_size[2:]
            material.pdf_file.external_url = '{}{}{}'.format(settings.PROTOCOL, settings.SITE_DOMAIN, material.pdf_file.url)

    context = {
        'wall': wall,
        'wallview': wallview,
        'wall_routegeomlist': routegeomlist,
        'markers': markers,
        'external_qrcode_url': external_qrcode_url,
        'wall_info_materials': wall_info_materials,
    }

    return render(request, 'miexport/wall_topo.html', context)


def wall_topo_pdf(request, wall_id):
    wall = get_object_or_404(Wall, pk=wall_id)

    url = absolute_snapshot_url(reverse('miexport:wall_topo', kwargs={'wall_id': wall_id}))

    logger.debug("PDF snapping URL: " + url)

    try:
        pdf = chrome_url2pdf(url)

        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="Topo_{}.pdf"'.format(wall.name)
    except Exception as e:
        logger.error("PDF EXPORT ERROR while snapping " + url + " : " + str(e) + " STACKTRACE: " + traceback.format_exc())
        response = HttpResponseServerError("Error creating pdf")

    return response


def chrome_url2pdf(url, outfile=None):
    import json
    import base64
    import os
    import tempfile
    from miexport.tasks import snap_url

    if outfile is None:
        file_handle, file_name = tempfile.mkstemp()
        os.close(file_handle)
        #png_file_handle, png_file_name = tempfile.mkstemp(suffix='.png')
        #os.close(png_file_handle)
    else:
        file_name = outfile

    snap_url(url, target_pdf=file_name)
    #snap_url(url, target_png=png_file_name, target_pdf=file_name)

    file_handle = open(file_name, mode='rb')

    pdf_bytes = file_handle.read()

    file_handle.close()

    if outfile is None:
        os.unlink(file_name)
        #os.unlink(png_file_name)

    return pdf_bytes
