import os
import time

from django.conf import settings


def absolute_snapshot_url(relativeUrl: str) -> str:
    if relativeUrl.startswith('http'):
        return relativeUrl

    if settings.SNAPSHOT_HOST:
        snap_host = settings.SNAPSHOT_HOST
    else:
        snap_host = "web:8000"

    auth_prefix = ""
    if settings.PHANTOMJS_BASIC_AUTH != "":
        auth_prefix = settings.PHANTOMJS_BASIC_AUTH + "@"

    return 'http://{}{}{}'.format(auth_prefix, snap_host, relativeUrl)

# The following approach failed on me because it checked before the file was even written to disk. Also if we are using the screenshot functions on temp files, it is not even guaranteed that there is a proper file handle... I implemented syscall flush and fsync when writing snapshots instead 
#def get_size(path: str) -> int:
#    return os.stat(path).st_size
#
#def block_until_file_size_stable(path: str):
#
#    INTERVAL = 0.1
#    ATTEMPTS = 10
#
#    for i in range(0,ATTEMPTS):
#        before = get_size(path)
#        time.sleep(INTERVAL)
#        after = get_size(path)
#
#        if before == after:
#            return
