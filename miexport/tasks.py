import logging
import os
import time

from PIL import Image
from io import BytesIO

from django.conf import settings
from django.urls import reverse
from selenium import webdriver

from miexport import util
from .util import absolute_snapshot_url

logger = logging.getLogger(__name__)

from celery import shared_task


def trim_snapshot(path):
    from subprocess import check_output, STDOUT
    cmd = ['mogrify', '-trim', '-verbose', path]
    try:
        rc = check_output(cmd, stderr=STDOUT)
    except Exception as e:
        print(("miexport/snapshottool :: Error trimming a snapshot {} => {}".format(path, e.output)))

def phantom_wait_for_div_hidden(browser, loading_class='phantom_loading', timeout=30, default_timeout=10, dt=.1):

    loading_content_divs = browser.find_elements_by_class_name(loading_class)
    if len(loading_content_divs) > 0:
        logger.info('found loading_content_div :: {}'.format(loading_content_divs))
        t = timeout  # seconds
        N  = int(t / dt)
        for i in range(N):
            try:
                print((browser.current_url,' ... waiting adaptively',i*dt,t))
                if all([ not div.is_displayed() for div in loading_content_divs]):
                    return True
            except: # it can happen that selenium is unresponsive while waiting.... in this case just wait a little
                pass 
            time.sleep(dt)

        if any([div.is_displayed() for div in loading_content_divs]):
            logger.warn("I waited for {} seconds but the loading_content div is still visible... seems that the js did not finish".format(t))

    else:
        logger.info('no loading_content_div found, setting to None, and wait {} seconds to be save'.format(default_timeout))
        time.sleep(default_timeout)

    return False


@shared_task(ignore_result=True, time_limit=10*60, soft_time_limit=5*60)
def snap_url(surl, dimensions=(1024, 768), target_png=None, target_pdf=None, waiting_info={}):
    from urllib.parse import urlparse
    import signal

    auth_prefix = ""
    if settings.PHANTOMJS_BASIC_AUTH != "":
        auth_prefix = settings.PHANTOMJS_BASIC_AUTH + "@"

    logger.info("Creating PhantomJS pic for url: {} with dims {} and saving to {}".format(surl, dimensions, target_png))
    absolute_url = absolute_snapshot_url(surl)
    logger.info("Final URL we fetch is now: {}".format(absolute_url))

    if any((int(d)>4096 for d in dimensions)):
        logger.warn("We try to snapshot a page with browser dimensions ({},{})".format(*dimensions) +
                    "... this is pretty big! Values above 4096 may lead to a crash of chrome and should not be used")

    try:
        #browser = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true'])
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        options.add_argument("--no-sandbox")
        options.add_argument("--window-size={},{}".format(*dimensions))
        options.add_experimental_option('w3c', False) # to prevent errors like "Cannot call non W3C standard command while in W3C mode"

        browser = webdriver.Chrome(chrome_options=options)
        browser.get(absolute_url)

        phantom_wait_for_div_hidden(browser, **waiting_info)
        time.sleep(.5)

        if target_pdf is not None:
            try:
                logger.info('SAVING PDF TO: {}'.format(target_pdf))
                success = chrome_save_as_pdf(browser, target_pdf, { 'landscape': False })
                os.sync()
                time.sleep(.5) # always wait .1 second -- this may help with read errors
            except:
                pass

        if target_png is not None:
            png_data = browser.get_screenshot_as_png()
            im = Image.open(BytesIO(png_data))
            im.save(target_png)
            if not os.path.exists(target_png):
                raise Exception("Failed writing the snapshot file")
            im.close()
            os.sync()
            time.sleep(.5) # always wait .5 second -- this may help with read errors

            # try to verify the png... sometimes the write is bad.
            #   in this case we try it again
            validation_im = Image.open(target_png)
            try:
                validation_im.verify()
            except Exception as e:
                im = Image.open(BytesIO(png_data))
                im.save(target_png)
                im.close()
            finally:
                validation_im.close()

            validation_im = Image.open(target_png)
            try:
                validation_im.verify()
            except Exception as e:
                raise e
            finally:
                validation_im.close()

            trim_snapshot(target_png)


        if target_pdf is not None:
            if not os.path.exists(target_pdf):
                raise Exception("Failed writing the snapshot pdf file")

    except Exception as e:
        logger.warn(('An error occured while snapshotting: {}'.format(e)))
        raise

    finally:
        time.sleep(.2) # needed, otherwise the quit does not close the browser correctly... there still does not seem to be a good fix...
        if 'browser' in locals():
            browser.quit()


"""
    The following two function to take chrome headless pdfs
    Info From: https://stackoverflow.com/questions/47023842/selenium-chromedriver-printtopdf
"""
def send_devtools(driver, cmd, params={}):
    import json
    resource = "/session/%s/chromium/send_command_and_get_result" % driver.session_id
    url = driver.command_executor._url + resource
    body = json.dumps({'cmd': cmd, 'params': params})
    response = driver.command_executor._request('POST', url, body)
    if response['status']:
        raise Exception(response.get('value'))
    return response.get('value')

def chrome_save_as_pdf(driver, path, options={}):
    import base64
    # https://timvdlippe.github.io/devtools-protocol/tot/Page#method-printToPDF
    result = send_devtools(driver, "Page.printToPDF", options)
    with open(path, 'wb') as file:
      file.write(base64.b64decode(result['data']))
      file.flush()
      os.fsync(file.fileno())
    return 'data' in result

def chrome_save_as_img(driver, path, options={}):
    import base64
    # https://timvdlippe.github.io/devtools-protocol/tot/Page#method-printToPDF
    result = send_devtools(driver, "Page.captureScreenshot", options)
    with open(path, 'wb') as f:
      f.write(base64.b64decode(result['data']))
      f.flush()
      os.fsync(f.fileno())
    return 'data' in result
