import os

from django.db import models
from django.dispatch import receiver
from django.core.files.storage import default_storage as storage
from django.utils.text import slugify

def get_phantom_snap_upload_path(fname):
    basedir = "phantom_snaps"
    path = os.path.join(basedir, fname)
    if not storage.exists(os.path.dirname(path)):
        os.makedirs(storage.path(os.path.dirname(path)))
    return path

class PhantomSnap(models.Model):
    """
    Holds the pdf/png and the associated url.
    """
    url = models.URLField()
    opt_file_name = models.CharField(max_length=100, null=True, blank=True)

    dim_x = models.PositiveIntegerField(default=1024)
    dim_y = models.PositiveIntegerField(default=768)

    img = models.ImageField(blank=True, null=True, upload_to=get_phantom_snap_upload_path, max_length=256)
    pdf = models.FileField (blank=True, null=True, upload_to=get_phantom_snap_upload_path, max_length=256)

    last_edited_date = models.DateTimeField(auto_now=True)

    @property
    def img_exists(self):
        if self.img:
            return storage.exists(self.img.path)
        else:
            return False

    @property
    def pdf_exists(self):
        if self.pdf:
            return storage.exists(self.pdf.path)
        else:
            return False

    def get_img_blocking(self):
        if not self.img or not storage.exists(self.img.name):
            self.get_snap_signature().apply()
        return self.img

    def get_pdf_blocking(self):
        if not self.pdf or not storage.exists(self.pdf.name):
            self.get_snap_signature().apply()
        return self.pdf

    def update_paths(self):
        if self.opt_file_name is None:
            prefix = slugify(self.url) + '_{}_{}'.format(self.dim_x, self.dim_y)
        else:
            prefix = self.opt_file_name

        self.pdf = get_phantom_snap_upload_path(prefix + '.pdf')
        self.img = get_phantom_snap_upload_path(prefix + '.png')

    def set_opt_file_name(self, fname):
        self.opt_file_name = fname
        self.update_paths()

    def create_phantom_snap(self, save_img=True, force_recreate=False):
        from django.utils.text import slugify
        from miexport.tasks import snap_url

        self.update_paths()
        self.save(save_img=False, update_fields=['img','pdf'])

        if not save_img:
            return # just return, we have saved the img and pdf objects but dont run the snapshotter
        if (not self.img
            or force_recreate
            or not storage.exists(self.img.name)
            or not self.pdf
            or not storage.exists(self.pdf.name)):
            self.get_snap_signature().apply_async()

    def save(self, save_img=True, force_recreate=False, *args, **kwargs):
        super(PhantomSnap, self).save(*args, **kwargs)
        if force_recreate or (not self.img) or (not self.pdf):
            self.create_phantom_snap(save_img, force_recreate=force_recreate)


    def get_snap_signature(self, signature_kwargs={}):
        from miexport.tasks import snap_url

        #print("Creating Snapshot Signature for", self.url)
        job_signature = snap_url.signature(
                args=(self.url,),
                kwargs={
                    'dimensions':(self.dim_x, self.dim_y),
                    'target_png':storage.path(self.img.name),
                    'target_pdf':storage.path(self.pdf.name)},
                **signature_kwargs,)

        return job_signature

@receiver(models.signals.post_delete, sender=PhantomSnap)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `PhantomSnap` object is deleted.
    """
    if instance.img:
        if os.path.isfile(instance.img.path):
            os.remove(instance.img.path)
    if instance.pdf:
        if os.path.isfile(instance.pdf.path):
            os.remove(instance.pdf.path)

@receiver(models.signals.pre_save, sender=PhantomSnap)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `PhantomSnap` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = PhantomSnap.objects.get(pk=instance.pk).img
        new_file = instance.img
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
    except:
        pass

    try:
        old_file = PhantomSnap.objects.get(pk=instance.pk).pdf
        new_file = instance.pdf
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
    except:
        pass
