from django.conf import settings
from django.test import TestCase

from miexport.util import absolute_snapshot_url


class TestUtil(TestCase):

    def test_absolute_snapshot_url(self):

        settings.PHANTOMJS_BASIC_AUTH = ""
        self.assertEqual("http://web:8000/some/relative/url", absolute_snapshot_url("/some/relative/url"))

        settings.PHANTOMJS_BASIC_AUTH = "mitopo:power"
        self.assertEqual("http://mitopo:power@web:8000/some/relative/url", absolute_snapshot_url("/some/relative/url"))

        settings.PHANTOMJS_BASIC_AUTH = ""
        settings.SNAPSHOT_HOST = "nginx:80"
        self.assertEqual("http://nginx:80/some/relative/url", absolute_snapshot_url("/some/relative/url"))

        settings.PHANTOMJS_BASIC_AUTH = "mitopo:power"
        settings.SNAPSHOT_HOST = "nginx:80"
        self.assertEqual("http://mitopo:power@nginx:80/some/relative/url", absolute_snapshot_url("/some/relative/url"))
