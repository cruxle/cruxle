from django.conf.urls import *

from miexport import views

app_name = 'miexport'

urlpatterns = [
    url(r'^wallmap_export/wall(?P<wall_id>[0-9]*)', views.wallmap_export, name='wallmap_export'),
    url(r'^wall_export/wall(?P<wall_id>[0-9]*)', views.wall_export, name='wall_export'),
    url(r'^wallimage_export/wall(?P<wall_id>[0-9]*)', views.wallimage_export, name='wallimage_export'),
    url(r'^wallimage_check/snap(?P<phantom_id>[0-9]*)', views.wallimage_check, name='wallimage_check'),
    url(r'^routegeom(?P<routegeom_id>\d+)$', views.display_routegeom, name='display_routegeom'),
    url(r'^wall(?P<wall_id>\d+)/qrcode_labels$', views.wall_qrcode_labels, name='wall_qrcode_labels'),
    url(r'^wall(?P<wall_id>\d+)/qrcode_labels_pdf$', views.wall_qrcode_labels_pdf, name='wall_qrcode_labels_pdf'),
    url(r'^wall(?P<wall_id>\d+)/topo$', views.wall_topo, name='wall_topo'),
    url(r'^wall(?P<wall_id>\d+)/topo_pdf$', views.wall_topo_pdf, name='wall_topo_pdf'),
]
