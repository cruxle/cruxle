"use strict";
var page = require('webpage').create();
var system = require('system');
var address = system.args[1];
var output = system.args[2];

page.viewportSize = {width: 794, height: 944};
page.paperSize = {width: "874px", height: "1240px", margin: '40px'};

page.open(address, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit(1);
    } else {
        window.setTimeout(function () {
            page.render(output, {format: "pdf"});
            phantom.exit();
        }, 10000);
    }
});
