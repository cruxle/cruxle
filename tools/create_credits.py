#!/bin/python
import json
import os
from subprocess import check_output

out = check_output(["/data/jonas/programs/node-v6.9.5-linux-x64/bin/bower list -j"],
                   cwd=os.path.dirname(os.path.realpath(__file__)) + "/../", shell=True)

out_json = json.loads(out)

print("<ul>")
for key, value in list(out_json['dependencies'].items()):
    print((
        "<li><a href=\"{}\">{}</a> ({}) - {}</li>".format(value['pkgMeta']['homepage'],
                                                          value['pkgMeta']['name'],
                                                          value['pkgMeta'].get('license', ""),
                                                          value['pkgMeta'].get('description', ""))))
print("</ul>")
