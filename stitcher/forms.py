"""Stitcher Forms"""
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML
from django import forms
from django.forms import MultipleChoiceField, ModelMultipleChoiceField
from django.utils.translation import ugettext_lazy as _
from django_file_form.forms import FileFormMixin, MultipleUploadedFileField

from miroutes.models import License
from miroutes.widgets import LicenseChooser


def get_licenses():
    return list(License.objects.all().order_by("-required", "short_name"))

class PanoForm(FileFormMixin, forms.Form):
    img = MultipleUploadedFileField(label=_('Panorama Parts'))
    licenses = ModelMultipleChoiceField(queryset=License.objects.all(), widget = LicenseChooser(licenses_callback=get_licenses))

    def __init__(self, *args, **kwargs):
        super(PanoForm, self).__init__(*args, **kwargs)

        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-1 col-md-2 col-xs-12'
        self.helper.field_class = 'col-lg-7 col-md-8 col-xs-12'
        self.helper.layout = Layout(*list(self.fields.keys()))  # use all fields

        self.helper.form_id = 'pano-form'
        self.helper.form_method = 'post'
        self.helper.layout.append(HTML("""
        {% load i18n %}
        <a type="button" href="{{ next_page }}" class="btn btn-info">
        <i class="glyphicon glyphicon-arrow-left" aria-hidden="true"></i>
        {% trans "Back" %}
        </a>
        <button id="submit-button" type="submit" class="btn btn-success">
        <span class="glyphicon glyphicon-cog"></span> {% trans "Try it!" %}</button>
        """))
