from django.contrib import admin
from reversion.admin import VersionAdmin

from stitcher.models import Pano, PanoPart

# Register your models here.

admin.site.register(Pano)
admin.site.register(PanoPart)
