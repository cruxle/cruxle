from celery import shared_task,task

from django.conf import settings

from django.core.files.storage import default_storage as storage

from subprocess import call, check_output, STDOUT
from django.core.mail import send_mail

import re

@shared_task(ignore_result=True, time_limit=3600*24*5, soft_time_limit=3600*24*4)
def stitch_files(infiles, output, workdir, restart=False, pano_id=None):
    """
    Automatic Panorama Stitcher with Hugin:
    Args:
        infiles -- absolute location of images, e.g. /code/www/media/pano.... img_a.JPG etc
        output -- django filename where to put the output, e.g. pano/pano.png
        workdir -- absolute path to a working directory, will be used to store intermediate files
        restart -- can omit restarting from intermediate files
        pano_id -- ID of Pano object that will hold info about progress etc.
    """
    from stitcher.models import StitcherStatus
    from PIL import Image
    from contextlib import closing
    import os, glob

    print(('Trying to create Pano for: ',infiles))
    print(('will save pano to: ',output))
    # Clear status and error message if there is any
    update_stitcher_info(pano_id, info=('output_creating', None))
    update_stitcher_info(pano_id, info=('output_cpfind', None))
    update_stitcher_info(pano_id, info=('output_cpclean', None))
    update_stitcher_info(pano_id, info=('output_linefind', None))
    update_stitcher_info(pano_id, info=('output_optimising', None))
    update_stitcher_info(pano_id, info=('output_cropping', None))
    update_stitcher_info(pano_id, info=('output_stitching', None))
    update_stitcher_info(pano_id, info=('output_blending', None))
    update_stitcher_info(pano_id, info=('output_convert_2_png', None))
    update_stitcher_info(pano_id, info=('output_enhancing', None))
    update_stitcher_info(pano_id, info=('output_error', None))

    try:
        os.environ['OMP_NUM_THREADS'] = '1'  # set openmp to just one thread, otherwise oversubscribe for N workers

        outdir = storage.path(os.path.dirname(output))
        if not storage.exists(outdir):
            os.mkdir(outdir)

        update_stitcher_info(pano_id, status=StitcherStatus.CREATING)
        if not storage.exists(workdir):
            os.mkdir(workdir)

        os.chdir(workdir)

        if not restart:
            [ os.remove(f) for f in glob.glob('*.pto') ]
            [ os.remove(f) for f in glob.glob('Makefile') ]
            [ os.remove(f) for f in glob.glob('pano*.tif') ]

        projectfile = os.path.join(workdir,'project.pto')
        projectfile = 'project.pto'

        rc = check_output(['pto_gen', '-o', projectfile,] + sorted(infiles))
        if rc:
            update_stitcher_info(pano_id, info=('output_creating', rc))

        update_stitcher_info(pano_id, status=StitcherStatus.CPFIND)
        cpfindpto = 'cpfind.pto'
        cmd = ['cpfind', '-o', cpfindpto, '--celeste', '--fullscale', '--ncores=1', '--multirow', projectfile]
        rc = run_pto_cmd(projectfile, cmd, cpfindpto)
        if rc:
            update_stitcher_info(pano_id, info=('output_cpfind', rc))

        update_stitcher_info(pano_id, status=StitcherStatus.CPCLEAN)
        cpcleanpto = 'cpclean.pto'

        cmd = ['cpclean', '-o', cpcleanpto, projectfile]
        rc = run_pto_cmd(projectfile, cmd, cpcleanpto)

        cmd = ['celeste_standalone', '-o', cpcleanpto, '-i', projectfile]
        rc_celeste = run_pto_cmd(projectfile, cmd, cpcleanpto)

        if rc_celeste:
            if rc:
                rc += rc_celeste
            else:
                rc = rc_celeste

        if rc:
            update_stitcher_info(pano_id, info=('output_cpclean', rc))
            if re.search(b'unconnected image pairs', rc):
                errmsg = 'output_cpfind :: output_cpclean :: There are images which I could not connect to any others.'
                raise Exception(errmsg)

        update_stitcher_info(pano_id, status=StitcherStatus.LINEFIND)
        linefindpto = 'linefind.pto'
        cmd = ['linefind', '-o', linefindpto, projectfile]
        rc = run_pto_cmd(projectfile, cmd, linefindpto)
        if rc:
            update_stitcher_info(pano_id, info=('output_linefind', rc))

        update_stitcher_info(pano_id, status=StitcherStatus.OPTIMISING)
        optipto = 'opti.pto'
        cmd = ['autooptimiser', '-a', '-l', '-m', '-s', '-o', optipto, projectfile]
        rc = run_pto_cmd(projectfile, cmd, optipto)
        if rc:
            update_stitcher_info(pano_id, info=('output_optimising', rc))
            selected_points = int(re.search(b'Selected\s(\d+)\spoints', rc).groups()[0])
            if selected_points < 5:
                raise Exception('{} :: Did not find enough control points : {}'.format('output_optimising', selected_points))

        # Pano modify
        update_stitcher_info(pano_id, status=StitcherStatus.CROPPING)
        panomodifypto = 'panomodify.pto'
        cmd = ['pano_modify', '--canvas=AUTO', '--projection=11', '-s', '-c', '-o', panomodifypto, projectfile]
        rc = run_pto_cmd(projectfile, cmd, panomodifypto)
        if rc:
            update_stitcher_info(pano_id, info=('output_cropping', rc))
            Nx, Ny = [ int(_) for _ in re.search(b'Setting canvas size to\s(\d+)\sx\s(\d+)', rc).groups()[:]]
            if Nx*Ny > 4e9:
                errmsg = """{} :: The Panorama would be {} x {} in size.
                        We do not allow panorama images that would exceed four giga pixel, because stitching those might overwhelm our machines.
                        Please try on your own computer or ask us to stitch it by mail.""".format('output_cropping', Nx, Ny)
                raise Exception(errmsg)

        # Stitching with nona
        update_stitcher_info(pano_id, status=StitcherStatus.STITCHING)

        if not os.path.exists('pano.tif') or restart:
            cmd = ['nona', '-v', '-g', '-m', 'TIFF_m', '-o', 'pano', projectfile]
            try:
                rc = check_output(cmd, stderr=STDOUT)
                update_stitcher_info(pano_id, info=('output_stitching', rc))
            except Exception as e:
                update_stitcher_info(pano_id, info=('output_stitching', e.output))
                errmsg = '{} :: {} : {}'.format('output_stitching', e, e.output)
                raise Exception(errmsg)


        # Enblend
        update_stitcher_info(pano_id, status=StitcherStatus.BLENDING)

        if not os.path.exists('pano.tif') or restart:
            filelist = glob.glob('pano0*.tif')
            cmd = ['enblend', '-v', '--pre-assemble', '--primary-seam-generator=nft', '--compression=lzw', '-o', 'pano.tif']
            cmd = ['verdandi', '--compression=LZW', '--seam=hard', '-o', 'pano.tif']
            cmd.extend(filelist)
            try:
                rc = check_output(cmd, stderr=STDOUT)
                update_stitcher_info(pano_id, info=('output_blending', rc))
            except Exception as e:
                update_stitcher_info(pano_id, info=('output_blending', e.output))
                errmsg = '{} :: {} : {}'.format('output_blending', e, e.output)
                raise Exception(errmsg)


        # Convert to PNG
        update_stitcher_info(pano_id, status=StitcherStatus.CONVERTING)
        panotif = os.path.join(workdir, 'pano.tif')
        if not os.path.exists(output) or restart:
            cmd = ['convert', '-trim', '-verbose', panotif, storage.path(output)]

            try:
                rc = check_output(cmd, stderr=STDOUT)
                update_stitcher_info(pano_id, info=('output_convert_2_png', rc))
            except Exception as e:
                update_stitcher_info(pano_id, info=('output_convert_2_png', e.output))
                errmsg = '{} :: {} : {}'.format('output_convert_2_png', e, e.output)
                raise Exception(errmsg)

        # Convert with Imagemagick, trim and enhance contrast
        update_stitcher_info(pano_id, status=StitcherStatus.ENHANCING)
        cmd = ['mogrify', '-trim', '-verbose', storage.path(output)]
        try:
            rc = check_output(cmd, stderr=STDOUT)
            update_stitcher_info(pano_id, info=('output_enhancing', rc))
        except Exception as e:
            update_stitcher_info(pano_id, info=('output_enhancing', e.output))
            pass # dont reraise error, enhancing is not that important...

        save_pano_2_django(pano_id, output)
        update_stitcher_info(pano_id, status=StitcherStatus.FINISHED)


        cmd = ['exiftool', '-TagsFromFile', infiles[0], storage.path(output)]
        rc = check_output(cmd, stderr=STDOUT)

    except Exception as e:
        msg = '{}'.format(e)
        if 'output' in e.__dict__:
            msg = '{} :: {}'.format(msg, e.output)
        print('ERROR in Stitcher:', msg)
        update_stitcher_info(pano_id, status=StitcherStatus.ERROR,
                            info=('output_error', msg))

    finally:
        from django.urls import reverse
        from stitcher.models import Pano, StitcherStatus
        from mitopo.utils import absolute_url

        pano = Pano.objects.get(pk=pano_id)

        # Send an email with the confirmation link
        if pano.status == StitcherStatus.FINISHED:
            email_body_plain = """
            Yay! We finished your panorama stitching.
            Please have a look at #Pano{}
            """.format(pano.pk)
        else:
            email_body_plain = """
            Doh! We tried to stitch your panorama. But we fear that something went wrong.
            Please have a look at #Pano{}
            """.format(pano.pk)

        from postbox.utils import message_from_cruxle
        message_from_cruxle(pano.created_by, email_body_plain)

    return

def save_pano_2_django(pano_id, output):
    from stitcher.models import Pano
    if pano_id is not None:
        from stitcher.models import Pano
        pano = Pano.objects.get(pk=pano_id)
        pano.img = output
        pano.save()

def run_pto_cmd(projectfile, cmd, savepto):
    import os, shutil
    if not os.path.exists(savepto):
        rc = check_output(cmd)
        shutil.copyfile(savepto, projectfile)
        return rc
    return None

def update_stitcher_info(pano_id, status=None, info=None):
    """
    Update Stitcher object with a new status and/or add the stitcher output.
    status may be one of StitcherStatus attributes and info is a tuple of (output_field, text)
    """
    from stitcher.models import Pano
    if pano_id is not None:
        from stitcher.models import Pano
        pano = Pano.objects.get(pk=pano_id)
        if status is not None:
            pano.status = status
        if info is not None:
            pano.__setattr__(*info)
        pano.save()
