from rest_framework.generics import RetrieveAPIView
from stitcher.models import Pano
from stitcher.serializers import PanoSerializer

class PanoDetail(RetrieveAPIView):
    """Generic API view to retrieve Pano status."""
    queryset = Pano.objects.all()
    serializer_class = PanoSerializer

