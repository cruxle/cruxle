from rest_framework import serializers
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import date as _date
from django.templatetags.static import static
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer

from users.serializers import UserSerializer
from stitcher.models import Pano

class PanoSerializer(serializers.ModelSerializer):
    """Serializer for the Pano class."""
    class Meta:
        model = Pano
        fields = [
            'id',
            'status',
            'output_creating',
            'output_cpfind',
            'output_cpclean',
            'output_linefind',
            'output_optimising',
            'output_cropping',
            'output_stitching',
            'output_blending',
            'output_convert_2_png',
            'output_enhancing',
            'output_error',
            'get_url',
            ]

class PanoWorkSerializer(serializers.ModelSerializer):
    """Serializer for the Pano class, used in my-edits section, has to have a certain form for workbench.js"""
    name = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    edit_url = serializers.SerializerMethodField(read_only=True)
    thumb = serializers.SerializerMethodField(read_only=True)
    last_edit = serializers.SerializerMethodField(read_only=True)
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = Pano
        fields = [
            'id',
            'status',
            'created_by',
            'last_edit',
            'name',
            'url',
            'edit_url',
            'thumb']

    def get_name(self, pano):
        return _('Panorama {id} @ {datestring}\n *{nrpics} images').format(id=pano.pk, datestring=_date(pano.updated_at), nrpics=pano.panopart_set.count())

    def get_thumb(self, pano):
        try:
            return get_thumbnailer(pano.img)['pano_parts'].url
        except InvalidImageFormatError:
            return static("topo/img/material-placeholder.png")

    def get_url(self, pano):
        return reverse('stitcher:view_pano', kwargs={'pano_id': pano.pk})

    def get_edit_url(self, pano):
        return reverse('stitcher:view_pano', kwargs={'pano_id': pano.pk})

    def get_last_edit(self, pano):
        return pano.updated_at
