from django.conf.urls import *

from stitcher import views, classviews

app_name = 'stitcher'

urlpatterns = [
    url(r'^add_pano$', views.add_pano, name='add_pano'),
    url(r'^pano(?P<pano_id>\d+)/view$', views.view_pano, name='view_pano'),
    url(r'^pano(?P<pano_id>\d+)/run_pano_stitcher$', views.run_pano_stitcher, name='run_pano_stitcher'),

    url(r'^pano(?P<pk>[0-9]+)/get_pano$', classviews.PanoDetail.as_view(), name='get_pano'),
    url(r'^processing_request', views.processing_request_page, name='processing_request_page'),
]
