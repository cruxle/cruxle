# -*- coding: utf-8 -*-
import os
from time import gmtime, strftime

from django.db import models
from django.core.files.storage import default_storage as storage
from django.utils.translation import gettext

from miroutes.models import License
from stitcher.tasks import stitch_files
from django.contrib.auth.models import User

from miroutes.wall_img_functions import get_exif_tags


def get_pano_img_path(pano, fname):
    """
    Define folder where to put wall images and tiles inside media dir
    Args:
      filename: Filename of image
    """
    basename = os.path.splitext(os.path.basename(fname))[0]
    path = os.path.join("panos", str(pano.pk), basename)
    # print('get_wall_upload_path', path)
    return path

def get_pano_part_img_path(part, fname):
    """
    Define folder where to put wall images and tiles inside media dir
    Args:
      filename: Filename of image
    """
    time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
    basename = time + '_' + os.path.basename(fname)
    path = os.path.join("panos", str(part.pano.pk), "parts", basename)
    # print('get_wall_img_path', path)
    return path

# Create your models here.

class StitcherStatus():
    PENDING       = 0
    QUEUED        = 1
    CREATING      = 2
    CPFIND        = 3
    CPCLEAN       = 4
    LINEFIND      = 5
    OPTIMISING    = 6
    CROPPING      = 7
    STITCHING     = 8
    BLENDING      = 11
    CONVERTING    = 9
    ENHANCING     = 10
    FINISHED      = 100
    ERROR         = 999
    CHOICES = (
            (PENDING      , 'not yet submitted'),
            (QUEUED       , 'queued'),
            (CREATING      , 'creating new panorama project'),
            (CPFIND        , 'searching for reference points'),
            (CPCLEAN       , 'cleaning unreliable reference points'),
            (LINEFIND      , 'search for horizontal/vertical reference lines, e.g. the horizon'),
            (OPTIMISING    , 'geometric optimization of picture alignment'),
            (CROPPING      , 'determine correct image size'),
            (STITCHING     , 'stitching... this may take a while'),
            (BLENDING      , 'blending ... this may take a while'),
            (ENHANCING     , 'Enhancing colorspace'),
            (FINISHED     , 'finished'),
            (ERROR        , 'error'))


class PanoPart(models.Model):
    """ Hosts panorama parts """
    img = models.ImageField(upload_to=get_pano_part_img_path, max_length=256)
    pano = models.ForeignKey('Pano', on_delete=models.CASCADE)

    def get_exif(self):
        return get_exif_tags(self.img.name)


class Pano(models.Model):
    """ Hosts all the info about the stitching process """
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    img = models.ImageField(blank=True, null=True, max_length=256)
    licenses = models.ManyToManyField(License, verbose_name=gettext("Image Licenses"))

    status = models.PositiveSmallIntegerField(default=0, choices=StitcherStatus.CHOICES)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    output_creating      = models.TextField(blank=True, null=True)
    output_cpfind        = models.TextField(blank=True, null=True)
    output_cpclean       = models.TextField(blank=True, null=True)
    output_linefind      = models.TextField(blank=True, null=True)
    output_optimising    = models.TextField(blank=True, null=True)
    output_cropping      = models.TextField(blank=True, null=True)
    output_stitching     = models.TextField(blank=True, null=True)
    output_blending      = models.TextField(blank=True, null=True)
    output_convert_2_png = models.TextField(blank=True, null=True)
    output_enhancing     = models.TextField(blank=True, null=True)
    output_error         = models.TextField(blank=True, null=True)

    def run_stitching(self, delay=True):
        files = [storage.path(part.img.name) for part in self.panopart_set.all() ]
        panodir  = get_pano_img_path(self, 'pano')
        panofile = os.path.join(panodir, 'pano.png')
        panoworkdir  = storage.path(os.path.join(panodir, 'work/'))
        if delay:
            stitch_files.delay(files, panofile, panoworkdir, restart=False, pano_id=self.id)
        else:
            stitch_files(files, panofile, panoworkdir, restart=False, pano_id=self.id)

    def get_url(self):
        return storage.url(self.img.name)

    def get_exif(self):
        return get_exif_tags(storage.path(self.img.name))
