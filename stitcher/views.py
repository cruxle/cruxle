# -*- coding: utf-8 -*-


from django.utils.translation import ugettext_lazy as _

from django.contrib import messages
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse

from django.contrib.auth.decorators import permission_required

from stitcher.models import PanoPart, Pano
from stitcher.forms import PanoForm
# Create your views here.

@permission_required('miroutes.add_wall')
def add_pano(request):
    """Add additional material."""
    next_page = request.GET.get('next', reverse('edit_spot:add_wall_img'))

    class ImageUploadError(Exception):
        pass

    if request.POST:
        form = PanoForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                images = form.cleaned_data['img']

                if images is None:
                    messages.add_message(request, messages.ERROR, _('The Images could not be sent to the server, please ask us whats going on'))
                    raise ImageUploadError('Images is None')

                if len(images) < 2:
                   messages.add_message(request, messages.ERROR, _('Need at least 2 images to stitch a panorama'))
                   raise ImageUploadError('Less than 2 Images provided')

                pano = Pano()
                pano.created_by = request.user
                pano.save()

                pano.licenses.set(form.cleaned_data['licenses'])
                pano.save()

                for image in images:
                    part = PanoPart(img=image, pano=pano)
                    part.save()
                form.delete_temporary_files()
                messages.add_message(request, messages.SUCCESS,
                                     _('Saved Pano Parts!'))

                view_pano_url = reverse('stitcher:run_pano_stitcher', kwargs={'pano_id': pano.id})
                processing_url = '{}/?next={}'.format(reverse('stitcher:processing_request_page'), view_pano_url)
                return redirect(processing_url)

            except ImageUploadError:
                pass # go back to original page and show form errors

        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to save Pano Parts!'))

    else:
        form = PanoForm()

    context = {
        'form': form,
        'next_page': next_page
    }
    return render(request, 'stitcher/add_pano.html', context)

@permission_required('miroutes.add_wall')
def view_pano(request, pano_id):
    pano = get_object_or_404(Pano, pk=pano_id)

    context = {
        'pano': pano,
    }

    return render(request, 'stitcher/view_pano.html', context)

@permission_required('miroutes.add_wall')
def run_pano_stitcher(request, pano_id):
    pano = get_object_or_404(Pano, pk=pano_id)

    pano.run_stitching()

    return redirect(reverse('stitcher:view_pano', kwargs={'pano_id': pano.id}))

def processing_request_page(request):
    """ This just displays a spinner and forwards to another page, which is given with ?next as url argument"""
    next_page = request.GET.get('next', reverse('miroutes:index'))
    return render(request, 'stitcher/processing_request_page.html', {'next_page': next_page})

