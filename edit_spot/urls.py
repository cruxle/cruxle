from django.conf.urls import *
from rest_framework.routers import DefaultRouter

from edit_spot import views, classviews
from miroutes.model_choices import GRADE_CHOICES, AID_GRADE_CHOICES

from django.http import HttpResponse, JsonResponse
from rest_framework.routers import DefaultRouter

app_name = 'edit_spot'

do_nothing = lambda *args, **kwargs: HttpResponse()

route_geom_router = DefaultRouter()
route_geom_router.register(r'routegeometries', classviews.RouteGeometryViewSet, basename='routegeometry')

urlpatterns = [
url(r'^wall_img(?P<tiler_id>\d+)/edit$', views.edit_wall_img, name='edit_wall_img'),
url(r'^wall_img(?P<tiler_id>\d+)/revert(?P<version_id>\d+)$', views.revert_wall_img, name='revert_wall_img'),
url(r'^add_wall_img$', views.add_wall_img, name='add_wall_img'),
url(r'^add_wall_from_pano/(?P<pano_id>\d+)$', views.add_wall_from_pano, name='add_wall_from_pano'),

url(r'^wall_img(?P<tiler_id>\d+)/add_wall$', views.add_wall, name='add_wall'),
url(r'^wall(?P<wall_id>\d+)/routelist_chunk$', views.routelist_chunk, name='routelist_chunk'),

url(r'^edit_route/$', do_nothing, name='edit_route_noid'),
url(r'^edit_route/(?P<route_id>\d+)$', views.edit_route, name='edit_route'),
url(r'^deprecate_route/(?P<route_id>\d+)$', views.deprecate_route, name='deprecate_route'),
url(r'^deprecate_route/$', views.deprecate_route, name='deprecate_route'),
url(r'^route(?P<route_id>\d+)/add_material$', views.add_material, name='add_material'),
url(r'^route(?P<route_id>\d+)/link_materials$', views.link_materials, name='link_materials'),
url(r'^route(?P<route_id>\d+)/toggle_link_material/(?P<material_id>\d+)$', views.toggle_link_material, name='toggle_link_material'),
url(r'^route(?P<route_id>\d+)/toggle_link_material/$', do_nothing, name='toggle_link_material'),

url(r'^add_route$', views.add_route, name='add_route'),
url(r'^revert_route/route(?P<route_id>\d+)/version(?P<version_id>\d+)$', views.revert_route, name='revert_route'),
url(r'^revert_wall/wall(?P<wall_id>\d+)/version(?P<version_id>\d+)$', views.revert_wall, name='revert_wall'),

url(r'^wall(?P<wall_id>\d+)/edit_wall$', views.edit_wall, name='edit_wall'),
url(r'^wall(?P<wall_id>\d+)/del_wall$', views.del_wall, name='del_wall'),

url(r'^wallview(?P<wall_id>\d+)/link_routes$', views.link_routes, name='link_routes'),
url(r'^wallview(?P<wallview_id>\d+)/toggle_link_route/(?P<route_id>\d+)$', views.toggle_link_route, name='toggle_link_route'),
url(r'^wallview(?P<wallview_id>\d+)/toggle_link_route/$', do_nothing, name='toggle_link_route_noid'),
url(r'^wallview(?P<wallview_id>\d+)/store_geojson$', views.store_geojson, name='store_geojson'),
url(r'^wallview(?P<wallview_id>\d+)/edit_gym_route$', views.edit_gym_route, name='edit_gym_route'),
url(r'^wallview(?P<wallview_id>\d+)/edit_gym_route(?P<route_id>\d+)$', views.edit_gym_route, name='edit_gym_route'),
url(r'^changeset/(?P<wallview1_id>\d+)/(?P<wallview2_id>\d+)$', views.wallview_changeset, name='wallview_changeset'),

url(r'^wall(?P<wall_id>\d+)/link_materials$', views.link_materials, name='link_materials'),
url(r'^wall(?P<wall_id>\d+)/toggle_link_material/(?P<material_id>\d+)$', views.toggle_link_material, name='toggle_link_material'),
url(r'^wall(?P<wall_id>\d+)/toggle_link_material/$', do_nothing, name='toggle_link_material'),
url(r'^wall(?P<wall_id>\d+)/add_material$', views.add_material, name='add_material'),

url(r'^wall(?P<wall_id>\d+)/draw_routes$', views.draw_routes, name='draw_routes'),
url(r'^wall(?P<wall_id>\d+)/publish_wall$', views.publish_wall, name='publish_wall'),
url(r'^wall(?P<wall_id>\d+)/reset_dev_wall$', views.reset_dev_wall, name='reset_dev_wall'),

url(r'^wall(?P<wall_id>\d+)/decorate_wall$', views.decorate_wall, name='decorate_wall'),
url(r'^wall(?P<wall_id>\d+)/finish_wall_edit$', views.finish_wall_edit, name='finish_wall_edit'),
url(r'^wall(?P<wall_id>\d+)/delete$', views.delete_wall, name='delete_wall'),

url(r'^wall_img(?P<tiler_id>\d+)/rotate_right$', views.rotate_wall_img_right, name='rotate_img_right'),
url(r'^wall_img(?P<tiler_id>\d+)/rotate_left$', views.rotate_wall_img_left, name='rotate_img_left'),

url(r'^routegeometry(?P<geom_id>\d+)/delete$', views.del_geom, name='del_geom'),
url(r'^rest/tiler/(?P<pk>[0-9]+)/$', classviews.TilerDetail.as_view(), name='get_tiler'),

url(r'^grades_table$', lambda *args, **kwargs:JsonResponse(GRADE_CHOICES, safe=False), name='grades_table'),
url(r'^aid_grades_table$', lambda *args, **kwargs:JsonResponse(AID_GRADE_CHOICES, safe=False), name='aid_grades_table')
]

urlpatterns += route_geom_router.urls
