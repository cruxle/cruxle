import os

from crispy_forms.bootstrap import (AppendedText, FormActions)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout, Field, Fieldset, Hidden,
    Button, HTML)
from django import forms
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django_file_form.forms import FileFormMixin

from edit_spot.fields import UploadedImageField, MultipleUploadedMaterialField
from miroutes.model_choices import GRADE_SYSTEMS, grade_to_grade_system
from miroutes.models import Route, RouteGeometry, Wall, get_grade_system, Userpic, Tiler, License
from miroutes.widgets import LicenseChooser


class RepresentableForm(forms.ModelForm):
    """A form that can be viewed in non-edit context.

    Attrs:
        editable (boolean, default=True): If false, disable all form fields.
        filled_only (boolean, default=False): If true, hide fields without content.
    """
    def __init__(self, *args, **kwargs):
        self.editable = kwargs.pop('editable', True)
        self.filled_only = kwargs.pop('filled_only', False)
        super(RepresentableForm, self).__init__(*args, **kwargs)

        if not self.editable:
            for fname in self.fields:
                self.fields[fname].disabled = True
        if self.filled_only:
            del self.fields['name']
            # If we are called with instance we hide all non-populated fields
            wall = kwargs.get('instance')
            if wall:
                to_del_keys = []
                for fname in self.fields:
                    if not wall.__dict__.get(fname):
                        to_del_keys.append(fname)
                for key in to_del_keys:
                    del self.fields[key]

class RevisionForm(forms.Form):
    """A form to take the revision comment of change to a model instance."""
    def __init__(self, *args, **kwargs):
        comment = kwargs.pop('comment', "")
        super(RevisionForm, self).__init__(*args, **kwargs)
        self.fields['revision_comment'].initial = comment
    revision_comment = forms.CharField(
        max_length = 1024,
        label='Comments for this version:',
        widget=forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2'})
    )

def get_licenses():
    return list(License.objects.all().order_by("-required", "short_name"))

class MaterialForm(FileFormMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MaterialForm, self).__init__(*args, **kwargs)
        self.fields['img'] = MultipleUploadedMaterialField(label=_('Files'))
        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-1 col-md-2 col-xs-12'
        self.helper.field_class = 'col-lg-7 col-md-8 col-xs-12'
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields

        self.helper.form_id = 'material-form'
        self.helper.form_method = 'post'

        save_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
            </button>
            {% endif %}
            """)
        cancel_btn = HTML(
            """{% if request.is_ajax is False %}{% load i18n %}
            <a href="{{ next_page }}" role="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove-circle"></span> {% trans "Cancel" %}</a>
            {% endif %}""")

        self.helper.layout.append(
            FormActions(save_btn, cancel_btn))

    def clean_img(self):
        pics = self.cleaned_data['img']
        for pic in pics:
            fname, ext = os.path.splitext(pic.name)
            pic.name = slugify(fname) + ext
        return pics

    class Meta:
        model = Userpic
        fields = (
            'description',
            'img',
            'external_url',
            'licenses'
        )
        widgets = {
            'description': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '3'}),
            'licenses': LicenseChooser(licenses_callback=get_licenses)
        }

class WallForm(FileFormMixin, RepresentableForm):
    def __init__(self, *args, **kwargs):
        super(WallForm, self).__init__(*args, **kwargs)

        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-2 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields
        self.helper['wall_height'].wrap(AppendedText, 'm') # add a [m] unit behind the wall_height

        self.helper.form_id = 'wall-form'
        self.helper.form_method = 'post'
        if self.editable:

            self.helper.layout.append(
                FormActions(
                    HTML("""{% load i18n %}
                    {% if tiler %}
                      {% url "edit_spot:add_wall" tiler_id=tiler.id as add_wall_url %}
                      {% url "edit_spot:edit_wall_img" tiler_id=tiler.id as edit_wall_img_url %}
                    {% elif wall %}
                      {% url "edit_spot:add_wall" tiler_id=wall.tiler.id as add_wall_url %}
                      {% url "edit_spot:edit_wall_img" tiler_id=wall.tiler.id as edit_wall_img_url %}
                    {% else %}
                      {% url "edit_spot:add_wall" as add_wall_url %}
                    {% endif %}
                    {% if wall.tiler or tiler %}
                    <a href="{{ edit_wall_img_url }}"
                       id="backward-button" type="button" class="btn btn-info">
                    <i class="glyphicon glyphicon-arrow-left"></i> {% trans "Edit Image" %}
                    </a>
                    {% else %}
                    <a href="{{ add_wall_img_url }}"
                       id="backward-button" type="button" class="btn btn-info">
                    <i class="glyphicon glyphicon-arrow-left"></i> {% trans "New Wall Image" %}
                    </a>
                    {% endif %}
                    """),
#                    HTML("""
#                    {% if editing %}{% load i18n %}
#                    <a type="button" href="{% url "edit_spot:link_materials" wall_id=wall.pk %}?next={{ request.path }}"
#                    title="{% trans "Add images that complement the wall info." %}" class="btn btn-info">
#                    <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span>
#                    {% trans "Link Materials" %}
#                    </a>{% endif %}
#                    """),
                    HTML("""
                    {% if editing %}{% load i18n %}
                    <a type="button" id="history-button" href="#" class="btn btn-info">
                    <i class="fa fa-database" aria-hidden="true"></i>
                    {% trans "History of" %} {{ wall.name }}
                    </a>
                    {% endif %}
                    """),
                    HTML("""
                    {% if editing %}{% if wall.is_deletable and wall.created_by == request.user %}
                    {% load i18n %}
                    <a type="button" href="{% url "edit_spot:delete_wall" wall_id=wall.pk %}?next={{ request.path }}" class="btn btn-danger"
                    title="{% trans "Delete this wall irreversibly. "%}">
                    <span class="glyphicon glyphicon-trash"></span> {% trans "Delete Wall" %}
                    </a>
                    {% endif %}
                    {% endif %}
                    """),
                    HTML("""{% load i18n %}
                    <button id="submit-button" type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save Wall Info" %}
                    </button>
                    """),
                    HTML("""{% load i18n %}
                    {% if wall %}
                    <a href="{% url "edit_spot:link_routes" wall_id=wall.id %}"
                       id="forward-button" type="button" class="btn btn-success">
                    <i class="fa fa-link"></i> {% trans "Add/Remove Routes" %} <i class="glyphicon glyphicon-arrow-right"></i>
                    </a>
                    {% endif %}
                    """),
                    ))

    class Meta:
        model = Wall
        exclude = (
            'is_active',
            'orig_img',
            'number_of_views',
            'created_by',
            'snapshot',
            'pdf_topo',
            'last_published_date',
        )
        widgets = {
            'tiler': forms.HiddenInput(),
            'pnt': forms.HiddenInput(),
            'description': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '3', 'placeholder': _('You can use markdown here.')}),
            'approach': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '3', 'placeholder': _('You can use markdown here.')}),
            'urgent_message': forms.widgets.Textarea(attrs={'cols': '30', 'rows': '1'}),
        }
        error_messages = {
            'pnt': {'required': 'Please select a Wall location by clicking on the map!'}
        }

def get_licenses():
    return list(License.objects.all().order_by("-required", "short_name"))

class TilerForm(FileFormMixin, RepresentableForm):
    def __init__(self, *args, **kwargs):
        use_django_file_form = kwargs.pop('use_django_file_form', True) # add option not to use the django file form because this is broken currently for our case where we want to replace a wall image... with it enabled, orig_img does not show up in form.changed_data
        super(TilerForm, self).__init__(*args, **kwargs)
        self.fields['orig_img'] = UploadedImageField(label=_('Wall Image'))
        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields
        self.helper.label_class = 'col-md-2 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'

        self.helper.form_id = 'tiler-form'
        self.helper.form_method = 'post'
        self.helper.layout.append(
            FormActions(
                HTML("""
                {% load i18n %}{% if editing %}
                <a type="button" id="history-button" href="#" class="btn btn-info">
                <i class="fa fa-database" aria-hidden="true"></i>
                {% trans "Image History" %}
                </a>
                {% endif %}
                """),
                HTML("""
                {% load i18n %}
                <button id="submit-button" type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}</button>
                """)))

    def clean_orig_img(self):
        pic = self.cleaned_data['orig_img']
        fname, ext = os.path.splitext(pic.name)
        pic.name = slugify(fname) + ext
        return pic

    class Meta:
        model = Tiler
        widgets = {
            'licenses': LicenseChooser(licenses_callback=get_licenses)
        }
        exclude = (
            'status',
            'percentage',
            'img',
            'wall',
            'created_by'
        )

class SmallRouteForm(forms.ModelForm):
    linecolor = forms.CharField(max_length=18, widget=forms.TextInput(attrs={'type': 'color'}), required=False)
    auto_color = forms.BooleanField(label=_('auto color'), required=False)
    color = forms.CharField(max_length=18, widget=forms.HiddenInput(), required=False)
    def __init__(self, *args, **kwargs):
        from miroutes.models import get_common_route_attributes
        from crispy_forms.bootstrap import InlineField
        nearest_wall = kwargs.pop('nearest_wall', None)
        action = kwargs.pop('action', '#')
        super(SmallRouteForm, self).__init__(*args, **kwargs)

        nearest_wall_grade_system = get_grade_system(nearest_wall) if nearest_wall is not None else None
        grade_system = grade_to_grade_system(self.initial['grade']) if 'grade' in self.initial else nearest_wall_grade_system
        self.fields['grade_choices'] = forms.ChoiceField(
            choices=GRADE_SYSTEMS,
            initial=grade_system,
        )

        if self.instance.pk is not None:
            if self.instance.color is None or self.instance.color == "":
                self.fields['auto_color'].initial = True
            save_btn = HTML(
                """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save" %}
                </button>""")
        else:
            save_btn = HTML(
                """{% load i18n %}<button id="new-button" type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span> {% trans "Add" %}
                </button>""")



        if not 'climbingstyle' in self.initial:
            climbingstyle = get_common_route_attributes(
                ['climbingstyle',],
                nearest_wall.pnt
            )['climbingstyle'] if nearest_wall is not None else None

            self.fields['climbingstyle'].initial = climbingstyle

        self.helper = FormHelper(self)
        self.helper.form_id = 'route-form'
        self.helper.form_class = 'form-inline'
        #self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.form_method = 'post'
        self.helper.form_action = action
        self.helper.form_show_labels = False


        title = _("Add Route")
        if self.instance.pk is not None:
            title = _("Edit Route")

        fset = Fieldset(title,
                        InlineField('name'),
                        InlineField('climbingstyle'),
                        InlineField('grade_choices'),
                        InlineField('grade'),
                        InlineField('linecolor'),
                        InlineField('auto_color'),
                        InlineField('color'),
                        'pnt', 'deprecated',
                        css_class="small-form-fields")

        fset.append(
            FormActions(save_btn)
        )
        self.helper.layout = Layout(fset)



    class Meta:
        model = Route
        fields = [
            'climbingstyle', 'name', 'grade', 'color', 'pnt', 'deprecated'
        ]
        widgets = {
            'pnt': forms.HiddenInput(),
            'deprecated': forms.HiddenInput(),
        }


class RouteForm(RepresentableForm):
    def __init__(self, *args, **kwargs):
        deleteable = kwargs.pop('deleteable', False)
        nearest_wall = kwargs.pop('nearest_wall', None)
        action = kwargs.pop('action', '#')
        next_page = kwargs.pop('next_page', None)
        if next_page is not None:
            action += '?next={}'.format(next_page)

        super(RouteForm, self).__init__(*args, **kwargs)

        nearest_wall_grade_system = get_grade_system(nearest_wall) if nearest_wall is not None else None

        grade_system = grade_to_grade_system(self.initial['grade']) if 'grade' in self.initial else nearest_wall_grade_system

        self.fields['grade_choices'] = forms.ChoiceField(
            choices=GRADE_SYSTEMS,
            initial=grade_system,
        )


        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_id = 'route-form'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-2 col-xs-12'
        self.helper.field_class = 'col-md-8 col-xs-12'
        self.helper.form_method = 'post'
        self.helper.form_action = action

        if self.editable:

            locate_here = Button('locate_here', _("Current location"),
                       css_class='btn-secondary locate_here',
                       onclick="button_locate_here();")
            locate_map = Button('locate__map', _("Use map"),
                       css_class='btn-secondary locate_map',
                       onclick="$('#MapModal').modal()")

            pnt_buttons = [locate_here, locate_map]
            if nearest_wall is not None:
                pnt_buttons.append(Button('locate_at_wall', _("Wall location"),
                    css_class='btn-secondary locate_wall',
                    onclick="$('#id_pnt').val('{}').change()".format(nearest_wall.pnt)))

            footer_buttons = []

            if self.instance.pk is None:
                footer_buttons.append(HTML(
                    """{% load i18n %}<button
                    onclick="$('input[name=create_new]').val('true');"
                    id="submit-create-button" type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> {% trans "Save & Create New (Alt+Enter)" %}
                    </button>&nbsp;
                    <script>
                    function shortkey_save_n_new(e) {
                        if (e.altKey && e.keyCode == 13) {
                            $("#submit-create-button").click();
                        }
                    }; document.addEventListener('keyup', shortkey_save_n_new, false);
                    </script>
                    """))
            footer_buttons.append(HTML(
                """{% load i18n %}<button id="submit-button" type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-save"></span> {% trans "Save (Ctrl+Enter)" %}
                </button>
                <script>
                function shortkey_save_n_new(e) {
                        if (e.ctrlKey && e.keyCode == 13) {
                            $("#submit-button").click();
                        }
                }; document.addEventListener('keyup', shortkey_save_n_new, false);
                </script>
                """))
            footer_buttons.append(HTML("""{% if request.is_ajax is False %}
                                      <a href="{{ next_page }}" role="button" class="btn btn-default">
                                      <span class="glyphicon glyphicon-remove-circle"></span> """ + str(_("Cancel")) + """</a>
                                 {% endif %}"""))

            footer_buttons.append(HTML("""<a id="revision-history-modal_button" type="button"
                                 class="btn btn-secondary revision-history-modal_button"
                                 onclick="$('#revision-history-modal').modal()">
                                 <span class="fa fa-database"></span> """ + str(_("History")) + "</a>"))

            self.helper.layout = Layout(
                'climbingstyle',
                'name',
                #AppendedText('grade', grade_choices_html_code),
                Field('grade_choices', id='grade-system-selector'),
                'grade',
                'aid_grade',
                AppendedText('length', 'm'),
                AppendedText('number_of_bolts', '#'),
                'bolt_quality',
                'description',
                'additional_gear',
                'tradclimb',
                'developer', 'developed_year',
                'pnt',
                FormActions(*pnt_buttons),
                'editor_notes',
                'color',
                'deprecated',
                Hidden('create_new', 'false'),
                FormActions(*footer_buttons),
            )

            self.helper.layout.append(HTML("""
            {% if route %}
                <div id="route-revision-modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">A comment, please!</h4>
                            </div>
                            <div class="modal-body">
                                <div class="fieldWrapper">
                                    {{ route_revision_form.revision_comment.errors }}
                                    {{ route_revision_form.revision_comment }}
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            {% endif%}"""))

            # Map Modal to select the route position
            self.helper.layout.append(HTML("""
                <div class="modal fade" id="MapModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Select the Route Location</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="mapdiv" class="leaflet_map" style="height:20em;"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                """))
        else:
            self.helper.layout = Layout(
                'name',
                #AppendedText('grade', grade_choices_html_code),
                Field('grade_choices', id='grade-system-selector'),
                'grade',
                'aid_grade',
                AppendedText('length', 'm'),
                AppendedText('number_of_bolts', '#'),
                'bolt_quality',
                'description',
                'additional_gear',
                'developer', 'developed_year', 'climbingstyle', 'tradclimb',
                'pnt',
                'editor_notes',
            )


        self.helper['length'].wrap(AppendedText, 'm') # add a [m] unit behind the wall_height
        # TODO: I know I know... this is ugly as shit.
        # lets try it and see if that makes sense to have the code here instead of keeping it in templates...
        # Anyway... this does not work in the ajaxed templates.. we have to fix this....
        self.helper.layout.append(HTML(""" {% load i18n %}
      <div id="revision-history-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">History of {{ route.name }}</h4>
            </div>
            <div class="modal-body">
              <table id="versiontable" class="table table-condensed table-striped table-responsive">
                <thead>
                  <tr>
                    <th>{% trans "Date" %}</th>
                    <th>{% trans "User" %}</th>
                    <th>{% trans "Commit Message" %}</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {% for version in versions %}
                    <tr>
                      <td>{{version.revision.date_created}}</td>
                      <td>
                          {% if version.revision.user %}
                              <a href="{% url 'users:user_detail' user_id=version.revision.user.pk %}">
                              {{ version.revision.user.username }}</a>
                          {%endif%}
                      </td>
                      <td>{{version.revision.comment | truncatechars:30}}</td>
                      <td>
                        {% if not forloop.first %}
                          <a href="{% url 'edit_spot:revert_route' route_id=route.id version_id=version.id %}?next={{request.path}}?next={{ next_page }}"
                                data-toggle="tooltip" data-container="body"
                             title="{{ version.serialized_data }}">
                            {% trans 'Compare to current version' %}
                          </a>
                        {% endif %}
                      </td>
                    </tr>
                    {% empty %}
                    <td><strong><i>{% trans '** Route is not under revision control.' %}</i></strong></td>
                  {% endfor %}
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
        """))

    class Meta:
        model = Route
        fields = [
            'name', 'grade', 'aid_grade', 'length',
            'description', 'number_of_bolts', 'bolt_quality', 'developer','additional_gear',
            'developed_year', 'editor_notes', 'climbingstyle', 'tradclimb', 'color', 'pnt', 'deprecated']
        widgets = {
            'name': forms.widgets.TextInput(attrs={'autofocus':'autofocus'}),
            'pnt': forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2'}),
            'description': forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2', 'placeholder': _('You can use markdown here.')}),
            'additional_gear': forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2', 'placeholder': _('You can use markdown here.')}),
            'editor_notes': forms.widgets.Textarea(attrs={'cols': '35', 'rows': '2'})
        }



class PolylineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        editable = kwargs.pop('editable', True)
        super(PolylineForm, self).__init__(*args, **kwargs)
        if not editable:
            for fname in self.fields:
                self.fields[fname].disabled = True
    class Meta:
        model = RouteGeometry
        fields = ('geojson',)
        widgets = {'geojson': forms.HiddenInput()}
