from rest_framework.generics import RetrieveAPIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet
from miroutes.models import Tiler, RouteGeometry, Route
from miroutes.serializers import TilerSerializer, RouteGeometrySerializer, RouteSerializer

class TilerDetail(RetrieveAPIView):
    """Generic API view to retrieve Tiler status."""
    queryset = Tiler.objects.all()
    serializer_class = TilerSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,AllowAny)

class RouteGeometryViewSet(ReadOnlyModelViewSet):
    """List of RouteGeometries filterable by wallview id."""
    permission_classes = (IsAuthenticated,AllowAny)
    serializer_class = RouteGeometrySerializer

    def get_queryset(self):
        """See if we are delivered a query parameter *wallview* and return
        a filtered set.
        """
        queryset = RouteGeometry.objects.all()
        wallview = self.request.query_params.get('wallview', None)
        if wallview is not None:
            queryset = queryset.filter(on_wallview__pk=wallview)
        return queryset
