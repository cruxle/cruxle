"""Provide utility functions to edit_spot views."""

def check_edit_lock(obj, user):
    """Check if the edit lock is present."""
    if obj.is_edited:
        if obj.is_edited.by == user:
            return True
    return False


def acquire_edit_lock(obj, user):
    """Check if the edit lock can be acquired."""
    import datetime
    import pytz
    from miroutes.models import Edit
    from miroutes.utils import slugify_me

    edit_tag = obj.is_edited
    if edit_tag:
        timedelta = datetime.datetime.now(pytz.utc) - edit_tag.since
        if timedelta.total_seconds()/60 < 30 and user != edit_tag.by:
            return False
        edit_tag.delete()
    edit = Edit(
        by=user,
        since=datetime.datetime.now(pytz.utc),
        obj=slugify_me(obj))
    edit.save()

    return True


def release_edit_lock(obj, user):
    """If the edit is the users, we release it"""
    if check_edit_lock(obj, user):
        obj.is_edited.delete()


def create_annotated_and_sorted_geomlist(wallroutegeomlist):
    """Annotate the geometries on the view with their position from left to right."""
    # also get all geoms asociated with wall routes

    # annotate with a label
    anchorpointlist = []
    for geom in wallroutegeomlist:
        try:
            anchorpointlist.append([geom.anchorpoint[1], geom])
        except:
            anchorpointlist.append([None, geom])
    # sort, None values first
    anchorpointlist = sorted(anchorpointlist, key=lambda x: (x[0] is not None, x[0]))
    none_num = [anchor[0] for anchor in anchorpointlist].count(None)
    for pos, entry in enumerate(anchorpointlist):
        if entry[0]:
            entry[1].label = pos + 1 - none_num
        else:
            entry[1].label = '*'
        wallroutegeomlist[pos] = entry[1]

    return wallroutegeomlist

def save_and_create_revision(form, request, comment=None):
    """Save already *validated* form and create a
    revision with a comment specified by the revision_form."""
    from django.utils.translation import ugettext_lazy as _
    import reversion
    from django.contrib import messages
    from edit_spot.forms import RevisionForm
    from miroutes.models import Wall

    if comment and form.changed_data:
        with reversion.create_revision():
            reversion.set_user(request.user)
            reversion.set_comment(comment)
            return form.save()

    revision_form = RevisionForm(request.POST)
    if revision_form.is_valid() and form.changed_data:
        with reversion.create_revision():
            # create initial wall revision
            revision_data = revision_form.cleaned_data

            reversion.set_user(request.user)
            reversion.set_comment(revision_data.get('revision_comment'))
            obj = form.save()
            # if we deal with a wall we have to add all wallview objects to revision
            if isinstance(obj, Wall):
                for routegeom in obj.pub_view.routegeometry_set.all():
                    reversion.add_to_revision(routegeom)
                for marker in obj.pub_view.simplemarker_set.all():
                    reversion.add_to_revision(marker)
                for marker in obj.pub_view.relationalmarker_set.all():
                    reversion.add_to_revision(marker)
                for marker in obj.pub_view.textmarker_set.all():
                    reversion.add_to_revision(marker)
                for marker in obj.pub_view.pitchmarker_set.all():
                    reversion.add_to_revision(marker)
            return obj
    else:
        messages.add_message(
            request, messages.ERROR,
            _('Failed to save changes, commit message invalid.'))


def exclude_pk(data):
    return {k: v for k, v in data[0].items() if k != "pk"}

def sort_by_model(json_set):
    sorted_dict = {}
    for obj in json_set:
        model = obj.pop('model')
        if model not in list(sorted_dict.keys()):
            sorted_dict[model] = [obj,]
        else:
            sorted_dict[model].append(obj)
    return sorted_dict

def wallview_revision_diff(rev1, rev2):
    """Compare two wallview revisions. Returns the differences starting from the first revision
    (only modelclasses and fields in rev1 are considered when calculating differences).

    Returns:
        dict: keys are model classes, values are lists of dicts of the form {fieldname: obj-json},
              where obj-json is the JSON of the obj where <fieldname> differs.
    """
    import json
    from miroutes.models import Route

    set1 = rev1.version_set.all()
    set2 = rev2.version_set.all()

    json_set1 = [exclude_pk(json.loads(obj.serialized_data)) for obj in set1]
    json_set2 = [exclude_pk(json.loads(obj.serialized_data)) for obj in set2]

    sorted_json1 = sort_by_model(json_set1)
    sorted_json2 = sort_by_model(json_set2)

    sorted_json1.pop('miroutes.wall')
    sorted_json2.pop('miroutes.wall')

    # now we are left with all objs sorted by classname

    allkeys = set(list(sorted_json1.keys()) + list(sorted_json2.keys()))
    changes = dict(list(zip(allkeys, [[] for k in allkeys])))

    # let us have a look at routes first
    modelname = 'miroutes.routegeometry'
    if modelname in allkeys:
        route_json1 = sorted_json1.pop(modelname, [])
        route_json2 = sorted_json2.pop(modelname, [])

        # which routes are removed or modified?
        for route1 in route_json1:
            found = False
            for route2 in route_json2:
                if route2['fields'] == route1['fields']:
                    found = True
                    break
                elif route2['fields']['route'] == route1['fields']['route']:
                    # this is representing the same route
                    found = True
                    changes[modelname].append(
                        "Modified route `{}`.".format(
                            Route.objects.get(pk=route1['fields']['route']).name))
                    break
            if not found:
                changes[modelname].append(
                    "Deleted route `{}`.".format(
                        Route.objects.get(pk=route1['fields']['route']).name))


        # which routes are added?
        routes_on1 = set([obj['fields']['route'] for obj in route_json1])
        routes_on2 = set([obj['fields']['route'] for obj in route_json2])

        # routes on 2 but not on 1
        route_diff2 = routes_on2 - routes_on1
        #import ipdb;ipdb.set_trace()
        changes[modelname] += ["Added route `{}`.".format(Route.objects.get(pk=rid).name) for rid in route_diff2]

    # the rest is markers
    for model, objs in list(sorted_json1.items()):
        for obj1 in objs:
            found = False
            for obj2 in sorted_json2.get(model, []):
                if obj1['fields'] == obj2['fields']:
                    found = True
                    break
            if not found:
                mtype = obj1['fields'].get('markertype')
                if mtype is not None:
                    changes[model].append("Deleted a marker of type `{}` at {}.".format(mtype, obj1['fields']['pnt']))

    for model, objs in list(sorted_json2.items()):
        for obj2 in objs:
            found = False
            for obj1 in sorted_json1.get(model, []):
                if obj1['fields'] == obj2['fields']:
                    found = True
                    break
            if not found:
                mtype = obj2['fields'].get('markertype')
                if mtype is not None:
                    changes[model].append("Added a marker of type `{}` at {}.".format(mtype, obj2['fields']['pnt']))

    return changes

def deserialize_version(obj):
    import json
    res = json.loads(obj.serialized_data)
    assert len(res) == 1, "De-serializing version failed: {}".format(obj)
    return res[0]

def del_unchanged_versions_from_wall_revision(rev1, rev2):
    """Compare two wallview revisions.

    Deletes versions from the version_set of rev1 that are in rev2.

    Returns:
      rev1 with a modified version_set
    """
    import json

    set1 = rev1.version_set.all()
    set2 = rev2.version_set.all()

    def load_and_add_pk(obj):
        res = deserialize_version(obj)
        res["version_pk"] = obj.pk
        return res

    json_set1 = [load_and_add_pk(obj) for obj in set1]
    json_set2 = [load_and_add_pk(obj) for obj in set2]

    sorted_json1 = sort_by_model(json_set1)
    sorted_json2 = sort_by_model(json_set2)

    sorted_json1.pop('miroutes.wall')
    sorted_json2.pop('miroutes.wall')

    # now we are left with all objs sorted by classname
    versions_to_del = []

    for model, objs in list(sorted_json1.items()):
        for obj1 in objs:
            for obj2 in sorted_json2.get(model, []):
                if obj1['fields'] == obj2['fields']:
                    versions_to_del.append(obj1["version_pk"])
                    break

    rev1.versions_to_del = versions_to_del

def does_obj_change(obj, form):
    """Check if a form contains changes to a model instance.
    Removes fields from form that are not part of the model before comparing.
    """
    # remove changed form fields that are not part of the model
    for field in form.changed_data:
        if field not in obj.__dict__.keys():
            form.changed_data.remove(field)
    return form.has_changed()

def markerset_difference(changedict, ms1, ms2):
    from . import constants
    markerids1 = set(ms1.values_list('dev_id', flat=True))
    markerids2 = set(ms2.values_list('dev_id', flat=True))
    added = list(markerids1 - markerids2)
    if added:
        changedict[constants.MARKER_ADD] += [ms1.get(dev_id=mpk).markertype if mpk is not None else None for mpk in added]
    removed = list(markerids2- markerids1)
    if removed:
        changedict[constants.MARKER_REM] += [ms2.get(dev_id=mpk).markertype if mpk is not None else None for mpk in removed]
    for m1 in ms1:
        # compare the matching marker
        matching = ms2.filter(dev_id=m1.dev_id).first()
        if matching:
            flds = filter(lambda x: x.name not in ['id', 'wallview', 'dev_id', 'markertype'], matching._meta.get_fields())
            for fld in flds:
                if getattr(matching, fld.name) != getattr(m1, fld.name):
                    changedict[constants.MARKER_MOD].append(m1.markertype)
                    break


def wallviews_difference(wv1, wv2):
    """Find differences between two wallviews.
    wv1 is the current active wallview with respect to which the differences
    are calculated (i.e., current dev when publishing or current pub when reverting)
    """
    from miroutes.models import Route
    from . import constants
    # Check routes
    changedict = {}
    changedict[constants.ROUTE_ADD] = []
    changedict[constants.ROUTE_REM] = []
    changedict[constants.ROUTE_MOD] = []
    routeids1 = set(wv1.routegeometry_set.exclude(geojson=None).values_list('route__id', flat=True))
    routeids2 = set(wv2.routegeometry_set.exclude(geojson=None).values_list('route__id', flat=True))
    added = list(routeids1 - routeids2)
    if added:
        changedict[constants.ROUTE_ADD] = [Route.objects.get(pk=rpk).name for rpk in added]
    removed = list(routeids2 -routeids1)
    if removed:
        changedict[constants.ROUTE_REM] = [Route.objects.get(pk=rpk).name for rpk in removed]
    for routegeom in wv1.routegeometry_set.all():
        # is it the same?
        matching = wv2.routegeometry_set.filter(route__id=routegeom.route.pk).first()
        if matching and matching.geojson != routegeom.geojson:
            changedict[constants.ROUTE_MOD].append(routegeom.route.name)

    # check markers
    changedict[constants.MARKER_ADD] = []
    changedict[constants.MARKER_REM] = []
    changedict[constants.MARKER_MOD] = []

    markerset_difference(changedict, wv1.simplemarker_set.all(), wv2.simplemarker_set.all())
    markerset_difference(changedict, wv1.relationalmarker_set.all(), wv2.relationalmarker_set.all())
    markerset_difference(changedict, wv1.textmarker_set.all(), wv2.textmarker_set.all())
    markerset_difference(changedict, wv1.pitchmarker_set.all(), wv2.pitchmarker_set.all())

    return changedict

def save_wall_img(request, form, tiler):
    """Save a wall image."""
    import reversion
    from django.contrib import messages
    from django.utils.translation import ugettext_lazy as _
    if form.is_valid():
        image = form.cleaned_data.pop('orig_img')
        if(image):
            with reversion.create_revision():
                tiler.orig_img = image
                # Image has been replaced
                reversion.set_user(request.user)
                reversion.set_comment(_("Replaced wall image."))
                tiler.save()
            form.delete_temporary_files()

            messages.add_message(
                request, messages.SUCCESS,
                _('Successfully saved Image!'))
        else:
            messages.add_message(
                request, messages.ERROR,
                _('Could not save Image. Image Field is empty!'))


    else:
        messages.add_message(
            request, messages.ERROR,
            _('Could not save new image!'))

def save_pdf_material(material):
    """Save first page of pdf as image to material"""
    import os
    from django.core.files.storage import default_storage as storage
    from wand.image import Image

    pdf_filename = material.pdf_file.name
    material.pdf_file.save(material.pdf_file.name, material.pdf_file.file)

    with Image(filename=material.pdf_file.path + "[0]", resolution=300) as img:
        pdf_to_jpg = lambda fname: os.path.splitext(fname)[0] + '.jpg'

        storage_fname = storage.path(pdf_to_jpg(material.pdf_file.path))
        img.save(filename=storage_fname)

        material.img = pdf_to_jpg(material.pdf_file.name)
        material.save()


def find_unique_routename(wallview):
    n = 0
    routes = wallview.route_set.all().values_list('name', flat=True)
    while True:
        n += 1
        rname = '{}{}'.format(wallview.wall.name, n)
        if rname not in routes:
            return rname


