import imghdr
import os
from django_file_form.forms import UploadedFileField, MultipleUploadedFileField
from django import forms
from django.utils.translation import ugettext

IMG_TYPES = ['gif', 'jpeg', 'png', 'tiff']
PDF_EXTENSIONS = ['.pdf', '.PDF']

class UploadedImageField(UploadedFileField):
    def validate(self, value):
        """Check if this is indeed an image file."""
        super(UploadedImageField, self).validate(value)

        if imghdr.what(value) not in IMG_TYPES:
            raise forms.ValidationError(
                ugettext("The selected file ({}) is not one of the valid image types: {}").format(imghdr.what(value), ",".join(IMG_TYPES))
            )

class MultipleUploadedMaterialField(MultipleUploadedFileField):
    def validate(self, value):
        """Check if all material items are either images or pdf."""
        super(MultipleUploadedMaterialField, self).validate(value)

        for material in value:
            if(imghdr.what(material) not in IMG_TYPES):
                name, ext = os.path.splitext(material.name)
                if ext not in PDF_EXTENSIONS:
                    raise forms.ValidationError(
                        ugettext("A selected file is not one the valid image types ({}) or a pdf.").format(",".join(IMG_TYPES))
                    )
