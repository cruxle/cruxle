

from django.apps import AppConfig


class EditSpotConfig(AppConfig):
    name = 'edit_spot'
