#from django.template.defaulttags import register
from django import template
from miroutes.models import Route
from django.utils.html import escape


register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.simple_tag
def get_object_by_id(qs, pk):
    try:
        return qs.get(pk=int(pk))
    except:
        return None

@register.filter
def nearby_route_display(route):
    qset = Route.objects.filter(pnt__distance_lt=(route.pnt, 10)).exclude(id=route.id)
    if len(qset) == 1:
        routestring = "Located next to the Route: {}".format(qset[0])
    if len(qset) > 1:
        names = [route.name for route in qset]
        routestring = "Located next to the Routes: "+", ".join(names)
    else:
        routestring = ""
    return routestring

@register.filter
def prettyprint_changeset(changeset):
    from django.utils.translation import ugettext as _, ungettext
    from miroutes.models import Route
    from edit_spot import constants
    if any(chg for chg in changeset.values()):
        html_str = "<ul>"
        for chg_type, chg in changeset.items():
            if chg:
                count = len(chg)
                html_str += "<li>"
                if chg_type == constants.ROUTE_ADD:
                    if count == 1:
                        html_str += _('Added route %(name)s.') % {'name': escape(chg[0])}
                    elif count <= 3:
                        html_str += _('Added routes %(namelist)s.') % {'namelist': escape(", ".join(chg))}
                    else:
                        html_str += _('Added %(count)d routes.') % {'count': count}
                elif chg_type == constants.ROUTE_REM:
                    if count == 1:
                        html_str += _('Removed route %(name)s.') % {'name': escape(chg[0])}
                    elif count <= 3:
                        html_str += _('Removed routes %(namelist)s.') % {'namelist': escape(", ".join(chg))}
                    else:
                        html_str += _('Removed %(count)d routes.') % {'count': count}
                elif chg_type == constants.ROUTE_MOD:
                    if count == 1:
                        html_str += _('Changed route %(name)s.') % {'name': escape(chg[0])}
                    elif count <= 3:
                        html_str += _('Changed routes %(namelist)s.') % {'namelist': escape(", ".join(chg))}
                    else:
                        html_str += _('Changed %(count)d routes.') % {'count': count}
                elif chg_type == constants.MARKER_ADD:
                    if count == 1:
                        html_str += _('Added %(mtype)s marker.') % {'mtype': chg[0]}
                    elif count <= 3:
                        html_str += _('Added markers: %(mtypelist)s.') % {'mtypelist': ", ".join(chg)}
                    else:
                        html_str += _('Added %(count)d markers.') % {'count': count}
                elif chg_type == constants.MARKER_REM:
                    if count == 1:
                        html_str += _('Removed %(mtype)s marker.') % {'mtype': chg[0]}
                    elif count <= 3:
                        html_str += _('Removed markers: %(mtypelist)s.') % {'mtypelist': ", ".join(chg)}
                    else:
                        html_str += _('Removed %(count)d markers.') % {'count': count}
                if chg_type == constants.MARKER_MOD:
                    if count == 1:
                        html_str += _('Changed %(mtype)s marker.') % {'mtype': chg[0]}
                    elif count <= 3:
                        html_str += _('Changed markers: %(mtypelist)s.') % {'mtypelist': ", ".join(chg)}
                    else:
                        html_str += _('Changed %(count)d markers.') % {'count': count}
                html_str += "</li>"
        html_str += "</ul>"
    else:
        html_str = _('No pending changes.')
    return html_str


