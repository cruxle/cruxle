"""Views related to the *edit* workflow, i.e., pages on which
route/wall-related content is modified.
"""
import itertools
import json
import os

import reversion
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.urls import reverse
from django.db.models import Q
from django.forms.models import model_to_dict
from django.forms import ChoiceField
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.files import get_thumbnailer
from reversion.models import Version, Revision

from generic_markers.models import SimpleMarker, RelationalMarker
from generic_markers.models import TextMarker, PitchMarker
from generic_markers.forms import RelationalModelChoiceField
from micomments.utils import generate_thread_id
from miroutes.models import Userpic
from miroutes.models import Wall, WallView, Route, RouteGeometry, Tiler
from mitopo.utils import notify_us, absolute_url
from miroutes.views import get_versions, get_all_versions
from miroutes.tasks import update_Wall_after_publish, update_after_Route_save
from stitcher.models import Pano, StitcherStatus
from users.models import get_puzzles
from .forms import RouteForm, WallForm, PolylineForm, RevisionForm, MaterialForm, TilerForm, SmallRouteForm
from .utils import *


def wallview_changeset(request, wallview1_id, wallview2_id, **kwargs):
    """Display wallview changesets.
    Wallview1 is the Wallview with respect to which the changes are displayed,
    i.e., an object on wv1 but not on wv2 is added etc.
    """
    wv1 = get_object_or_404(WallView, pk=wallview1_id)
    wv2 = get_object_or_404(WallView, pk=wallview2_id)
    changeset = wallviews_difference(wv1, wv2)
    return render(request, 'edit_spot/wallview_changeset.html', {'changeset': changeset})

@permission_required('miroutes.change_wall')
def publish_wall(request, wall_id, **kwargs):
    """
        Publish Wall
    """
    from reversion.models import Version

    wall = get_object_or_404(Wall, pk=wall_id)

    if request.POST:
        if not wall.deprecated:
            if check_edit_lock(wall, request.user):
                # In case this wall is not active, we just publish (no comment)
                if not wall.is_active:
                    request.user.userprofile.change_puzzle_pieces('publish_wallview')
                    wall.publish(ugettext("Published Wall."), request.user)

                    notify_us(
                        "´{}´ published by {}".format(wall.name, request.user.username),
                        absolute_url(reverse('miroutes:wall_detail', kwargs={'wall_id': wall.pk})))
                    messages.add_message(request, messages.SUCCESS, mark_safe(
                        str(_('Successfully published.')) + \
                        ' <a href="' + reverse('edit_spot:link_materials', kwargs={'wall_id': wall.pk}) + '">' + \
                        str(_('Do you have additional material to link')) + \
                        '</a>?'))
                    release_edit_lock(wall, request.user)
                    return redirect(reverse('miroutes:wall_detail_published', kwargs={
                        'wall_id': wall.id}))

                # Check if any differences occur in this publication
                if any(diff for diff in wallviews_difference(wall.dev_view, wall.pub_view).values()):
                    revision_form = RevisionForm(request.POST)
                    if revision_form.is_valid():
                        request.user.userprofile.change_puzzle_pieces('publish_wallview')
                        wall.publish(
                            revision_form.cleaned_data.get('revision_comment'),
                            request.user)

                        messages.add_message(request, messages.SUCCESS, mark_safe(
                            str(_('Successfully published.')) + \
                            ' <a href="' + reverse('edit_spot:link_materials', kwargs={'wall_id': wall.pk}) + '">' + \
                            str(_('Do you have additional material to link')) + \
                            '</a>?'))
                        release_edit_lock(wall, request.user)
                        return redirect(reverse('miroutes:wall_detail_published', kwargs={'wall_id': wall.id}))
                    else:
                        # case no comment
                        messages.add_message(
                            request, messages.WARNING,
                            _('You are not allowed to publish `{}` without a comment.').format(escape(wall.name)))
                else:
                    # case no changes
                    messages.add_message(
                        request, messages.ERROR,
                        _('Failed to publish `{}`. There seem to be no changes with respect to the current version.').format(
                            escape(wall.name)))

            else:
                # case no edit lock
                messages.add_message(
                    request, messages.ERROR,
                    _('Failed to publish `{}`. You have insufficient permissions or your edit session is expired.').format(
                        escape(wall.name)))
        else:
            # case flagged deprecated
            messages.add_message(
                request, messages.ERROR,
                _('Failed to publish. `{}` is flagged deprecated.').format(
                    escape(wall.name)))

    dev_view = wall.dev_view
    pub_view = wall.pub_view
    revision_form = RevisionForm(comment="")

    if not acquire_edit_lock(wall, request.user):
        messages.add_message(
            request, messages.ERROR,
            _('{} is currently edited by {}. You will not be able to publish the wall.').format(
                escape(wall.name), escape(wall.is_edited.by.username)))

    if wall.is_active:
        dev_geomlist = create_annotated_and_sorted_geomlist(list(dev_view.routegeometry_set.all()))
        pub_geomlist = create_annotated_and_sorted_geomlist(list(pub_view.routegeometry_set.all()))

        # differences: what to publish?
        changeset = wallviews_difference(dev_view, pub_view)
        # any route without geometries?
        no_geom = dev_view.routegeometry_set.filter(Q(geojson={}) | Q(geojson=None))

        # get all routes that are linked to any of the two views
        # and annotate them accordingly
        union_routelist = list(set(dev_view.route_set.all()) | set(pub_view.route_set.all()))
        for route in union_routelist:
            if route in dev_view.route_set.all():
                geom = dev_view.routegeometry_set.filter(route__pk=route.pk).first()
                if geom.geojson:
                    route.on_dev = True
            if route in pub_view.route_set.all():
                route.on_pub = True


        versions = get_versions(wall)

        context = {'wall': wall,
                   'pubview': pub_view,
                   'devview': dev_view,
                   'changeset': changeset,
                   'pub_geomlist': pub_geomlist,
                   'dev_geomlist': dev_geomlist,
                   'revision_form': revision_form,
                   'union_routelist': union_routelist,
                   'show_edit_pane': True,
                   'versions': versions,
                   'no_geom': no_geom
        }
        return render(request, 'edit_spot/publish_wall.html', context)
    else:
        no_geom = dev_view.routegeometry_set.filter(Q(geojson={}) | Q(geojson=None))

        context = {'wall': wall,
                   'devview': dev_view,
                   'revision_form': revision_form,
                   'show_edit_pane': True,
                   'no_geom': no_geom
        }
        return render(request, 'edit_spot/publish_first_time.html', context)


def routelist_chunk(request, wall_id):
    """Return a chunk of routes to display with datatables on link_routes."""
    from django.db.models import Q
    wall = get_object_or_404(Wall, pk=wall_id)

    show_deprecated = request.GET.get('show_deprecated')

    search = request.GET.get('search[value]')

    records_total = 0
    records_filtered = 0

    # we filter gym routes
    if show_deprecated == "true":
        routes = Route.objects.filter(gym_route=False)
    else:
        # only exclude deprecated routes that are not on the wall and gym routes
        onwall = Route.objects.filter(walls=wall.dev_view)
        not_deprecated = Route.objects.filter(gym_route=False).filter(deprecated=False)
        routes = (onwall | not_deprecated).distinct()

    routes = routes.filter(
        pnt__dwithin=(wall.pnt, .5)).filter(
            pnt__distance_lte=(
                wall.pnt, D(m=50000))).annotate(
                    distance=Distance('pnt', wall.pnt)).order_by('distance')
    #import ipdb;ipdb.set_trace()

    if search:
        routes = routes.filter(name__icontains=search)
        records_filtered = routes.count()
    else:
        #routes = Route.objects.annotate(distance=Distance('pnt', wall.pnt)).order_by('distance')
        records_total = routes.count()
        records_filtered = records_total

    routesonwall = wall.dev_view.route_set.values_list('pk', flat=True)
    # Build data array
    data = []
    for route in routes:
        onthiswall = route.pk in routesonwall
        onwalls = list(route.walls.filter(is_dev=False).values_list('wall__name', flat=True))
        data.append([route.pk,
                     escape(route.name),
                     route.get_full_grade_display,
                     "%.2f" % route.distance.km,
                     ', '.join(onwalls),
                     onthiswall,
                     route.deprecated])
    # Build JSON for table
    chunk_json = {
        'recordsTotal': records_total,
        'recordsFiltered': records_filtered,
        'data': data,
    }
    chunk_json = json.dumps(chunk_json)
    return HttpResponse(chunk_json, content_type='application/json')

@permission_required('miroutes.add_wall')
def reset_dev_wall(request, wall_id, **kwargs):
    """
        reset dev Wall
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    dev_view = wall.dev_view
    pub_view = wall.pub_view

    next_page = request.GET.get('next', '/')

    if check_edit_lock(wall, request.user):
        if request.user.userprofile.change_puzzle_pieces('revert_wallview'):
            wall.reset_dev_view()
            wall.save()

            messages.add_message(request, messages.SUCCESS,
                             _('Successfully resetted {} to its public state!').format(escape(wall.name)))
        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to reset `{}`. Not enough puzzle pieces ({} needed).').format(escape(wall.name), get_puzzles('reset_wallview')))

    else:
        messages.add_message(request, messages.ERROR,
                             _('Failed to reset `{}`. Your edit session expired.').format(escape(wall.name)))
    return go_to(next_page)

@permission_required('miroutes.change_wall')
def decorate_wall(request, wall_id, **kwargs):
    """Decorate Wallview with bolts, belays and stuff.
    """
    wall = get_object_or_404(Wall, pk=wall_id)
    wallview = wall.dev_view
    #wallroutegeoms_query = wallview.routegeometry_set.all()

    toolbox_simple = [
        SimpleMarker(wallview=wallview, markertype=mtype[0])
        for mtype in SimpleMarker.MARKERTYPE_CHOICES
    ]
    toolbox_relational = [
        RelationalMarker(wallview=wallview, markertype=mtype[0])
        for mtype in RelationalMarker.MARKERTYPE_CHOICES
    ]
    toolbox_text = [
        TextMarker(wallview=wallview),
        PitchMarker(wallview=wallview),
    ]

    simple_markers = wallview.simplemarker_set.all()
    relational_markers = wallview.relationalmarker_set.all()
    text_markers = list(wallview.textmarker_set.all())
    pitch_markers = list(wallview.pitchmarker_set.all())

    text_markers += pitch_markers

    editable = acquire_edit_lock(wall, request.user)
    if not editable:
        messages.add_message(
            request, messages.ERROR,
            _('{} is currently edited by {}. Changes will not take effect.').format(
                escape(wall.name), escape(wall.is_edited.by.username)))

    context = {
        'wall': wall,
        'wallview': wallview,
        'simple_markers': simple_markers,
        'relational_markers': relational_markers,
        'text_markers': text_markers,
        'toolbox_simple': toolbox_simple,
        'toolbox_relational': toolbox_relational,
        'toolbox_text': toolbox_text,
        'editable': editable
    }

    return render(request, 'edit_spot/decorate_wall.html', context)

@permission_required('miroutes.change_wall')
def toggle_link_route(request, wallview_id, route_id, **kwargs):
    """Link a route using an ajax call.
    """
    wallview = get_object_or_404(WallView, pk=wallview_id)
    route = get_object_or_404(Route, pk=route_id)

    if check_edit_lock(wallview.wall, request.user):
        geom_qs = wallview.routegeometry_set.filter(route=route)
        if geom_qs:
            # route is linked. Unlink.
            # there should not be multiple geoms with same route and wv
            for geom in geom_qs:
                if request.user.userprofile.change_puzzle_pieces('unlink_route'):
                    payload = {
                        'success': True,
                        'message': _('Removed `{}` from `{}`').format(
                            escape(geom.route.name), escape(geom.on_wallview.wall.name))}
                    geom.delete()
                else:
                    payload = {
                        'success': False,
                        'message': _("Not enough puzzle pieces to unlink {} ({} required)").format(
                            escape(geom.route.name), get_puzzles('unlink_route'))
                        }
        else:
            if wallview.wall.deprecated:
                payload = {
                    'success': False,
                    'message': _('Could not link route to {}, wall is flagged deprecated!').format(escape(wallview.wall.name))}
            elif route.deprecated:
                payload = {
                    'success': False,
                    'message': _('Could not link route to {}, route is flagged deprecated!').format(escape(wallview.wall.name))}
            else:
                # Link route to wv.
                geom = RouteGeometry(on_wallview=wallview, route=route, geojson=None)
                geom.save()
                request.user.userprofile.change_puzzle_pieces('link_route')
                payload = {
                    'success': True,
                    'message': _('Added {} to {}').format(escape(route.name), escape(wallview.wall.name))}

    else:
        payload = {
            'success': False,
            'message': _('Could not link routes to {}, your editing session expired!').format(escape(wallview.wall.name))}

    return JsonResponse(json.dumps(payload), safe=False)


@permission_required('miroutes.change_wall')
def link_routes(request, wall_id, **kwargs):
    """
    Return a template to link a route to the wallview with wallview_id.
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    if wall.gym:  # if this is a gym, we dont want to use link routes
        return redirect(reverse('edit_spot:draw_routes', kwargs={'wall_id': wall.pk}))

    wallview = wall.dev_view
    next_page = request.GET.get('next', None)

    routelist = Route.objects.filter(pnt__distance_lt=(wallview.wall.pnt, D(km=2))).annotate(
        distance=Distance('pnt', wallview.wall.pnt))
    other_routelist = list(routelist.exclude(walls=wallview))
    this_routelist = list(routelist.filter(walls=wallview))
    for route in this_routelist:
        route.onthiswall = True

    routelist = other_routelist + this_routelist

    # annotate with walls (dev walls get an asterisk on front)
    active_walldict = {}
    for route in routelist:
        active_walllist = [view.wall.name for view in route.walls.filter(is_dev=False)]
        dev_walllist = ['*'+view.wall.name for view in route.walls.filter(is_dev=True)]
        active_walldict[route.id] = ", ".join(dev_walllist + active_walllist)

    can_edit = acquire_edit_lock(wallview.wall, request.user)
    if not can_edit:
        messages.add_message(
            request, messages.ERROR,
            _('{} is currently edited by {}. Changes will not take effect.').format(
                wallview.wall.name, wallview.wall.is_edited.by.username))


    context = {
        'wall': wall,
        'wallview': wallview,
        'routelist': routelist,
        'active_walldict': active_walldict,
        'show_edit_pane': True,
        'editable': can_edit,
        'next_page': next_page
    }

    return render(request, 'edit_spot/link_routes.html', context)

@permission_required('miroutes.change_wall')
def link_materials(request, wall_id=None, route_id=None, **kwargs):
    """
    Return a template to link materials.
    """
    wall = None
    route = None
    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
        wall = obj
    elif route_id:
        obj = get_object_or_404(Route, pk=route_id)
        route = obj
    else:
        raise Http404("Please specify either wall or route id.")

    next_page = request.GET.get('next', None)
    # store next_page in sessions.
    # This way the redirect is persistent also when coming from add material.
    if(next_page):
        request.session['next_page'] = next_page
    else:
        next_page = request.session.get('next_page', None)

    materials = Userpic.objects.filter(
        content='info').filter(
            pnt__distance_lt=(obj.pnt, D(km=10))).annotate(
                distance=Distance('pnt', obj.pnt))
    if wall:
        other_matlist = list(materials.exclude(on_wall=wall))
        this_matlist = list(materials.filter(on_wall=wall))
    else:
        other_matlist = list(materials.exclude(on_route=route))
        this_matlist = list(materials.filter(on_route=route))

    for material in this_matlist:
        material.onthis = True

    matlist = other_matlist + this_matlist
    for mat in matlist:
        mat.thread_id = generate_thread_id(mat)

    can_edit = acquire_edit_lock(obj, request.user)
    if not can_edit:
        messages.add_message(
            request, messages.ERROR,
            _('{} is currently edited by {}. You can not link materials.').format(
                obj.name, obj.is_edited.by.username))


    material_form = MaterialForm()

    context = {
        'wall': wall,
        'route': route,
        'matlist': matlist,
        # 'active_walldict': active_walldict,
        'editable': can_edit,
        'material_form': material_form,
        'next_page': next_page
    }

    return render(request, 'edit_spot/link_materials.html', context)

@permission_required('miroutes.change_wall')
def toggle_link_material(request, wall_id=None, route_id=None, material_id=None, **kwargs):
    """
    Link info material using an ajax call.
    """
    material = get_object_or_404(Userpic, pk=material_id)
    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
    elif route_id:
        obj = get_object_or_404(Route, pk=route_id)
    else:
        raise Http404("Please specify either wall or route id.")
    if check_edit_lock(obj, request.user):
        if obj.userpic_set.filter(pk=material.pk).exists():
            request.user.userprofile.change_puzzle_pieces('unlink_material')
            payload = {
                'success': True,
                'message': _('Removed material from `{}`').format(
                    escape(obj.name))}
            obj.userpic_set.remove(material)

        else:
            obj.userpic_set.add(material)
            request.user.userprofile.change_puzzle_pieces('link_material')
            message = _('Added material to `{}`').format(escape(obj.name))
            payload = {
                'success': True,
                'message': message}

        # We have to regenerate PDF Topos, those functions are excessive
        # because they also update route geoms but we should probably introduce
        # some kind of cache invalidation anyway.
        # I leave a TODO tag here and hopefully come back to make that more performant one day
        if wall_id:
            if obj.is_active:
                update_Wall_after_publish.run(wall_id, snapshots=False, pdf=True, activate=False, surrounding_topos=False, notify=False)
        if route_id:
            update_after_Route_save.run(route_id)


    else:
        payload = {
            'success': False,
            'message': _('Could not link the additional material to {}, your editing session expired!').format(escape(obj.name))}

    return JsonResponse(json.dumps(payload), safe=False)

@permission_required('miroutes.delete_wall')
def del_wall(request, wall_id, **kwargs):
    """
    Delete a Wall
    """
    wall = get_object_or_404(Wall, pk=wall_id)

    next_page = request.GET.get('next')

    context = {
        'wall': wall,
        'next_page': next_page
    }

    if request.method == 'POST':
        wall.delete()
        return go_to(next_page)

    return render(request, 'edit_spot/del_wall.html', context)

@permission_required('miroutes.change_wall')
def del_geom(request, geom_id, **kwargs):
    """
    Delete a RouteGeometry
    """
    geom = get_object_or_404(RouteGeometry, pk=geom_id)

    messages.add_message(request, messages.SUCCESS,
            _('Successfully removed `{}` from `{}`').format(geom.route.name, geom.on_wallview.wall.name))

    geom.delete()
    next_page = request.GET.get('next', None)

    return go_to(next_page)

def get_prepopulated_wallform(wall):
    """Obtain a wallform pre-populated with meta-info."""
    from django.forms.models import model_to_dict
    exclude = ['id', 'name', 'is_active', 'orig_img', 'background_img']
    initial_fields = {}
    model_dict = model_to_dict(wall)
    for field in model_dict:
        if field not in exclude:
            initial_fields[field] = model_dict[field]
    return WallForm(initial=initial_fields)


@permission_required('miroutes.add_wall')
def add_wall(request, tiler_id, **kwargs):
    """
    Adding a new wall.
    """
    tiler = get_object_or_404(Tiler, pk=tiler_id)

    wall_template = Wall()

    if request.method == 'POST':
        form = WallForm(request.POST, request.FILES)
        if form.is_valid():
            wall = save_and_create_revision(form, request, "New Wall created.")

            if wall:
                wall.created_by = request.user
                wall.save(update_fields=['created_by'])
                messages.add_message(request, messages.SUCCESS,
                                     _('Successfully added {}!').format(wall.name))
                request.user.userprofile.change_puzzle_pieces('add_wall')
                if wall.gym:
                    return redirect(reverse('edit_spot:draw_routes', kwargs={'wall_id': wall.pk}))
                else:
                    return redirect(reverse('edit_spot:link_routes', kwargs={'wall_id': wall.pk}))
        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to create wall!'))

    elif request.method == 'GET': # 'GET'
        wall_toclone = request.GET.get('wall_toclone')
        wall_lat = request.session.get('wall_lat', None)
        wall_lng = request.session.get('wall_lng', None)
        wall_pnt = None
        if wall_lat and wall_lng:
            wall_pnt = Point(float(wall_lat), float(wall_lng))
            wall_template = Wall(pnt=wall_pnt)
            form = WallForm(initial={'pnt': wall_pnt, 'tiler': tiler_id})
        elif wall_toclone:
            wall_toclone = get_object_or_404(Wall, pk=wall_toclone)
            form = get_prepopulated_wallform(wall_toclone)
        else:
            form = WallForm(initial={'tiler': tiler_id})

    form.fields['subwall_of'] = ChoiceField(
        disabled=True, help_text=_("Wall has to be saved at least once to select a wall."), required=False)

    context = {
        'editing': False,
        'wall_template': wall_template,
        'tiler': tiler,
        'show_edit_pane': True,
        'form': form,
    }
    return render(request, 'edit_spot/edit_wall.html', context)

def delete_wall(request, wall_id, **kwargs):
    """Deleting a wall that has not been edited yet."""
    wall = get_object_or_404(Wall, pk=wall_id)
    next_page = request.GET.get('next', None)

    if wall.is_deletable and wall.created_by == request.user:
        messages.add_message(request, messages.SUCCESS,
                             ugettext('Successfully deleted {}!').format(wall.name))
        wall.delete()
        return redirect(reverse('miroutes:index'))
    else:
        messages.add_message(request, messages.ERROR,
                             _('Failed to delete wall! Other users contributed.'))
        go_to(next_page)

@permission_required('miroutes.add_wall')
def add_wall_img(request, **kwargs):
    """
    Adding a new wall image.
    """

    tiler = None
    if request.method == 'POST':
        form = TilerForm(request.POST, request.FILES)
        if form.is_valid():
            with reversion.create_revision():
                # create initial tiler revision
                reversion.set_user(request.user)
                reversion.set_comment(_("Added new wall image."))
                tiler = form.save(commit=False)
                tiler.created_by = request.user
                tiler.save()
                form.save_m2m()
            form.delete_temporary_files()

            messages.add_message(request, messages.SUCCESS,
                                 _('Successfully added Wall Image!'))
            request.user.userprofile.change_puzzle_pieces('add_wall_img')

        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to create wall!'))
    else:
        form = TilerForm()

    request.session['wall_lat'] = request.GET.get('wall_lat', None)
    request.session['wall_lng'] = request.GET.get('wall_lng', None)
    lat = request.GET.get('center_lat', None)
    lng = request.GET.get('center_lng', None)
    zoom = request.GET.get('zoom', None)

    mapview = {}
    if lat and lng and zoom:
        request.session['mapview'] = {'lat': lat, 'lng': lng, 'zoom': zoom}

    context = {
        'form': form,
        'tiler': tiler
    }
    return render(request, 'edit_spot/edit_wall_img.html', context)


@permission_required('miroutes.add_wall')
def add_wall_from_pano(request, pano_id):
    import os
    pano = get_object_or_404(Pano, pk=pano_id)
    if pano.status != StitcherStatus.FINISHED:
        return HttpResponse('Stitcher is not finished, please make sure the panorama is ready...')

    os.sync() # make sure that the filesystem has finished writing the pano
    tiler = Tiler()
    img = pano.img
    tiler.orig_img.save(os.path.basename(img.name), img.file)
    os.sync()
    tiler.created_by = request.user
    tiler.save()

    return redirect(reverse('edit_spot:add_wall', kwargs={'tiler_id': tiler.pk}))


@permission_required('miroutes.change_tiler')
def edit_wall_img(request, tiler_id, **kwargs):
    """
    Editing a wall.
    """

    tiler = get_object_or_404(Tiler, pk=tiler_id)

    if request.method == 'POST':
        form = TilerForm(request.POST, request.FILES)
        if hasattr(tiler, 'wall'):
            if check_edit_lock(tiler.wall, request.user):
                save_wall_img(request, form, tiler)
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Could not save {}, your editing session expired!').format(tiler.wall.name))
        else:
            save_wall_img(request, form, tiler)
    else:
        if hasattr(tiler, 'wall'):
            if acquire_edit_lock(tiler.wall, request.user):
                form = TilerForm()
            else:
                # the wall form is disabled when the lock can not be acquired
                form = TilerForm(editable=False)
                messages.add_message(
                    request, messages.ERROR,
                    _('{} is currently edited by {}!').format(tiler.wall.name, tiler.wall.is_edited.by.username))
        else:
            form = TilerForm()

    versions = get_all_versions(tiler)

    # we pass a wall object in the context to make edit pane work as expected
    wall = None
    if hasattr(tiler, 'wall'):
        wall = tiler.wall

    context = {
        'editing': True,
        'tiler': tiler,
        'wall' : wall,
        'show_edit_pane': True,
        'form': form,
        'versions': versions,
    }

    return render(request, 'edit_spot/edit_wall_img.html', context)


def add_material(request, wall_id=None, route_id=None):
    """Add additional material."""
    from miroutes.wall_img_functions import get_exif_location
    next_page = request.GET.get('next', None)
    if wall_id:
        obj = get_object_or_404(Wall, pk=wall_id)
    else:
        obj = get_object_or_404(Route, pk=route_id)

    if request.POST:
        form = MaterialForm(request.POST, request.FILES)
        if form.is_valid():
            request.user.userprofile.change_puzzle_pieces('add_material')
            images = form.cleaned_data['img']
            if(images):
                description = form.cleaned_data.pop('description')
                external_url = form.cleaned_data.pop('external_url')
                for m_file in images:
                    name, ext = os.path.splitext(m_file.name)
                    if ext in [".pdf", ".PDF"]:
                        mat = Userpic(
                            pdf_file=m_file,
                            content='info',
                            pnt=obj.pnt,
                            description=description,
                            external_url=external_url,
                            created_by=request.user)
                        save_pdf_material(mat)
                    else:
                        mat = Userpic(
                            img=m_file,
                            content='info',
                            pnt=obj.pnt,
                            description=description,
                            external_url=external_url,
                            created_by=request.user)
                        mat.save()
                        # check and set exif location
                        exif_location = get_exif_location(mat.img)
                        if(exif_location):
                            pnt = Point(*exif_location)
                            # Check if we are more then 10km away
                            if pnt.distance(obj.pnt)*111. > 10.:
                                messages.add_message(request, messages.INFO,
                                                     _('EXIF Tag of Image is not consistent with location!'))
                            else:
                                mat.pnt = pnt

                        mat.save()
                    obj.userpic_set.add(mat)
                    if wall_id:
                        update_Wall_after_publish.run(wall_id, snapshots=False, pdf=True, activate=False, surrounding_topos=False, notify=False)
                    if route_id:
                        update_after_Route_save.run(route_id)

            form.delete_temporary_files()
            messages.add_message(request, messages.SUCCESS,
                                 _('Saved Material!'))

            return go_to(next_page)
        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to save Material!'))
    else:
        form = MaterialForm()

    context = {
        'material_form': form,
        'next_page': next_page,
    }
    return render(request, 'edit_spot/material_form.html', context)

@permission_required('miroutes.change_wall')
def edit_wall(request, wall_id, **kwargs):
    """
    Editing a wall.
    """

    wall = get_object_or_404(Wall, pk=wall_id)

    if request.method == 'POST':
        form = WallForm(request.POST, request.FILES, instance=wall)
        if check_edit_lock(wall, request.user):
            if form.is_valid():
                # check if object changed
                if does_obj_change(wall, form):
                    if 'orig_img' in form.changed_data:
                        wall.tiler.delete()

                    new_wall = save_and_create_revision(form, request)

                    if new_wall:
                        if new_wall.is_active: # only call after hook if the wall is already active, otherwise it needs to be published first
                            update_Wall_after_publish.run(wall_id=new_wall.id, activate=False)

                        request.user.userprofile.change_puzzle_pieces('edit_wall')
                        messages.add_message(
                            request, messages.SUCCESS,
                            _('Successfully saved {}!').format(new_wall.name))
                else:
                    messages.add_message(
                        request, messages.WARNING,
                        _('No changes to {}!').format(wall.name))
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Could not save changes to {} due to errors in the form!').format(wall.name))

        else:
            messages.add_message(
                request, messages.ERROR,
                _('Could not save {}, your editing session expired!').format(wall.name))



    else:
        if acquire_edit_lock(wall, request.user):
            form = WallForm(instance=wall)
        else:
            # the wall form is disabled when the lock can not be acquired
            form = WallForm(instance=wall, editable=False)
            messages.add_message(
                request, messages.ERROR,
                _('{} is currently edited by {}!').format(wall.name, wall.is_edited.by.username))
    # subwall dropdown choices
    nearby_walls = Wall.objects.exclude(pk=wall.pk).filter(pnt__distance_lt=(wall.pnt, D(km=2))).annotate(
        distance=Distance('pnt', wall.pnt)).order_by('distance')
    form.fields['subwall_of'] = RelationalModelChoiceField(nearby_walls, required=False)

    wall_list = Wall.objects.exclude(pk=wall_id)
    wall_template = Wall()

    versions = get_all_versions(wall)

    revision_form = RevisionForm(comment="")

    context = {
        'editing': True,
        'wall': wall,
        'tiler': wall.tiler,
        'wall_template': wall_template,
        'wall_list': wall_list,
        'show_edit_pane': True,
        'form': form,
        'versions': versions,
        'revision_form': revision_form,
    }

    return render(request, 'edit_spot/edit_wall.html', context)

@permission_required('miroutes.change_tiler')
def rotate_wall_img_left(request, tiler_id, **kwargs):
    tiler = get_object_or_404(Tiler, pk=tiler_id)

    if hasattr(tiler, 'wall') and not acquire_edit_lock(tiler.wall, request.user):
            messages.add_message(
                request, messages.ERROR,
                _('Image can not be rotated. {} is currently edited by {}!').format(tiler.wall.name, tiler.wall.is_edited.by.username))
            return redirect(reverse('edit_spot:edit_wall_img', kwargs={'tiler_id': tiler.pk}))
    with reversion.create_revision():
        reversion.set_user(request.user)
        reversion.set_comment("Rotated wall image to the left.")
        tiler.rotate_left()
        messages.add_message(request, messages.SUCCESS,
                                 _('Image rotated!'))
    return redirect(reverse('edit_spot:edit_wall_img', kwargs={'tiler_id': tiler.pk}))

@permission_required('miroutes.change_tiler')
def rotate_wall_img_right(request, tiler_id, **kwargs):
    tiler = get_object_or_404(Tiler, pk=tiler_id)

    if hasattr(tiler, 'wall') and not acquire_edit_lock(tiler.wall, request.user):
            messages.add_message(
                request, messages.ERROR,
                _('Image can not be rotated. {} is currently edited by {}!').format(tiler.wall.name, tiler.wall.is_edited.by.username))
            return redirect(reverse('edit_spot:edit_wall_img', kwargs={'tiler_id': tiler.pk}))
    with reversion.create_revision():
        reversion.set_user(request.user)
        reversion.set_comment("Rotated wall image to the right.")
        tiler.rotate_right()
        messages.add_message(
            request, messages.SUCCESS, _('Image rotated!'))
    return redirect(reverse('edit_spot:edit_wall_img', kwargs={'tiler_id': tiler.pk}))

@permission_required('miroutes.change_route')
def deprecate_route(request, route_id, **kwargs):
    """
    Flag route as deprecated.
    Should not show up on link_routes by default.
    Should not be linked anymore.
    """
    route = get_object_or_404(Route, pk=route_id)

    route.deprecated = True
    # geoms on gyms (dev_views) are removed
    gym_geoms = route.routegeometry_set.filter(on_wallview__wall__gym=True)
    gym_geoms.filter(on_wallview__is_dev=True).delete()

    route.save()
    message = _('{} flagged as deprecated.').format(route.name)
    if request.is_ajax():
        payload = {
            'message': message,
            'data': None,
            'success': True
        }
        return JsonResponse(json.dumps(payload), safe=False)


    next_page = request.GET.get('next')

    messages.add_message(request, messages.SUCCESS, message)
    return go_to(next_page)

@permission_required('miroutes.add_route')
def add_route(request):
    """Add a route."""
    next_page = request.GET.get('next', None)
    wall_id = request.GET.get('wall_id')
    if request.POST:
        form = RouteForm(request.POST)
        if form.is_valid():
            route = save_and_create_revision(form, request, "New Route created.")
            request.user.userprofile.change_puzzle_pieces('add_route')
            route.created_by = request.user
            route.save(update_fields=['created_by'])

            messages.add_message(request, messages.SUCCESS,
                                 _('Saved {}!').format(route.name))
            # immediately link the route if called from a wallview
            if wall_id and route:
                wall = get_object_or_404(Wall, pk=wall_id)
                if not wall.deprecated:
                    geom = RouteGeometry(on_wallview=wall.dev_view, route=route, geojson=None)
                    geom.save()

            if request.POST.get('create_new') == 'false':
                return go_to(next_page)

        else:
            messages.add_message(request, messages.ERROR,
                                 _('Failed to save Route!'))

    route_template = Route()

    wall = None
    wall_id = request.GET.get('wall_id')
    if wall_id:
        wall = get_object_or_404(Wall, pk=wall_id)
        route_form = RouteForm(initial={
            'pnt': wall.pnt,
            'climbingstyle': wall.dev_view.preferred_climbstyle
        }, nearest_wall=wall)
    else:
        route_form = RouteForm()

    context = {
        'route_form': route_form,
        'route_template': route_template,
        'next_page': next_page,
        'wall': wall
    }
    return render(request, 'edit_spot/route_form.html', context)

@permission_required('miroutes.change_route')
def edit_gym_route(request, wallview_id, route_id=None):
    """Edit existing gym route, add new gym route, deliver form."""
    wallview = get_object_or_404(WallView, pk=wallview_id)
    form = None

    if request.POST:
        if route_id is not None:
            # editing existing route
            route = get_object_or_404(Route, pk=route_id)
            form = SmallRouteForm(request.POST, instance=route)
            if form.is_valid() and form.changed_data:
                route = save_and_create_revision(form, request, "Route Edit.")
                request.user.userprofile.change_puzzle_pieces('edit_route')
                if 'deprecated' in form.changed_data:
                    messages.add_message(
                        request, messages.SUCCESS,
                        _('Deleted {}!').format(route.name))
                else:
                    messages.add_message(
                        request, messages.SUCCESS,
                        _('Saved {}!').format(route.name))
                form = route_id = None
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Failed to save {}!').format(route.name))
        else:
            # new route
            form = SmallRouteForm(request.POST)
            if form.is_valid() and not wallview.wall.deprecated:
                route = save_and_create_revision(form, request, "New Route created.")
                request.user.userprofile.change_puzzle_pieces('add_route')
                route.created_by = request.user
                route.save(update_fields=['created_by'])

                messages.add_message(request, messages.SUCCESS,
                                     _('Saved {}!').format(route.name))
                # immediately link the route
                geom = RouteGeometry(on_wallview=wallview, route=route, geojson=None)
                geom.save()
                # reinitialize form
                form = None
            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Failed to create route! Is the wall deprecated?'))

    if not form:
        # only create a new form if we do not return error prone form from POST
        if route_id:
            route = get_object_or_404(Route, pk=route_id)
            form = SmallRouteForm(
                instance=route,
                nearest_wall=wallview.wall,
                action=reverse('edit_spot:edit_gym_route', kwargs={'wallview_id': wallview.pk})
            )
        else:
            form = SmallRouteForm(
                nearest_wall=wallview.wall,
                action=reverse('edit_spot:edit_gym_route', kwargs={'wallview_id': wallview.pk}),
                initial={
                    'pnt': wallview.wall.pnt,
                    'name': find_unique_routename(wallview)
                }
            )

    context = {
        'form': form,
    }
    return render(request, 'edit_spot/gym_route_form.html', context)


@permission_required('miroutes.change_route')
def edit_route(request, route_id, **kwargs):
    """Save a Route or return the form to edit a route.
    """

    route = get_object_or_404(Route, pk=route_id)
    next_page = request.GET.get('next', None)

    if request.method == 'POST':
        if check_edit_lock(route, request.user):
            form = RouteForm(request.POST, instance=route)
            if form.is_valid():
                # only create a revision if there are actual changes
                if does_obj_change(route, form):
                    route = save_and_create_revision(form, request)
                    if route:
                        request.user.userprofile.change_puzzle_pieces('edit_route')
                        messages.add_message(
                            request, messages.SUCCESS,
                            _('Saved {}!').format(route.name))
                        release_edit_lock(route, request.user)
                else:
                    messages.add_message(
                        request, messages.WARNING,
                        _('No changes to {}!').format(route.name))

                return go_to(next_page)

            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('Failed to save {}!').format(route.name))

        else:
            messages.add_message(
                request, messages.ERROR,
                _('Failed to save {}. Your edit session expired or another user is editing the route!').format(route.name))

    route_template = Route()

    can_edit = acquire_edit_lock(route, request.user)
    if not can_edit:
        messages.add_message(request, messages.ERROR,
                             _('`{}` is currently being edited!').format(route.name))
    wall = request.GET.get('wall_id')
    if wall:
        wall = get_object_or_404(Wall, pk=wall)

    route_form = RouteForm(
        nearest_wall=wall,
        instance=route,
        editable=can_edit,
        deleteable=route.can_be_deleted_by(request.user),
        action=reverse('edit_spot:edit_route', kwargs={'route_id': route.id}),
        next_page=next_page)

    revision_form = RevisionForm(comment=_("Updated {}.").format(route.name))

    context = {
        'route_form': route_form,
        'route_revision_form': revision_form,
        'route': route,
        'route_template': route_template,
        'next_page': next_page,
        'versions': get_all_versions(route),
        'wall': wall,  # have wall here to populate edit_pane if editing a route that is already attached to a wall
    }
    return render(request, 'edit_spot/route_form.html', context)

def go_to(next_page):
    """Return a HTTP request for the next_page url."""
    if next_page:
           return redirect(next_page)
    else: # we have to send them somewhere? this is probably not wanted but lets show the way to /
           return redirect('/')

def revert_wallview_DEPRECATED(request, wall_id, revision_id):
    """Revert a (public) wallview to a older version of the public wallview."""

    next_page = request.GET.get('next', None)
    revision = get_object_or_404(Revision, pk=revision_id)
    wall = get_object_or_404(Wall, pk=wall_id)

    wallview = wall.pub_view

    if request.POST:
        if check_edit_lock(wall, request.user):
            revision_form = RevisionForm(request.POST)
            if revision_form.is_valid():
                if request.user.userprofile.change_puzzle_pieces('revert_wallview'):
                    comment = revision_form.cleaned_data['revision_comment']
                    wall.revert_wallview(revision, comment, request.user)

                    messages.add_message(
                        request, messages.SUCCESS,
                        _('Reverted the edit view on {}!').format(wallview.wall.name))
                    return go_to(next_page)
                else:
                    messages.add_message(
                        request, messages.ERROR,
                        _('`{}` can not be reverted. Not enough puzzle pieces ({} needed).').format(wall.name, get_puzzles('revert_wallview')))

            else:
                messages.add_message(
                    request, messages.ERROR,
                    _('`{}` can not be reverted. The commit message is invalid!').format(wall.name))

        else:
            messages.add_message(
                request, messages.ERROR,
                _('`{}` can not be reverted, it is currently being edited!').format(wall.name))



    return render(request, 'edit_spot/revert_wallview.html', context)

@permission_required('miroutes.change_route')
def revert_route(request, route_id, version_id, **kwargs):
    """
    Restore a Route from revisions
    """
    next_page = request.GET.get('next', None)
    route = get_object_or_404(Route, pk=route_id)
    route_form = RouteForm(instance=route, editable=False)

    version = get_object_or_404(Version, pk=version_id)
    versioned_route_form = RouteForm(initial=version.field_dict, editable=False)

    if request.method == 'POST':
        if check_edit_lock(route, request.user):
            revision_form = RevisionForm(request.POST)
            revision_form.is_valid()
            if request.user.userprofile.change_puzzle_pieces('revert_route'):
                try:
                    comment = revision_form.cleaned_data['revision_comment']
                    with reversion.create_revision():
                        version.revert()
                        reversion.set_user(request.user)
                        reversion.set_comment("[Reverted {} to state from {}] {}".format(
                            route.name,
                            version.revision.date_created.strftime("%d. %b %y %H:%M:%S"),
                            comment))

                    messages.add_message(request, messages.SUCCESS,
                                         _('Saved {}!').format(route.name))
                    return go_to(next_page)

                except reversion.RevertError as e:
                    messages.add_message(request, messages.WARNING,
                            _('Could not revert {}! :: {}').format(route.name, e))
            else:
                messages.add_message(request, messages.ERROR,
                    _('Route {} can not be reverted. Not enough puzzle pieces ({} needed)').format(route.name, get_puzzles('revert_route')))

        else:
            messages.add_message(request, messages.ERROR,
                    _('Route {} can not be reverted, it is currently edited!').format(route.name))

    changed_fields = compare_model_fields(route.__dict__, version.field_dict)

    if not acquire_edit_lock(route, request.user):
        messages.add_message(request, messages.ERROR,
                             _('Route {} can not be reverted, it is currently edited!').format(route.name))
    revision_form = RevisionForm()

    context = {
        'route_form': route_form,
        'route': route,
        'version': version,
        'versioned_route_form': versioned_route_form,
        'changed_fields': changed_fields,
        'next_page': next_page,
        'revision_form': revision_form
    }
    return render(request, 'edit_spot/revert_route.html', context)

@permission_required('miroutes.change_wall')
def revert_wall(request, wall_id, version_id, **kwargs):
    """
    Restore a Wall from revisions
    """
    next_page = request.GET.get('next', None)
    wall = get_object_or_404(Wall, pk=wall_id)

    target_version = get_object_or_404(Version, pk=version_id)
    current_version = Version.objects.get_for_object(wall).first()

    target_revision = target_version.revision
    current_revision = current_version.revision

    this_wall_data = model_to_dict(wall)
    # here we need some compare wall versions that does it all (wallview and wall)

    changed_fields = compare_model_fields(this_wall_data, target_version.field_dict)

    changeset = []
    for model, changes in list(wallview_revision_diff(target_revision, current_revision).items()):
        if changes:
            changeset += changes

    # objects of the current version
    old_routegeometries = create_annotated_and_sorted_geomlist(list(wall.pub_view.routegeometry_set.all()))
    old_routeids = wall.pub_view.routegeometry_set.values_list('route_id', flat=True)
    old_markerlists = [
        list(wall.pub_view.simplemarker_set.all()),
        list(wall.pub_view.relationalmarker_set.all()),
        list(wall.pub_view.textmarker_set.all()),
        list(wall.pub_view.pitchmarker_set.all())
    ]
    old_markers = list(itertools.chain.from_iterable(old_markerlists))

    # objects of the target version
    obj_vers = target_revision.version_set.all()
    markertypes = [
        SimpleMarker.__name__,
        RelationalMarker.__name__,
        TextMarker.__name__,
        PitchMarker.__name__
    ]

    new_routegeometries = []
    new_routeids = []
    new_markers = []
    for ver in obj_vers:
        obj = ver._object_version.object
        clsname = obj.__class__.__name__
        if clsname == RouteGeometry.__name__:
            # does the route still exist?
            rcount = Route.objects.filter(pk=obj.route_id).count()
            if rcount:
                new_routegeometries.append(obj)
                new_routeids.append(obj.route_id)
        elif clsname in markertypes:
            new_markers.append(obj)

    new_routegeometries = create_annotated_and_sorted_geomlist(new_routegeometries)

    union_routeidlist = list(set(new_routeids) | set(old_routeids))
    union_routelist = []
    for routeid in union_routeidlist:
        route = Route.objects.get(pk=routeid)
        if routeid in new_routeids:
            route.on_new = True
        if routeid in old_routeids:
            route.on_old = True
        union_routelist.append(route)


    if request.method == 'POST':
        if check_edit_lock(wall, request.user):
            if request.user.userprofile.change_puzzle_pieces('revert_wall'):
                revision_form = RevisionForm(request.POST)
                if revision_form.is_valid():
                    if not changed_fields and not changeset:  # i.e. empty dict
                        messages.add_message(request, messages.WARNING, _('Did not revert to old version. Seems that nothing has changed between versions of {wallname}!').format(wallname=wall.name))
                    else:
                        comment = revision_form.cleaned_data['revision_comment']
                        wall.revert_wall(target_revision, comment, request.user)
                        messages.add_message(request, messages.SUCCESS,
                                            _('Reverted {}!').format(wall.name))

                else:
                    messages.add_message(request, messages.ERROR,
                                         _('Wall {} can not be reverted, the comment is not valid!').format(wall.name))

                return go_to(next_page)
            else:
                messages.add_message(request, messages.ERROR,
                                     _('Wall {} can not be reverted. Not enough puzzle pieces ({} needed)').format(wall.name, get_puzzles('revert_wall')))

        else:
            messages.add_message(request, messages.ERROR,
                                 _('Wall {} can not be reverted, it is currently edited!').format(wall.name))

    if not acquire_edit_lock(wall, request.user):
        messages.add_message(request, messages.ERROR,
                             _('Wall {} can not be reverted, it is currently edited!').format(wall.name))

    revision_form = RevisionForm()

    revision_form = RevisionForm(comment="")

    context = {
        'changeset': changeset,
        'new_routegeometries': new_routegeometries,
        'old_routegeometries': old_routegeometries,
        'new_markers': new_markers,
        'old_markers': old_markers,
        'union_routelist': union_routelist,
        'versioned_wall': target_version.field_dict,
        'changed_fields': changed_fields,
        'wall': wall,
        'target_revision': target_revision,
        'next_page': next_page,
        'revision_form': revision_form
    }
    return render(request, 'edit_spot/revert_wall.html', context)

@permission_required('miroutes.change_wall')
def revert_wall_img(request, tiler_id, version_id, **kwargs):
    """
    Restore a Wall from revisions
    """
    tiler = get_object_or_404(Tiler, pk=tiler_id)
    next_page = request.GET.get('next', None)

    version = get_object_or_404(Version, pk=version_id)

    if check_edit_lock(tiler.wall, request.user):
        if request.user.userprofile.change_puzzle_pieces('revert_wall_img'):
            with reversion.create_revision():
                version.revert()
                reversion.set_user(request.user)
                reversion.set_comment("[Reverted to state from {}]".format(
                    version.revision.date_created.strftime("%d. %b %y %H:%M:%S")))

                messages.add_message(request, messages.SUCCESS,
                                     _('Reverted Image!'))

        else:
            messages.add_message(request, messages.ERROR,
                                 _('Wall image can not be reverted. Not enough puzzle pieces ({} needed)').format(get_puzzles('revert_wall_img')))

    else:
        messages.add_message(request, messages.ERROR,
                             _('Wall image can not be reverted, it is currently edited!'))
    return go_to(next_page)


def compare_model_fields(dict1, dict2):
    excluded_keys = 'id', 'created', '_state', 'timestamp', 'user', 'uid', 'changed', 'number_of_views', 'is_active', 'last_published_date'

    def dict_compare(d1, d2):
        d1_keys = set(d1.keys())
        d2_keys = set(d2.keys())
        intersect_keys = d1_keys.intersection(d2_keys)
        added = d1_keys - d2_keys
        removed = d2_keys - d1_keys

        modified = {}
        same = set()
        for k in intersect_keys:
            # be careful when comparing objects
            isequal = d1[k] == d2[k]
            if isinstance(d1[k], Point):
                isequal = d1[k].equals_exact(d2[k], tolerance=1e-12)
            if isequal:
                same.add(k)
            else:
                modified[k] = (d1[k], d2[k])
        return added, removed, modified, same

    added, removed, modified, same = dict_compare(dict1, dict2)
    changes = {}

    for k, v in modified.items():
        if k not in excluded_keys:
            changes[k] = v
    return changes


@permission_required('miroutes.change_wall')
def store_geojson(request, wallview_id):
    """Store ajaxed geojson to geom."""

    wallview = get_object_or_404(WallView, pk=wallview_id)
    geom_id = int(request.GET.get('geom_id', 0))
    geojson = request.GET.get('geojson', None)


    if geom_id:
        geom = get_object_or_404(RouteGeometry, pk=geom_id)
        if check_edit_lock(wallview.wall, request.user):
            old_geojson = geom.geojson
            if geojson is not None:
                geom.geojson = json.loads(geojson)
            else:
                geom.geojson = None
            geom.full_clean()

            if geom.geojson != old_geojson:
                request.user.userprofile.change_puzzle_pieces('edit_routegeometry')
                geom.save()

                message = _('Saved {}.').format(geom.route.name)
                payload = {
                    'message': escape(message),
                    'data': None,
                    'success': True
                }
            else:
                payload = {
                    'success': True
                }

        else:
            payload = {
                'message': _('Failed to edit {}. Your edit session maybe expired.').format(escape(geom.route.name)),
                'data': None,
                'success': False
            }

    else:
        payload = {
            'message': _('Invalid geojson ({}) or object ID ({}).').format(geojson, geom_id),
            'data': None,
            'success': False
        }

    return JsonResponse(json.dumps(payload), safe=False)



@permission_required('miroutes.change_wall')
def draw_routes(request, wall_id, **kwargs):
    """Draw route geometries in gyms."""
    wall = get_object_or_404(Wall, pk=wall_id)

    wallview = wall.dev_view
    can_edit = acquire_edit_lock(wall, request.user)

    if not can_edit:
        messages.add_message(
            request, messages.ERROR,
            _('{} is currently edited by {}! Changes will have no effect.'.format(escape(wall.name), escape(wall.is_edited.by.username))))

    context = {'wall': wall,
               'wallview': wallview,
               'show_edit_pane': True}

    return render(request, 'edit_spot/draw_routes.html', context)

def finish_wall_edit(request, wall_id, **kwargs):
    wall = get_object_or_404(Wall, pk=wall_id)
    release_edit_lock(wall, request.user)
    next_page = request.GET.get('next', '/')
    return go_to(next_page)
