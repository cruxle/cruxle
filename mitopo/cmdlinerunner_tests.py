import os
import unittest

import sys

scriptdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(scriptdir + '/../')

test_loader = unittest.defaultTestLoader.discover( 'tests' )
test_runner = unittest.TextTestRunner()
testresult = test_runner.run( test_loader )

if testresult == True:
    sys.exit(0)
else:
    sys.exit(1)