import hashlib
import os

def tuple_to_mail_string(mail_tuple):
    return "\"{}\" <{}>".format(mail_tuple[0],mail_tuple[1])

def tuples_to_mail_string(mail_tuple_array):
    return [tuple_to_mail_string(mail_tuple) for mail_tuple in mail_tuple_array]

def md5sum(in_string):
    m = hashlib.md5()
    m.update(in_string.encode('utf-8'))
    return m.hexdigest()

def md5sum_file(path):
    if os.path.isfile(path):
        m = hashlib.md5()
        m.update(str(os.path.getmtime(path)).encode('utf-8'))
        return m.hexdigest()
    else:
        raise Exception("file not found: " + path)
