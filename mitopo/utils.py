"""Module for app-wide utils."""
from django.conf import settings
from django.core.mail import send_mail

def absolute_url(relative_url):
    """Generate absolute URL for mails etc."""
    return settings.PROTOCOL + settings.SITE_DOMAIN + relative_url

def notify_us(subject, msg):
    """Send mail to admin mail for new user/wall notifications."""
    try:
        send_mail(
            subject,
            msg,
            settings.DEFAULT_FROM_EMAIL,
            [settings.DEFAULT_TO_EMAIL, ])
    except Exception as e:
        # One should find the exact error that is thrown here if the
        # backend mail tool refuses to collaborate.
        raise e  # reraise the error, if we cant send out mails, this is bad!
        pass
