"""mitopo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import *
from django.views.generic import RedirectView, TemplateView
from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import JavaScriptCatalog

from rest_framework_swagger.views import get_swagger_view

from mitopo.views import schema_view, greeter_index

app_name = 'mitopo'

urlpatterns = [
    path(r'robots.txt', TemplateView.as_view(template_name='mitopo/robots.txt', content_type='text/plain')),
    path(r'admin/', admin.site.urls),
    path("soc/", include("social_django.urls", namespace="social")),
    path(r'i18n/', include('django.conf.urls.i18n')),
    path(r'micomments/', include('micomments.urls')),
    path(r'upload/', include('django_file_form.urls')),
    url(r'apidoc/', schema_view)
]

urlpatterns += i18n_patterns(
    path('', greeter_index, name='greeter_index'),
    path('miroutes/', include('miroutes.urls')),
    path('edit_spot/', include('edit_spot.urls')),
    path('generic_markers/', include('generic_markers.urls')),
    path('users/', include('users.urls', namespace="users")),
    path('stitcher/', include('stitcher.urls')),
    path('micontactform/', include('micontactform.urls')),
    path('miexport/', include('miexport.urls')),
    path('postbox/', include('postbox.urls')),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),

)

# if in debug mode, redirect static requests to media_ROOT
from django.conf import settings
from  django.views import static

if settings.DEBUG == True:
    urlpatterns += [
        re_path(r'^media/(?P<path>.*)$', static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        re_path(r'^static/(?P<path>.*)$', static.serve, {
            'document_root': settings.STATIC_ROOT,
        }), ]
