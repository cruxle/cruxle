import fnmatch
import os
import unittest
from unittest import skip

from django.conf import settings
import time

import imgcompare
from PIL import Image
from miroutes import tasks

import shutil


SRC_IMAGE = settings.BASE_DIR + "/misc/lost_arrow.jpg"
TILER_DIR = os.path.join(settings.TEST_OUTPUT_DIR, 'test_images')
ORIGINAL_IMAGE = os.path.join(TILER_DIR, 'lost_arrow.jpg')
EXTENDED_IMAGE = os.path.join(TILER_DIR, 'lost_arrow_extended.jpg')
DESTINATION_DIR_PREFIX = os.path.join(TILER_DIR, 'test_tiles_')
REFERENCE_TILES_DIR = settings.BASE_DIR + "/mitopo/tests/test_images/test_tiles_reference"
MAX_ALLOWED_DIFF = 0.5


def tiles_in_dir(dir):
    import glob
    matches = ["/".join(match.split('/')[-3:]) for match in glob.glob(dir+'/*/*/*.png')]
    return matches

@skip("Tiler Test currently broken because pngquant produces a wee bit different images. Look the same for the naked eye though")
class TilingTaskTest(unittest.TestCase):
    def setUp(self):
        #and delete the output folder
        if os.path.exists(TILER_DIR):
            shutil.rmtree(TILER_DIR)

        # always start with a fresh image, just to be safe
        path = os.path.dirname(ORIGINAL_IMAGE)
        if not os.path.exists(path):
            os.makedirs(path)
        shutil.copy(SRC_IMAGE, ORIGINAL_IMAGE)

    def tearDown(self):
        pass

    def test_tile_image(self):

        timestamp_suffix = str(int(time.time()))
        dest_dir = DESTINATION_DIR_PREFIX + timestamp_suffix

        start = time.time()
        tasks.tile_image(ORIGINAL_IMAGE, EXTENDED_IMAGE, dest_dir)

        diff = time.time() - start
        print(("took " + str(diff) + " secs"))
        print("waiting 10 secs for quant tasks to complete")
        time.sleep(10)
        print(("Comparing reference folder: " +  REFERENCE_TILES_DIR + " with " + dest_dir))
        assert(os.path.exists(dest_dir)==True)
        self.compare_tile_dirs(REFERENCE_TILES_DIR, dest_dir)
        self.assertLessEqual(diff, 10, "tiler took more than 10 seconds")

        self.put_tiles_together(EXTENDED_IMAGE, dest_dir, timestamp_suffix)

    def compare_tile_dirs(self, dir_a, dir_b):
        files_a = tiles_in_dir(dir_a)
        files_b = tiles_in_dir(dir_b)

        files_a.sort()
        files_b.sort()
        print(("comparing " + dir_a + " with " + dir_b))
        self.assertSetEqual(set(files_a), set(files_b))

        compared_files = 0

        for png_file in files_a:
            image_a = Image.open(dir_a + "/" + png_file)
            image_b = Image.open(dir_b + "/" + png_file)
            image_a = image_a.convert('RGBA')
            image_b = image_b.convert('RGBA')
            diff_percent = imgcompare.image_diff_percent(image_a, image_b)
            self.assertLessEqual(diff_percent, MAX_ALLOWED_DIFF,
                                 "image_a=" + dir_a + "/" + png_file + " != " + "image_b=" + dir_b + "/" + png_file + " diff=" + str(
                                     diff_percent) )
            compared_files = compared_files + 1

        self.assertEqual(compared_files, 74)

    def put_tiles_together(self, image, tile_dir, timestamp):

        tiles = tiles_in_dir(tile_dir)
        original_image = Image.open(image)

        # only concat the image with maxzoom
        max_zoom = int(max([ t.split('/')[0] for t in tiles ]))

        """ puts subtiles into a new canvas, as big as the orig_img """
        def combine_tiles(tile_dir, orig_img, tiles):
            canvas = Image.new('RGB', orig_img.size, (255, 255, 255))

            for ztile in ztiles:
                timg = Image.open(os.path.join(tile_dir, ztile))
                zoomlvl, x, y = list(map(int, filter(str.isdigit, ztile)))
                offset = x * timg.size[0], y * timg.size[1]
                canvas.paste(timg, offset)
            return canvas

        ztiles = filter(lambda x: x.startswith(str(max_zoom)+'/') and ('quant' not in x), tiles)
        canvas = combine_tiles(tile_dir, original_image, ztiles)

        put_together_image = TILER_DIR + "/put_together_" + timestamp + ".png"
        canvas.save(put_together_image)
        diff_percent = imgcompare.image_diff_percent(original_image, put_together_image)

        self.assertLessEqual(diff_percent, MAX_ALLOWED_DIFF,
                             "original and put-together-image differ: " + str(diff_percent))


if __name__ == "__main__":
    import unittest

    unittest.main()
