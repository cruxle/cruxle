from mitopo.tests.utils import MitopoTestCase


class Step():

    def __init__(self, testcase : MitopoTestCase):
        self.tc = testcase
        self.execute()

    def name(self):
        return self.__class__.__name__

    def before(self):
        pass

    def step(self):
        pass

    def after(self):
        pass

    def execute(self):

        try:
            self.before()
        except Exception as e:
            self.tc.fail("Failed in before phase condition of step \"{}\" with exception: {}".format(self.name(), str(e)))
            self.tc.take_screenshot()

        try:
            self.step()
        except Exception as e:
            self.tc.fail("Failed in step phase of step \"{}\" with exception: {}".format(self.name(), str(e)))
            self.tc.take_screenshot()

        try:
            self.after()
        except Exception as e:
            self.tc.fail("Failed in after phase of step \"{}\" with exception: {}".format(self.name(), str(e)))
            self.tc.take_screenshot()

class LoginStep(Step):

    def before(self):
        self.login_button = self.tc.ensure_present("#login-button")

    def step(self):
        self.login_button.click()

        username_input = self.tc.ensure_present("input[name=\"username\"]")
        username_input.send_keys("root")

        password_input = self.tc.ensure_present("input[name=\"password\"]")
        password_input.send_keys("root")

        submit_login = self.tc.ensure_present("button#submit-login")
        submit_login.click()

    def after(self):
        # THEN the user should be logged in (name instead of loginbutton visible)
        # AND after logout should be back to the start page

        self.tc.ensure_absent("#login_button")

class CookieLawStep(Step):

    COOKIELAW_OK_BUTTON = "#CookielawBanner .btn-primary"

    def before(self):
        self.cookie_law_button = self.tc.ensure_present(CookieLawStep.COOKIELAW_OK_BUTTON)

    def step(self):
        self.cookie_law_button.click()

    def after(self):
        # self.tc.ensure_absent(CookieLawStep.COOKIELAW_OK_BUTTON)
        pass
