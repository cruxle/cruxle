import time
from unittest import skip

from .utils import MitopoTestCase


class MarkerClickTest(MitopoTestCase):

    @skip("UI Test currently broken")
    def test_preclick(self):

        # uncomment this to add an artifical slow down of X milliseconds to every call
        # of self.ensure_present or self.ensure_absent. You can also put additional slow
        # downs in the code by using self.try_slow_down() at the locations where you want
        # to add waits.
        #
        # The idea is to make it easier for humans to observe the selenium interactions
        # during test development. When you finished developing the test comment it out
        # so the test runs with maximum speed.
        #
        self.slow_down = 1000

        self.browser.get('http://localhost:8000')

        cookie_law_button = self.ensure_present("#CookielawBanner .btn-primary")
        cookie_law_button.click()

        time.sleep(5)

        # wait until ajax load populates map
        self.ensure_present("div.leaflet-marker-icon > img")

        # We do not use the return of the ajax waiter because marker cluster will fiddle with the markers between load and what we actually display...

        # Make sure that no popups are open yet
        self.ensure_absent(".leaflet-popup-content-wrapper")

        # TODO: if there are many marker, we can not click on one and assume that the next one is still visible.... this breaks the test but is a good feature... need to test this somehow differently
        # import ipdb
        # ipdb.set_trace()

        # We get all leaflet markers again from the front page... most probably spots
        markers = self.browser.find_elements_by_css_selector("div.leaflet-marker-icon > img")


        # We then click on up to three markers on which a popup should open
        for m in markers[:3]:
            m.click()
            self.ensure_present(".leaflet-popup-content-wrapper")
            popups = self.browser.find_elements_by_css_selector(".leaflet-popup-content-wrapper")
            # and make sure that there is only one popup open at a time
            self.assertEqual(1, len(popups))

        # Then search for a link in the popup and remember it
        marker_links = self.browser.find_elements_by_css_selector(".leaflet-popup-content a")

        # This test assumes that there is only one link to be found and that it directs us to where we would want to go if we click on it again...
        self.assertGreaterEqual(len(marker_links), 1)
        url = marker_links[0].get_attribute("href")

        # If we click the marker again while the popup is open we should go to the url...
        m.click()

        self.assertEqual(url, self.browser.current_url)


if __name__ == "__main__":
    import unittest

    unittest.main()
