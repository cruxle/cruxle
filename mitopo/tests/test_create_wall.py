import datetime
import os
import random
import time
from unittest import skip

from selenium.webdriver.support.select import Select

from mitopo.tests.steps.steps import *

@skip("UI Test currently broken")
class CreateWallTest(MitopoTestCase):

    def open_burgermenu(self):
        burgermenu_button = self.ensure_present('.mitopo-hamburger')
        burgermenu_button.click()

    def create_new_wall(self):
        self.open_burgermenu()

        add_wall_button = self.ensure_present('#sandwich-dropdown li:nth-of-type(5) a')
        add_wall_button.click()

    def accept_help_tutorial(self):

        help_button = self.ensure_present_xpath('//*[@id="help-modal"]//button')
        help_button.click()

    def check_license(self):
        time.sleep(1)
        help_button = self.ensure_present_xpath('//*[@id="id_licenses"]')
        help_button.click()

    def upload_wall_image(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))

        SRC_IMAGE = os.path.realpath(dir_path + "/../../misc/lost_arrow.jpg")

        print("PATH: " + SRC_IMAGE)

        # make hidden field visible for selenium
        self.browser.execute_script("document.getElementsByName('qqfile')[0].style.opacity=1")

        file_input = self.ensure_present('input[type=file]')
        file_input.send_keys(SRC_IMAGE)

        submit_button = self.ensure_present('#submit-button')
        submit_button.click()

        continue_button = self.ensure_present('#continue-to-edit-wallinfo')
        continue_button.click()

    def fillin_wall_form(self):
        edit_wall_map = self.ensure_present("#edit_wall_map")
        self.click_xy(edit_wall_map, random.randint(10, 20), random.randint(10, 20))

        name_field = self.ensure_present("#id_name")
        name_field.send_keys("Selenium-Test Wall " + str(datetime.datetime.now()).split('.')[0])

        description_field = self.ensure_present("#id_description")
        description_field.send_keys("Nice Wall to climb, really good")

        approach_field = self.ensure_present("#id_approach")
        approach_field.send_keys("Approach is easy")

        submit_button = self.ensure_present('#submit-button')
        submit_button.click()

    def link_routes(self):

        add_routes_button = self.ensure_present('#add-routes-button')
        add_routes_button.click()

        new_route_button = self.ensure_present('.btn.new-route-button')
        new_route_button.click()

        name_field = self.ensure_present('#id_name')
        name_field.send_keys("TestRoute " + str(datetime.datetime.now()).split('.')[0])

        grade_select = Select(self.ensure_present('#id_grade'))

        # select by value
        grade_select.select_by_value('1330')

        submit_button = self.ensure_present('#submit-button')
        submit_button.click()

        submit_button = self.ensure_present('#back-button')
        submit_button.click()

    def add_route_geometry(self):
        self.ensure_present_xpath(".//*[@id='routetable']/tbody/tr//td[2]").click()
        wallmap = self.ensure_present("#wallmap")
        self.click_xy(wallmap, random.randint(10, 20), random.randint(10, 20))
        self.click_xy(wallmap, random.randint(25, 30), random.randint(25, 30))
        self.click_xy(wallmap, random.randint(35, 40), random.randint(35, 40))
        self.ensure_present("#leaflet-polyline-edit-confirm").click()
        self.ensure_present("#next-button").click()

    def decorate_wall(self):
        self.ensure_present("#next-button").click()

    def publish_wall(self):
        self.ensure_present('button#publish-wall').click()

    def test_create_wall(self):
        # uncomment this to add an artifical slow down of X milliseconds to every call
        # of self.ensure_present or self.ensure_absent. You can also put additional slow
        # downs in the code by using self.try_slow_down() at the locations where you want
        # to add waits.
        #
        # The idea is to make it easier for humans to observe the selenium interactions
        # during test development. When you finished developing the test comment it out
        # so the test runs with maximum speed.
        #
        self.slow_down = 100

        # GIVEN is the start page of the application with login and register buttons
        #       present

        print("UI-TEST: Create Wall")

        self.load_page(self.get_topo_url())

        CookieLawStep(self)
        LoginStep(self)
        self.create_new_wall()
        self.accept_help_tutorial()
        self.check_license()
        self.upload_wall_image()
        self.fillin_wall_form()
        self.accept_help_tutorial()
        self.link_routes()
        self.add_route_geometry()
        self.decorate_wall()
        self.publish_wall()


if __name__ == "__main__":
    import unittest

    unittest.main()
