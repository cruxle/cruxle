from mitopo.tests.steps.steps import *
from .utils import MitopoTestCase
from unittest import skip


@skip("Test broken")
class LoginLogoutTest(MitopoTestCase):

    def test_login_logout(self):
        # uncomment this to add an artifical slow down of X milliseconds to every call
        # of self.ensure_present or self.ensure_absent. You can also put additional slow
        # downs in the code by using self.try_slow_down() at the locations where you want
        # to add waits.
        #
        # The idea is to make it easier for humans to observe the selenium interactions
        # during test development. When you finished developing the test comment it out
        # so the test runs with maximum speed.
        #
        # self.slow_down = 1000

        # GIVEN is the start page of the application with login and register buttons
        #       present

        print("UI-TEST: Login and logout")

        self.load_page(self.get_topo_url())

        # WHEN the loginbutton is clicked
        # AND the user credentials are input and submitted
        LoginStep(self)

        # Ensure cookie law banner is presented
        CookieLawStep(self)

        user_menu_button = self.ensure_present("#user-dropdown-menu img.user-menu-button")
        user_menu_button.click()

        span_loginname = self.ensure_present(".user-menu-username")

        self.assertEqual(span_loginname.text, "root")

        logout_button = self.ensure_present("#logout-button")
        logout_button.click()

        self.ensure_present("#login-button")
        self.ensure_absent("#logout-button")


if __name__ == "__main__":
    import unittest

    unittest.main()
