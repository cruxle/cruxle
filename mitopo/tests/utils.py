import logging
import os
import sys
import threading
import time
from datetime import datetime

from django.test import TestCase
from django.conf import settings

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.remote_connection import LOGGER
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

LOGGER.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
LOGGER.addHandler(ch)


def runs_in_dockercontainer():
    return os.path.exists('/.dockerenv')


def choose_webdriver():
    try:

        chrome_options = Options()

        if runs_in_dockercontainer():
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--no-sandbox")
            chrome_options.add_argument("--disable-dev-shm-usage")
        return webdriver.Chrome(chrome_options=chrome_options)
    except Exception as e:
        print(("There was an error creating the Chrome Browser for testing:", str(e)))
        print(
            "Chrome driver may be missing, just downloaded it from: <https://sites.google.com/a/chromium.org/chromedriver/downloads>"
        )


class MitopoTestCase(TestCase):

    def timeout_watcher(self):
        self.loadStatus = True
        print("waiting for pageload started")
        time.sleep(30)
        print("waiting for pageload finished")
        if self.loadStatus is True:
            print("killing browser, as timeout expired")
            self.browser.close()

    # Function to load
    def load_page(self, url):
        try:
            threading.Thread(target=self.timeout_watcher).start()
            self.browser.get(url)
            self.browser.delete_all_cookies()
            self.loadStatus = False
        except:
            print("Could not load page: " + url)
            self.browser.close()

    def setUp(self):
        self.slow_down = None

        self.browser = choose_webdriver()
        self.browser.set_page_load_timeout(30)
        self.browser.set_window_size(1600,900)

    def switch_frame(self, frame_selector):

        self.browser.switch_to.frame(self.browser.find_element_by_css_selector(frame_selector))

    def switch_to_default(self):

        self.browser.switch_to.default_content()

    def tearDown(self):
        self.browser.quit()

    def try_slow_down(self):
        if self.slow_down is not None:
            time.sleep(self.slow_down / 1000.0)

    def get_topo_url(self):
        if runs_in_dockercontainer():
            return "http://web:8000"
        else:
            return "http://localhost:8000"

    def ensure_absent(self, css_selector):
        self.try_slow_down()
        try:
            self.browser.find_element_by_css_selector(css_selector)
            self.fail("css selector '" + css_selector + "' is supposed to be absent, but was found")
        except NoSuchElementException:
            pass

    def click_xy(self, element, x, y):

        webdriver.ActionChains(self.browser).move_to_element(element).move_by_offset(x, y).click().perform()

    def take_screenshot(self):
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        screenshot_dir = os.path.join(settings.TEST_OUTPUT_DIR, 'screenshots')
        os.makedirs(screenshot_dir, exist_ok=True)

        screenshot_path = os.path.join(screenshot_dir, 'screenshot-%s.png' % now)
        print("SAVING SCREENSHOT TO: " + screenshot_path)
        self.browser.save_screenshot(screenshot_path)

    def ensure_present_xpath(self, xpath_selector, wait=10):
        self.try_slow_down()

        try:
            element = WebDriverWait(self.browser, wait).until(
                expected_conditions.visibility_of_element_located((By.XPATH, xpath_selector))
            )

            return element

        except Exception as e:

            print(("Could not find element or multiple elements found for: " + xpath_selector))
            print(e)
            self.take_screenshot()
            self.browser.quit()

    def ensure_present(self, css_selector, wait=10):

        self.try_slow_down()

        try:
            element = WebDriverWait(self.browser, wait).until(
                expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, css_selector))
            )

            return element

        except Exception as e:

            print(("Could not find element or multiple elements found for: " + css_selector))
            print(str(e))
            self.take_screenshot()
            self.browser.quit()
