from django.shortcuts import render
from rest_framework import schemas, response
from rest_framework.decorators import renderer_classes, api_view, permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

@api_view()
@permission_classes([IsAdminUser])
@renderer_classes([SwaggerUIRenderer, OpenAPIRenderer])
def schema_view(request):
    generator = schemas.SchemaGenerator(title='cruxle.org API')
    return response.Response(generator.get_schema(request=request))

def greeter_index(request):
    context = {}

    return render(request, 'mitopo/greeter_index.html', context)
