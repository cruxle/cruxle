{% load miroutes_tags %}
{% url 'miroutes:sitemap' as sitemap_url %}
{% absolute_url sitemap_url as abs_sitemap_url %}

User-agent: *
Disallow: /media/wall_pictures/*/tiles/*.png
Disallow: /static/cookielaw/js/cookielaw.js
Disallow: /edit_spot/*
Disallow: /miroutes/tick_route/*
Sitemap: {{ abs_sitemap_url }}

