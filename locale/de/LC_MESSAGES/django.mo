��    �     �  �  �/      �?    �?  �   �@  ;  �A  *  C  �   -D  Y   E    jE  U  rF  �  �G  j  PI  �  �J  t  KL  �  �S    BW  V   FX  ~   �X  P   Y  �   mY  �   Z  H   �Z    �Z  2   �[  S  \  \   s_  �  �_  &   qd  �   �d  {   ae  X   �e  3   6f  �   jf  �   �f  �   �g  �   h  4   �h  �   i  P   �i  �   �i  9   �j  P   �j     Jk  �   hk  �   Zl      Qm    rm  �   �n  �   ho  �   Sp  �   *q  �   �q  �   �r  *   Ss  (   ~s  %   �s  	   �s     �s  &   �s     t     "t     5t  
   =t  �   Ht     �t  J   �t  ?   Eu  R   �u     �u     �u     v     v  ?   #v     cv     lv     zv     �v     �v     �v     �v     �v  .   �v     w     ,w     8w  	   Ew  
   Ow     Zw     cw     qw     �w  
   �w     �w     �w     �w     �w  A   �w  4   ;x  	   px     zx     �x     �x     �x     �x     �x     y     )y     ?y     Uy     py     y     �y     �y  _   �y  L   "z  	   oz     yz  	   �z     �z  B   �z      �z     {  &   {     ={     F{     T{     c{     k{     �{     �{     �{     �{     �{     �{     �{     �{     �{  @   �{  5    |  3   V|     �|     �|     �|     �|     �|     �|     �|     }     7}     O}     l}     �}  1   �}     �}     �}     �}     ~     )~  F   /~  ,   v~     �~     �~     �~     �~     �~     �~     �~     �~            
   0     ;     K     Y     j  8   �  7   �  :   �  K   /�     {�  +   ��  7        ��  0   �  ?   E�     ��     ��     ��     ��  ,   Ł     �     �      $�  
   E�     P�     X�     h�     y�     ~�     ��     ��     ��     ��  !   ւ     ��     �     �  
   �     (�  �   9�     ݃  	   �  ]   �  >   Q�     ��     ��  
   ��     Ƅ     ̈́     ӄ  '   �  $   �  G   8�     ��     ��     ��     Ʌ     �  0   �     $�     7�     C�     W�     l�  2   |�     ��     ��  
   ʆ     Ն     �     �  
   ��  	   �     �     #�     3�     B�     Q�     i�     ��     ��     ��     ��     ��     ��  $   ؇  4   ��     2�     :�  v   P�     ǈ     ֈ     �  /   �     $�  /   ;�  3   k�  X   ��  Z   ��  .   S�  ;   ��  0   ��     �     �     #�  /   9�     i�  R   |�  
   ϋ     ڋ     ��     �     �  +   ��     &�     <�  M   T�     ��     ��     ��  $   ˌ  #   ��  �   �     ��     ʍ     ֍     ݍ     �     ��  ;   �     =�     S�     Z�  <   b�     ��     ��     Ύ     Ԏ     �     �  y   �     i�  
   p�     {�  	   ��     ��     ��     ��     ��  .   ӏ     �  $   �  :   :�     u�  B   ~�  j   ��  ]   ,�  Z   ��     �     �  7   �     :�     F�     U�     \�     d�     |�     ��     ��     ��     ��     ��  	   ��  :   ˒     �     �  '   &�  $   N�  `   s�  )   ԓ     ��  i   �     q�     ��  <   ��     ɔ     Д     ܔ     ��  +   �  (   0�  Y   Y�     ��     ҕ  
   �     �     ��     �     �  	   �  p   #�  "   ��  
   ��          ʖ     ݖ     �     �     �  
   �     �     "�     .�     @�     I�     W�  +   \�  $   ��  	   ��  	   ��     ��     ʗ     ٗ     ޗ     �     ��     �  ,   �     K�  -   b�  $   ��     ��  3   ј  *   �     0�     B�     S�     f�     |�     ��     ��      ��     י     �     ��  3   �     L�     Q�     g�     w�     ��     ��     ��     ��     Ț     ֚  �   �     k�  "   |�     ��     ��     ��     ��     ԛ     �     �  8   �  6   F�  J   }�     Ȝ  U   ߜ  5   5�  	   k�     u�     ~�     ��     ��  
   ��  .   ��  %   ֝  N   ��     K�     S�     `�     n�     ��     ��     ��     ��     ��  
   ��     ̞     Ӟ     �     �     �     '�     5�  6   N�     ��     ��     ��     ��     ��     ��     ǟ     �     ��     �     -�     M�     h�     ��     ��     ��     Š     Ԡ     ڠ     �     ��     ��     �     %�     C�     P�     \�  <   i�     ��     ��  5   ��  B   �  	   2�     <�     C�     R�     W�     g�     y�  	   ��  	   ��     ��     ��  4   ��     ��  5   	�     ?�     U�     b�  �   t�     ��     �  "   6�  0   Y�  
   ��     ��     ��  *   ��     ۤ     �     �      �  	   �     �  	   )�     3�     A�     X�     n�     z�     ��     ��  c   ��      �     :�     B�  ;   b�     ��     ��     ��     ʦ     ڦ     �     �     ��  �   �     ��     ��  #   ϧ     �     �     0�     N�     m�     ��  #   ��  H   ¨  -   �     9�     S�     j�     n�     u�     ��     ��  q   ��     
�     �     �     :�  H   Q�  6   ��  '   Ѫ  7   ��  >   1�  &   p�  �   ��  N   5�  S   ��      ج  >   ��  N   8�  &   ��      ��  F   ϭ     �     $�  
   -�     8�     T�  
   e�     p�  &   ��     ��     Ů  
   ή  2   ٮ     �  0   �  	   E�     O�     i�      p�     ��  �   ��     i�     u�     ��     ��     ��  4   ��     �  3   �     8�     D�      S�     t�     |�     ��     ��     ��     ��     ��     ϱ     ر  G   �  !   2�  *   T�     �  8   ��     ղ  
   �     �  
   ��     �     �  
   2�     =�     T�  4   l�     ��  7   ��  D   �     *�     8�     I�     [�  4   z�  6   ��  A   �  	   (�  	   2�     <�  z   B�     ��     ˵     �  *   ��  0   "�  �   S�  #   �  W   :�  2   ��     ŷ  l   ͷ     :�     P�  +   U�     ��     ��     ��  6   ��     �  h   �  5   v�  R   ��  ;   ��  *   ;�      f�     ��  
   ��      ��  d   Ѻ     6�  :   S�     ��  7   ��  ?   ߻  8   �     X�     x�     ��     ��  
   ��  %   ��     ܼ     �  
   
�  2   �     H�     b�      ��     ��     ��  $   ߽     �     !�     =�     [�     {�  8   ��  #   Ӿ     ��     ��     �     �     /�     6�     <�     M�  "   \�     �  !   ��  
   ��     ��     ڿ     �     ��     �     �     �  	   '�     1�  	   @�     J�     S�     [�  	   h�     r�     x�  
   �     ��     ��     ��     ��     ��     ��     ��  	   ��     ��     ��     �  :   -�  ;   h�  9   ��  G   ��  a  &�  g  ��  �   ��  i  ��  U  @�  �   ��  J   z�  ]  ��  7  #�  }  [�  W  ��  �  1�  �  ��  �  ��     ��  X   ��  }   �  S   ��  �   ��  �   {�  O   ,�  �   |�  +   U�  H  ��  N   ��  �  �  +   ��  �   �  �   ��  k   ��  ;   ��  �   5�  �   ��  �   ��  �   �  9   ��  d   �  a   y�  
  ��  P   ��  o   7�     ��  �  ��  �   M�     )�    I�  �   Y�  �   +�  �   �  �   ��  �   z�  �   b�  +   7�  (   c�  ,   ��     ��     ��  #   ��     �     
�  	   "�  
   ,�  �   7�     ��  [   �  Y   l�  O   ��  -   �     D�     U�     m�  <   }�     ��     ��  '   ��     ��     �     (�  )   B�     l�  J   ��     ��     ��     ��     �     �     1�     B�     X�     u�     ��     ��  %   ��     ��     ��  8   	�  8   B�     {�     ��  %   ��  $   ��     ��  #     )   8  +   b      �     �  !   �     �              < l   X L   �         	   /    9 :   S    �    � &   �    �    � 
   � 
            +    3    E    ` 	   f    p     	   �    � S   � H   � I   = 	   �    �    �    � !   �    �     &   $    K    e '   �    � 1   �    �    	 !   "    D 	   U b   _ K   �         
   /    :    @ 	   P 
   Z $   e    � %   �    �    � 	   �    � )   � L    K   d I   � T   � $   O	 C   t	 =   �	 %   �	 D   
 $   a
    �
    �
    �
 "   �
 =   �
     '   5 =   ]    �    �    �    �    �    �    � 	               ;    X    k 	   {    �    � �   �    i 	   v w   � \   �    U    s 
   �    �    �    � +   � %   � /   	    9 %   V    |    �    � >   �    �        4    G    f 6   y    � 	   �    �    �    �            #    2    A    V    j    ~    �    �    �    �    �         -   + K   Y    � "   � �   �    X    q    � G   � "   � R   � <   P \   � �   � ?   q M   � N   � +   N +   z #   � 5   � &     x   ' 	   �    �    �    �    � 0   �    �      O   ;    �    �    � %   � 1   � �        �    �    �    �    �    � [       c    |    � 9   �    �    �    �             �   ,    �    �    �    � 
                =    X    v ,   � =   �    � q    ~   x 7   � d   /    �    � B   �    �         	       &    ?    L    a    m    y    ~    � W   �    �    � *     (   B  {   k  7   �     ! �   '!    �! 	   �! K   �!    "    " &   !"    H" 5   Q" -   �" Q   �"    #    "#    ;#    J#    P# 1   ^#    �# 	   �# }   �# !   $    @$    M$    Z$    s$ 	   �$    �$    �$    �$    �$    �$    �$    �$    �$    % 9   % "   E% 
   h% 
   s% 	   ~%    �%    �%    �%    �%    �%    �% A   �%    7&    T& +   r& .   �& $   �& $   �&    '    0'    G'    _'    '    �'    �' -   �'    �'    ( )   1( <   [(    �(    �(    �(    �( "   �(    )    )    )    ')    :) �   K)    �)     �)    �)    *    *    !* '   9*    a*    t* K   {* I   �* G   + %   Y+ 8   + D   �+    �+ 	   ,    ,    #,    3,    :, 8   T, +   �, R   �,    -    "-    8-    O-    h-    ~-    �-    �-    �-    �- 	   �- ,   �- +   �-    .    1.    H.    X. _   n.    �.    �.    �.    �.    /    /     "/    C/    ^/ "   }/ %   �/    �/    �/    �/    0    60    L0    Z0    h0    }0    �0    �0    �0 ,   �0    1    1    -1 -   @1    n1    u1 Y   �1 M   �1    .2    :2    A2 	   Q2    [2     u2    �2    �2    �2    �2    �2 <   �2    (3 ;   >3    z3    �3    �3 �   �3    c4 %   �4 '   �4 4   �4    5    5    "5 ?   35    s5 
   �5 
   �5    �5    �5    �5 
   �5    �5    �5    6    .6    E6    W6 $   w6 f   �6    7    7 )   "7 C   L7    �7 &   �7    �7    �7    �7    �7    �7    8 �   8    �8    �8 +   9    :9    Z9    x9 !   �9    �9    �9 "   �9 c   : G   r: #   �: #   �:    ;    ;    ;     ;    '; �   >; 
   �;    �; #   �; #   �; E   < 7   d< $   �< N   �< *   =    ;= �   Y= G   > Q   _>    �> >   �> N   ? )   ^? '   �? *   �?    �?    �?    �? &   	@    0@    D@ +   Q@ O   }@    �@    �@ 
   �@ T   �@ 	   TA -   ^A    �A    �A    �A '   �A    �A �   B    �B    �B    �B    C    C J   *C    uC 3   �C    �C    �C '   �C    D    D    %D    3D    ?D    LD .   YD    �D    �D R   �D    E 2   !E    TE ^   jE 	   �E    �E    �E    �E    �E    F    %F    3F    GF J   \F 	   �F U   �F L   G    TG 
   bG    mG C   yG U   �G F   H L   ZH 	   �H 	   �H    �H �   �H    EI    UI    sI Z   �I 1   �I �   J +   �J ~   K @   �K    �K s   �K    @L    ZL .   _L    �L    �L    �L ;   �L #   	M w   -M C   �M H   �M 6   2N +   iN !   �N    �N    �N #   �N �   	O (   �O K   �O    P R   (P a   {P C   �P    !Q    ?Q    QQ    kQ    �Q "   �Q    �Q %   �Q    �Q (   R "   +R    NR     nR    �R %   �R    �R !   �R     S "   6S    YS    vS 4   �S $   �S    �S    �S    T    T    2T    ;T    NT    aT +   nT    �T (   �T    �T O  �T �$  �    {�    ��    ��    ��    ��    ��    ��    ��    ��    �    �    �    /�    8�    F�    R�    ^�     y�    ��    ��    ��    �� 	   ��    ��    �� "   �� H   � P   b� K   �� W   ��        �   Z       �       :   $            D      0  7     �               �  �  	  z      �   (  >   �   �   �   �  �    �  �      k  �   @  �  �  �      S  �  �               �       �              �  �  )  �            �      �    �    �      �      S      9   2  �  �  �     +  I          *         �   �  �  �      �        �      e  �              	       �   �  �  �   �      �  U   d      x      /  �      �     -  p  b  �          ;           �  �           �   �  �  g  U  �    �          �   �  �  �          &      }  �  O  �  �   �          b   �  �   x     W  |   �   u   a  �      P      m   @   �       9  �  H  �  6  V      �  =  V   k  �  X  A        �        �  D   �  �             �  %   �   5   :  �        8  �   �         �      t  ?  �  �   B      q          �           �  �          �  �   �  �  G      j      �         `  !  �  �   �        �  �       �   '   r  d   �               �  s  �             �      �          �   Y   �   �  I  �  �       X  R          �  Q   �  �  ^           
    �   a    \   �   &  �      b  �   �                    �  ~   �   �   w   �  ~    4      �  
  �   �  �           �  u  4      1  �  �   �  �   v   y  �  �  
   ,      �   v  7   �       �   E  �  �      �   �          �      �      F       �   c     C          �   �          ~    �  �       �  z           ]   =  �     �  �               �  �  �       R          �  �     �      *      `      �  ?   �      �  `   *      w      P   �          q   �  �   �  Z    �       J   �  T   �  �  w      A  1      Q          �          �  @      �     S   N   h  �       �    �      #  �   �   �   �   �  �  h   �  �   O  ]  �       �  �   n  �  $   K   N  �  -           "  �   &   �      M   �     <  k   �      �      )   e           M  �      !  F      �  �  �      �  �  .   g   �  U  (   �  :  �  N  4   \      �      [          B  �             A   �   Z  Y  �  �  �  x   r  �      G  �   �   ;  '        �  �  1       e         �      V      h  �  �          �                  $       �  n       c  �  �  �   3  �         �  >      �  z      �      ^  �  �  �   �   �  �  }    r   D  L  �  �   �          W  �   {  g            �             {           O   �       �   �  H    +   f    T      �     9  �   t   �   �            �      ;  �  �        I          �      [   �  �  v  �  L   .  |  �           �          �  �  	      �          p  �        �  �       a       o      �  �  �   u          8  !   7  �    �   #   i  �   �  ^  �          E  �  �  -  <   �  �   �   �    �       =   �   �         3  5  l  �         P  �  |  Y  �   �   m  �               �  �  �       �  _  F    �  0      �  5      �   c  ]      R  t  �  �  n        0   %  �  �  �  �  y      �  �       �  T         K  �  y   G  l   �  C  �      �  '      �  �          f   �  �  �   �   �    �  q  s         l      �  �  %  �  \  m  6                   d  �  �  �          �       /  �  �     �     �       �   �   H          �      p             (  �        �   �  _   �  �   B         o      �   �  �   i      >  [      J  �  �           �   _  j   f    �  �  �  �        �  �  Q  +  o          M  ,   "  �           �  �    X   E   �  �  �       .  #  �  �  �  �  �  2   �   K      /       �  �  {      ?    6      �   �  �  �   �   )  i                   �  �      �        �   �   �   �   �      j  �           �           �      s     3   �   �    <      �  �  �          �   �  2       �    J  }   �      �  �      C       "      ,  8   �   �  W   �  �           L     

    <h3>Your cruxle.org account has been cleared!</h3>

    We hope you had a nice time nonetheless?
    <br>
    In any case, we welcome suggestions how to improve the user experience on our page.
    <br>
    Just use the contact form or send a mail to feedback@mitopo.de
   

  <h3>New mail adress for %(name)s on cruxle.org</h3>

  <br>
  Please click on the following link to confirm the new adress for %(name)s on cruxle.org:
  <br>
  <br>
  <b><a href="%(confirm_link)s">Confirm new adress!</a></b>
 

  <h3>Welcome to cruxle.org,</h3>

  the collaborative climbing community project.
  Nice to have you with us!
  <br>
  To activate your account (%(name)s) on cruxle.org please visit the following link within <b>two days</b>:
  <br>
  <br>
  <b><a href="%(reply_link)s">Click here to finish registration!</a></b>
 

  <h3>Your cruxle.org password has been reset.</h3>

  <p>
  <b>New Password:</b> %(new_password)s
  </p>

  <br>
  Please login to your account (%(name)s) on cruxle.org,
  and change it as soon as possible.
  <br>
  <br>
  <b><a href="%(reply_link)s">Click here to update your password!</a></b>
 
                        <span class="mitopo-route"></span> %(routename)s on
                        <a href="%(wall_detail_url)s">
                            %(wallname)s
                        </a>
                         
                      Add and Draw Routes on top of the Wall image.
                     
                      Select the Routes you want to draw on the Wall.
                      <ul>
                        <li>Select existing Routes from a pool.</li>
                        <li>Create Routes.</li>
                      </ul>
                     
                    I like to hike and watch the walls from a safe distance ;).
                    For me, the great thing about cruxle.org is that I can fully contribute my skillset
                    as a software engineer and system administrator and I leave the climbing
                    to my fellow cruxlers.
                     
                    I love to climb moderate but long trad routes.
                    Otherwise I am trying to learn more about the weather and clouds (Meteorologist).
                    One distinct feature I like a lot about the project is the panorama stitcher
                    which allows me to create stunning high resolution images with low-end camera gear.
                     
                    I moved to Berlin and since then I mostly go Bouldering indoors.
                    What I miss most about climbing actual mountains is the opportunity to wear my
                    collection of fancy fashionable helmets.
                    At least I can remember the good ol' days with my pictures from cruxle.org.
                     
                    Your message has been successfully sent.<br>
                    We will get back to you as soon as possible!<br>
                    <br>
                    Many Thanks!
		    <br>
		    <br>
                    <i class="fas fa-circle-notch fa-spin"></i>
                    You will be redirected to the <a href="%(url)s">previous page</a> in 5 seconds.
                     
                <h3>cruxle.org - collaborative climbing topos</h3>
                <br>
                <p>
                cruxle.org is a database for climbing metadata
                and climbing topos presented as overlays on zoomable wall photographs.
                </p>
                <p>
                It is the wikipedia idea applied to topos and climbing information.
                Everything can be edited by everyone - no paywalls.
                You can contribute by uploading new topos or adding routes and
                descriptions to existing ones.
                </p>
                <p>
                The full software stack is opensource and available under the GNU Affero GPL at:
                <a href="https://gitlab.com/cruxle/cruxle" target="_blank">https://gitlab.com/cruxle/cruxle</a>
                Contributions are welcome!
                <p>
                All pictures and routes are created by members of the cruxle.org community
                and get published under the Creative Commons License
                to ensure that the work you put in is free to use for everyone and will stay free in the future.
                <br>
                Join your fellow cruxlers and help to provide the best topo guides to the climbing community!
                </p>
                <p>
                Your cruxle.org team
                <br/>
                Fabi, Al and Jonas
                </p>
                </p>
                <h3>Support Us</h3>
                <p>
                cruxle.org is free of charge, but running our infrastructure isn't.
                If you want to help us out, use the following account:
                <p style="font-family:'Lucida Console', monospace">
                mitopo.UG<br>
                IBAN / BIC:<br>
                DE16 7015 0000 1004 9803 12 / SSKMDEMMXXX
                </p>
                 
                <p>
                The idea for cruxle was born 2015 in the evening after a long climbing tour out of
                frustration about the
                quality of commercial topos.
                </p>
                <p>
                A vision emerged:
                Easy creation and editing functionalities provided to a lively community
                should be the key ingredients for up-to-date topos, decreasing climbers frustration and
                increasing their safety.
                The wikipedia idea applied to topos and climbing information.
                </p>
                <p>
                Developing the project only on our free time, it took us about three years to finally move it online.
                So in 2018, we present <b class="fa-lg">cruxle.org, a platform for sharing climbing topos</b>.
                </p>
                 
                To combine multiple pictures to a panoramic view, try our
                <a href="%(panotool)s?next=%(path)s">
                  <i class="fa fa-columns" aria-hidden="true"></i> Experimental Panorama Tool
                </a>.
               
              Feel free to close this window and look around in the meantime.
	       
              Field: %(fieldname)s current version: `%(current_version)s` target version: `%(target_version)s`
               
              When we are ready, the wall will show up on the main map.
	       
              You can download the PDF for offline use by clicking on the <span class="far fa-file-pdf fa-lg"></span> button below the wall name.
	       
            <h4>You can log in now with the username: %(username)s</h4>
            <h4>The account email address is: %(emailstr)s</h4>
             
            <h4>Your new mail adress is: %(emailstr)s</h4>
             
            Mitopo UG (haftungsbeschränkt)<br/>
            z.H. Fabian Jakub<br/>
            Isartalstr. 12<br/>
            80469 Munich, Germany<br/>
            <br/>
            or via the
            <a href="%(contact_url)s">contact form</a>
         
            Send a Message to %(name)s
           
            You can point other users to routes, walls, users and
        panorama images using simple markup in your private messages and comments:
        To link a
            <ul>
                <li>Route, use #Route&lt;ID&gt;</li>
                <li>Wall, use #Wall&lt;ID&gt;</li>
                <li>User, use #User&lt;ID&gt;</li>
                <li>Panorama, use #Pano&lt;ID&gt;</li>
            </ul>
            The ID of the object can be found when hovering over the name on the page showing the
            details of the object in question, i.e., the wall and route pages or the user profile.
            Alternatively, the ID can be found in the adress bar for these pages. E.g.,
            <code>
              https://cruxle.org/en-us/users/user_detail/11
            </code>
            is the profile page for user #11.
         
          Changes for %(name)s with respect to the version
          from %(date)s
         
        <p>
        We as the climbing community heavily rely on the information of route setters and developers as well as on up to date reports from climbers that repeat ascents.<br>
        The core concept of Cruxle.org is that everyone can help to gather all necessary information about crags and keep everything up to date.<br>
        </p>
        <p>
        As a registered user you may:
        <ul>
            <li>Add new walls</li>
            <li>Create new routes</li>
            <li>Draw routes, markers, etc. on the walls</li>
            <li>Add additional materials, like for example self-drawn topo maps</li>
        </ul>
        </p>
        <p>
        Also, even if your local crag is already published, maybe you could supply addition info, enhance the description, provide pictures of the surroundings or draw detailed topo's...
        <br>
        We would love if this site is of help to you and we hope to welcome you in our effort to map the worlds climbing spots!
        </p>
        <span class='text-success'>P.S. If you find a bug or bad content please let us know! - We would be forever grateful if you could tell us, ... really!</span>
         
        Profile %(username)s
         
        The open-source tool <a href="http://hugin.sourceforge.net/">Hugin</a>
      is used to stitch the images. Better results may be achieved by downloading Hugin and using the tool locally.
     
        You may change or delete your account on your
        <a href="%(profile_url)s">private profile page</a>.
         
      <p class="alert alert-danger">Has already an account( %(has_account)s)</p>
       
      Add / Remove Routes from %(wallname)s
       
      As long as noone else contributed to the wall,
      you may still <a href="%(edit_wall_url)s"> delete or hide the wall.</a>
     
      If you want to add additional graphical material (e.g., handrawn topos),
      <a href="%(link_url)s?next=%(path)s">click here.</a>
     
    A single route can be visible on multiple wall images.
    You need to select which routes are present on this wall, before you can draw them.
     
    Mitopo UG (haftungsbeschränkt)<br/>
    Fabian Jakub<br/>
    Isartalstr. 12<br/>
    80469 Munich, Germany<br/>
    <br/>
    or via the
    <a href="%(contact_url)s">contact form</a>
     
    cruxle.org - Add/Remove Routes to %(wallname)s
 
    the cruxle.org team.


    You are receiving this mail because you (or someone on your behalf) registered on cruxle.org.
     
  - Field: %(fieldname)s old version: `%(old)s` current version: `%(current)s`
 
  <h4>A confirmation mail for user <strong>%(name)s</strong> was sent to
    <strong>%(mail)s</strong>.</h4>
  <h4>Please <strong>click the confirmation link</strong> in the mail to complete registration.</h4>
   
  Congratulations! You are about to publish %(name)s.
   
  Field: %(fieldname)s old version: `%(old)s` current version: `%(current)s`
   
  Routes on %(wallname)s:
   
  Your cruxle.org account has been cleared!

  We hope you had a nice time nonetheless?
  In any case, we welcome suggestions how to improve the user experience on our page.
  Just use the contact form or send a mail to feedback@mitopo.de

 
%(wall_link)s
Congratulations!

`%(wall_name)s` has just been published!
We hope everything worked to your liking and if you have any feedback,
don't hesitate to get in touch with us.

Visit the following url to enjoy your result!
%(wall_link)s
 
- climbing route on cruxle.org
 
<h3>Change notification</h3>
<p>
<a href="%(wall_link)s">%(wall_name)s</a> has just been edited!<br>
You receive this notification because you have contributed to this wall in the past.
</p>

<p>
The corresponding commit message was `%(comment)s`.<br>
The changeset contains:
</p>
 
<h3>Congratulations!</h3>
<p>
<a href="%(wall_link)s">%(wall_name)s</a> has just been published!<br>
We hope everything worked to your liking and if you have any feedback,
don't hesitate to get in touch with us.
</p>
 
Change notification!
%(wall_name)s has just been edited!
%(wall_link)s
You receive this notification because you have contributed to this wall in the past.

The corresponding commit message was `%(comment)s`.
The changeset contains:
 
New Message from %(name)s:

%(message)s

To reply to %(name)s you can use the following link:

%(reply_link)s


If you don't want to receive this kind of messages anymore you can
change this on your profile page:
 
New mail adress for %(name)s on cruxle.org
  
Please click on the following link to confirm the new adress for %(name)s on cruxle.org:

%(confirm_link)s

 
Welcome to cruxle.org,

the collaborative climbing community project.
Nice to have you with us!
To activate your account (%(name)s) on cruxle.org please visit the following link within two days:

%(reply_link)s

 
Your cruxle.org password has been reset,

New Password: %(new_password)s

Please login to your account (%(name)s) on cruxle.org,
and change it as soon as possible.

%(reply_link)s

 
cruxle.org - PDF Export for %(wallname)s
 
cruxle.org - User Profile %(username)s
 (choose from walls within 2km radius) (enabled) (must be checked to proceed) ** This commit did not change anything 1 hour 15 minutes or less 2 hours 30 minutes A Subwall is not visible on maps. It can be accessed only by clickable links on nearby walls. This is intended for small sections of larger walls. A new Climbing Topo for {name} A route has to be linked to this wall before it can be drawn on the image. A selected file is not one the valid image types ({}) or a pdf. A special thanks goes to Adrian Jaeschke for his contributions to the page design. A spot suited for belaying. About cruxle.org Abseil Anchor Account Details Actively invite contributions from the community for this wall. Activity Activity Feed Add Bolts and Link Walls Add File Add Info Material Add Info Material! Add Information on the wall. Add Markers Add Markers for Bolts, Belays or nearby Walls. Add Panorama Image Add Picture Add Picture! Add Route Add Routes Add Wall Add Wall-Info Add a Picture to %(name)s Add a Picture. Add a note Add a pitch detail marker Add a relation to another wall. Add comment Add material. Add or remove routes to this wall using the checkbox in the list. Add or remove routes using the checkbox in the list. Add route Add/Remove Routes Add/Remove Routes to this Wall Added %(count)d markers. Added %(count)d routes. Added %(mtype)s marker. Added markers: %(mtypelist)s. Added material to `{}` Added new wall image. Added route %(name)s. Added routes %(namelist)s. Added {} to {} Additional Gear Additional Info and Exif-Data Additional Materials After publication, the wall is visible to other users on the map and can be edited by everyone. After the next page reload, a thumbnail of the wall image is displayed here. Afternoon Afternoon Sun Aid Grade All kind of bolts. All your contributions will be anonymized. This is not reversible. Allgemeine Geschäftsbedingungen Alpine Climbs Anchor to do abseiling or to top-rope. Approach Approach Time Ascent Details Ascents Ascents Sunburst Chart: Back Back to Map Back to previous page. Belay Bolt Bolt Quality Bolt quality Bolts Boulders By registering you agree to our <br> privacy and copyright terms Can not delete the file. Are you the initial creator? Can not edit the file. Are you the initial creator? Cancel Change Avatar Change Password Change your Email Adress Changed %(count)d markers. Changed %(count)d routes. Changed %(mtype)s marker. Changed markers: %(mtypelist)s. Changed route %(name)s. Changed routes %(namelist)s. Changes to routes and markers: Changes to wall metadata: Changes with respect to the version from %(date)s Changes: Check out the Topo for Checking for pending changes... Children Friendly Clear Click on a route in the list or on the map to edit the route geometry. Click on one of the buttons to place markers Click to edit  Climbing Topo of {name} Close Color Com&shy;ments Comment Comments Compare to current version Confirm Contact Form Submission Contact Us Contact address Contributions Copyright Issues Could not create ascent. Could not link route to {}, route is flagged deprecated! Could not link route to {}, wall is flagged deprecated! Could not link routes to {}, your editing session expired! Could not link the additional material to {}, your editing session expired! Could not revert {}! :: {} Could not save Image. Image Field is empty! Could not save changes to {} due to errors in the form! Could not save new image! Could not save {}, your editing session expired! Could you relax if your kids were running around, unsupervised? Create New Route Create Route Create Wall Create a new comment. Create a new route if it does not yet exist. Create a new route. Create a wall from this Image Create routes that do not exist. Created By Credits Current Version Current location Date Datenschutzerklärung Decorate Wall Delete Delete Account Delete Account (irreversible) Delete this piece of Information? Deleted Ascent. Deleted {}! Deletion Deprecated Deprecated Walls Deprecated walls are not visible on the default map and it is not possible to add new routes. It should be set if a newer/better version of this crag is available. Description Developer Did not revert to old version. Seems that nothing has changed between versions of {wallname}! Difficulty and required gear might vary on traditional climbs. Disable Google Login Discard your changes. Dist. (km) Do It! Do it Do not show this again! Do you have additional material to link Do you second the difficulty rating? Does direct sunlight hit the face of the wall during the morning hours? Download Full PDF Download List of QR Codes Download PDF Version of Wall Download PDF externally Download QR Codes Drag the red marker to change the wall position. Draw & Edit Routes Draw Routes Draw Routes on Wall Draw Selected Routes During the day? EXIF Tag of Image is not consistent with location! Edit %(name)s Edit Details Edit Image Edit Markers Edit Picture Edit Profile Edit Route Edit Wall Edit Wall Details Edit Wall Image Edit Wall Info Edit Wall-Info Edit the route details. Edit the route geometry. Edited Ascent. Editor Notes Edits Edits of %(user)s Email Email (only visible to you) Enable Google Login for this Account Enable or disable login via social network accounts. English Error saving profile! Error saving profile! Currently you cannot change your mail address. Please get in touch with us if you wish to do so. Exit Edit Mode External Source: External URL Failed to create route! Is the wall deprecated? Failed to create wall! Failed to delete wall! Other users contributed. Failed to edit {}. Your edit session maybe expired. Failed to publish `{}`. There seem to be no changes with respect to the current version. Failed to publish `{}`. You have insufficient permissions or your edit session is expired. Failed to publish. `{}` is flagged deprecated. Failed to reset `{}`. Not enough puzzle pieces ({} needed). Failed to reset `{}`. Your edit session expired. Failed to save Material! Failed to save Pano Parts! Failed to save Route! Failed to save changes, commit message invalid. Failed to save {}! Failed to save {}. Your edit session expired or another user is editing the route! Favourites Field Files Filter Filter Walls Filter Walls by selecting a Range of Grades Filters activated ... Filters deactivated ... Finally, publish the wall and make it available to the community for editing. Finish Edit Finish Editing. First Ascent Flag route information as deprecated Follow the menu on top of the page. For best results, we recommend you use pictures which have at least 10-20%% overlap and posses correct EXIF Tags about the Field of View and camera-optics settings. Forgot Password? Found a bug French Full license text Fuzzy Search Gear General Info on the history, rock and culture of the place. Generating thumbnail. German Go Back Go to the `Draw` page and start drawing the selected routes. Go, Tick a Climb! Going back to normal view... Grade Grading System: Graphs Gym Has to be a unique name... If this is a series of views of just one block or a sector, maybe append a number to the name. Height Help / FAQ Help to improve this Topo! Hi there! Hide History History of %(name)s How To Add A Wall How long does it take on average to get there? How to contribute? How to delete my cruxle.org account? How to get to the Wall, e.g. by public transport or by car Humidity I agree to the privacy notice and the terms and conditions of use. If you forgot your password, please fill in your Username and Email adress and we will send you a new one. If you received this mail by mistake or you don't want to receive these kind of mails, please If you want to disable this feature but keep your cruxle account please define a password. Image History Image Licenses Image can not be rotated. {} is currently edited by {}! Image of {} Image rotated! Images Imprint In the meantime you can In the shadow. Inappropriate Content Inbox Inbox. Info Info Material Info Text Inform me when routes or walls change where I am involved. Information Input is not valid! Invalid geojson ({}) or object ID ({}). Is it worth the hike after rainfall? Is the Image from a publicly available webpage, maybe with more detailed info? Add the URL here! Label the pitches of a multi-pitch route. Language Last but not least, we would like to express our special gratitude to the relentless input and testing of Latest Contributions Leave Leave blank to auto-choose the color according to difficulty Length Let's rock. Level/Difficulty of Protection Licenses Link Materials to %(wallname)s%(routename)s Link Walls, Routes and Users in Messages Link a nearby wall in the direction of the arrow. Enables quick navigation between walls. Link this piece of Information Link to Original File Linked to: Loading Location Log in to rate Login Login via Maintenance starts in %(minute)s minutes. We apologize for any inconvenience. We'll be back as soon as possible. Manage visible routes on this wall Map & Info Markers Materials uploaded Member since Message Message Notifications Midday Midday Sun Morning Morning Sun Multi-Pitch Label My Edits My Favourites Name Need at least 2 images to stitch a panorama New Climbing Topo for Route `{name}` New Image New Route New Wall New Wall Image News Next No Data Available No Info Material available. No Messages. No Routes. Click here to create a new Route. No Userpics available. No additional information material available. No additional materials supplied ... No changes to mail options! No changes to routes and markers for this revision. No changes to wall meta for this revision. No changes to {}! No comments yet. No events to show. No information given. No items to display. No pending changes. No results found No routes attached to this wall. No routes on wall. No walls within 2km Not a valid grade system. Not enough puzzle pieces to unlink {} ({} required) Note Notification Settings Number of Bolts Number of Pages Numbering from left to right OK Old Version Or in the afternoon? Other Ascents Other Options Over the years there have been so many people who contributed to the success of this page that it seems impossible to name all of you. Overview of Wall PDF generated at %(generate_date)s Page Panorama Parts Password Password (Confirmation) Password could not be changed! Personal Content Pictures Please <a href="%(href)s">login</a> to create a comment. Please <a href="%(href)s">login</a> to rate the Route. Please choose the licenses under which your contributions will be licensed Please drop us a line! Please wait... we will try our best to stitch your panorama. This may take a while... Point out unexpected issues or dangers. Use sparsely. Posted by Previous Privacy Processing... Profile Protection Provide an image (jpeg, png or tiff) or a pdf. Provide an image (jpeg, png or tiff). Provide as much information on the wall as possible in the form below the map. Publish Publish Wall Publish Wall. Published Version Published Wall. Puzzle Pieces Puzzlepieces QR-Code QR-Labels for Rate Route Rating Really delete this Account? Really delete this picture? Really delete? Really reset Really rotate Really rotate the image? Really unlink this route? The route path will be lost. Really unlink? Really? Reason Received from Register Registration Removed %(count)d markers. Removed %(count)d routes. Removed %(mtype)s marker. Removed `{}` from `{}` Removed markers: %(mtypelist)s. Removed material from `{}` Removed route %(name)s. Removed routes %(namelist)s. Replace Wall Image Replaced wall image. Report Content Reset Reset Filters Reset Password Revert Revert to this version Reverted Image! Reverted the edit view on {}! Reverted {}! Rotate Left Rotate Right Rotating the image invalidates route geometries and markers. Route Route setter Route {} can not be reverted, it is currently edited! Route {} can not be reverted. Not enough puzzle pieces ({} needed) Routelist Routes Routes created Save Saved Material! Saved Pano Parts! Saved Userpic! Saved {}! Saved {}. Search Search for Cities/Mountains Search for the route(s) you want to add to the wall. Search in Cruxle.org Search the route you want to draw on this wall image. Select Grading System Select Image Select View modes Select a (main)wall in the vicinity. Hides the current wall from the default map. It is reachable from the mainwall through link markers. Select a Grading System Select and upload a wall image. Select and upload panorama images. Select the wall position by clicking on the map. Selected:  Send Message Send Message. Send me an email when new messages arrive. Set Wall-Info Show All Show Comments Show Deprecated Show Help Show Materials Show More Show Userpics Show deprecated Routes Show deprecated walls Show on map Show subwalls Show unpublished walls Show walls with trad climbs. Show walls with trad climbs. I.e. routes where you have to take of protection(bolts etc.) yourself. Show walls without routes Sign in Social Network Authentification Somewhere lost the picture. Please tell us it is missing... Source Specific info for Route Sport Climbs Staging Version Stitch Panoramas Style Submit Subwalls Subwalls display <b>sub-sections of regular walls</b>. They are not shown on the global map by default and can be reached through links on other walls. Successfully added Wall Image! Successfully added {}! Successfully changed mail settings! Successfully changed password! Successfully changed profile! Successfully deleted Picture. Successfully deleted the file. Successfully deleted {}! Successfully published. Successfully removed `{}` from `{}` Successfully resetted password! - We sent you an Email with the new one. Successfully resetted {} to its public state! Successfully saved Image! Successfully saved {}! Sun Sunny. Surroundings System Tag as Subwall Tag this wall as part of a climbing-gym. We have a simplified interface to create routes which are changing alot. Temperature Tendency Thank you for registering! Thanks for Publishing! The Images could not be sent to the server, please ask us whats going on The PDF is currently rendered. Please try again later. The account already has been activated. The activation key expired.<br />Please register again. The cruxle community is looking forward to your contributions! The image contains EXIF location data! The information in this topo is not complete or needs considerable review. If you know this place, we would be grateful if you could spare a minute and help! The key for this process expired.<br />Please try to change your adress again. The key for this process is not valid.<br />Please try to change your adress again. The registration was successful. The selected file ({}) is not one of the valid image types: {} There are unsaved changes to route geometries. Are you sure you want to leave? This route is not linked to any walls. This wall is flagged deprecated! This website makes extensive use of the following opensource projects: Tick %(name)s Tick It! Tick Route Tick Route (Login required) Tick `%(name)s`. Ticked {}. Ticklist update failed. Tiling image... This may take a while. Tiling process status Timeline To Profile To publish, draw (or unlink) the following routes: Toolbox Topo of Climbing Route `{name}` on Wall `{wall}` Tradclimb Unable to delete Picture. Unlink Unlink this piece of Information Unpublished Walls Unpublished walls are those that were recently uploaded and are <b>not yet ready</b> for the public. I.e. the creator hasn`t published them yet. Enable this option here to get a sneak preview. 😉 Unticked {} Updated ticklist. Updated {}. Upload Image Upload Multiple Pictures! Upload a wall image by clicking on the button below. Upload a wall image. Upload up to 20 images to form a panoramic picture. Uploaded by Urgent Message Use EXIF Location to Locate Wall Use map User User Profile User Ratings User Search Username Username and Email dont match! Userpics Userpics uploaded Value must be a list of at least 2 2-element lists or {lat, lng} dicts. Value must be a list of length 2. Value must be a list or a {lat, lng} dict. View Panorama Image Stitcher Wait for tiler to finish if you want to rotate the image Wall Height Wall Image Wall Images Wall above Wall above to the left Wall above to the right Wall below Wall below to the left Wall below to the right Wall has to be saved at least once to select a wall. Wall height Wall image can not be reverted, it is currently edited! Wall image can not be reverted. Not enough puzzle pieces ({} needed) Wall location Wall to the left Wall to the right Wall updated! Click to reload! Wall {} can not be reverted, it is currently edited! Wall {} can not be reverted, the comment is not valid! Wall {} can not be reverted. Not enough puzzle pieces ({} needed) Wall-Info Wall-info Walls Walls are usually marked as deprecated if they contain old or erroneous information or if there is a newer/better version. Walls created Walls with trad routes Walls without routes Walls without routes are shown by default. Warnings about safety issues or access problems. We are now fabricating snapshots of the routes and of the wall and create a PDF that can be used as an offline topo. <b>This process can take 10 minutes</b> or more depending on how busy we are. We are working on it, just a sec... We send a mail to your new adress. Click on the link in the mail to confirm the change. We sent a mail to the new adress for confirmation! Weather Welcome to cruxle! Check out contributions by other users on the news page or start creating your own walls! Welcome to cruxle.org Wind With this you anonymize your contributions. Work in Progress Year Developed You are leaving edit mode You are not allowed to publish `{}` without a comment. You can use markdown here. You can watch the progress or come back later. We will send you an email when the stitching is finished. You have to log in again after changing the password. You just submitted the following message to us via the contactform on the website. You loose all your changes to route geometries and markers. You successfully changed your mail adress. You will be forwarded shortly... Your Account has been Deleted Your Edits Your Password has been resetted! Your changes are persistent in edit mode but invisible to the public until you publish your changes. Your contact form submission Your username and password didn't match. Please try again. [cruxle] Message from {} `{}` can not be reverted, it is currently being edited! `{}` can not be reverted. Not enough puzzle pieces ({} needed). `{}` can not be reverted. The commit message is invalid! `{}` is currently being edited! added Image added Info Material and rated it a <b> auto color average height of climbs on this wall commented on %(name)s contact form submission contact us cruxle.org - A global map of climbing destinations cruxle.org - Add New Wall cruxle.org - Add New Wall Image cruxle.org - Change Email Adress cruxle.org - Change Password cruxle.org - Decorate the Wall cruxle.org - Draw Routes on the Wall cruxle.org - Edit Wall Image cruxle.org - Edit Wall Info cruxle.org - Publish the Wall cruxle.org - Topos for everyone cruxle.org - User Registration cruxle.org - a collaborative platform for climbing topos cruxle.org - confirm new mailadress current current version dries quickly dries within days edited grade grade just right grade too high grade too high (climb felt easier) grade too low grade too low (climb felt harder) last edit: licenses.cc-by-nc-sa-4.fulltext licenses.ccl.fulltext link routes more more than 3 hours name nearby places: never dry never gets wet next time on Walls on wall provided by: published rated revert route list route paths show printer friendly version staged target target version ticked views wall meta walls loaded {} flagged as deprecated. {} is currently edited by {}! {} is currently edited by {}! Changes will have no effect. {} is currently edited by {}. Changes will not take effect. {} is currently edited by {}. You can not link materials. {} is currently edited by {}. You will not be able to publish the wall. Project-Id-Version: master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-08-02 15:27+0200
Last-Translator: alois <alodi@directbox.com>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 

    <h3>Dein cruxle.org account wurde gelöscht!</h3>

    Wir hoffen du hattest eine gute Zeit!
    <br>
    Falls du Verbesserungsvorschläge hast wie wir das Nutzererlebnis unserer Webseite verbessern können würden wir uns freuen davon zu hören.
    <br>
    Benutze einfach das Kontaktformular oder schicke uns eine E-Mail an feedback@cruxle.org
     

  <h3>Neue Mailadresse für %(name)s auf cruxle.org</h3>

  <br>
  Klick auf den folgenden Link um die neue Mailadresse von %(name)s zu bestätigen:
  <br>
  <br>
  <b><a href="%(confirm_link)s">Dies ist meine Adresse!</a></b>
 

    <h3>Willkommen bei cruxle.org,</h3>

    der kollaborativen Kletterwebseite.
    Wir freuen uns Dich hier begrüssen zu dürfen!
    <br>
    Um dein Benutzerkonto (%(name)s) zu aktivieren, folge bitte dem untenstehen Link innerhalb der nächsten <b>2 Tage</b>:
    <br>
    <br>
    <b><a href="%(reply_link)s">Click here to finish registration!</a></b>
 

    <h3>Dein cruxle.org Passwort wurde zurückgesetzt.</h3>

  <p>
  <b>Neues Passwort:</b> %(new_password)s
  </p>

  <br>
  Bitte melde dich mit deinem Konto (%(name)s) bei cruxle.org an,
  und ändere dein Passwort so bald wie möglich.
  <br>
  <br>
  <b><a href="%(reply_link)s">Klicke hier um dein Passwort neu zu setzen!</a></b>
b>
 
                        <span class="mitopo-route"></span> %(routename)s auf
                        <a href="%(wall_detail_url)s">
                            %(wallname)s
                        </a>
                         
                Zeichne die Routen auf dem Wandpanorama ein.
             
                      Füge die Routen, die Du einzeichnen willst, zur Wand hinzu.
                      <ul>
                        <li>Wähle die Routen aus bereits vorhandenen Routen in der Nähe aus.</li>
                        <li>Falls Du eine Route nicht findest, erstelle sie einfach.</li>
                      </ul>
                     
                  Ich gehe gerne wandern und schaue mir die Kletterwände aus sicherer Entfernung an ;).
                  Dafür trage ich zu cruxle.org durch meine Erfahrung als Software-Engineer und System-Administrator bei.
                  Das Klettern überlasse ich den anderen Cruxlern.
               
          Ich liebe es moderate aber lange traditionelle Routen zu klettern.
          Ansonsten versuche ich mehr über das Wetter und Wolken herauszufinden (Meterologe).
          Ein Feature was mir hier an diesem Projekt sehr gut gefällt ist unser Panorama Sticher.
          Damit kann ich atemberaubend hochauflösende Bilder mit meiner Low-end Kamera schiessen.
           
          Seit ich nach Berlin gezogen bin gehe ich hauptsächlich Indoor Bouldern.
          Was ich am meisten am draußen klettern vermisse? Wann soll ich jetzt meine Kollektion von modisch extravagenten Helmen tragen?
          Na wenigstens kann ich mich an die guten alten Zeiten mit den Bildern hier von cruxle.org erinnern.
           
                    Deine Nachricht wurde erfolgreich versendet.<br>
                    Wir werden uns schnellstmöglichst bei dir melden!<br>
                    <br>
                    Vielen Dank!
                    <br>                
                    <br>
                    <i class="fas fa-circle-notch fa-spin"></i>
                    Du wirst in 5 Sekunden auf die <a href="%(url)s"
>vorherige Seite</a> weitergeleitet
                     
                <h3>cruxle.org - Topos von Allen für Alle</h3>
                <br>
                <p>
                cruxle.org ist eine Datenbank für Kletterinfos und Topos,
                die Routen über Wandfotografien darstellen.
                </p>
                <p>
                Wir wollen die Wikipediaidee auf Kletterinfos übertragen.
                Jeder kann alles editieren - nichts verschwindet hinter Bezahlschranken.
                Mach mit indem du Wandfotos hochlädst, Routen einträgst, oder existierende
                Topos verbesserst.
                </p>
                <p>
                Die Software hinter cruxle.org is OpenSource und unter der GNU Affero GPL hier verfügbar:
                <a href="https://gitlab.com/cruxle/cruxle" target="_blank">https://gitlab.com/cruxle/cruxle</a> Beiträge sind jederzeit willkommen!
                <p>
                Alle Bilder und Infos sind von Mitgliedern der cruxle.org Community erstellt und
                werden unter der Creative Commons Lizenz veröffentlicht, um sicherzustellen, dass
                Dein Beitrag auch in Zukunft frei zugägnlich sein wird.
                <br>
                Werd auch ein Cruxler und hilf uns dabei, die besten Topos im ganzen Internet zu erstellen.
                </p>
                <p>
                Dein Team von cruxle.org
                <br/>
                Fabi, Al und Jonas
                </p>
                </p>
                <h3>Unterstütze uns</h3>
                <p>
                cruxle.org ist umsonst, leider gilt das nicht für die Infrastruktur.
                Falls Du uns einen Zuschuss zu unseren laufenden Kosten zukommen lassen willst,
                verwende die folgende Bankverbindung.
                <p style="font-family:'Lucida Console', monospace">
                mitopo.UG<br>
                IBAN / BIC:<br>
                DE16 7015 0000 1004 9803 12 / SSKMDEMMXXX
                </p>
                 
<p>
        Die Idee für Cruxle entstand am Abend nach einer langen Klettertour aus Frust über die
        Qualität von kommerziellen Topos.
        </ p>
        <p>
        Nach etwas Wein und Diskussion entstand eine Vision:
        Wir wollen einer motivierten Klettercommunity Werkzeuge in die Hand geben, so dass Topos einfach zu Erstellen und zu Bearbeiten sind.
        Das ist die beste Grundlage für topaktuelle Topos, die Kletterern weniger Frust bereiten und mehr sicherheitsrelevante Infos zur Hand geben.
        Die Wikipedia-Idee - auf Topos und Kletterinformationen angewendet.
        </ p>
        <p>
        Drei Jahre gingen ins Land bis wir schliesslich 2018 cruxle.org präsentieren konnten. 
                Du willst mehrere Bilder in ein Wandpanorama zusammenzufügen? Versuchs mit unserem
                <a href="%(panotool)s?next=%(path)s">
                  <i class="fa fa-columns" aria-hidden="true"></i> Experimentellen Panorama Tool
                </a>.
               
In der Zwischenzeit kannst Du dieses Fenster gerne schliessen und Dich umsehen.
	       
              Feld: %(fieldname)s derzeitige Version: `%(current_version)s` Zielversion: `%(target_version)s`
               
Sobald wir fertig sind, wird die Wand auf der Übersichtskarte auftauchen.
	       
Du kanns das Topo als PDF herunterladen, indem Du auf die
<span class="far fa-file-pdf fa-lg"></span> Schaltfläche unterhalb des
Wandnamens klickst. 
            <h4>Du kannst dich jetzt mit folgendem Benutzernamen anmelden: %(username)s</h4>
            <h4>Die hinterlegte E-Mail Adresse ist: %(emailstr)s</h4>
             
            <h4>Deine neuen Mailadresse lautet: %(emailstr)s</h4>
             
    Mitopo UG (haftungsbeschränkt)<br/>
    z.H. Fabian Jakub<br/>
    Isartalstr. 12<br/>
    80469 München, Deutschland<br/>
    <br/>
    oder auf folgender Seite:
    <a href="%(contact_url)s">Kontakt</a>
     
Nachricht an %(name)s versenden
           
        Du kannst andere Nutzer auf Route, Wände, Nutzer und Wandpanoramen in Nachrichten und Kommentaren hinweisen:
        Verlinke
            <ul>
                <li>eine Route mit #Route&lt;ID&gt;</li>
                <li>eine Wand mit #Wall&lt;ID&gt;</li>
                <li>einen Nutzer mit #User&lt;ID&gt;</li>
                <li>ein Wandpanorama mit #Pano&lt;ID&gt;</li>
            </ul>
            Die ID der Objekte findest Du wenn Du den Mauszeiger auf den jeweiligen Detailseiten
            (Wand oder Routendetails, Nutzerprofile) über den Namen des Objekts plazierst.
            Außerdem findet sich die ID in der Adresszeile auf diesen Seiten:
            <code>
              https://cruxle.org/en-us/users/user_detail/11
            </code>
            ist die Adresse des Nutzerprofils von Nutzer #11.
         
        Änderungen an %(name)s im Vergleich zur Version vom %(date)s.
       
        <p>
        Als Klettercommunity sind wir von verlässlichen Informationen von Routenentwicklern und Wiederholern abhängig.
        Die aktive Teilnahme der Nutzer ist unser wichtigstes Anliegen und wir versuchen, die Funktionen möglichst intuitiv zu gestalten.
        </p>
        <p>
        Registrierte Nutzer können
        <ul>
            <li>Neue Wände und Routen hinzufügen und ändern,</li>
            <li>Routenverläufe, Bohrhaken und Gefahrenstellen auf den Topos markieren,</li>
            <li>zusätzliche Informationen und Bilder, z.B. selbstemalte Topos, zur Verfügung stellen,</li>
            <li>eine Tickliste führen,</li>
            <li>sich mit anderen Kletterern über die Topos und Kletttererfahrungen austauschen.</li>
        </ul>
        </p>
        <p>
        Ist dein Lieblingsgebiet bereits eingetragen, kannst Du die Wand- und Routenbeschreibungen verbessern oder bessere, höher aufgelöste
        Wandpanoramen bereitstellen.
        <br>
        Wir hoffen, cruxle.org verschönert dir dein Klettererlebnis!
        </p>
        <span class='text-success'>P.S. Wir freuen uns besonders über Hinweise zu Fehlern und Unklarheiten auf der Webseite!</span>
         
        Nutzerprofil %(username)s
         
        Das Open-Source Werkzeug <a href="http://hugin.sourceforge.net/">Hugin</a>
        wird zur Erstellung des Panoramas verwendet.
        Für bessere Resultate können die Parameter in einer lokalen Installation angepasst werden.
     
        Du kannst dein Benutzerkonto auf deiner
        <a href="%(profile_url)s">privaten Profilseite</a> verändern oder löschen.
         
       <p class="alert alert-danger">Hat bereits einen Account ( %(has_account)s)</p>
                     
      Routen zu %(wallname)s hinzufügen/entfernen.
       
So lange kein anderer Nutzer die Wand bearbeitet hat kannst Du sie <a href="%(edit_wall_url)s">löschen oder deaktivieren</a>.
Sie ist dann für andere Nutzer nicht sichtbar. 
      Falls du zusätzliches Infomaterial hinzufügen willst (z.B. handgezeichnete Topos).
      <a href="%(link_url)s?next=%(path)s">click here.</a>
      
    Eine Route kann auf mehreren Wandbildern angezeigt werden.
    Wähle die Routen aus, um Sie auf diesem Wandbild einzuzeichnen.
     
    Mitopo UG (haftungsbeschränkt)<br/>
    Fabian Jakub<br/>
    Isartalstr. 12<br/>
    80469 Munich, Germany<br/>
    <br/>
    oder auf folgender Seite:
    <a href="%(contact_url)s">Kontakt</a>
     
    cruxle.org - Routenliste von %(wallname)s editieren
 
Das cruxle.org Team.


Du erhältst diese E-Mail, weil Du dich bei cruxle.org registriert hast.
    
              Feld: %(fieldname)s frühere Version: `%(old)s` derzeitige Version: `%(current)s`
 
  <h4>Eine Bestätigungs-Email für die Registierung von <strong>%(name)s</strong> wurde an 
  <strong>%(mail)s</strong> gesendet.</h4>
  <h4>Bitte folgen Sie dem <strong>Bestätigungslink</strong> in der Email um die Registrierung abzuschließen.</h4>
             
 Herzlichen Glückwunsch! Du bist kurz davor, %(name)s zu veröffentlichen.
    
              Feld: %(fieldname)s frühere Version: `%(old)s` derzeitige Version: `%(current)s`
               
  Routen auf %(wallname)s:
   
    Dein cruxle.org account wurde gelöscht!

    Wir hoffen du hattest eine gute Zeit!
    <br>
    Falls du Verbesserungsvorschläge hast wie wir das Nutzererlebnis unserer Webseite verbessern können würden wir uns freuen davon zu hören.
    <br>
    Benutze einfach das Kontaktformular oder schicke uns eine E-Mail an feedback@cruxle.org
    <br>
    <br>

    Dein cruxle.org Team.
 
%(wall_link)s
Glückwunsch!

`%(wall_name)s` wurde soeben veröffentlicht!
Wir hoffen, dass alles zu Deiner Zufriedenheit geklappt hat und freuen uns über eine Rückmeldung!

Hier geht's zum neuen Topo:
%(wall_link)s
 
- Kletterroute bei cruxle.org
 
<h3>Es gibt Neuigkeiten!</h3>
<p>
<a href="%(wall_link)s">%(wall_name)s</a> wurde eben bearbeitet!<br>
Wir benachrichtigen Dich, weil Du an diesem Topo mitgewirkt hast.
</p>

<p>
Der Änderungskommentar lautet: `%(comment)s`.<br>
Folgende Objekte wurden geändert:
</p>
 
<h3>Glückwunsch!</h3>
<p>
<a href="%(wall_link)s">%(wall_name)s</a> wurde soeben veröffentlicht!<br>
Wir hoffen, dass alles zu Deiner Zufriedenheit geklappt hat und freuen
uns über eine Rückmeldung!
</p>
 
Es gibt Neuigkeiten!

%(wall_name)s wurde eben bearbeitet!
%(wall_link)s
Wir benachrichtigen Dich, weil Du an diesem Topo mitgewirkt hast.

Der Änderungskommentar lautet: `%(comment)s`.
Folgende Objekte wurden geändert:
 
Neue Nachricht von  %(name)s:

%(message)s

Um %(name)s zu antworten, folge diesem Link:

%(reply_link)s


Falls Du solche Nachrichten nicht mehr erhalten möchtest, kanns Du das in Deinen Einstellungen ändern:
 
Neue Mailadresse für %(name)s auf cruxle.org
  
Klicke auf den folgenden Link um die neue Mailadresse von %(name)s zu bestätigen:

%(confirm_link)s

 
Willkommen zu cruxle.org.

dem kollaborativen Kletter Community Projekt.
Schön dass du dabei bist!
Um deinen Account (%(name)s) auf cruxle.org zu aktivieren besuche bitte folgenden Link innerhalb von zwei Tagen:

%(reply_link)s

 
Dein cruxle.org Passwort wurde zurückgesetzt,

Neues Passwort: %(new_password)s

Bitte melde dich mit deinem Konto (%(name)s) bei cruxle.org an,
und ändere dein Passwort so bald wie möglich.

%(reply_link)s

 
cruxle.org - PDF Export für %(wallname)s
 
cruxle.org - Nutzerprofil %(username)s
 (Wähle aus Wänden in einem Radius von 2km) (aktiviert) (muss ausgewählt sein) ** Keine Änderungen zur Vorversion 1 Stunde 15 Minuten oder weniger 2 Stunden 30 Minuten Eine Teil-Wand ist nicht auf der Karte sichtbar. Sie kann über Verweise von Wänden in der Nähe erreicht werden. Eine Teil-Wand stellt Abschnitte von größeren Wänden detaillierter dar. Ein neues Topo für {name} Eine Route muss zunächst zur Wand hinzugefügt werden bevor sie eingezeichnet werden kann. Eine ausgewählte Datei hat ein ungültiges Dateiformat (unterstützt werden: {} und PDF) Ein besonderer Dank gilt Adrian Jaeschke für Seine Beiträge zum Seitenlayout. Eine Stelle die sich für einen Stand eignet. Über cruxle.org Haken oder Abseil-Anker Account Details Aktiv um Mithilfe für dieses Topo aus der Community bitten. Aktivitäten Aktivitäten Bohrhaken hinzufügen, Wände verlinken Datei hinzufügen Infomaterial hinzufügen. Infomaterial hinzufügen. Füge Informationen über die Wand hinzu. Markierungen hinzufügen Füge Haken, Standplätze oder Verbindungen zu benachbarten Wänden hinzu. Füge Panorama Bild hinzu Bild hinzufügen Bild hinzufügen Route hinzufügen Routen hinzufügen Wand hinzufügen Wandinfos hinzufügen Bild zu %(name)s hinzufügen Bild Hinzufügen Notiz hinzufügen Beschrifte eine Seillänge Verweis auf anderere Wand hinzufügen Kommentar hinzufügen Infomaterial hinzufügen. Verlinke die Route(n) indem Du sie in der Liste abhakst. Verlinke die Route(n) indem Du sie in der Liste abhakst. Route hinzufügen Routen hinzufügen/entfernen Routen zur Wand hinzufügen/entfernen %(count)d Markierungen hinzugefügt. %(count)d Routen hinzugefügt.  %(mtype)s Markierung hinzugefügt. Markierungen hinzugefügt: %(mtypelist)s. Zusätzliches Material zu `{}` hinzugefügt Neues Wandpanorama hinzugefügt. Route %(name)s hinzugefügt. Routen %(namelist)s hinzugefügt. {} zu {} hinzugefuegt Zusätzliches Material Zusätliche Infos und EXIF Daten Zusätzliches Informaterial Nach dem Veröffentlichen ist die Wand für andere Nutzer auf der Karte sichtbar und kann bearbeitet werden. Nach dem nächsten Seiten-Reload wird ein Thumbnail der Wand hier angezeigt. Nachmittags Nachmittagssonne Aid Grade Alle Arten von Bohrhaken. Alle Deine Beiträge werden unwiederbringlich anonymisiert General Business Terms Alpine Touren Anker zum Abseilen oder für Top-Rope. Zustieg Dauer des Zustiegs Begehungen Begehungen Sunburst Graph für Begehungen: Zurück Zurück zur Karte Zurück zur vorigen Seite. Stand Bohrhaken Hakenqualität Hakenqualität Bohrhaken Boulder Mit dem einloggen akzeptierst du unsere <br> Datenschutz- und Geschäftsbedingungen Datei konnte nicht gelöscht werden. Wurde diese Datei von dir erstellt? Datei konnte nicht verändert werden. Wurde diese Datei von Dir erstellt? Abbrechen Avatar ändern Passwort ändern Mailadresse ändern %(count)d Markierungen geändert. %(count)d Routen geändert.. %(mtype)s Markierung geändert. Markierungen geändert: %(mtypelist)s. Route %(name)s geändert. Routen %(namelist)s geändert. Änderungen an Routen und Markierungen: Änderungen an den Metadaten: Änderungen im Vergleich zur Version vom %(date)s Änderungen: Schau Dir dieses Topo an Prüfe ausstehende Änderungen... Kinderfreundlich Verwerfen Klicke auf eine Route in der Liste oder auf dem Wandpanorama um die Routengeometrie zu verändern. Benutze die Schaltflächen auf der Karte um neue Markierungen zu erstellen. Route editieren Topo für {name} Schließen Farbe Kommen&shy;tare Kommentar Kommentare Vergleiche mit der aktuellen Version Bestätigen Neue Nachricht übers Kontaktformular Kontakt Kontakt Beiträge Copyright Verletzungen Begehung konnte nicht registriert werden. Konnte Routen nicht mit {} verbinden, die Route wurde als veraltet markiert! Konnte Routen nicht mit {} verbinden, die Wand wurde als veraltet markiert! Konnte Routen nicht mit {} verbinden, die Editier-Sitzung ist abgelaufen! Konnte das Infomaterial nicht mit {} verknüpfen, die Editiersitzung ist abgelaufen! Konnte {} nicht zurücksetzen! :: {} Bild konnte nicht gespeichert werden. Feld für Bilddaten ist leer! Konnte Änderungen an {} nicht speichern: Fehler im Formular! Bild konnte nicht gespeichert werden. Speichern von {} fehlgeschlagen, die Editier-Sitzung ist abgelaufen! Ist es ein sicherer Ort für Kinder? Neue Route Erstellen Route Erstellen Wand Erstellen Füge einen neuen Kommentar hinzu. Falls Du die Route nicht finden kannst, erstelle sie einfach. Erstelle eine neue Route. Erstelle eine Wand mit diesem Panorama. Wenn Du eine Route nicht finden kannst, erstelle sie einfach. Erstellt Von Credits Aktuelle Version Aktuelle Position Datum Privacy Statement Markierungen hinzufügen Entfernen Konto löschen Konto unwiderruflich löschen Diese Information entfernen? Begehung gelöscht `{}` gelöscht! Entfernen Veraltet Veraltete Wände Veraltete Wände werden nicht auf der Hauptkarte angezeigt und es können keine Routen hinzugefügt werden. Eine Wand gilt als veraltet wenn eine neue oder bessere Version dieses Gebiets vorhanden ist. Beschreibung Schrauber Die Wand `{wallname}` wurde nicht zurückgesetzt. Anscheinend wurde nichts verändert im Vergleich zur letzten Version. Schwierigkeitsstufe und benötigte Ausrüstung kann bei klassischen Kletterrouten variieren. Google Anmeldung deaktivieren Änderungen verwerfen. Dist. (km) Ja! Mach das Nicht mehr anzeigen! Hast du zusätzliches Infomaterial zur Wand Stimmst Du dem Schwierigkeitsgrad zu? Scheint die Sonne in den frühen Morgenstunden? Komplettes PDF herunterladen Lade eine Liste der QR Codes herunter PDF herunterladen PDF herunterladen QR Codes herunterladen Verschiebe den roten Marker um die Wandposition zu verändern. Routen einzeichnen & editieren Routen einzeichnen & editieren Routen einzeichnen Routen einzeichnen & editieren Während des Tages EXIF Tag des Bildes stimmt nicht mit dem Ort überein! %(name)s editieren Editieren Bild ändern Markierungen verändern Bilddetails ändern Profil ändern Route ändern Wand Editieren Wand Editieren Wandpanorama ändern Wandinfos Editieren Wandinfos Editieren Verändere die Routeninfo. Verändere die Routengeometrie. Begehung editiert. Entwicklerkommentar Änderungen Änderungen von %(user)s E-Mail Email (nur für Dich sichtbar) Google Anmeldung für dieses Konto aktivieren Aktiviere oder deaktiviere die Anmeldung mit Konten von Sozialen Netzwerken Englisch Fehler beim Speichern des Profils! Speichern fehlgeschlagen! Im Moment kannst Du Deine Mailadresse hier nicht ändern. Bitte schreib uns eine Mail mit der neuen Adresse. Editierbereich verlassen Externe Adresse: Externe Adresse Speichern der Route fehlgeschlagen! Ist die Wand als veraltet markiert? Speichern der Wand fehlgeschlagen! Löschen der Wand fehlgeschlagen! Andere Nutzer haben die Wand bereits bearbeitet. Editieren von {} fehlgeschlagen. Editier-Sitzung abgelaufen? Veröffentlichung von `{}` fehlgeschlagen. Keine Änderungen zur aktuellen Version gefunden. Veröffentlichung von `{}` fehlgeschlagen. Dein Benutzer hat nicht die nötige Berechtigung oder deine Editier-Sitzung ist abgelaufen. Publizieren ist fehlgeschlagen. `{}` ist als veraltet markiert. Wiederherstellung von `{}` fehlgeschlagen. Nicht genug Puzzleteile (min. {}). Wiederherstellung von `{}` fehlgeschlagen. Die Editier-Sitzung ist abgelaufen. Speichern des Infomaterials fehlgeschlagen! Speichern der Panoramateile fehlgeschlagen! Speichern der Route fehlgeschlagen! Speichern abgebrochen, Änderungskommentar ungültig. Speichern von `{}` ist fehlgeschlagen! Speichern von {} fehlgeschlagen. Deine Editier-Session ist abgelaufen oder ein anderer Nutzer editiert gerade die Route! Favoriten Feld Dateien Filter Wände Filtern Zeige nur einen bestimmten Schwierigkeitsbereich Filter wurden aktiviert ... Filter wurden zurückgesetzt ... Veröffentliche die Wand. Öffentliche Wände sind für alle Nutzer editierbar. Editieren abschließen Editieren abschließen. Erst-Begehung Markiere die Routeninfos als veraltet Nutze das Menu oben auf der Seite zur Navigation. Für gute Resultate sollten die Bilder zu 10-20%% überlappen und korrekte EXIF Informationen über die Kameraeinstellungen enthalten. Passwort vergessen? Fehler melden Französisch Kompletter Lizenztext Volltext Suche Ausrüstung Allgemeine Infos zum Fels, geschichtliche Einordnung oder Details zum Umfeld dieses Gebiets Erzeuge Miniaturversion. Deutsch Zurück Auf der nächsten Seite kannst Du die Routen einzeichnen. Hak gleich eine Route ab! Zurück zur Normalansicht... Grad Schwierigkeitsskala: Graphen Kletterhalle Der Name muss eindeutig sein. Wenn es sich um mehrere Fotos von einem Felsen oder um einen Sektor handelt, kannst Du einfach eine Nummer hinten anhängen Höhe Hilfe / FAQ Hilf bei diesem Topo mit! Hurra die Gams! Ausblenden Versionsgeschichte Versionsverlauf von %(name)s Eine neue Wand hinzufügen Wie lange dauert der Zustieg? Wie kann ich mitmachen? Wie kann ich mein cruxle.org Konto löschen? Infos zur Anreise, per öffentlichem Verkehr oder mit dem PKW Feuchtigkeit Ich habe die Datenschutzerklärung und die Geschäftsbedingungen gelesen und bin mit beiden Texten einverstanden. Falls du dein Passwort vergessen hast, gib einfach Username und Email-Adresse an um ein neues Passwort zugesendet zu bekommen. Falls Sie diese Mail irrtümlicherweise erhalten, bitte Wenn Du diese Funktion deaktvieren aber dein cruxle-Konto behalten willst, setze bitte ein Passwort. Versionsverlauf Bild Lizenzen Bild kann nicht gedreht werden. {} wird im Moment von {} editiert! Bild von {} Bild wurde gedreht! Bilder Impressum Währenddessen kannst du Im Schatten. Unzulässiger Inhalt Posteingang Nachrichten Info Infomaterial Infotext Gib mir Bescheid, wenn sich an Wänden etwas ändert, die ich schonmal bearbeitet habe. Informationen Eingabe ist nicht zulässig! Ungültiges JSON ({}) oder object ID ({}). Rentiert sich ein Besuch nach Regenfall? Ist das Bild von einer anderen öffentlichen Seite? Vielleicht mit mehr/weiterführenden Details? Füge die URL hier hinzu! Beschrifte die Seillängen einer Mehrseillängen-Route. Sprache Trotzdem wollen wir den folgenden Freunden für ihr ständiges Feedback und ausgiebiges Testing unseren besonderen Dank ausprechen Neueste Beiträge Verlassen Lasse das Feld leer für eine automatische Farbwahl nach Schwierigkeitsgrad Routenlänge   Schwierigkeitsgrad bei der Absicherung Lizenzen Infomaterial zu %(wallname)s%(routename)s hinzufügen Wände, Routen und Nutzer in Texten verlinken Verknüpfe eine Wand in Pfeilrichtung. Für schnelle Navigation zwischen Wänden. Verlinke diese Information Verweis auf das Original Verbunden mit: Lädt Wand Position Bitte melden Sie sich an um Bewertungen abzugeben Login Login via In %(minute)s Minuten müssen wir uns kurz um den Server kümmern. Bitte entschuldige die Umstände. Wir sind gleich zurück. Routen auf dieser Wand bearbeiten Karte & Info Markierungen Infomaterial hochgeladen Mitglied seit Nachricht Benachrichtigungen Mittags Mittagssonne Morgens Morgensonne Mehrseillängen Beschriftung Meine Änderungen Meine Favoriten Name Es werden mindestens 2 Fotos für ein Panorame benötigt. Neues Topo für die Route `{name}` Neues Bild Neue Route Neue Wand Neues Wandpanorama Neuigkeiten Weiter Keine Daten verfügbar Kein Infomaterial verfügbar. Keine Nachrichten Keine Routen vorhanden. Hier kannst Du eine neue Route erstellen. Keine Nutzerbilder vorhanden Kein Infomaterial verfügbar. Kein Zusätzliches Informaterial verfügbar Keine Änderungen an den E-Mail Einstellungen. Keine Änderungen in dieser Version. Keine Änderungen in dieser Version. Keine Änderungen an {}! Noch keine Kommentare. Keine Events vorhanden. Keine Informationen verfügbar. Nichts anzuzeigen. Keine ausstehenden Änderungen. Keine Ergebnisse gefunden Mit dieser Wand sind keine Routen verknüpft. Keine Routen vorhanden Keine Wände im Umkreis von 2km Kein gültiges Schwierigkeitsgrad-System. Nicht genugend Puzzleteile um {} zu entfernen ({} benötigt) Notiz Benachrichtigungseinstellungen Anzahl der Haken Anzahl der Seiten Nummerierung von links nach rechts OK Alte Version Oder am Nachmittag? Weitere Begehungen Weitere Optionen Über all die Jahre haben so viele Leute zu dem Erfolg dieser Seite beigetragen, dass es unmöglich ist alle namentlich zu nennen. Wandübersicht PDF erzeugt am %(generate_date)s Seite Panorama Teilbilder Passwort Passwort (Bestätigung) Passwort konnte nicht geändert werden! Persönliche Daten Bilder Bitte <a href="%(href)s">melden Sie sich an</a> um Kommentare zu verfassen. Bitte <a href="%(href)s">melden Sie sich an</a> um die Route zu bewerten. Bitte wähle die Lizenzen unter denen deine Beiträge lizensiert werden Bitte hinterlasse uns eine Nachricht! Bitte einen Moment warten. Das Panorama wird erstellt... Unerwartete Probleme oder Gefahren. Sollte sparsam verwendet werden. Hochgeladen von Vorherige Datenschutz Beschäftigt... Profil Qualität der Absicherung Füge ein Bild (jpeg, png oder tiff) oder ein PDF hinzu. Füge ein Bild (jpeg, png oder tiff) hinzu. Trage soviele Informationen wie möglich in die Eingabefelder unter der Karte ein. Wand veröffentlichen Wand veröffentlichen Wand veröffentlichen. Veröffentlichte Version Wand veröffentlicht. Puzzleteile Puzzleteile QR-Code QR-Codes für Route bewerten Bewertung Möchten Sie dieses Konto wirklich löschen? Möchten Sie dieses Bild wirklich löschen? Wirklich löschen? Wirklich zurücksetzen Wirklich drehen Bild wirklich drehen? Möchtest du diese Route wirklich von der Wand trennen? Der Routen-Pfad wird dadurch gelöscht. Wirklich löschen? Wirklich löschen? Grund Erhalten von Registrieren Registrierung %(count)d Markierungen entfernt. %(count)d Routen entfernt. %(mtype)s Markierung entfernt. `{}` erfoglreich von `{}` entfernt Markierungen entfernt: %(mtypelist)s. Infomaterial von `{}` entfernt Route %(name)s entfernt. Routen  %(namelist)s entfernt. Wandpanorama austauschen Wandpanorama ersetzt. Fehler melden Zurücksetzen Filter zurücksetzen Passwort zurücksetzen Wiederherstellen Diese Version wiederherstellen `{}` zurückgesetzt! Editier-Ansicht auf {} wurde zurückgesetzt! `{}` zurückgesetzt! Nach links drehen Nach rechts drehen Routen und Markierungen werden nicht gedreht! Routen Routen-Schrauber Route {} kann nicht wiederhergestellt werden, sie wird gerade von einem Nutzer geändert! Route {} kann nicht zurückgesetzt werden. Nicht genug Puzzleteile (min. {}). Routenliste Routen Routen erstellt Speichern Infomaterial gespeichert! Panorama Teilbilder gespeichert! Bild gespeichert! `{}` gespeichert! {} gespeichert! Suche Suche nach Städten/Bergen Suche nach den Routen die du zur Wand hinzufügen möchtest. Durchsuche Cruxle.org Suche nach der Route die du zur Wand hinzufügen möchtest. Schwierigkeitskala auswählen Bild auswählen Anzeige-Modus auswählen Wähle eine (Haupt)Wand in der Nähe aus. Diese Wand wird dann nicht mehr in der Standard Karte angezeigt und sollte durch Verweis Markierungen verbunden werden. Schwierigkeitskala auswählen Wanpanorama auswählen und hochladen. Auswahl und Upload eines Wandpanoramas. Lokalisiere die Wand indem Du auf die Karte klickst! Ausgewählt: Nachricht senden Nachricht Senden Ich möchte eine Mail erhalten, wenn neue Nachrichten ankommen. Setze Wandinfos Zeige alle Kommentare Zeige auch veraltete Routen Hilfe anzeigen Infomaterial anzeigen Zeige mehr Nutzerbilder anzeigen Zeige auch veraltete Routen Zeige veraltete Wände Auf der Karte anzeigen Zeige Teil-Wände Zeige unveröffentlichte Wände Zeige Wände mit klassischen Routen. Zeige Wände mit klassischen Routen. Also Routen in denen man selbst für die Absicherung sorgen muss. Zeige Wände ohne Routen Login Authentifizierung über soziale Netzwerke Dieses Bild ging verloren. Bitte kontaktiere das cruxle.org-Team... Quelle Genauere Informationen zu dieser Route Sportrouten Editierte Version Panorama Zusammenfügen Stil Absenden Teil-Wände Teil-Wände sind detaillierte Ansichten kleinerer Wandabschnitte. Teil-Wände werden nicht auf den Karten angezeigt und können durch Verbindungs-Markierungen von anderen Wänden erreicht werden. Bild erfolgreich hinzugefügt! {} erfolgreich hinzugefügt! E-Mail Einstellungen erfolgreich geändert! Passwort erfolgreich geändert! Profil erfolgreich geändert! Bild wurde gelöscht Datei wurde erfolgreich gelöscht {} wurde erfolgreich gelöscht! Erfolgreich veröffentlicht `{}` erfoglreich von `{}` entfernt Passwort erfolgreich zurückgesetzt! - Wir haben Dir eine Email mit einem neuen Passwort geschickt. `{}` wurde erfolgreich in seinen öffentlichen Zustand zurückversetzt! Bild wurde erfolgreich gespeichert! `{}` wurde erfolgreich gespeichert! Sonnenschein Sonnig. Umgebung System Markiere als Teil-Wand Markiere diese Wand als Kletterhalle. Der Editier-Bereich für Kletterhallen bietet ein vereinfachtes Werkzeug zur Routenerstellung. Temperatur Tendenz Vielen Dank für die Registrierung! Vielen Dank fürs Veröffentlichen! Die Fotos konnten nicht hochgeladen werden. Bitte versuche es erneut! Das PDF wird gerade erstellt. Ein bischen Geduld bitte. Der Account wurde bereits aktiviert. Der Aktivierungsschlüssel ist abgelaufen.<br />Bitte registriere dich erneut. Cruxle lebt von den Beiträgen der Nutzer! Das Bild enthält EXIF Daten. Dieses Topo ist unvollständig oder sollte dringend überprüft werden. Falls Du Dich auskennst wären wir sehr dankbar wenn Du Dir eine Minute Zeit nehmen könntest und mithelfen würdest! Der Aktivierungsschlüssel ist abgelaufen.<br />Bitte versuchs nochmal. Der Schlüssel ist leider nicht gültig.<br />Bitte ändere deine Adresse erneut! Registrierung war erfolgreich. Die ausgewählte Datei entspricht keinem gültigem Format ({}) Wenn Du die Seite verlässt, gehen deine Änderungen verloren. Bist Du sicher? Diese Route ist mit keiner Wand verbunden Diese Wand wurde als veraltet markiert! Wir benutzen folgende Opensource Projekte: %(name)s abhaken Route abhaken! Route abhaken Route abhaken (Anmeldung erforderlich) `%(name)s` abhaken. {} abgehakt. Aktualisierung der Ticklist fehlgeschlagen. Generiere Bild-Kacheln für Panorama-Zoom ... dieser Vorgang kann etwas dauern. Status der Kachel-Generierung Timeline Zum Profil Um die Wand zu veröffentlichen, entferne die folgenden Routen oder zeichne sie ein: Werkzeuge Topo für die Route `{name}` on Wall `{wall}` Klassische Route Konnte Bild nicht löschen. Trennen Löse Verlinkung für diese Information Unveröffentlichte Wände Unveröffentlichte Wände sind noch nicht fertig eingetragen.
Der Autor hat Sie noch nicht veröffentlicht.
Du kannst Sie auf der Karte anzeigen und einen Blick hinter die Kulissen werfen. 😉 {} abgewählt Routen-Logbuch aktualisiert. {} aktualisiert. Bild hochladen Mehrere Bilder hochladen! Lade ein Wandpanorama hoch indem Du auf den entsprechenden Button klickst. Wähle ein Wandpanorama aus. Lade bis zu 20 Teilbilder eines Wandpanoramas hoch. Erstellt von Wichtige Nachricht EXIF Daten zur Ortsbestimmung verwenden Karte verwenden Nutzer Nutzer Profil Bewertungen Nutzer Suche Benutzername Benutzername und Email stimmen nicht überein! Nutzerbilder Nutzerbilder hochgeladen Erwarte eine Liste mit mindestens 2 2-Element-Listen oder {lat, lng} Dictionaries. Erwarte eine Liste der Länge 2 Erwarte eine Liste oder ein {lat, lng} Dictionary. Zeige Panoramwerkzeug Die Erzeugung der Kacheln muss abgeschlossen sein, bevor das Wandpanorama rotiert werden kann. Wandhöhe Wandpanorama Wandpanoramen Wand darüber Wand links darüber Wand rechts darüber Wand darunter Wand links darunter Wand rechts darunter Wand muss gespeichert werden bevor eine Unterwand ausgewählt werden kann. Wandhöhe Wand {} kann nicht zurückgesetzt werden, sie wird gerade von einem Nutzer geändert! Wand {} kann nicht zurückgesetzt werden. Nicht genug Puzzleteile (min. {}). Wand Position Wand links Wand rechts Wand wurde gerade geändert! Klicke hier um die Seite neu zu laden! Wand {} kann nicht zurückgesetzt werden, sie wird gerade von einem Nutzer geändert! Wand {} kann nicht zurückgesetzt werden. Der Kommentar ist ungültig! Wand {} kann nicht zurückgesetzt werden. Nicht genug Puzzleteile (min. {}). Wandinfos Wandinfos Wände Wände werden normalerweise als veraltet markiert wenn sie falsche Infos beinhalten oder wenn es eine neuere/bessere Version gibt. Wände erstellt Wände mit klassischen Routen Wände ohne Routen Wände ohne Routen werden normalerweise angezeigt. Entferne den Haken um sie zu verbergen. Wichtige Infos, Warnungen, Zugangsbeschränkungen Wir erzeugen Bildausschnitte mit Routen, eine Wandübersicht und ein PDF,
dass als Offline-Topo verwendet werden kann. <b>Das kann mehr als 10 Minuten dauern</b>,
je nachdem wieviel gerade los ist. Wir sind dran, gib uns einen Moment Zeit... Wir schicken eine Bestätigungsmail an die neue Adresse. Klicke auf den Link in der Nachricht um die Änderung durchzuführen. Wir haben eine Bestätigungsmail an die neue Adresse verschickt. Wetter Willkommen bei cruxle! Schau Dir Beiträge anderer Nutzer auf der News-Seite an
oder erstelle gleich eigene Wände! Willkommen bei cruxle.org Wind Hiermit anonymisieren wir alle Deine Beiträge In Bearbeitung Erstellungsjahr Du verlässt den Editierbereich Veröffentlichung von `{}` fehlgeschlagen. Kommentar fehlt. Hier kann Markdown verwendet werden Du kannst den Fortschritt verfolgen oder die Seite verlassen. Wir verschicken eine E-Mail wenn das Panorama fertig ist. Du musst Dich neu anmelden nachdem Du dein Passwort geändert hast. Die folgende Nachricht wurde von Dir über das Kontaktformular versandt. Änderungen an Routen und Markierungen gehen verloren! E-Mail Einstellungen erfolgreich geändert! Du wirst gleich weitergeleitet... Dein Konto wurde gelöscht. Deine Änderungen Dein Passwort wurde zurückgesetzt. Deine Änderungen werden im Editiermodus gespeichert, sind jedoch im öffentlichen Bereich nicht sichtbar bis du deine Änderungen publizierst. Ihre Nachricht über das Kontaktformular Benutzername und Passwort stimmen nicht überein. Bitte versuche es erneut. [cruxle] Nachricht von {} `{}` kann nicht zurückgesetzt werden, sie wird gerade von einem Nutzer geändert! `{}` kann nicht zurückgesetzt werden. Nicht genügend beigetragene Puzzlestücke ({} benötigt). `{}` kann nicht zurückgesetzt werden. Der Kommentar ist ungültig. `{}` wird im Moment editiert! Bild hinzugefügt Infomaterial hinzugefügt und bewertete sie mit <b> automatische Farbwahl Durchschnittliche Höhe der Routen kommentierte %(name)s Neue Nachricht übers Kontaktformular Kontakt cruxle.org - Weltkarte des Klettersports cruxle.org - Neue Wand hinzufügen cruxle.org - Neues Wandpanorama cruxle.org - Mailadresse ändern cruxle.org - Passwort ändern cruxle.org - Markierungen hinzufügen cruxle.org - Routen einzeichnen cruxle.org - Wandpanorama ändern cruxle.org - Wandinfos Editieren cruxle.org - Wand veröffentlichen cruxle.org - Topos für alle cruxle.org - Registrierung cruxle.org - eine Mitmachplattform für Klettertopos cruxle.org - Mailadresse bestätigen Aktuell aktuelle Version trocknet schnell trocknet innerhalb von Tagen editiert Schwierigkeitsgrad Grad genau richtig Grad zu hoch Grad zu hoch (hat sich leichter angefühlt) Grad zu niedrig Grad zu niedrig (schwerer als angegeben) letzte Änderung: <h3>Creative Commons Namensnennung-Nicht kommerziell-Share Alike 4.0 International Public License</h3><p>Durch die Ausübung der lizenzierten Rechte (wie unten definiert) erklären Sie sich rechtsverbindlich mit den Bedingungen dieser Creative Commons Namensnennung – Nicht kommerziell – Share Alike 4.0 International Public License (“Public License”) einverstanden. Soweit die vorliegende Public License als Lizenzvertrag anzusehen ist, gewährt Ihnen der Lizenzgeber die in der Public License genannten lizenzierten Rechte im Gegenzug dafür, dass Sie die Lizenzbedingungen akzeptieren, und gewährt Ihnen die entsprechenden Rechte in Hinblick auf Vorteile, die der Lizenzgeber durch das Verfügbarmachen des lizenzierten Materials unter diesen Bedingungen hat.</p><p id="s1"><strong>Abschnitt 1 – Definitionen</strong></p><ol type="a"><li id="s1a"><strong>Abgewandeltes Material</strong> bezeichnet Material, welches durch Urheberrechte oder ähnliche Rechte geschützt ist und vom lizenzierten Material abgeleitet ist oder darauf aufbaut und in welchem das lizenzierte Material übersetzt, verändert, umarrangiert, umgestaltet oder anderweitig modifiziert in einer Weise enthalten ist, die aufgrund des Urheberrechts oder ähnlicher Rechte des Lizenzgebers eine Zustimmung erfordert. Im Sinne der vorliegenden Public License entsteht immer abgewandeltes Material, wenn das lizenzierte Material ein Musikwerk, eine Darbietung oder eine Tonaufnahme ist und zur Vertonung von Bewegtbildern verwendet wird.</li><li id="s1b"><strong>Abwandlungslizenz</strong> bezeichnet die Lizenz, die Sie in Bezug auf Ihr Urheberrecht oder ähnliche Rechte an Ihren Beiträgen zum abgewandelten Material in Übereinstimmng mit den Bedingungen der vorliegenden Public License erteilen.</li><li id="s1c"><strong>BY-NC-SA-kompatible Lizenz</strong> bezeichnet eine unter <a href="//creativecommons.org/compatiblelicenses"> creativecommons.org/compatiblelicenses</a> genannte Lizenz, die Creative Commons als der vorliegenden Public License im Wesentlichen gleichwertig anerkannt hat.</li><li id="s1d"><strong>Urheberrecht und ähnliche Rechte</strong> bezeichnet das Urheberrecht und/oder ähnliche, dem Urheberrecht eng verwandte Rechte, einschließlich insbesondere des Rechts des ausübenden Künstlers, des Rechts zur Sendung, zur Tonaufnahme und des Sui-generis-Datenbankrechts, unabhängig davon, wie diese Rechte genannt oder kategorisiert werden. Im Sinne der vorliegenden Public License werden die in Abschnitt <a href="#s2b">2(b)(1)-(2)</a> aufgeführten Rechte nicht als Urheberrecht und ähnliche Rechte angesehen.</li><li id="s1e"><strong>Wirksame technische Schutzmaßnahmen</strong> bezeichnet solche Maßnahmen, die gemäß gesetzlichen Regelungen auf der Basis des Artikels 11 des WIPO Copyright Treaty vom 20. Dezember 1996 und/oder ähnlicher internationaler Vereinbarungen ohne entsprechende Erlaubnis nicht umgangen werden dürfen.</li><li id="s1f"><strong>Ausnahmen und Beschränkungen</strong> bezeichnet Fair Use, Fair Dealing und/oder jegliche andere Ausnahme oder Beschränkung des Urheberrechts oder ähnlicher Rechte, die auf Ihre Nutzung des lizenzierten Materials Anwendung findet.</li><li id="s1g"><strong>Lizenzelemente</strong> bezeichnet die Lizenzeigenschaften, die in der Bezeichnung einer Creative Commons Public License aufgeführt werden. Die Lizenzelemente der vorliegenden Public License sind Namensnennung, Nicht kommerziell und Share Alike.</li><li id="s1h"><strong>Lizenziertes Material</strong> bezeichnet das Werk der Literatur oder Kunst, die Datenbank oder das sonstige Material, welches der Lizenzgeber unter die vorliegende Public License gestellt hat.</li><li id="s1i"><strong>Lizenzierte Rechte</strong> bezeichnet die Ihnen unter den Bedingungen der vorliegenden Public License gewährten Rechte, welche auf solche Urheberrechte und ähnlichen Rechte beschränkt sind, die Ihre Nutzung des lizenzierten Materials betreffen und die der Lizenzgeber zu lizenzieren berechtigt ist.</li><li id="s1j"><strong>Lizenzgeber</strong> bezeichnet die natürliche(n) oder juristische(n) Person(en), die unter der vorliegenden Public License Rechte gewährt (oder gewähren).</li><li id="s1k"><strong>Nicht kommerziell</strong> meint nicht vorrangig auf einen geschäftlichen Vorteil oder eine geldwerte Vergütung gerichtet. Der Austausch von lizenziertem Material gegen anderes unter Urheberrecht oder ähnlichen Rechten geschütztes Material durch digitales File-Sharing oder ähnliche Mittel ist nicht kommerziell im Sinne der vorliegenden Public License, sofern in Verbindung damit keine geldwerte Vergütung erfolgt.</li><li id="s1l"><strong>Weitergabe</strong> meint, Material der Öffentlichkeit bereitzustellen durch beliebige Mittel oder Verfahren, die gemäß der lizenzierten Rechte Zustimmung erfordern, wie zum Beispiel Vervielfältigung, öffentliche Vorführung, öffentliche Darbietung, Vertrieb, Verbreitung, Wiedergabe oder Übernahme und öffentliche Zugänglichmachung bzw. Verfügbarmachung in solcher Weise, dass Mitglieder der Öffentlichkeit auf das Material von Orten und zu Zeiten ihrer Wahl zugreifen können.</li><li id="s1m"><strong>Sui-generis Datenbankrechte</strong> bezeichnet Rechte, die keine Urheberrechte sind, sondern gegründet sind auf die Richtlinie 96/9/EG des Europäischen Parlaments und des Rates vom 11. März 1996 über den rechtlichen Schutz von Datenbanken in der jeweils gültigen Fassung bzw. deren Nachfolgeregelungen, sowie andere im Wesentlichen funktionsgleiche Rechte anderswo auf der Welt.</li><li id="s1n"><strong>Sie</strong> bezeichnet die natürliche oder juristische Person, die von lizenzierten Rechten unter der vorliegenden Public License Gebrauch macht. <strong>Ihr bzw. Ihre</strong> hat die entsprechende Bedeutung.</li></ol><p id="s2"><strong>Abschnitt 2 – Umfang</strong></p><ol type="a"><li id="s2a"><strong>Lizenzgewährung</strong><ol><li id="s2a1">Unter den Bedingungen der vorliegenden Public License gewährt der Lizenzgeber Ihnen eine weltweite, vergütungsfreie, nicht unterlizenzierbare, nicht-ausschließliche, unwiderrufliche Lizenz zur Ausübung der lizenzierten Rechte am lizenzierten Material, um:<ol type="A"><li id="s2a1A">das lizenzierte Material ganz oder in Teilen zu vervielfältigen und weiterzugeben, jedoch nur für nicht kommerzielle Zwecke; und</li><li id="s2a1B">abgewandeltes Material zu erstellen, zu vervielfältigen und weiterzugeben, jedoch nur für nicht kommerzielle Zwecke.</li></ol><li id="s2a2"><span style="text-decoration: underline;">Ausnahmen und Beschränkungen</span>. Es sei klargestellt, dass, wo immer gesetzliche Ausnahmen und Beschränkungen auf Ihre Nutzung Anwendung finden, die vorliegende Public License nicht anwendbar ist und Sie insoweit ihre Bedingungen nicht einhalten müssen.</li><li id="s2a3"><span style="text-decoration: underline;">Laufzeit</span>. Die Laufzeit der vorliegenden Public License wird in Abschnitt <a href="#s6a">6(a)</a> geregelt.</li><li id="s2a4"><span style="text-decoration: underline;">Medien und Formate; Gestattung technischer Modifikationen</span>. Der Lizenzgeber erlaubt Ihnen, die lizenzierten Rechte in allen bekannten und zukünftig entstehenden Medien und Formaten auszuüben und die dafür notwendigen technischen Modifikationen vorzunehmen. Der Lizenzgeber verzichtet auf jegliche und/oder versichert die Nichtausübung jeglicher Rechte und Befugnisse, Ihnen zu verbieten, technische Modifikationen vorzunehmen, die notwendig sind, um die lizenzierten Rechte ausüben zu können, einschließlich solcher, die zur Umgehung wirksamer technischer Schutzmaßnahmen erforderlich sind. Im Sinne der vorliegenden Public License entsteht kein abgewandeltes Material, soweit lediglich Modifikationen vorgenommen werden, die nach diesem Abschnitt <a href="#s2a4">2(a)(4)</a> zulässig sind.</li><li id="s2a5"><span style="text-decoration: underline;">Nachfolgende Empfänger</span><div class="para"><ol type="A"><li id="s2a5A"><span style="text-decoration: underline;">Angebot des Lizenzgebers – Lizenziertes Material</span>. Jeder Empfänger des lizenzierten Materials erhält automatisch ein Angebot des Lizenzgebers, die lizenzierten Rechte unter den Bedingungen der vorliegenden Public License auzuüben.</li><li id="s2a5B"><span style="text-decoration: underline;">Zusätzliches Angebot des Lizenzgebers – Abgewandeltes Material</span>. Jeder, der abgewandeltes Material von Ihnen erhält, erhält automatisch vom Lizenzgeber ein Angebot, die lizenzierten Rechte am abgewandelten Material unter den Bedingungen der durch Sie vergebenen Abwandlungslizenz auszuüben.</li><li id="s2a5C"><span style="text-decoration: underline;">Keine Beschränkungen für nachfolgende Empfänger</span>. Sie dürfen keine zusätzlichen oder abweichenden Bedingungen fordern oder das lizenzierte Material mit solchen belegen oder darauf wirksame technische Maßnahmen anwenden, sofern dadurch die Ausübung der lizenzierten Rechte durch Empfänger des lizenzierten Materials eingeschränkt wird.</li></ol></div><li id="s2a6"><span style="text-decoration: underline;">Inhaltliche Indifferenz</span>. Die vorliegende Public License begründet nicht die Erlaubnis, zu behaupten oder den Eindruck zu erwecken, dass Sie oder Ihre Nutzung des lizenzierten Materials mit dem Lizenzgeber oder den Zuschreibungsempfängern gemäß Abschnitt <a href="#s3a1Ai">3(a)(1)(A)(i)</a> in Verbindung stehen oder durch ihn gefördert, gutgeheißen oder offiziell anerkannt werden.</li></ol><li id="s2b"><p><strong>Sonstige Rechte</strong></p><ol><li id="s2b1">Urheberpersönlichkeitsrechte, wie etwa zum Schutz vor Werkentstellungen, werden durch die vorliegende Public License ebenso wenig mitlizenziert wie das Recht auf Privatheit, auf Datenschutz und/oder ähnliche Persönlichkeitsrechte; gleichwohl verzichtet der Lizenzgeber auf derlei Rechte bzw. ihre Durchsetzung, soweit dies für Ihre Ausübung der lizenzierten Rechte erforderlich und möglich ist, jedoch nicht darüber hinaus.</li><li id="s2b2">Patent- und Kennzeichenrechte werden durch die vorliegende Public License nicht lizenziert.</li><li id="s2b3">Soweit wie möglich verzichtet der Lizenzgeber auf Vergütung durch Sie für die Ausübung der lizenzierten Rechte, sowohl direkt als auch durch eine Verwertungsgesellschaft unter welchem freiwilligen oder abdingbaren gesetzlichen oder Pflichtlizenzmechanismus auch immer eingezogen. In allen übrigen Fällen behält sich der Lizenzgeber ausdrücklich jedes Recht vor, Vergütungen zu fordern, einschließlich für Nutzungen des lizenzierten Materials für andere als nicht kommerzielle Zwecke.</li></ol></li></ol><p id="s3"><strong>Abschnitt 3 – Lizenzbedingungen</strong></p><p>Ihre Ausübung der lizenzierten Rechte unterliegt ausdrücklich folgenden Bedingungen.</p><ol type="a"><li id="s3a"><p><strong>Namensnennung</strong></p><ol><li id="s3a1"><p>Wenn Sie das lizenzierte Material weitergeben (auch in veränderter Form), müssen Sie:</p><ol type="A"><li id="s3a1A">die folgenden Angaben beibehalten, soweit sie vom Lizenzgeber dem lizenzierten Material beigefügt wurden:<ol type="i"><li id="s3a1Ai">die Bezeichnung der/des Ersteller(s) des lizenzierten Materials und anderer, die für eine Namensnennung vorgesehen sind (auch durch Pseudonym, falls angegeben), in jeder durch den Lizenzgeber verlangten Form, die angemessen ist;</li><li id="s3a1Aii">einen Copyright-Vermerk;</li><li id="s3a1Aiii">einen Hinweis auf die vorliegende Public License; </li><li id="s3a1Aiv">einen Hinweis auf den Haftungsausschluss;</li><li id="s3a1Av">soweit vernünftigerweise praktikabel einen URI oder Hyperlink zum lizenzierten Material;</li></ol><li id="s3a1B">angeben, ob Sie das lizenzierte Material verändert haben, und alle vorherigen Änderungsangaben beibehalten; und</li><li id="s3a1C">angeben, dass das lizenzierte Material unter der vorliegenden Public License steht, und deren Text oder URI oder einen Hyperlink darauf beifügen.</li></ol></li><li id="s3a2">Sie dürfen die Bedingungen des Abschnitts <a href="#s3a1">3(a)(1)</a> in jeder angemessenen Form erfüllen, je nach Medium, Mittel und Kontext in bzw. mit dem Sie das lizenzierte Material weitergeben. Es kann zum Beispiel angemessen sein, die Bedingungen durch Angabe eines URI oder Hyperlinks auf eine Quelle zu erfüllen, die die erforderlichen Informationen enthält.</li><li id="s3a3">Falls der Lizenzgeber es verlangt, müssen Sie die gemäß Abschnitt <a href="#s3a1A">3(a)(1)(A)</a> erforderlichen Informationen entfernen, soweit dies vernünftigerweise praktikabel ist.</li></ol></li><li id="s3b"><strong>Share Alike</strong><p>Zusätzlich zu den Bedingungen in Abschnitt <a href="#s3a">3(a)</a> gelten die folgenden Bedingungen, falls Sie abgewandeltes Material weitergeben, welches Sie selbst erstellt haben.</p><ol><li id="s3b1">Die Abwandlungslizenz, die Sie vergeben, muss eine Creative-Commons-Lizenz der vorliegenden oder einer späteren Version mit den gleichen Lizenzelementen oder eine BY-NC-SA-kompatible Lizenz sein.</li><li id="s3b2">Sie müssen den Text oder einen URI oder Hyperlink auf die von Ihnen gewählte Abwandlungslizenz beifügen. Diese Bedingung dürfen Sie in jeder angemessenen Form erfüllen, je nach Medium, Mittel und Kontext in bzw. mit dem Sie abgewandeltes Material weitergeben.</li><li id="s3b3">Sie dürfen keine zusätzlichen oder abweichenden Bedingungen anbieten oder das abgewandelte Material mit solchen belegen oder darauf wirksame technische Maßnahmen anwenden, sofern dadurch die Ausübung der Rechte am abgewandelten Material eingeschränkt wird, die Sie unter der Abwandlungslizenz gewähren.</li></ol></li></ol><p id="s4"><strong>Abschnitt 4 – Sui-generis-Datenbankrechte</strong></p><p>Soweit die lizenzierten Rechte Sui-generis-Datenbankrechte beinhalten, die auf Ihre Nutzung des lizenzierten Materials Anwendung finden, gilt:</p><ol type="a"><li id="s4a">es sei klargestellt, dass Abschnitt <a href="#s2a1">2(a)(1)</a> Ihnen lediglich zu nicht kommerziellen Zwecken das Recht gewährt, die gesamten Inhalte der Datenbank oder wesentliche Teile davon zu entnehmen, weiterzuverwenden, zu vervielfältigen und weiterzugeben;</li><li id="s4b">sofern Sie alle Inhalte der Datenbank oder wesentliche Teile davon in eine Datenbank aufnehmen, an der Sie Sui-generis-Datenbankrechte haben, dann gilt die Datenbank, an der Sie Sui-generis-Datenbankrechte haben (aber nicht ihre einzelnen Inhalte) als abgewandeltes Material, insbesondere in Bezug auf Abschnitt <a href="#s3b">3(b)</a>; und</li><li id="s4c">Sie müssen die Bedingungen des Abschnitts <a href="#s3a">3(a)</a> einhalten, wenn sie alle Datenbankinhalte oder wesentliche Teile davon weitergeben.</li></ol>Es sei ferner klargestellt, dass dieser Abschnitt <a href="#s4">4</a> Ihre Verpflichtungen aus der vorliegenden Public License nur ergänzt und nicht ersetzt, soweit die lizenzierten Rechte andere Urheberrechte oder ähnliche Rechte enthalten.<p id="s5"><strong>Abschnitt 5 – Gewährleistungsausschluss und Haftungsbeschränkung</strong></p><ol style="font-weight: bold;" type="a"><li id="s5a"><strong>Sofern der Lizenzgeber nicht separat anderes erklärt und so weit wie möglich, bietet der Lizenzgeber das lizenzierte Material so wie es ist und verfügbar ist an und sagt in Bezug auf das lizenzierte Material keine bestimmten Eigenschaften zu, weder ausdrücklich noch konkludent oder anderweitig, und schließt jegliche Gewährleistung aus, einschließlich der gesetzlichen. Dies umfasst insbesondere das Freisein von Rechtsmängeln, Verkehrsfähigkeit, Eignung für einen bestimmten Zweck, Wahrung der Rechte Dritter, Freisein von (auch verdeckten) Sachmängeln, Richtigkeit und das Vorliegen oder Nichtvorliegen von Irrtümern, gleichviel ob sie bekannt, unbekannt oder erkennbar sind. Dort, wo Gewährleistungsausschlüsse ganz oder teilweise unzulässig sind, gilt der vorliegende Ausschluss möglicherweise für Sie nicht.</strong></li><li id="s5b"><strong>Soweit wie möglich, haftet der Lizenzgeber Ihnen gegenüber nach keinem rechtlichen Konstrukt (einschließlich insbesondere Fahrlässigkeit) oder anderweitig für irgendwelche direkten, speziellen, indirekten, zufälligen, Folge-, Straf- exemplarischen oder anderen Verluste, Kosten, Aufwendungen oder Schäden, die sich aus der vorliegenden Public License oder der Nutzung des lizenzierten Materials ergeben, selbst wenn der Lizenzgeber auf die Möglichkeit solcher Verluste, Kosten, Aufwendungen oder Schäden hingewiesen wurde. Dort, wo Haftungsbeschränkungen ganz oder teilweise unzulässig sind, gilt die vorliegende Beschränkung möglicherweise für Sie nicht.</strong></li></ol><ol start="3" type="a"><li id="s5c">Der Gewährleistungsausschluss und die Haftungsbeschränkung oben sollen so ausgelegt werden, dass sie soweit wie möglich einem absoluten Haftungs- und Gewährleistungsausschluss nahe kommen.</li></ol><p id="s6"><strong>Abschnitt 6 – Laufzeit und Beendigung</strong></p><ol type="a"><li id="s6a">Die vorliegende Public License gilt bis zum Ablauf der Schutzfrist des Urheberrechts und der ähnlichen Rechte, die hiermit lizenziert werden. Gleichwohl erlöschen Ihre Rechte aus dieser Public License automatisch, wenn Sie die Bestimmungen dieser Public License nicht einhalten.</li><li id="s6b"><p>Soweit Ihr Recht, das lizenzierte Material zu nutzen, gemäß Abschnitt <a href="#s6a">6(a)</a> erloschen ist, lebt es wieder auf:</p><ol><li id="s6b1">automatisch zu dem Zeitpunkt, an welchem die Verletzung abgestellt ist, sofern dies innerhalb von 30 Tagen seit Ihrer Kenntnis der Verletzung geschieht; oder</li><li id="s6b2">durch ausdrückliche Wiedereinsetzung durch den Lizenzgeber.</li></ol>Es sei klargestellt, dass dieser Abschnitt <a href="#s6b">6(b)</a> die Rechte des Lizenzgebers, Ausgleich für Ihre Verletzung der vorliegenden Public License zu verlangen, nicht einschränkt.</li><li id="s6c">Es sei klargestellt, dass der Lizenzgeber das lizenzierte Material auch unter anderen Bedingungen anbieten oder den Vertrieb des lizenzierten Materials jederzeit einstellen darf; gleichwohl erlischt dadurch die vorliegende Public License nicht.</li><li id="s6d">Die Abschnitte <a href="#s1">1</a>, <a href="#s5">5</a>, <a href="#s6">6</a>, <a href="#s7">7</a> und <a href="#s8">8</a> gelten auch nach Erlöschen der vorliegenden Public License fort.</li></ol><p id="s7"><strong>Abschnitt 7 – Sonstige Bedingungen</strong></p><ol type="a"><li id="s7a">Der Lizenzgeber ist nicht an durch Sie gestellte zusätzliche oder abweichende Bedingungen gebunden, wenn diese nicht ausdrücklich vereinbart wurden.</li><li id="s7b">Jedwede das lizenzierte Material betreffenden und hier nicht genannten Umstände, Annahmen oder Vereinbarungen sind getrennt und unabhängig von den Bedingungen der vorliegenden Public License.</li></ol><p id="s8"><strong>Abschnitt 8 – Auslegung</strong></p><ol type="a"><li id="s8a">Es sei klargestellt, dass die vorliegende Public License weder besagen noch dahingehend ausgelegt werden soll, dass sie solche Nutzungen des lizenzierten Materials verringert, begrenzt, einschränkt oder mit Bedingungen belegt, die ohne eine Erlaubnis aus dieser Public License zulässig sind.</li><li id="s8b">Soweit wie möglich soll, falls eine Klausel der vorliegenden Public License als nicht durchsetzbar anzusehen ist, diese Klausel automatisch im geringst erforderlichen Maße angepasst werden, um sie durchsetzbar zu machen. Falls die Klausel nicht anpassbar ist, soll sie von der vorliegenden Public License abgeschieden werden, ohne dass die Durchsetzbarkeit der verbleibenden Bedingungen tangiert wird.</li><li id="s8c">Auf keine Bedingung der vorliegenden Public License wird verzichtet und kein Verstoß dagegen soll als hingenommen gelten, außer der Lizenzgeber hat sich damit ausdrücklich einverstanden erklärt.</li><li id="s8d">Nichts in der vorliegenden Public License soll zu einer Beschränkung oder Aufhebung von Privilegien und Immunitäten führen, die dem Lizenzgeber oder Ihnen insbesondere aufgrund rechtlicher Regelungen irgendeiner Rechtsordnung oder Rechtsposition zustehen, oder dahingehend interpretiert werden.</li></ol>       <li><b>Pflichten des Nutzers</b><br>          <ol>            <li>Der Nutzer ist für den Inhalt seiner Anmeldung und somit für die Daten, die er über sich macht, alleine verantwortlich.              Insbesondere verpflichtet er sich keine rassistischen, beleidigenden, diskriminierenden, belästigenden, verleumderischen,              sexuellen, pornografischen, gewaltverherrlichenden oder sonstige rechtswidrige Inhalte, Personen oder Darstellungen zu speichern,              zu veröffentlichen, zu übermitteln und zu verbreiten.              Des Weiteren wird der Nutzer ausdrücklich darauf hingewiesen, dass es verboten ist rechtlich geschützte              Begriffe, Namen, Bilder, Videos, oder andere Materialien zu verwenden.</li>            <li>Ferner ist der Nutzer verpflichtet, geeignete Maßnahmen zu treffen, um eine unautorisierte              Nutzung seiner Daten insbesondere Kennwörter durch Dritte zu verhindern. Er verpflichtet sich,              MITOPO unverzüglich über eine bemerkte oder vermutete unautorisierte Nutzung seines Accounts zu informieren.</li>            <li>Der Nutzer versichert, dass er keine Lichtbilder oder sonstigen Werke einstellt, ohne nach den geltenden urheberrechtlichen Regelungen hierzu berechtigt zu sein.</li>            <li>Der Nutzer verpflichtet sich, MITOPO schadlos von jeder Art von Klagen, Schäden, Verlusten oder              Forderungen zu halten, die durch seine Anmeldung und / oder die Teilnahme an diesem Service entstehen können, insofern diese Schäden nicht auf Vorsatz oder Fahrlässigkeit MITOPOs, bzw. dessen gesetzlicher Vertreter oder Erfüllungsgehilfen beruhen. Besonders verpflichtet sich der Nutzer, MITOPO              von jeglicher Haftung und von allen Verpflichtungen, Aufwendungen und Ansprüchen, die sich aus Schäden wegen übler Nachrede, Beleidigung, Verletzung von Persönlichkeitsrechten, wegen dem Ausfall              von Dienstleistungen für Nutzer, wegen der Verletzung von Immaterialgütern oder sonstiger Rechte ergeben, freizusprechen.</li>            <li>Der Nutzer verpflichtet sich, nicht vorsätzlich die Daten dritter Personen (inkl. E-Mail-Adresse)              als seine eigenen anzugeben. Insbesondere verpflichtet er sich dazu, nicht in              betrügerischer Absicht die Bankverbindung oder die Kreditkartendaten Dritter anzugeben.</li>            <li>Der Nutzer verpflichtet sich des Weiteren, bei Anmeldung und Nutzung des Angebots unter              cruxle.org oder der mobilen Apps die jeweils anwendbaren Gesetze einzuhalten.</li>            <li>Ferner verpflichtet sich jeder einzelne Nutzer, den Service nicht missbräuchlich zu benutzen,              insbesondere:              <ul>                <li>über ihn kein diffamierendes (z. B. üble Nachreden), anstößiges oder in sonstiger Weise                  rechtswidriges Material oder solche Informationen weiterzugeben</li>                <li>ihn nicht zu benutzen, um andere Personen / Nutzer zu bedrohen, belästigen oder die Rechte                  (einschließlich Persönlichkeitsrechte) Dritter zu verletzen</li>                <li>keine Daten hochzuladen, die einen Virus beinhalten (infizierte Software). Generell keine                  Software oder anderes Material hochzuladen, das urheberrechtlich geschützt ist, es sei denn, der                  Nutzer hat die Rechte daran oder die erforderlichen Zustimmungen - hier kann durch den Betreiber ein schriftlicher Nachweis gefordert werden</li>                <li>ihn nicht auf eine Art und Weise zu nutzen, welche die Verfügbarkeit der Leistungsangebote für andere Nutzer nachteilig beeinflusst</li>                <li>keine E-Mails abzufangen und dies auch nicht zu versuchen</li>                <li>keine Werbung für andere Internetportale zu betreiben</li>                <li>keine Kettenbriefe oder Chatnachrichten zu verfassen und an mehreren Personen gleichzeitig zu versenden</li>                <li>in den persönlichen und freiwilligen Angaben (Nutzer-Profil) keine Namen, Adressen, Telefon- oder Faxnummern sowie E-Mail Adressen zu nennen.</li>              </ul>            </li>            <li>Folgende Bilder dürfen nicht hochgeladen werden:              <ul>                <li>Bilder, die Nacktheit; sexuelle und/oder pornografische Handlungen andeuten oder zeigen</li>                <li>Urheberrechtlich geschütze Bilder / gestohlene Bilder (z.B. Bilder aus dem Internet)</li>                <li>Bilder, die Gewalt, Drogen, Waffen oder Ähnliches abbilden</li>                <li>Bilder die den Konsum von Alkohol, Zigaretten, Shisha etc. von Personen unter 18 Jahren zeigen</li>                <li>Bilder, die Text und/oder Kontaktdaten enthalten bzw. Rückschlüsse auf persönliche Daten ermöglichen</li>                <li>Bilder, die verbotene Symbole, Fahnen oder sonstige Zeichen und offensive Gesten jeder Art (Ausnahme: anerkannte Staatsflaggen)</li>                <li>Bilder von geschützten Markenzeichen</li>                <li>Bilder, auf fremde Webseiten verweisen oder andere kommerzielle Inhalte zeigen</li>              </ul>            </li>          </ol>        </li>        <li><b>Verstoß gegen Nutzerpflichten</b>          <ol>            <li>MITOPO hat das Recht, Inhalte, Personen oder Darstellungen, die gegen die vorliegenden              Allgemeinen Nutzungsbedingungen verstoßen oder rechtwidrig sind, z.B. gegen Gesetzes- und              Rechtsvorschriften, insbesondere Jugendschutz, Datenschutz, Schutz des Persönlichkeitsrechts,              Schutz vor Beleidigung, Urheberrechte, Markenrechte verstoßen selbst zu entfernen.</li>            <li>Ein Anspruch auf Wiederherstellung der durch MITOPO gelöschten Informationen besteht nicht.</li>            <li>Darüber hinaus ist MITOPO berechtigt den Benutzer bei Nichtbeachtung der              Allgemeinen Nutzungsbedingungen zu verwarnen, temporär oder endgültig von den MITOPO Diensten auszuschließen.              Je nach Schwere bzw. Art und Weise des Vergehens kann ein Verstoß auch zivil- und strafrechtliche              Folgen für den Nutzer selbst mit sich bringen.</li>            <li>Die Geltendmachung weiterer Ansprüche, insbesondere Schadensersatzansprüche, bleibt MITOPO ausdrücklich vorbehalten.</li>          </ol>        </li>        <li><b>Nutzungsrechte</b>          <ol>            <li>MITOPO gestattet seinen registrierten Nutzern das angebotene Produkt- und Dienstleistungsportfolio unter Berücksichtigung der gesetzlichen Bestimmungen und der vorliegenden Allgemeinen Geschäftsbedingungen zu nutzen, um              Inhalte hochzuladen, zu speichern, zu veröffentlichen, zu verbreiten, zu übermitteln und mit anderen Nutzern zu teilen.</li>            <li><b>Nutzungsrechte an Lichtbildern des Nutzers:</b>              Der Nutzer (Lizenzgeber) räumt mitopo (Lizenznehmer) betreffend der von Ihm auf cruxle.org hochgeladenen Lichtbilder die auf              Dritte übertragbaren, räumlich unbeschränkten (weltweiten),              zeitlich unbeschränkten (für die Dauer der für das lizenzierte Werk nach dem              deutschen Urheberrechtsgesetz vorgesehenen Schutzfrist) und inhaltlich              unbeschränkten (z.B. für Darstellung auf cruxle.org oder einer anderen Webseite der mitopo UG, Darstellung in einer Smartphone Applikation,              Darstellung als Teil anderer Werke, Darstellung und Download als Teil von Werken die zum Druck vorgesehen sind,              Download des Werkes, Weitergabe des Werkes in analoger, digitaler oder abgeleiteter Form an Dritte,              Druck des Werkes, Druck des Werkes durch Dritte, Bearbeiten des Werkes, Versenden des Werkes in digitaler und analoger Form, Speichern des Werkes)              Nutzungsrechte ein. MITOPO ist daher berechtigt die Inhalte für Werbezwecke oder sonstige Veröffentlichungen,              sowohl in Teilen oder im Ganzen, in jedem Format oder Medium zu nutzen. Der Nutzer räumt MITOPO in diesem Zusammenhang              Urheberpersönlichkeitsrechte ein, wobei MITOPO ausdrücklich darauf hinweist, dass MITOPO keine Inhaberschaft an der von den              Nutzern bereitgestellten Inhalten erhält und somit keine Aufsichtsfunktion der Inhalte durch MITOPO stattfindet.              MITOPO erhebt keinen Anspruch auf das ausschließliche Nutzungsrecht.</li>            <li><b>Zustandekommen des Vertrags:</b> Der Nutzer willigt durch den Upload des Lichtbildes (durch Klick auf die jeweils              gekennzeichnete Schaltfläche z.B. bei der Erstellung einer Wand ("Wall"),              bei der Bereitstellung von Informationsmaterialien oder der Bereitstellung von Userbildern) in diese Nutzungsrechte ein.</li>            <li><b>Speichern der Vertragsdaten:</b> mitopo.de speichert die Details des Vertragsabschlusses in einer Datenbank.</li>            <li>MITOPO weist den Nutzer ausdrücklich darauf hin, dass Inhalte gespeichert und an Dritte weitergegeben werden dürfen, soweit dies              gesetzlich vorgeschrieben oder nach pflichtgemäßem Ermessen notwendig und rechtlich zulässig ist.</li>            <li>Ferner weist MITOPO ausdrücklich darauf hin, dass Nutzer-Daten zu jeder Zeit ohne Angaben von Gründen und ohne              Benachrichtigung der betroffenen Nutzer entfernt werden können.</li>          </ol>        </li> Routen hinzufügen mehr mehr als 3 Stunden Benutzername in der Nähe: so gut wie nie trocken wird niemals nass Nächstes Mal verlinkt mit auf der Wand Erstellt von veröffentlicht bewertet Zurücksetzen Routenliste Routenpfade Druckerfreundliche Ansicht zur Veröffentlichung vorgemerkt Ziel Zielversion abgehakt Besucher Wandinfos Wände geladen {} wurde als veraltet markiert. {} wird im Moment von {} editiert! {} wird im Moment von {} editiert! Änderungen haben keine Auswirkungen. {} wird im Moment von {} editiert. Weitere Änderungen werden nicht gespeichert. {} wird im Moment von {} editiert. Du kannst kein Infomaterial hinzufügen. {} wird im Moment von {} editiert. Du kannst die Wand im Moment nicht veröffentlichen. 