��             +         �     �     �     �  -   �  9        H     P     U     a  }   n     �     �  	   �                    %     B  	   W     a     m     �  	   �     �     �  �   �  �   O  
     	               b  '     �     �     �  2   �  )   �  	                    �   3     �     �     �     �     �     	  #   	     A	     \	     i	  !   v	     �	     �	     �	  
   �	  �   �	  �   c
       	   )     3     9           
                                                                            	                                                          entries  of  # Routes Click on the map to start drawing a new route Continue to add more points along the course of the route Creator Date Description Difficulties Finish the route by either clicking again on the last point, hitting the green button or click on the entry in the route-list Grades Inbox Last Edit Name New Messages No Ascents yet. No Ascents yet. Go Climbing! No Items to display. No Routes Provided by Really Delete Ascent? Routes Show More Style Submit Uups, we tried to render an ascent sunburst chart into a div that does not contain an svg element. Could you please tell us where you found this confusion here? Many Thanks! Uups, we tried to render an route histogram of a wall into a div that does not contain an svg element. Could you please tell us where you found this confusion here? Many Thanks! Your reply anonymous showing showing  Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-07-09 16:07+0200
Last-Translator: alois <alodi@directbox.com>
Language-Team: German-Mitopo <fabian@jakub.com>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
  Einträgen  von  # Routen Klick auf die Karte um eine neue Route zu zeichnen Füge mehr Punkte entlang der Route hinzu Ersteller Datum Beschreibung Schwierigkeitsgrade Stelle die Route fertig indem Du nochmals auf den letzten Punkt klickt. Oder auf den grossen Gruenen Knopf oder auf den Eintrag in der Liste. Schwierigkeitsgrade Eingang Letzte Änderung Name Neue Nachrichten Bisher keine Begehungen. Bisher keine Begehungen. Auf gehts! Keine Einträge vorhanden. Keine Routen Erstellt von Diese Begehung wirklich löschen? Routen Mehr ... Stil Abschicken Uups, wir haben versucht, eine Aufstiegs-Sunburst-Tabelle in ein div zu rendern, das kein Svg-Element enthält. Können Sie uns diesen Bug bitte reporten? Danke vielmals! Uups, wir haben versucht, eine Aufstiegs-Sunburst-Tabelle in ein div zu rendern, das kein Svg-Element enthält. Können Sie uns bitte auf diesen Bug aufmerksam machen? Danke vielmals! Deine Antwort Unbekannt Zeige Zeige 