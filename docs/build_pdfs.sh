#!/usr/bin/env bash

docker run -v $PWD:/documents/ asciidoctor/docker-asciidoctor bash ./build.sh