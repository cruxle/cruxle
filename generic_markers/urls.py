from django.conf.urls import *

from generic_markers import views

def do_nothing(*args, **kwargs):
    from django.http import HttpResponse
    return HttpResponse()

app_name = 'generic_markers'

urlpatterns = [
    url(r'^wallview(?P<wallview_id>\d+)/add_simple_(?P<markertype>\w+)$', views.add_simple_marker, name='add_simple_marker'),
    url(r'^wallview(?P<wallview_id>\d+)/add_relational_(?P<markertype>\w+)$', views.add_relational_marker, name='add_relational_marker'),
    url(r'^wallview(?P<wallview_id>\d+)/add_text$', views.add_text_marker, name='add_text_marker'),
    url(r'^wallview(?P<wallview_id>\d+)/add_pitch$', views.add_pitch_marker, name='add_pitch_marker'),

    url(r'^del_simple_marker(?P<marker_id>\d+)$', views.del_simple_marker, name='del_simple_marker'),
    url(r'^del_relational_marker(?P<marker_id>\d+)$', views.del_relational_marker, name='del_relational_marker'),
    url(r'^del_text_marker(?P<marker_id>\d+)$', views.del_text_marker, name='del_text_marker'),
    url(r'^del_pitch_marker(?P<marker_id>\d+)$', views.del_pitch_marker, name='del_pitch_marker'),

    url(r'^relocate_simple_marker(?P<marker_id>\d+)$', views.relocate_simple_marker, name='relocate_simple_marker'),
    url(r'^relocate_relational_marker(?P<marker_id>\d+)$', views.relocate_relational_marker, name='relocate_relational_marker'),
    url(r'^relocate_text_marker(?P<marker_id>\d+)$', views.relocate_text_marker, name='relocate_text_marker'),
    url(r'^relocate_pitch_marker(?P<marker_id>\d+)$', views.relocate_pitch_marker, name='relocate_pitch_marker'),

    url(r'^relational_marker(?P<marker_id>\d+)/marker_detail$', views.relational_marker_detail, name='relational_marker_detail'),
    url(r'^text_marker(?P<marker_id>\d+)/marker_detail$', views.text_marker_detail, name='text_marker_detail'),
    url(r'^pitch_marker(?P<marker_id>\d+)/marker_detail$', views.pitch_marker_detail, name='pitch_marker_detail'),
    url(r'^simple_marker(?P<marker_id>\d+)/marker_detail$', views.simple_marker_detail, name='simple_marker_detail'),

    url(r'^wall_markers/$', views.wall_markers, name='wall_markers'),

    url(r'^wallview(?P<wallview_id>\d+)/simple_markers/$', views.simple_wallmarkers, name='simple_wallmarkers'),
    url(r'^wallview(?P<wallview_id>\d+)/relational_markers/$', views.relational_wallmarkers, name='relational_wallmarkers'),
    url(r'^wallview(?P<wallview_id>\d+)/text_markers/$', views.text_wallmarkers, name='text_wallmarkers'),
    url(r'^wallview(?P<wallview_id>\d+)/pitch_markers/$', views.pitch_wallmarkers, name='pitch_wallmarkers'),
]
