# Generated by Django 2.0 on 2018-01-06 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generic_markers', '0005_auto_20171207_0143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='simplemarker',
            name='markertype',
            field=models.CharField(choices=[('bolt', 'Bolt'), ('belay', 'Belay'), ('abseil_anchor', 'Abseil Anchor')], max_length=20),
        ),
    ]
