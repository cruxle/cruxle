import json
import os

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.postgres.fields import JSONField
from django.core import exceptions
from django.urls import reverse
from django.utils.html import escape
from django.utils.translation import ugettext as _

from miroutes.model_choices import GRADE_CHOICES


class PointField(JSONField):
    """A point field accepts a JSON that represents a list with 2 elements,
    being floats or integers or a Latlng object of the form {lat: value, lng: value}
    where value is float or int.
    """

    def to_python(self, value):
        """Check if we can generate a list and if the elements are given by a dict with lat and lngs
        we build a 2-element list instead.
        """
        value = super(PointField, self).to_python(value)

        try:
            if isinstance(value, list):
                return value
            elif isinstance(value, dict):
                value = [value['lat'], value['lng']]
                return value
            else:
                raise TypeError
        except (TypeError, KeyError) as e:
            raise exceptions.ValidationError(
                _("Value must be a list or a {lat, lng} dict."),
                code='invalid',
                params={'value': value},
            )


    def validate(self, value, model_instance):
        """Check if the list has correct size == 2 and the elements are
        floats or ints."""
        super(PointField, self).validate(value, model_instance)
        try:
            if len(value) != 2:
                raise TypeError
            elif not isinstance(value[0], (int, float)) or not isinstance(value[1], (int, float)):
                raise TypeError

        except TypeError:
            raise exceptions.ValidationError(
                _("Value must be a list of length 2."),
                code='invalid',
                params={'value': value},
            )


class MapLocation(models.Model):
    """Standard map decorator."""

    pnt = models.PointField(verbose_name="Location")

    @property
    def popup_url(self):
        """URL to fetch the content of a popup via ajax.
        For use on normal (non-edit) templates.
        """
        return ''

    @property
    def click_url(self):
        """URL to call on click on the marker for use
        in non-edit templates.
        """
        return ''

    @property
    def popup_url_edit(self):
        """URL to fetch the content of a popup via ajax.
        For use on edit templates.
        """
        return ''

    @property
    def click_url_edit(self):
        """URL to call on click on the marker for use in
        edit templates.
        """
        return ''

    @property
    def popup_on_hover(self):
        """Whether the popup is opened on-mouseover (True)
        or on-click (False).
        For use in non-edit templates.
        """
        return False

    @property
    def popup_on_hover_edit(self):
        """Whether the popup is opened on-mouseover (True)
        or on-click (False).
        For use in edit templates.
        """
        return False

    @property
    def add_url(self):
        """URL to add the marker on-click in an edit template.

        The coordinates are supplied to the view with the ajax request,
        using {lat: <lat>, lng: <lng>}
        """
        return ''

    @property
    def popup_add_url(self):
        """URL to open a form in a template to add the marker on-click
        in an edit template.

        The coordinates are supplied to the view with the ajax request,
        using {lat: <lat>, lng: <lng>}
        """
        return ''

    @property
    def relocate_url(self):
        """URL to call when the marker is relocated.
        This URL has to be stored in the marker's options
        and used when the marker is dragged to a new location.

        The coordinates are supplied to the view with the ajax request,
        using {lat: <lat>, lng: <lng>}

        Packed with the edit_marker_json.
        """
        return ''

    @property
    def icon_url(self):
        """URL to the icon for the marker."""
        return os.path.join(
            settings.STATIC_URL,
            'generic_markers/img/spot_map_icon.png')


    @property
    def icon_config(self):
        """Configuration of the icon used with the location."""
        return {
            'iconUrl': self.icon_url,
            'iconSize': [30, 35],
            'iconAnchor': [15, 35],
            'popupAnchor': [0, -35]
        }

    @property
    def icon_url_edit(self):
        """URL to the icon for the marker when the marker position is editable."""
        return self.icon_url

    @property
    def icon_config_edit(self):
        """Configuration of the icon used with the location when the position is editable."""
        cdict = self.icon_config
        cdict['iconUrl'] = self.icon_url_edit
        return cdict

    @property
    def icon_config_json(self):
        """Returns a JSON of the marker config."""
        return json.dumps(self.icon_config)

    @property
    def icon_config_edit_json(self):
        """Returns a JSON of the marker config."""
        return json.dumps(self.icon_config_edit)

    @property
    def marker_dict(self):
        """Config dict for the marker. This is used for one-shot setup of
        a marker on a non-edit template.
        """

        marker_dict = {
            'icon_config': self.icon_config,
            'popup_url': self.popup_url,
            'click_url': self.click_url,
            'popup_on_hover': self.popup_on_hover,
        }
        if(self.pnt):
            marker_dict['point'] = self.pnt.coords
        return marker_dict

    @property
    def marker_dict_edit(self):
        """Config dict for the marker. This is used for one-shot setup of
        a marker on an edit template.
        """
        marker_dict = {
            'icon_config': self.icon_config_edit,
            'popup_url': self.popup_url_edit,
            'click_url': self.click_url_edit,
            'popup_on_hover': self.popup_on_hover_edit,
            'relocate_url': self.relocate_url
        }
        if(self.pnt):
            marker_dict['point'] = self.pnt.coords
        return marker_dict

    @property
    def marker_json(self):
        """JSON of the marker configuration."""
        return json.dumps(self.marker_dict)

    @property
    def marker_json_edit(self):
        """JSON of the marker config for edit templates."""
        return json.dumps(self.marker_dict_edit)

    class Meta:
        abstract = True

class Marker(MapLocation):
    """A marker on a wall view."""
    wallview = models.ForeignKey('miroutes.WallView', on_delete=models.CASCADE)
    dev_id = models.PositiveIntegerField(blank=True, null=True, editable=False)
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        super(Marker, self).save(*args, **kwargs)
        if self.wallview.is_dev and not self.dev_id:
            self.dev_id = self.pk
            super(Marker, self).save(*args, **kwargs)


class SimpleMarker(Marker):
    """Locations on a wall view that represent real-world objects on a wall
    such as bolts, belays etc.
    """
    _icon_paths = {
        'bolt': 'generic_markers/img/bolt_icon.png',
        'belay': 'generic_markers/img/belay_icon.png',
        'abseil_anchor': 'generic_markers/img/single_belay_icon.png'
    }
    MARKERTYPE_CHOICES = (
        ('bolt', _('Bolt')),
        ('belay', _('Belay')),
        ('abseil_anchor', _('Abseil Anchor'))
    )
    markertype = models.CharField(max_length=20, choices=MARKERTYPE_CHOICES)

    @property
    def icon_url(self):
        return os.path.join(
            settings.STATIC_URL,
            self._icon_paths[self.markertype]
        )

    @property
    def add_url(self):
        return reverse(
            'generic_markers:add_simple_marker',
            kwargs={
                'wallview_id': self.wallview.pk,
                'markertype': self.markertype,
            })

    @property
    def relocate_url(self):
        return reverse(
            'generic_markers:relocate_simple_marker',
            kwargs={
                'marker_id': self.pk
            })


    @property
    def popup_url_edit(self):
        return reverse('generic_markers:simple_marker_detail', kwargs={'marker_id': self.pk})

    @property
    def icon_config(self):
        return {
            'iconUrl': self.icon_url,
            'iconSize': [22, 22],
            'iconAnchor': [11, 11],
            'popupAnchor': [0, -5]
        }

class RelationalMarker(Marker):
    """Locations on a wall view that link to other wallviews.
    """
    _icon_paths = {
        'left': 'generic_markers/img/left_wall.png',
        'right': 'generic_markers/img/right_wall.png',
        'above': 'generic_markers/img/above_wall.png',
        'below': 'generic_markers/img/below_wall.png',
        'above_left': 'generic_markers/img/leftabove_wall.png',
        'above_right': 'generic_markers/img/rightabove_wall.png',
        'below_left': 'generic_markers/img/leftbelow_wall.png',
        'below_right': 'generic_markers/img/rightbelow_wall.png',
    }
    MARKERTYPE_CHOICES = (
        ('left', _('Wall to the left')),
        ('right', _('Wall to the right')),
        ('above', _('Wall above')),
        ('below', _('Wall below')),
        ('above_left', _('Wall above to the left')),
        ('above_right', _('Wall above to the right')),
        ('below_left', _('Wall below to the left')),
        ('below_right', _('Wall below to the right'))
    )
    markertype = models.CharField(max_length=20, choices=MARKERTYPE_CHOICES)
    related_wall = models.ForeignKey('miroutes.Wall', on_delete=models.CASCADE)

    @property
    def icon_url(self):
        return os.path.join(
            settings.STATIC_URL,
            self._icon_paths[self.markertype]
        )

    @property
    def popup_on_hover(self):
        return True

    @property
    def popup_url(self):
        """This property defines the url from which the popup content
        is ajaxed.
        """
        if hasattr(self, 'related_wall'):
            return reverse('miroutes:wall_popup', kwargs={'wall_id': self.related_wall.pk})
        else:
            return ''


    @property
    def click_url(self):
        if hasattr(self, 'related_wall'):
            return self.related_wall.click_url
        else:
            return ''

    @property
    def popup_url_edit(self):
        return reverse(
            'generic_markers:relational_marker_detail',
            kwargs={
                'marker_id': self.pk
            })

    @property
    def popup_add_url(self):
        return reverse(
            'generic_markers:add_relational_marker',
            kwargs={
                'wallview_id': self.wallview.pk,
                'markertype': self.markertype,
            })

    @property
    def relocate_url(self):
        return reverse(
            'generic_markers:relocate_relational_marker',
            kwargs={
                'marker_id': self.pk
            })


    @property
    def icon_config(self):
        return {
            'iconUrl': self.icon_url,
            'iconSize': [36, 36],
            'iconAnchor': [18, 18],
            'popupAnchor': [0, -5]
        }

    @property
    def marker_json_editdisplay(self):
        """Special marker to be displayed on draw routes without being editable,
        but redirecting to other walls draw routes views.
        """
        marker_dict = self.marker_dict
        if hasattr(self, 'related_wall'):
            marker_dict['click_url'] = reverse('edit_spot:draw_routes', kwargs={'wall_id': self.related_wall.pk})
        return json.dumps(marker_dict)

    @property
    def marker_dict(self):
        m_dict = super(RelationalMarker, self).marker_dict
        if hasattr(self, 'related_wall'):
            m_dict['label_text'] = self.related_wall.name
        return m_dict



class TextMarker(Marker):
    """Put little notes on a wallview with an exclamation mark icon."""

    text = models.CharField(max_length=200)

    @property
    def markertype(self):
        return _("Information")

    @property
    def icon_url(self):
        return os.path.join(
            settings.STATIC_URL,
            'generic_markers/img/note_icon.png'
        )

    @property
    def popup_on_hover(self):
        return False

    @property
    def icon_config(self):
        return {
            'iconUrl': self.icon_url,
            'iconSize': [16, 16],
            'iconAnchor': [8, 8],
            'popupAnchor': [0, -5]
        }

    @property
    def popup_url_edit(self):
        return reverse(
            'generic_markers:text_marker_detail',
            kwargs={
                'marker_id': self.pk
            })

    @property
    def popup_add_url(self):
        return reverse(
            'generic_markers:add_text_marker',
            kwargs={
                'wallview_id': self.wallview.pk,
            })

    @property
    def relocate_url(self):
        return reverse(
            'generic_markers:relocate_text_marker',
            kwargs={
                'marker_id': self.pk
            })



    @property
    def marker_dict(self):
        """Config dict for the marker. This is used for one-shot setup of
        a text marker on a non-edit template.

        Note that for a text-type marker, the text content is written to marker_text
        property, while in a note-type marker, the text appears in the popup.
        See leaflet_base and the create_marker function for details.
        """
        mdict = {
            'icon_config': self.icon_config,
            'popup_on_hover': self.popup_on_hover,
            'popup_content': escape(self.text),
            'marker_text': None,
        }
        if(self.pnt):
            mdict['point'] = self.pnt.coords

        return mdict

    @property
    def marker_dict_edit(self):
        """Config for text markers on edit templates (decorate_wall).

        In the edit version we set the marker text for the text icon.
        In the popup we load the same template for both note and text style markers.
        """
        mdict = {
            'icon_config': self.icon_config,
            'popup_on_hover': False,
            'popup_url': self.popup_url_edit,
            'marker_text': None,
            'relocate_url': self.relocate_url,
        }
        if(self.pnt):
            mdict['point'] = self.pnt.coords
        return mdict

class PitchMarker(Marker):
    """Marker for single pitches in multi-pitch routes."""
    length = models.PositiveSmallIntegerField()
    grade = models.IntegerField(choices=GRADE_CHOICES)
    number_of_bolts = models.PositiveSmallIntegerField(blank=True, null=True)

    @property
    def markertype(self):
        return _("Multi-Pitch Label")

    @property
    def icon_url(self):
        return os.path.join(
            settings.STATIC_URL,
            'generic_markers/img/pitch_icon.png'
        )

    @property
    def icon_config(self):
        return {
            'iconUrl': self.icon_url,
            'iconSize': [16, 16],
            'iconAnchor': [8, 8],
            'popupAnchor': [0, -5]
        }

    @property
    def popup_url_edit(self):
        return reverse(
            'generic_markers:pitch_marker_detail',
            kwargs={
                'marker_id': self.pk
            })

    @property
    def popup_add_url(self):
        return reverse(
            'generic_markers:add_pitch_marker',
            kwargs={
                'wallview_id': self.wallview.pk,
            })

    @property
    def relocate_url(self):
        return reverse(
            'generic_markers:relocate_pitch_marker',
            kwargs={
                'marker_id': self.pk
            })


    @property
    def marker_dict(self):
        """Config dict for the marker. This is used for one-shot setup of
        a text marker on a non-edit template.

        Note that for a text-type marker, the text content is written to marker_text
        property, while in a note-type marker, the text appears in the popup.
        See leaflet_base and the create_marker function for details.
        """
        if(self.length and self.grade):
            bolt_text = ' ({})'.format(self.number_of_bolts) if self.number_of_bolts else ''
            marker_txt = '<div style="text-align:center;">{}<br><small>{}m{}</small></div>'.format(
                self.get_grade_display(),
                self.length,
                bolt_text
            )
        elif(self.length):
            marker_txt = '{}m'.format(self.length)
        elif(self.grade):
            marker_txt = '{}'.format(self.get_grade_display())
        else:
            marker_txt = 'n.a.'
        mdict = {
            'icon_config': self.icon_config,
            'popup_on_hover': False,
            'popup_url': None,
            'marker_text': marker_txt
        }
        if(self.pnt):
            mdict['point'] = self.pnt.coords

        return mdict

    @property
    def marker_dict_edit(self):
        """Config for text markers on edit templates (decorate_wall).

        In the edit version we set the marker text for the text icon.
        In the popup we load the same template for both note and text style markers.
        """
        mdict = self.marker_dict
        mdict['popup_url'] = self.popup_url_edit
        mdict['relocate_url'] = self.relocate_url
        return mdict
