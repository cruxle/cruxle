"""Views related to the management of the markers on maps and walls."""
import json
from django.shortcuts import get_object_or_404, render, redirect

from django.contrib.gis.geos import Point
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.http import HttpResponse
from django.http import JsonResponse

from miroutes.models import WallView
from miroutes.models import Wall
from miroutes.models import get_grade_system

from edit_spot.utils import check_edit_lock

from generic_markers.models import SimpleMarker
from generic_markers.models import RelationalMarker, TextMarker, PitchMarker

from generic_markers.forms import RelationalMarkerForm, TextMarkerForm, PitchMarkerForm, RelationalModelChoiceField

def errors_to_json(errors):
    """
    Convert a Form error list to JSON::
    """
    return dict(
        (k, list(map(str, v)))
        for (k,v) in errors.items()
    )

@permission_required('miroutes.change_wall')
def add_simple_marker(request, wallview_id, markertype):

    wallview = get_object_or_404(WallView, pk=wallview_id)
    lat = request.GET.get('lat', None)
    lng = request.GET.get('lng', None)

    if request.is_ajax():

        pnt = Point(float(lat), float(lng))

        location = SimpleMarker(wallview=wallview, markertype=markertype, pnt=pnt)
        location.save()
        payload = {
            'message': 'Successfully created {}.'.format(markertype),
            'data': location.marker_json_edit,
            'success': True
        }
        return JsonResponse(json.dumps(payload), safe=False)


@permission_required('miroutes.change_wall')
def relational_marker_detail(request, marker_id):
    next_page = request.GET.get('next', None)

    marker = get_object_or_404(RelationalMarker, pk=marker_id)

    context = {}

    if next_page is not None:
        context['next'] = next_page

    if request.method == 'POST':

        form = RelationalMarkerForm(request.POST, instance=marker)
        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Wall Link update successful.',
                'data': marker.marker_json_edit,
                'success': True
            }
        else:

            payload = {
                'message': 'Could not update Wall Link.',
                'data': errors_to_json(form.errors),
                'success': False
            }

        return JsonResponse(json.dumps(payload), safe=False)

    else:
        wall = marker.wallview.wall

        nearby_walls = Wall.objects.exclude(pk=wall.pk).filter(pnt__distance_lt=(wall.pnt, D(km=2))).annotate(
            distance=Distance('pnt', wall.pnt)).order_by('distance')
        form = RelationalMarkerForm(instance=marker, nearby_walls=nearby_walls)

    context['form'] = form
    context['marker'] = marker

    if request.session.get('last_wall_id'):
        context['last_wall_id'] = request.session['last_wall_id']

    return render(request, 'generic_markers/relational_details.html', context)

@permission_required('miroutes.change_wall')
def text_marker_detail(request, marker_id):
    next_page = request.GET.get('next', None)

    marker = get_object_or_404(TextMarker, pk=marker_id)

    context = {}

    if next_page is not None:
        context['next'] = next_page

    if request.method == 'POST':

        form = TextMarkerForm(request.POST, instance=marker)
        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Text info update successful.',
                'data': marker.marker_json_edit,
                'success': True
            }
        else:
            payload = {
                'message': 'Could not update Text info.',
                'data': errors_to_json(form.errors),
                'success': False
            }

        return JsonResponse(json.dumps(payload), safe=False)

    else:
        form = TextMarkerForm(instance=marker)

    context['form'] = form
    context['marker'] = marker

    if request.session.get('last_wall_id'):
        context['last_wall_id'] = request.session['last_wall_id']

    return render(request, 'generic_markers/text_details.html', context)

@permission_required('miroutes.change_wall')
def pitch_marker_detail(request, marker_id):

    marker = get_object_or_404(PitchMarker, pk=marker_id)

    context = {}

    if request.method == 'POST':

        form = PitchMarkerForm(request.POST, instance=marker)
        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Pitch Info update successful.',
                'data': marker.marker_json_edit,
                'success': True
            }
        else:
            payload = {
                'message': 'Could not update Pitch Info.',
                'data': errors_to_json(form.errors),
                'success': False
            }

        return JsonResponse(json.dumps(payload), safe=False)

    else:
        form = PitchMarkerForm(
            instance=marker,
            initial={'grade_system': get_grade_system(marker.wallview.wall)})

    context['form'] = form
    context['marker'] = marker

    return render(request, 'generic_markers/pitch_details.html', context)

@permission_required('miroutes.change_wall')
def add_relational_marker(request, wallview_id, markertype):

    wallview = get_object_or_404(WallView, pk=wallview_id)

    if request.method == 'POST':

        form = RelationalMarkerForm(request.POST)

        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Successfully created Wall Link to {}.'.format(marker.related_wall.name),
                'data': marker.marker_json_edit,
                'success': True
            }
        else:
            payload = {
                'message': 'Could not create Wall Link.',
                'data': errors_to_json(form.errors),
                'success': False
            }
        return JsonResponse(json.dumps(payload), safe=False)

    else:
        lat = request.GET.get('lat', None)
        lng = request.GET.get('lng', None)

        context = {
            'wallview': wallview,
            'markertype': markertype,
            'lat': lat,
            'lng': lng
        }

        pnt = Point(float(lat), float(lng))

        wall = wallview.wall
        nearby_walls = Wall.objects.exclude(pk=wall.pk).filter(pnt__distance_lt=(wall.pnt, D(km=2))).annotate(
            distance=Distance('pnt', wall.pnt)).order_by('distance')

        form = RelationalMarkerForm(initial={
            'wallview': wallview,
            'pnt': pnt,
            'markertype': markertype
        }, nearby_walls=nearby_walls)


        context['form'] = form
        return render(request, 'generic_markers/relational_details.html', context)

@permission_required('miroutes.change_wall')
def add_text_marker(request, wallview_id):

    wallview = get_object_or_404(WallView, pk=wallview_id)
    lat = request.GET.get('lat', None)
    lng = request.GET.get('lng', None)

    context = {
        'wallview': wallview,
        'lat': lat,
        'lng': lng
    }

    if request.method == 'POST':

        form = TextMarkerForm(request.POST)
        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Successfully created Info marker.',
                'data': marker.marker_json_edit,
                'success': True
            }
        else:
            payload = {
                'message': 'Could not create Info marker.',
                'data': errors_to_json(form.errors),
                'success': False
            }
        return JsonResponse(json.dumps(payload), safe=False)

    else:
        pnt = Point(float(lat), float(lng))

        form = TextMarkerForm(initial={
            'wallview': wallview,
            'pnt': pnt,
        })

        context['form'] = form

        return render(request, 'generic_markers/text_details.html', context)

@permission_required('miroutes.change_wall')
def add_pitch_marker(request, wallview_id):

    wallview = get_object_or_404(WallView, pk=wallview_id)
    lat = request.GET.get('lat', None)
    lng = request.GET.get('lng', None)

    context = {
        'wallview': wallview,
        'lat': lat,
        'lng': lng
    }

    if request.method == 'POST':

        form = PitchMarkerForm(request.POST)
        if form.is_valid():
            marker = form.save()
            payload = {
                'message': 'Successfully created Pitch information.',
                'data': marker.marker_json_edit,
                'success': True
            }
        else:
            payload = {
                'message': 'Could not create Pitch information.',
                'data': errors_to_json(form.errors),
                'success': False
            }
        return JsonResponse(json.dumps(payload), safe=False)

    else:
        pnt = Point(float(lat), float(lng))

        form = PitchMarkerForm(initial={
            'wallview': wallview,
            'pnt': pnt,
            'grade_system': get_grade_system(wallview.wall)
        })

        context['form'] = form

        return render(request, 'generic_markers/pitch_details.html', context)

@permission_required('miroutes.change_wall')
def simple_marker_detail(request, marker_id, **kwargs):
    """Delete a wallview location object."""

    marker = get_object_or_404(SimpleMarker, pk=marker_id)

    context = {
        'marker': marker,
    }

    return render(request, 'generic_markers/simple_details.html', context)


def del_marker(request, marker):
    """Generic marker deletion method. Returns JSON on deletion."""
    if check_edit_lock(marker.wallview.wall, request.user):

        payload = {
            'message': 'Marker #{} deleted.'.format(marker.pk),
            'data': None,
            'success': True
        }
        marker.delete()
    else:
        payload = {
            'message': 'Marker not deleted, your edit session is invalid.'.format(marker.id),
            'data': None,
            'success': False
        }

    return JsonResponse(json.dumps(payload), safe=False)


@permission_required('miroutes.change_wall')
def del_simple_marker(request, marker_id, **kwargs):
    """Delete a simple marker (bolt, belay, etc). To be ajaxed."""

    return del_marker(request, get_object_or_404(SimpleMarker, pk=marker_id))



@permission_required('miroutes.change_wall')
def del_relational_marker(request, marker_id, **kwargs):
    """Delete a link to another wall. To be ajaxed."""

    return del_marker(request, get_object_or_404(RelationalMarker, pk=marker_id))

@permission_required('miroutes.change_wall')
def del_text_marker(request, marker_id, **kwargs):
    """Delete a text marker. To be ajaxed."""

    return del_marker(request, get_object_or_404(TextMarker, pk=marker_id))

@permission_required('miroutes.change_wall')
def del_pitch_marker(request, marker_id, **kwargs):
    """Delete a pitch marker. To be ajaxed."""

    return del_marker(request, get_object_or_404(PitchMarker, pk=marker_id))


def relocate_marker(marker, request):
    if check_edit_lock(marker.wallview.wall, request.user):
        lat = request.GET.get('lat', None)
        lng = request.GET.get('lng', None)

        marker.pnt = Point(float(lat), float(lng))
        marker.save()
        payload = {
            'message': 'Marker #{} moved to (lat: {}, lng {}).'.format(marker.id, lat, lng),
            'data': None,
            'success': True
        }
    else:
        payload = {
            'message': 'Marker #{} not saved, your edit session is invalid.'.format(marker.id),
            'data': None,
            'success': False
        }
    return JsonResponse(json.dumps(payload), safe=False)



@permission_required('miroutes.change_wall')
def relocate_simple_marker(request, marker_id):
    marker = get_object_or_404(SimpleMarker, pk=marker_id)
    return relocate_marker(marker, request)

@permission_required('miroutes.change_wall')
def relocate_relational_marker(request, marker_id):
    marker = get_object_or_404(RelationalMarker, pk=marker_id)
    return relocate_marker(marker, request)

@permission_required('miroutes.change_wall')
def relocate_text_marker(request, marker_id):
    marker = get_object_or_404(TextMarker, pk=marker_id)
    return relocate_marker(marker, request)

@permission_required('miroutes.change_wall')
def relocate_pitch_marker(request, marker_id):
    marker = get_object_or_404(PitchMarker, pk=marker_id)
    return relocate_marker(marker, request)

def wall_markers(request, *args, **kwargs):

    inactive_only = request.GET.get('inactive_only', False)
    show_subwalls = request.GET.get('show_subwalls', False)
    show_all = request.GET.get('show_all', False)


    if request.is_ajax():
        wall_listing = Wall.objects.all()
        if inactive_only:
            wall_listing = wall_listing.filter(is_active=False)
        if not show_subwalls:
            wall_listing = wall_listing.filter(subwall_of__isnull=False).distinct()
        if show_all:
            wall_listing = Wall.objects.all()

        wall_id = request.GET.get('editwall_id')
        if wall_id:
            wall_listing = wall_listing.exclude(pk=int(wall_id))
            marker_list = [wall.marker_json_editwall for wall in wall_listing]
        else:
            marker_list = [wall.marker_json for wall in wall_listing]
        return JsonResponse(json.dumps(marker_list), safe=False)


def simple_wallmarkers(request, wallview_id, *args, **kwargs):
    wallview = get_object_or_404(WallView, pk=wallview_id)
    if request.is_ajax():
        marker_query = wallview.simplemarker_set.all()
        marker_list = [marker.marker_json for marker in marker_query]
        return JsonResponse(json.dumps(marker_list), safe=False)

def relational_wallmarkers(request, wallview_id, *args, **kwargs):
    wallview = get_object_or_404(WallView, pk=wallview_id)
    if request.is_ajax():
        marker_query = wallview.relationalmarker_set.all()
        if request.GET.get('on_edit'):
            marker_list = [marker.marker_json_editdisplay for marker in marker_query]
        else:
            marker_list = [marker.marker_json for marker in marker_query]
        return JsonResponse(json.dumps(marker_list), safe=False)

def text_wallmarkers(request, wallview_id, *args, **kwargs):
    wallview = get_object_or_404(WallView, pk=wallview_id)
    if request.is_ajax():
        marker_query = wallview.textmarker_set.all()
        marker_list = [marker.marker_json for marker in marker_query]
        return JsonResponse(json.dumps(marker_list), safe=False)

def pitch_wallmarkers(request, wallview_id, *args, **kwargs):
    wallview = get_object_or_404(WallView, pk=wallview_id)
    if request.is_ajax():
        marker_query = wallview.pitchmarker_set.all()
        marker_list = [marker.marker_json for marker in marker_query]
        return JsonResponse(json.dumps(marker_list), safe=False)
