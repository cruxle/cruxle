from django import forms
from generic_markers.models import RelationalMarker, TextMarker, PitchMarker
from miroutes.models import Wall, get_grade_system
from miroutes.model_choices import GRADE_SYSTEMS
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Submit, Layout, Field, Fieldset,
        Button, HTML, Row, MultiField, Div)
from crispy_forms.bootstrap import (AppendedText, PrependedText)

from django.utils.translation import ugettext_lazy as _

class RelationalModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return str("{0} ({1:.2f} {2})").format(obj.__str__(), obj.distance.km, obj.distance.unit_attname('Kilometer'))

class RelationalMarkerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        nearby_walls = kwargs.pop('nearby_walls', None)
        super(RelationalMarkerForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = 'relational_marker_form'
        self.helper.form_method = 'post'
        
        self.helper.layout = Layout(*list(self.fields.keys())) # use all fields
        # note that if the kwarg is given, it is a list that is either empty or not
        # than we have to care about the form field display
        # when init is called with POST data, the kwarg is not present and we just do nothing
        set_save_btn_disabled = False
        if nearby_walls is not None:
            if len(nearby_walls) > 0:
                self.fields['related_wall'] = RelationalModelChoiceField(nearby_walls)
            else:
                self.fields['related_wall'] = forms.CharField(initial=_("No walls within 2km"), disabled=True)
                set_save_btn_disabled = True

        # Beware here that we need to quote the template curly brackets with additional ones {} because of the python string formatting
        self.helper.layout.append(HTML("""
      {% load i18n %}
      {% if marker.id %}
        <a href="{% url 'edit_spot:decorate_wall' wall_id=marker.related_wall.pk %}"
           class="btn btn-link">
          <i class="glyphicon glyphicon-link"></i> {% trans "Follow Link" %}
        </a>
        <button data-url="{% url 'generic_markers:relational_marker_detail' marker_id=marker.id %}"
                class="submit-button btn btn-success">
          <i class="glyphicon glyphicon-floppy-save"></i> {% trans "Save"%}
        </button>
        <button data-url="{% url 'generic_markers:del_relational_marker' marker_id=marker.id %}"
                class="delete-button btn btn-primary">
          <i class="glyphicon glyphicon-trash"></i>
        </button>
      {% else %}
        <button id="submit-form"
                data-url="{% url 'generic_markers:add_relational_marker' wallview_id=wallview.id markertype=markertype %}"
                class="submit-button btn btn-success"
                """+ ("disabled" if set_save_btn_disabled else "") +""">
            <i class="glyphicon glyphicon-floppy-save"></i> {% trans "Save"%}
        </button>
        <button id="delete-form" class="delete-button btn btn-primary">
            <i class="glyphicon glyphicon-trash"></i>
        </button>
      {% endif %}
        """))

    class Meta:
        model = RelationalMarker
        exclude = ()
        widgets = {
            'wallview': forms.HiddenInput(),
            'markertype': forms.HiddenInput(),
            'pnt': forms.HiddenInput(),
        }


class TextMarkerForm(forms.ModelForm):
    class Meta:
        model = TextMarker
        exclude = ()
        widgets = {
            'wallview': forms.HiddenInput(),
            'pnt': forms.HiddenInput(),
            'text': forms.TextInput(attrs={'placeholder': _('Info Text'), 'autofocus': ''}),
        }

class PitchMarkerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PitchMarkerForm, self).__init__(*args, **kwargs)

        self.fields['grade_system'] = forms.ChoiceField(
            choices=GRADE_SYSTEMS,
        )
        # Use crispy-forms to generate the code
        self.helper = FormHelper(self)
        self.helper.form_id = 'text_marker_form'
        self.helper.form_method = 'post'
        self.helper.form_action = 'javascript:0'

        self.helper.layout = Layout(
            Field('grade_system', id='grade-system-selector'),
            'grade',
            AppendedText('length','m'),
            AppendedText('number_of_bolts','#'),
            'pnt',
            'wallview'
                )

        self.helper.layout.append(HTML("""
        {% if marker.id %}
        <button data-url="{% url 'generic_markers:pitch_marker_detail' marker_id=marker.id %}" class="submit-button btn btn-success">
        <i class="glyphicon glyphicon-floppy-save"></i>""" + str(_("Save")) + """
        </button>
        <a data-url="{% url 'generic_markers:del_pitch_marker' marker_id=marker.id %}" href="#" class="delete-button btn btn-primary">
        <i class="glyphicon glyphicon-trash"></i>
        </a>
        {% else %}
        <button id="submit-form" data-url="{% url 'generic_markers:add_pitch_marker' wallview_id=wallview.id %}" class="submit-button btn btn-success">
        <i class="glyphicon glyphicon-floppy-save"></i>""" + str(_("Save")) + """
        </button>
        <a id="delete-form" class="delete-button btn btn-danger">
        <i class="glyphicon glyphicon-trash"></i>
        </a>
        {% endif %}
        """))

    class Meta:
        model = PitchMarker
        fields = ['wallview', 'pnt', 'grade', 'length', 'number_of_bolts']
        widgets = {
            'wallview': forms.HiddenInput(),
            'pnt': forms.HiddenInput(),
        }
