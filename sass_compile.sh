#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/static/topo
sass --watch scss/styles.scss:css/styles.css
