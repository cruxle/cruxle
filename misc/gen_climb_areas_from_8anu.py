#!/bin/env python3
from bs4 import BeautifulSoup
from urllib import parse

from selenium import webdriver
browser = webdriver.Chrome()

base_url = 'https://www.8a.nu/'

country_urls = set()
visited_urls = set()

all_results = []

def soupify_url(url):
    browser.get(url)
    html = browser.page_source
    soup = BeautifulSoup(html)
    return soup
    #import mechanize
    #print('Loading URL', url)
    #br = mechanize.Browser()
    #br.set_handle_robots(False)   # ignore robots
    #br.set_handle_refresh(False)  # can sometimes hang without this
    #br.addheaders = [('User-agent', 'Firefox')]

    #page = br.open(url)
    #html = page.read().decode('utf-8')
    #soup = BeautifulSoup(html)
    #return soup

    #import urllib2
    #html = urllib2.urlopen(url).read().decode('utf-8')
    #soup = BeautifulSoup(html)
    #return soup

def find_country_urls(soup):
    right_hand_div = soup.find('div', attrs={'id':'Country'})

    ctables = soup.find('div', attrs={'id':'Country'}).findAll('table')
    itable_Country = [ 'Country' in _.td.getText() for _ in ctables].index(True)
    country_table = ctables[itable_Country]

    country_urls = set([ _.get('href') for _ in country_table.findAll('a')])
    return country_urls

def visit_spot_url(spot_url):
    soup = soupify_url(base_url+spot_url)
    if soup.find('span', attrs={'id':'ctl00_ContentPlaceholder_LabelUpdateMap'}):
        print('No pnt info for', spot_url)
        loc = None
    else:
        src = soup.find('img', attrs={'alt':'Map Crag'}).get('src')
        query = parse.parse_qs(parse.urlparse(src).query)
        loc = query['markers']

    name = soup.find('span', attrs={'id':'ctl00_ContentPlaceholder_LabelCragName'}).getText()

    return {'name':name, 'loc':loc, }


def scan_through_all_country_urls(country_urls):
    for country_url in list(country_urls)[:20]:
        if country_url in visited_urls:
            print('Skipping ',country_url)
            continue
        visited_urls.update([country_url,])

        soup = soupify_url(base_url+country_url)

        country_urls.update(find_country_urls(soup))


        table_rows = soup.find('div', attrs={'id':'List'}).findAll('tr')

        for tr in table_rows:
            if tr.td.a:
                spot_url = tr.td.a.get('href')
                print('Found Spot',spot_url)
                if spot_url in visited_urls:
                    print('Skipping ',spot_url)
                    continue
                visited_urls.update([spot_url,])

                res = visit_spot_url(spot_url)
                print(res)

        all_results.append(res)


soup = soupify_url(base_url+'/crags/routes/')
country_urls.update(find_country_urls(soup))
scan_through_all_country_urls(country_urls)

