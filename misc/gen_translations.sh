#!/bin/bash

docker-compose run --rm web python3 manage.py makemessages -a
docker-compose run --rm web python3 manage.py makemessages -d djangojs -a

#for d in locale/*
#do
#    msgcat $d/LC_MESSAGES/django*.po > $d/LC_MESSAGES/merged.po
#done

vim locale/de/LC_MESSAGES/*.po

docker-compose run --rm web python3 manage.py compilemessages
