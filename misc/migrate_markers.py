# coding: utf-8
from miroutes.models import *
    
for sp in Wall.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))

for sp in Spot.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))

for sp in Route.objects.all():
    if sp.walls.count():
        sp.pnt = sp.walls.all()[0].wall.pnt
    else:
        sp.pnt = sp.spot.pnt
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
from generic_markers.models import *
for sp in ParkingLocation.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
for sp in SimpleMarker.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
for sp in RelationalMarker.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
for sp in PitchMarker.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
for sp in TextMarker.objects.all():
    sp.pnt = 'POINT({} {})'.format(sp.point[0], sp.point[1])
    try:
        sp.save()
    except:
        print("Could not save {}.".format(sp.name))
    
