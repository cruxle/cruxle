#!/usr/bin/env bash
TAGSFILE=.topo_ctags

EXCLUDE='*.pyc *.mod'

ctags -R --extra=f --langdef=file --langmap=FILE:.html.xml.yml.js.css --exclude="$EXCLUDE" -o $TAGSFILE ./
