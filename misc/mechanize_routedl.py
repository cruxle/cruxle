#!/usr/bin/env python
import re
import pickle
import os

def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def save_obj(obj, name ):
    with open(name, 'wb') as f:
        protocol = 0 # pickle.HIGHEST_PROTOCOL for binary format
        pickle.dump(obj, f, protocol)


def load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f)


def compare(s1, s2):
    import string
    remove = string.punctuation + string.whitespace
    return s1.lower().translate(None, remove) == s2.lower().translate(None, remove)


def get_routes_from_spot(url, follow=True):
    from BeautifulSoup import BeautifulSoup as BS
    import mechanize

    br = mechanize.Browser()
    br.set_handle_robots(False)   # ignore robots
    br.set_handle_refresh(False)  # can sometimes hang without this
    br.addheaders = [('User-agent', 'Firefox')]

    index = br.open(url)
    index_html = BS(index.read())

    route_site_links = {} # put here all links of the various pages

    def next_route_page(url):
        r = br.open(url)
        html = BS(r.read())
        next_links = html.findAll('li', attrs={"class" : 'next'})
        try:
            new_url = next_links[0].find('a')
            return new_url.get('href')
        except:
            return None

    next_url = url # 'initial_page'
    if follow:
        while next_url != None:
            print('Adding next_url:', next_url, follow)
            route_site_links[next_url] = 'add url through next iterations'
            next_url = next_route_page(next_url)
    else:
        route_site_links[next_url] = 'initial_page'

    ROUTES = []

    for site_link in list(route_site_links.keys()):
      print('visitng url:', site_link)
      r = br.open(site_link)

      html = BS(r.read())
      routes = html.findAll('span', attrs={"class" : 'route'})

      route_urls = [r.find('a').get('href') for r in routes]

      for route_url in route_urls:
        try:
            r = br.open(route_url)
            html = BS(r.read().decode('utf-8'))

            name = html.find('span', attrs={"itemprop" : 'name'}).text
            grade = html.find('span', {'class': lambda x: x and 'grade' in x.split()}).text

            route_infos = html.findAll('dl', attrs={"class" : 'areaInfo'})[-1]
            info_str = route_infos.text.replace('\n','')
            location = re.match(r'.*Located in (.*) approx', info_str).groups(0)[0]

            lat, lon = re.match(r'.*Lat\/Long:.*?(\d+\.\d+),(\d+\.\d+)', info_str).groups(0)

            height = None
            for li in html.find('ul', {'class': 'stats'}).findAll('li'):
                tmp = re.match(r'Height:(\d+)m', li.text)
                if tmp:
                    height=tmp.groups(0)[0]

            route = {
              'name': name,
              'grade': grade,
              'location': location,
              'lat': lat,
              'lon': lon,
              'height': height,
            }


            print(route)
            ROUTES.append(route)
        except Exception as e:
            print(("Error occured adding a route, skipping:",route_url,"::",e))

    return ROUTES


def kochel():
    if not os.path.exists('KOCHEL_ROUTES.pkl'):
        kochel_url = 'https://www.thecrag.com/climbing/germany/kochel/routes'
        ROUTES = get_routes_from_spot(kochel_url)
        save_obj(ROUTES, 'KOCHEL_ROUTES.pkl')
    else:
        ROUTES = load_obj('KOCHEL_ROUTES.pkl')

    return ROUTES


def insert_routes(routes):
    from miroutes.model_choices import GRADE_CHOICES, UIAA_NUMERIC_2_ROMAN, GRADE_CHOICES_ROMAN_2_PK
    from miroutes.models import Route

    for r in routes:
        if not Route.objects.filter(name__icontains=r['name']).exists():
            try:
                try:
                    grade_id = GRADE_CHOICES_ROMAN_2_PK[UIAA_NUMERIC_2_ROMAN[r['grade']]]
                except:
                    grade_id = 0

                descr = 'Located @ {}'.format(r['location'])
                height = r['height']
                if height is not None:
                    new_route = Route(name=r['name'], pnt='POINT({} {})'.format(r['lat'],r['lon']), grade=grade_id, description=descr, length=height)
                else:
                    new_route = Route(name=r['name'], pnt='POINT({} {})'.format(r['lat'],r['lon']), grade=grade_id, description=descr)
                print('Creating route:', new_route)
                new_route.save()
            except Exception as e:
                print('Error creating route:', r.items(), 'because:', e)
        else:
            print(('{} already exists... skipping.'.format(r['name'])))


def _main():
    import argparse
    parser = argparse.ArgumentParser(description='Automated download of routes')
    parser.add_argument('base_url', type=str, help='url such as https://www.thecrag.com/climbing/germany/kochel/routes')
    parser.add_argument('output_pickle', default='routes.pkl', help='output file name')
    parser.add_argument('--follow', dest='follow', action='store_true')
    parser.add_argument('--no-follow', dest='follow', action='store_false')
    parser.set_defaults(follow=True)
    args = parser.parse_args()

    if not os.path.exists(args.output_pickle):
        routes = get_routes_from_spot(args.base_url, follow=args.follow)
        save_obj(routes, args.output_pickle)
    else:
        routes = load_obj(args.output_pickle)
        for route in routes:
          print(route)

if __name__ == '__main__':
    _main()
