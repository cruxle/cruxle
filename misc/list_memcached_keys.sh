docker-compose run web bash -c "pip install 'git+git://github.com/dlrust/python-memcached-stats.git' && python3 -c '\
import memcached_stats; \
mem = memcached_stats.MemcachedStats(\"memcached\", \"11211\"); \
print(\"Status:  \", mem.stats())
print(\"Keys:    \", mem.keys())
'"
