#!/usr/bin/env bash

echo "This script runs on backup server -- this is here in the repo to keep everythin in one place..."
exit

cd /home/mitopo_backup/

rm -rf backup.8

for i in {7..0}
do
  mv backup.$i backup.$(($i+1))
done

rsync --partial --progress -a --delete --link-dest=../backup.1 -e "ssh -p 22" gucki@topo:/storage/sites/mitopo.de/ backup.0

ssh -p 22 gucki@topo "cd topo; docker-compose run --rm web ./manage.py dumpdata" > backup.0/mitopo_db.json
ssh -p 22 gucki@topo "cd topo; docker-compose run --rm web pg_dump -h postgresql -U postgres --clean" > backup.0/mitopo_db.pgdump
