import reversion
from reversion.models import Revision, Version

from miroutes.models import Route, Wall

def clear_revisions():
    """Delete all existing revisions."""
    for rev in Revision.objects.all():
        rev.delete()

def create_obj_revision(user, comment, obj):
    """Create a revision for a single object."""
    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(comment)
        try:
            obj.save()
        except TypeError:
            # walls sometimes throw errors due to their images
            print("The object {} could not be saved.".format(obj.name))

def create_wallview_revision(user, comment, wallview):
    """Wallview revisions are a little more complicated."""
    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(comment)
        reversion.add_to_revision(wallview)
        for rgeom in wallview.routegeometry_set.all():
            rgeom.save()
        for smarker in wallview.simplemarker_set.all():
            smarker.save()
        for rmarker in wallview.relationalmarker_set.all():
            rmarker.save()
        for tmarker in wallview.textmarker_set.all():
            tmarker.save()
        for pmarker in wallview.pitchmarker_set.all():
            pmarker.save()

def create_initial_revisions(user, clear_all=False):
    """Create a new set of revisions."""
    if clear_all:
        clear_revisions()
    comment = "Initial version."
    # Routes
    for route in Route.objects.all():
        create_obj_revision(user, comment, route)
    # Walls
    for wall in Wall.objects.all():
        create_obj_revision(user, comment, wall)
    # WallViews
    for wall in Wall.objects.all():
        if wall.is_active:
            create_wallview_revision(user, comment, wall.pub_view)

