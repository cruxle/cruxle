#!/bin/bash
#
# Script to setup inital project
# includes 
#   docker image bulding
#   creation of superuser
#   insert test wall with 2 routes

# fail if one of the commands fails
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# then use docker-compose up to restart all containers and run them in background
docker-compose -f docker-compose-staging.yml up -d
sleep 30

# make migrations and migrate to setup db
docker-compose -f docker-compose-staging.yml run --rm web sh -c 'python3 manage.py migrate'
docker-compose -f docker-compose-staging.yml run --rm web sh -c 'python3 manage.py migrate miroutes'
sleep 5

# Create root admin user
docker-compose -f docker-compose-staging.yml run --rm web python3 manage.py create_admin

docker-compose -f docker-compose-staging.yml restart
sleep 10

# In order to overwrite the django-star_rating images, we need to call collectstatic at least once
docker-compose -f docker-compose-staging.yml run --rm web python3 manage.py collectstatic --noinput

sleep 5

docker-compose -f docker-compose-staging.yml restart nginx || echo ""

exit 0
