"""Utility functions for micomments app."""

def generate_thread_id(obj):
    """Generate a unique thread ID for an object."""
    return "{}{}".format(obj.__class__.__name__.lower(), obj.pk)

