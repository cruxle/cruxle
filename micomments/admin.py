from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from micomments.models import Comment

@admin.register(Comment)
class CommentModelAdmin(ModelAdmin):
        pass