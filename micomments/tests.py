from django.test import TestCase


# Create your tests here.
from micomments.utils import generate_thread_id
from miroutes.models import Wall, Route


class UtilsTest(TestCase):

    def test_generate_thread_id(self):

        wall = Wall()
        wall.pk = 123

        route = Route()
        route.pk = 124

        self.assertEqual(generate_thread_id(wall), "wall123")
        self.assertEqual(generate_thread_id(route), "route124")