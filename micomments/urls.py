from django.conf.urls import *

from micomments import views

app_name = 'micomments'

urlpatterns = [
    url(r'^show/(?P<thread_id>[0-9a-zA-Z]+)', views.comments, name='show'),
    url(r'^add/(?P<thread_id>[0-9a-zA-Z]+)', views.add, name='add'),
    url(r'^delete/(?P<comment_id>\d+)', views.delete, name='delete'),
    url(r'^views/full/(?P<thread_id>[0-9a-zA-Z]*)', views.full, name='views_full'),
    url(r'^views/full/', views.full, name='views_full'),
]
