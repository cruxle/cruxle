# MiComments

## Functionality

### List all comments of a thread in descending order

    /micomments/thread123

    {"comments": [{"comment": "und noch einer", "thread_id": 1, "user_id": 1, "id": 12, "created": "2017-01-25T00:47:17.237Z"}, {"comment": "asdfasdf", "thread_id": 1, "user_id": 1, "id": 11, "created": "2017-01-25T00:47:08.241Z"}, {"comment": "asdfasdf", "thread_id": 1, "user_id": 1, "id": 4, "created": "2017-01-25T00:36:35.107Z"}]}

### Add new comments as logged in user

    /micomments/add/thread123
    
    POST this JSON to the URL
    
    { "comment": "foobar" }
    
### Delete comment as logged in user (can only delete own)

    GET /micomments/delete/123