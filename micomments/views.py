import html
import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render

from micomments.models import Comment

from django.shortcuts import get_object_or_404
from miroutes.utils import replace_objs

def jsonifyComment(comment):
    txt = html.escape(comment.comment, quote=False)
    txt = replace_objs(txt, html=True, absolute_urls=False)
    return {
        "id": comment.pk,
        "comment": txt,
        "user_id": comment.user.id,
        "user_name": comment.user.username,
        "avatar": comment.user.userprofile.thumb_url(),
        "thread_id": comment.threadId,
        "created": comment.created
    }

def createCommentFromDict(commentDict):
    if "user_id" not in commentDict or "comment" not in commentDict or "thread_id" not in commentDict:
        raise ValueError("missing a value: " + commentDict)
    else:
        comment = Comment()
        comment.user = User.objects.get(id=int(commentDict['user_id']))
        comment.comment = commentDict['comment']
        comment.threadId = commentDict['thread_id']
        return comment


def comments(request, thread_id):
    """
    process search request
    """

    chunk_size = request.GET.get('chunk_size')
    try:
        chunksize = int(chunksize)
        allComments = Comment.objects.filter(threadId=thread_id).order_by('-created')[:chunk_size]
    except:
        allComments = Comment.objects.filter(threadId=thread_id).order_by('-created')

    return JsonResponse({"comments": list(map(jsonifyComment, allComments))})


@login_required
def add(request, thread_id):
    """
    process search request
    """
    if request.method == 'POST':
        user = None

        if request.user.is_authenticated:
            user = request.user
        else:
            return JsonResponse({'message': 'Not authorized', 'success': False}, status=403)

        input = json.loads(request.body)

        input['user_id'] = user.id
        input['thread_id'] = thread_id

        try:
            commentObject = createCommentFromDict(input)
            if commentObject.comment != "":
                try:
                    commentObject.save()
                    return JsonResponse({'message': 'Saved comment', 'success': True}, status = 200)
                except Exception as e:
                    return JsonResponse({'message': f'Could not save comment: {e}', 'success': False}, status=400)
            else:
                return JsonResponse({'message': 'not saving empty comment', 'success': False}, status=400)
        except ValueError as e:
            return JsonResponse({'message': str(e), 'success': False}, status=400)

    else:
        return JsonResponse({'message': 'Invalid http method, only POST allowed', 'success': False}, status=405)

    return JsonResponse({'message': 'Bug encountered, please report this', 'success': False}, status=500)


@login_required
def delete(request, comment_id):
    """
    process search request
    """
    if (request.method == 'GET') or (request.method == 'POST'):
        user = None

        if request.user.is_authenticated:
            user = request.user
            comment = get_object_or_404(Comment, pk=comment_id)
            if comment.user != user:
                return JsonResponse({'message': 'Not authorized', 'success': False}, status=403)
            else:
                try:
                    comment.delete()
                    return JsonResponse({'message': 'Deleted comment', 'success': True}, status=200);
                except Exception as e:
                    return JsonResponse({'message': f'Could not delete comment {comment.pk}: {e}', 'success': False}, status=400)
        else:
            return JsonResponse({'message': 'Not authorized', 'success': False}, status=403)
    else:
        return JsonResponse({'message': 'Invalid http method, only GET OR POST allowed', 'success': False}, status=405)
    return JsonResponse({'message': 'Bug encountered, please report this', 'success': False}, status=500)


def full(request, thread_id):
    chunk_size = request.GET.get('chunk_size')
    return render(request, 'micomments/show_comments.html', {
        'thread_id': thread_id,
        'chunk_size': chunk_size
    })
