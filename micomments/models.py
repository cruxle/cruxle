from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    threadId = models.CharField(max_length=100, db_index=True)
    comment = models.TextField(default='', blank=True)
    created = models.DateTimeField(auto_now_add=True)
