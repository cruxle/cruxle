amqp
celery
cryptography
Django==3.0.5
django-appconf==1.0.4
django-celery-results==1.2.1
django-classy-tags==1.0.0
django-colorfield==0.3.0
django-cookie-law==2.0.3
django-crispy-forms==1.9.0
django-dbbackup==3.3.0
django-extensions==2.2.9
django-file-form==1.0.0
django-filter==2.2.0
django-memcached==0.1.2
django-rest-swagger==2.2.0
django-reversion==3.0.7
django-settings-export==1.2.1
djangorestframework==3.11.0
easy-thumbnails==2.7
ipython
jsonfield==3.1.0
Markdown==3.2.1
MarkupSafe==1.1.1
Pillow
python-memcached
psycopg2==2.8.6
qrcode
redis
selenium
PyPDF2
simplejson
social-auth-app-django
social-auth-core
uWSGI
urllib3==1.25.9
Werkzeug==1.0.1
